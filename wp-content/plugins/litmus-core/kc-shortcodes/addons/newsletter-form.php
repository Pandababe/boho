<?php
add_shortcode( 'sh_lm_newslatter_form', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'title_top' => '', 
		'title' => '', 
		'btn_title' => '', 
	), $atts));  
	ob_start(); ?>   
	<section class="newsletter-block-v2">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content">
	                <h3 class="sub-section-title color-citrus"><?php echo esc_html($title_top); ?></h3>
	                <h2 class="section-title no-border"><?php echo esc_html($title); ?></h2>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	        <div class="row">
	            <div class="col-md-8 full-content">
	                <form class="newsletter-form mc-embedded-subscribe-form validate citrus-theme" method="post" name="mc-embedded-subscribe-form">
	                    <input type="email" class="form-controller" placeholder="Enter your email" />
	                    <button type="submit" class="mailchip-button"><?php echo esc_html($btn_title); ?></button>
	                    <div class="newsletter-message">                    
	                        <div class="subscription-success"></div>
	                        <div class="subscription-error"></div>
	                    </div><!--  /.message -->
	                </form>
	            </div><!--  /.col-md-8 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.newsletter-block -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_newslatter_form', 115 );
function sh_lm_kc_newslatter_form() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_newslatter_form' => array(
	                'name' => esc_html__('Newsletter Form', 'litmus'),
	                'description' => esc_html__('Newsletter Form Section', 'litmus'),
	                'icon' => 'et-newspaper',
	                'category' => 'Litmus',
	                'params' => array(
	                	array(
	                		'name' => 'title_top',
	                		'type' => 'text',
	                		'label' => esc_html__('Title Top', 'litmus'),  
	                		'value' => 'Want to know about our events?',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'Get Weekly Email',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'btn_title',
	                		'type' => 'text',
	                		'label' => esc_html__('Button Title', 'litmus'),  
	                		'value' => 'Subscribe',  
	                		'admin_label' => true,
	                	),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}