<?php
add_shortcode( 'sh_lm_coming_soon', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'bg_img' => '',  
		'title_top' => '',  
		'title' => '',  
		'subtitle' => '',  
		'time_top_text' => '',  
		'date' => '',  
		'time' => '',  
	), $atts));  
	ob_start(); ?>     
	<section class="coming-soon-content overlay-bg-snow-95" style="background-image: url(<?php echo esc_url(sh_lm_get_image_crop_size($bg_img, 1200, 1200)); ?>);">
	    <div class="litmus-container">
	        <div class="row justify-content-md-center">
	            <div class="col-md-12">
	                <div class="text-center">
	                    <h2 class="error-title"><?php echo esc_html($title_top); ?></h2><!--  /.error-title -->
	                    <h3 class="error-subtitle"><?php echo esc_html($title); ?></h3>
	                    <p class="error-desc"><?php echo esc_html($subtitle); ?></p>

	                    <div class="clearfix"></div><!--  /.clearfix -->
	                    <div class="pd-tb-15"></div>
	                    
	                    <h4 class="text-uppercase go-live"><?php echo esc_html($time_top_text); ?></h4>
	                    <?php 
		                    $date_stirng = strtotime($date);
		                    $year = date("Y",$date_stirng);
		                    $month = date("n",$date_stirng);
		                    $day = date("j",$date_stirng);
		                    if(!empty($time)) {
			                    $time_string = explode(':', $time); 
			                    $hour = $time_string[0];
			                    $minute = $time_string[1]; 
		                    }
	                    ?>

	                    <div class="commingsoon-count" data-year="<?php echo esc_attr($year); ?>" data-month="<?php echo esc_attr($month); ?>"  data-day="<?php echo esc_attr($day); ?>" data-hour="<?php if(isset($hour)) echo esc_attr($hour); ?>" data-minutes="<?php if(isset($minute)) echo esc_attr($minute); ?>"></div><!-- /.defaultCountdown -->
	                </div><!--  /.col-12 -->
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.coming-soon-content -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_coming_soon', 122 );
function sh_lm_kc_coming_soon() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_coming_soon' => array(
	                'name' => esc_html__('Coming Soon', 'litmus'),
	                'description' => esc_html__('Coming Soon', 'litmus'),
	                'icon' => 'sl-compass',
	                'category' => 'Litmus',
	                'params' => array( 
	                	array(
                			'name' => 'bg_img',
                			'label' => esc_html__('Background Image', 'litmus'), 
                			'type' => 'attach_image',  
                		),
                		array(
                			'name' => 'title_top',
                			'type' => 'text',
                			'value' => 'Coming Soon',
                			'label' => esc_html__('Title Top', 'litmus'),  
                			'admin_label' => true,
                		),
	                    array(
	                    	'name' => 'title',
	                    	'type' => 'text',
	                    	'value' => 'We’re currently under construction',
	                    	'label' => esc_html__('Title', 'litmus'),  
	                    	'admin_label' => true,
	                    ),
	                    array(
	                    	'name' => 'subtitle',
	                    	'type' => 'text',
	                    	'value' => 'Sit tight, we won’t be unavailable that much longer!',
	                    	'label' => esc_html__('Subtitle', 'litmus'),   
	                    ),
	                    array(
	                    	'name' => 'time_top_text',
	                    	'type' => 'text', 
	                    	'value' => 'Going Live In',
	                    	'label' => esc_html__('Time Top Text', 'litmus'),   
	                    ),
	                    array(
	                    	'name' => 'date',
	                    	'type' => 'date_picker', 
	                    	'label' => esc_html__('Date', 'litmus'),   
	                    ),
	                    array(
	                    	'name' => 'time',
	                    	'type' => 'text',
	                    	'value' => '13:40',
	                    	'label' => esc_html__('Time', 'litmus'),  
	                    	'description' => 'Give the time like 14:20, hour:minute', 
	                    ),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}