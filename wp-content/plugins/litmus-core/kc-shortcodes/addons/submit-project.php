<?php
add_shortcode( 'sh_lm_submit_project', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'title' => '', 
		'pricing_list' => '',   
	), $atts));  
	ob_start(); ?>   
	<section class="projection-submission pd-t-90">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-12">
	                <h2 class="section-title"><?php echo esc_html($title); ?></h2>
	            </div><!--  /.col-12 -->
	        </div><!--  /.row -->

	        <div class="clearfix"></div><!--  /.clearfix -->
	        <div class="pd-tb-30"></div><!--  /.pd-tb-15 -->

	        <div class="row">
	        	<?php 
	        	if(!empty($pricing_list)) {
	        	foreach ($pricing_list as $price) { 
	        	?>
	            <div class="col-md-6">
	                <div class="price-box">
	                	<?php 
	                		$array_from_to = array(
	                			'&lt;' => '<', 
	                			'&gt;' => '>', 
	                		);
	                		$price_title = strtr($price->title, $array_from_to); 
	                	?> 
	                    <p><?php echo wp_kses( $price_title, LITMUS_Static::html_allow() ); ?></p>
	                </div><!--  /.price-box -->
	            </div><!--  /.col-md-6 --> 
	            <?php } } ?>

	        </div><!--  /.row -->

	        <div class="clearfix"></div><!--  /.clearfix -->
	        <div class="pd-tb-45"></div><!--  /.pd-tb-45 -->

	        <div class="project-form-content">
	            <?php echo do_shortcode( $content ); ?>
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.projection-submission -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_submit_project', 106 );
function sh_lm_kc_submit_project() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_submit_project' => array(
	                'name' => esc_html__('Submit Project', 'litmus'),
	                'description' => esc_html__('Submit Project', 'litmus'),
	                'icon' => 'et-megaphone',
	                'category' => 'Litmus',
	                'is_container' => true,
	                'params' => array(
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'Submit Your Project',  
	                		'admin_label' => true,
	                	),
	                    array(
	                    	'name'			=> 'pricing_list',
	                    	'type'			=> 'group',
	                    	'label'			=> esc_html__('Pricing List', 'litmus'), 
	                    	'options'		=> array('add_text' => esc_html__('Add new price', 'litmus')), 
	                    	'value' => base64_encode( json_encode( array(
	                    		"1" => array( 
	                    			"title" => '<span class="title">PSD to HTML/CSS:</span> Starting form <span class="price">$99</span> (for the homepage)',
	                    		),
	                    		"2" => array( 
	                    			"title" => '<span class="title">jQuery Slide shows, Drop-down menus:</span> Starting form <span class="price">$20</span>',
	                    		),
	                    		"3" => array( 
	                    			"title" => '<span class="title">PSD to HTML/CSS:</span> Starting form <span class="price">$60</span> (internal pages)',
	                    		),
	                    		"4" => array( 
	                    			"title" => '<span class="title">WordPress integration:</span> Starting form <span class="price">$199</span> per project!',
	                    		)
	                    	))), 
	                    	'params' => array(  
	                    		array(
	                    			'name' => 'title',
	                    			'type' => 'textarea',
	                    			'label' => esc_html__('Title', 'litmus'),  
	                    		), 
	                    	)
	                    ),
	                    array(
	                    	'name' => 'content',
	                    	'type' => 'textarea_html',
	                    	'label' => esc_html__('Project Summit CF7 Shortcode', 'litmus'),  
	                    	'admin_label' => true,
	                    ), 
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}