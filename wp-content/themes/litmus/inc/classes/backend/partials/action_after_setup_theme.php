<?php 
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

/*
 * Make theme available for translation.
 * Translations can be filed in the /languages/ directory.
 * If you're building a theme based on Litmus, use a find and replace
 * to change 'litmus' to the name of your theme in all the template files
 */
load_theme_textdomain( 'litmus', get_template_directory() . '/languages' );

// Add default posts and comments RSS feed links to head.
add_theme_support( 'automatic-feed-links' );

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

/*
 * Enable support for Post Thumbnails on posts and pages.
 *
 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
 */
add_theme_support( 'post-thumbnails' );

// This theme uses wp_nav_menu() in one location.
register_nav_menus( array(
    'main-menu' => esc_html__( 'Main Menu', 'litmus' ),  
    'hamburger-menu' => esc_html__( 'Hamburger Menu', 'litmus' ),  
    'footer-menu' => esc_html__( 'Footer Menu', 'litmus' ),  
) );

/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
    ) 
);

/*
 * Enable support for Post Formats.
 * See http://codex.wordpress.org/Post_Formats
 */
add_theme_support( 'post-formats', array(
    'image', 'gallery', 'audio', 'video', 'quote', 'link', 'status', 'chat'
    ) 
);

// Set up the WordPress core custom background feature.
add_theme_support( 'custom-background', apply_filters( 'sh_lm_custom_background_args', array (
    'default-color' => 'fff',
    'default-image' => '',
) ) );

// Add theme support for selective refresh for widgets.
add_theme_support( 'customize-selective-refresh-widgets' );

/*
 * Enable support for custom Header Image.
 *
 *  @since Litmus
 */
$args = array(
	'flex-width'    => true,
	'width'         => 980,
	'flex-height'    => true,
	'height'        => 200,
);
add_theme_support( 'custom-header', $args );
/*
 * Enable support for custom Editor Style.
 *
 *  @since Litmus
 */
add_editor_style( 'assets/css/custom-editor-style.css' );
