<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Litmus
 */
$col_class = (isset($post_style) && $post_style == 'list') ? 'col-md-12' : 'col-md-6'; 
if(isset($only_item_class) && $only_item_class == true){
    $col_class = '';
} else {
    if(get_option( 'woocommerce_catalog_columns')) {    
        $postShowInRow = get_option( 'woocommerce_catalog_columns');
        $moveonInteger = intval(str_replace(" ", "",$postShowInRow));
        $newCol = ( 12 / $moveonInteger);
        $col_class = 'col-md-'.$newCol;
    } else {
        $col_class = 'col-md-4';
    }
}
?>
<div class="item <?php echo esc_attr($col_class); ?>">
    <div class="woo-menu-item">                                
        <div class="menu-thumbnail">
            <?php
                global $product; 
                if( $product->is_on_sale() ) {
                    $badge_class = 'badge-discount';
                } else {
                    $badge_class = 'badge-new';
                }
            ?>
            <?php if( $product->is_on_sale() && $product->get_regular_price() ) { ?>
                <span class="<?php echo esc_attr($badge_class); ?>">
                <?php
                    echo '-'.round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 ).'%'; ?>
                </span>
               <?php } else { ?>
                <span class="<?php echo esc_attr($badge_class); ?>">
                   <?php $postdate       = get_the_time( 'Y-m-d' );          // Post date
                    $postdatestamp  = strtotime( $postdate );           // Timestamped post date
                    $newness        = 30;
                    if ( ( time() - ( 60 * 60 * 24 * $newness ) ) < $postdatestamp ) {
                        esc_html_e( 'New', 'litmus' );
                    } ?>
                </span> 
            <?php } ?>
            
            <?php if ( has_post_thumbnail() ) { 
                sh_lm_post_featured_image(260, 340);
            } ?> 
            <div class="hover-content">
                <div class="hover-wrap"> 
                    <a rel="nofollow" href="#" data-quantity="1" data-product_id="<?php echo esc_attr( get_the_ID() ); ?>" data-product_sku="" class="add-cart order-btn button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="pe-7s-shopbag"></i></a>

                    <?php if ( function_exists( 'sh_lm_product_favorite_btn' ) ) {
                        sh_lm_product_favorite_btn( get_the_ID() );
                    } ?>
                     
                    <a href="<?php the_permalink(); ?>"><i class="pe-7s-search"></i></a>
                </div><!--  /.hover-wrap -->
            </div>
        </div>
        <div class="menu-desc pd-lr-0">
            <h2 class="menu-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="menu-price">
                <div class="price"><?php sh_lm_product_price(); ?></div><!--  /.price -->
            </div><!--  /.menu-price -->
        </div><!--  /.menu-desc -->
    </div><!--  /.woo-menu-item -->
</div><!--  /.item -->
 