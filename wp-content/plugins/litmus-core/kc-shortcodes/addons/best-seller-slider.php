<?php
add_shortcode( 'sh_lm_best_seller_slider', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'bg_img' => '',
		'title' => '',  
		'icon' => '',  
		'cats' => '',    
		'number' => '',  
	), $atts));  
	ob_start(); ?>  
	<section class="best-seller-block pd-tb-90 overlay-bg" style="background-image: url(<?php echo esc_url(sh_lm_get_image_crop_size($bg_img, 1200, 1200)); ?>);">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content">
	                <h2 class="section-title no-border color-white"><?php echo esc_html($title); ?></h2>
	                <div class="resturent-title-border dark-border">
	                    <i class="<?php echo esc_attr($icon); ?>"></i>
	                </div>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	        <div class="row">
	            <div class="col-12">                
	                <ul class="resturent-tab-v1 bsm-tabs"> 
	                    <?php 
    	                    $product_cat_slugs = array(); 
    	                    if(!empty($cats)) {
    	                        $catss = explode(",", $cats); 
    	                        foreach ( $catss as $cat ) {  
    	                            $cat = explode(":", $cat); 
    	                            $product_cat_slugs[] = $cat[0]; ?>
    								<li class="bsm-tab"><a href="#dining-slider-<?php echo esc_attr($cat[0]); ?>"><?php echo esc_html($cat[1]); ?></a></li>
    	                    <?php }
    	                	} 
	                    ?>
	                </ul> 
	                <?php 
            	    if(!empty($cats)) { 
            	        $cats_two = explode(",", $cats);  
            	        foreach ( $cats_two as $cat_two ) {  
            	            $cat_two = explode(":", $cat_two); 
            	            echo '<div class="bsm-tab-content" id="dining-slider-'.$cat_two[0].'">'; ?>
            	            <div class="bestseller-carousel citrus-theme owl-carousel">
            	            	<?php  
            	            	$product_query = new WP_Query(
            	            	    array(
            	            	        'post_type' => 'product', 
            	            	        'meta_key' => 'total_sales',
            	            	        'orderby' => 'meta_value_num',
            	            	        'posts_per_page' => $number,
            	            	        'tax_query' => array(
            	            	            array(
            	            	                'taxonomy' => 'product_cat',
            	            	                'field' => 'slug',
            	            	                'terms' => $cat_two[0],
            	            	            )
            	            	        ),  
            	            	    )
            	            	); 
            	            	while ($product_query->have_posts()) : $product_query->the_post();
            	            		$only_item_class = true; 
	                	    		require get_template_directory().'/woocommerce/template-parts/content-product-two.php'; 
            	            	endwhile; wp_reset_postdata(); ?> 
            	            </div><!--  /.bestseller-carousel --> 
            	            <?php  echo '<a href="'.esc_url( get_term_link($cat_two[0], 'product_cat') ).'" class="show-all-btn">'.esc_html__( 'Show All', 'litmus' ).'</a>'; ?>
            	            <?php echo '</div>';
            	        }  
            		} 
	                ?>
 
	            </div><!--  /.col-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.best-seller-block -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_best_seller_slider', 109 );
function sh_lm_kc_best_seller_slider() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_best_seller_slider' => array(
	                'name' => esc_html__('Best Seller Slider', 'litmus'),
	                'description' => esc_html__('Product Best Seller Slider', 'litmus'),
	                'icon' => 'et-bargraph',
	                'category' => 'Litmus',
	                'params' => array( 
	                	array(
                			'name' => 'bg_img',
                			'label' => esc_html__('Background Image', 'litmus'), 
                			'type' => 'attach_image',  
                		), 
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'BestSeller',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'icon',
	                		'type' => 'icon_picker',
	                		'label' => esc_html__('Icon', 'litmus'),  
	                		'value' => 'lit lit-restaurent-knife',  
	                	),
	                	array(
	                	    'type'          => 'autocomplete',
	                	    'label'         => esc_html__('Select Category', 'litmus'),
	                	    'name'          => 'cats',
	                	    'options'       => array( 
	                	        'multiple'      => true, 
	                	        'post_type'     => 'product', 
	                	        'taxonomy'      => 'product_cat', 
	                	    ), 
	                	), 
	                    array(
	                    	'name' => 'number',
	                    	'label' => esc_html__('Total Post In Tab', 'litmus'), 
	                    	'type' => 'number_slider',  
	                    	'options' => array(  
	                    		'min' => 1,
	                    		'max' => 20, 
	                    		'show_input' => true
	                    	), 
	                    	'value' => 6
	                    ),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}