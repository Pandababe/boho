<?php
add_shortcode( 'sh_lm_our_skills', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'title' => '', 
		'desc' => '', 
		'our_skills' => '', 
		'skill_imgs' => '', 
	), $atts));  
	ob_start(); ?>    
	<section class="our-skill">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-lg-6">
	                <h2 class="section-title"><?php echo esc_html($title); ?></h2>
	                <p class="section-subtitle"><?php echo esc_html($desc); ?></p><!--  /.section-subtitle -->

	                <div class="skill-progress">
	                	<?php 
			        	if(!empty($our_skills)) {
			        	foreach ($our_skills as $skills) { 
			        	?>
	                    <div class="skill-bar" data-percentage="<?php echo esc_attr($skills->percent); ?>">
	                        <h4 class="progress-title-holder">
	                            <span class="progress-title"><?php echo esc_html($skills->title); ?></span>
	                            <span class="progress-wrapper">
	                                <span class="progress-mark">
	                                    <span class="percent"></span>
	                                </span>
	                            </span>
	                        </h4>
	                        <div class="progress-outter">
	                            <div class="progress-content"></div>
	                        </div>
	                    </div><!-- /.skill-bar -->
						<?php } } ?>
	                </div>
	            </div><!--  /.col-lg-6 -->

	            <div class="col-lg-6 d-none d-lg-block d-xl-block">
	                <div class="hexa-grid">         
	                	<?php 
			        	if(!empty($skill_imgs)) {
			        	$skill_imgs = explode(',', $skill_imgs);
			        	$i = 1;
			        	foreach ($skill_imgs as $img) { 
			        	?>         
	                    <div class="hexagonal hexa-item <?php if($i % 2 == 0) echo 'hexa-middle' ?>">
	                        <div class="hexagonal-img-container">
	                            <div class="hexagonal-img-hex">
	                                <div class="hexagonal-img-hex1 hexagonal-img-hex2">
	                                    <div class="hexagonal-img-hex-in1">
	                                        <div class="hexagonal-img-hex-in2">
	                                            <img src="<?php echo esc_url(sh_lm_get_image_crop_size($img, 250, 230)); ?>" class="hexagonal-image" alt="<?php esc_attr_e('skill iamge', 'litmus'); ?>">
	                                        </div>

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div><!-- /.hexagonal --> 
	                    <?php $i++; } } ?>

	                </div><!--  /.hexa-grid -->
	            </div><!--  /.col-md-6 -->
	        </div><!-- /.row -->
	    </div><!-- /.container -->
	</section><!-- /.our-skill -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_skills', 100 );
function sh_lm_kc_our_skills() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_skills' => array(
	                'name' => esc_html__('Our Skills', 'litmus'),
	                'description' => esc_html__('Our Skills', 'litmus'),
	                'icon' => 'sl-layers',
	                'category' => 'Litmus',
	                'params' => array(
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'Our Skill',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'desc',
	                		'type' => 'textarea',
	                		'label' => esc_html__('Short Description', 'litmus'), 
	                		'value' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',  
	                	),
	                    array(
	                    	'name'			=> 'our_skills',
	                    	'type'			=> 'group',
	                    	'label'			=> esc_html__('Our Skills', 'litmus'), 
	                    	'options'		=> array('add_text' => esc_html__('Add new skill', 'litmus')), 
	                    	'value' => base64_encode( json_encode( array(
	                    		"1" => array( 
	                    			"title" => "Graphic Design",
	                    			"percent" => "89%"
	                    		),
	                    		"2" => array(
	                    			"title" => "HTML/CSS",
	                    			"percent" => "96%"
	                    		),
	                    		"3" => array(
	                    			"title" => "Marketing",
	                    			"percent" => "80%"
	                    		),
	                    		"4" => array(
	                    			"title" => "Adwords",
	                    			"percent" => "85%"
	                    		)
	                    	))), 
	                    	'params' => array(  
	                    		array(
	                    			'name' => 'title',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Title', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    		array(
	                    			'name' => 'percent',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Percent', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    	)
	                    ),
	                    array(
	                    	'name' => 'skill_imgs',
	                    	'label' => esc_html__('Skill Images', 'litmus'), 
	                    	'type' => 'attach_images',  
	                    )
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}