<?php 
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

class LITMUS_Static {

    /**
     * Allow HTML tag from escaping HTML 
     * 
     * @return void
     * @since v1.0
     */
    public static function html_allow() {
        return array(
            'a' => array(
                'href' => array(),
                'title' => array()
            ),
            'br' => array(),
            'span' => array(
                'class' => array(),
            ),
            'em' => array(),
            'strong' => array(),
            'h2' => array(
                'class' => array(),
            ),
            'p' => array(),
        );
    }

    /**
     * @since v1.0
     */
    public static function html_allow_iframe() {
        return array(
            'iframe' => array (
                'align'       => true,
                'frameborder' => true,
                'height'      => true,
                'width'       => true,
                'sandbox'     => true,
                'seamless'    => true,
                'scrolling'   => true,
                'srcdoc'      => true,
                'src'         => true,
                'class'       => true,
                'id'          => true,
                'style'       => true,
                'border'      => true,
            )
        );
    }

    /**
     * @since v1.0
     */
    public static function total_grid() {
        return array(
            '1' => esc_html__( '1 Grid', 'litmus' ),
            '2' => esc_html__( '2 Grid', 'litmus' ),
            '3' => esc_html__( '3 Grid', 'litmus' ),
            '4' => esc_html__( '4 Grid', 'litmus' ),
            '5' => esc_html__( '5 Grid', 'litmus' ),
            '6' => esc_html__( '6 Grid', 'litmus' ),
            '7' => esc_html__( '7 Grid', 'litmus' ),
            '8' => esc_html__( '8 Grid', 'litmus' ),
            '9' => esc_html__( '9 Grid', 'litmus' ),
            '10' => esc_html__( '10 Grid', 'litmus' ),
            '11' => esc_html__( '11 Grid', 'litmus' ),
            '12' => esc_html__( '12 Grid', 'litmus' ),
        );
    }

    /**
     * @since v1.0
     */
    public static function total_items() {
        return array(
            '2' => esc_html__( 'Two', 'litmus' ),
            '3' => esc_html__( 'Three', 'litmus' ),
            '4' => esc_html__( 'Four', 'litmus' ),
            '5' => esc_html__( 'Five', 'litmus' ),
            '6' => esc_html__( 'Six', 'litmus' ),
            '7' => esc_html__( 'Seven', 'litmus' ),
        );
    }

    /**
     * @since v1.0
     */
    public static function star_option() {
        return array(
            '1' => '1',
            '1.5' => '1.5',
            '2' => '2',
            '2.5' => '2.5',
            '3' => '3',
            '3.5' => '3.5',
            '4' => '4',
            '4.5' => '4.5',
            '5' => '5',
        );
    }

    /**
     * Functions for converts 0 - 5 to stars
     * @since v1.0
     */
    public static function num_to_star($total_stars) { 
        $star_integer = intval($total_stars); 
        $total_star = ''; 
        // this echo full stars
        for ($i = 0; $i < $star_integer; $i++) {
            $total_star .= '<i class="fa fa-star"></i>';
        }

        $star_rest = $total_stars - $star_integer;

        // this echo full star or half or empty star
        if ($star_rest >= 0.25 and $star_rest < 0.75) {
            $total_star .= '<i class="fa fa-star-half-o"></i>';
        } else if ($star_rest >= 0.75) {
            $total_star .= '<i class="fa fa-star"></i>';
        } else if ($total_stars != 5) {
            $total_star .= '<i class="fa fa-star-o"></i>';
        }

        // this echo empty star
        for ($i = 0; $i < 4-$star_integer; $i++) {
            $total_star .= '<i class="fa fa-star-o"></i>';
        } 
        return $total_star;
    }

}
new LITMUS_Static;
