<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'coming_soon_page_settings' => array(
        'title'   => esc_html__( 'Coming Soon', 'litmus' ),
        'type'    => 'tab',
        'options' => array(   
            'coming_soon_page' => array(
                'type'         => 'multi-picker',
                'label'        => false,
                'desc'         => false,
                'picker'       => array(
                    'coming_soon' => array(
                        'label'    => esc_html__( 'Coming Soon', 'litmus' ),
                        'desc' => esc_html__( 'You can show coming soon page from here', 'litmus' ),
                        'type'         => 'switch',
                        'right-choice' => array(
                            'value' => '1',
                            'label' => esc_html__( 'On', 'litmus' )
                        ),
                        'left-choice'  => array(
                            'value' => '0',
                            'label' => esc_html__( 'Off', 'litmus' )
                        ),
                        'value'        => '0',
                    ),
                ),
                'choices' => array(
                    '1'  => array(
                        'coming_soon_page_id'      => array(
                            'type'       => 'multi-select',
                            'label'      => esc_html__( 'Coming Soon Page', 'litmus' ),
                            'population' => 'posts',
                            'source'     => 'page',
                            'limit' => 1,
                        ),
                    ),
                ),
            ), 
        )
    ),
    'general_settings' => array(
        'title'   => esc_html__( 'General Settings', 'litmus' ),
        'type'    => 'tab',
        'options' => array(
            'favicon_tab' => array(
                'title'   => esc_html__( 'Favicon', 'litmus' ),
                'type'    => 'tab',
                'options' => array( 
                    'favicon'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Favicon Main', 'litmus'), 
                        'desc' => esc_html__('Favicon for all (16px X 1px OR 32px X 32px)', 'litmus'),
                    ),
                    'icon_iphone'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPhone Icon', 'litmus'),
                        'desc' => esc_html__('Icon for Apple iPhone (57px X 57px)', 'litmus'),
                    ),
                    'icon_iphone_retina'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPhone Retina Icon', 'litmus'),
                        'desc' => esc_html__('Icon for Apple iPhone Retina (114px X 114px)', 'litmus'),
                    ),
                    'icon_ipad'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPad Icon', 'litmus'),
                        'desc' => esc_html__('Icon for Apple iPad (72px X 72px)', 'litmus'),
                    ),
                    'icon_ipad_retina'   => array(
                        'type'  => 'upload', 
                        'label' => esc_html__('Apple iPad Retina Icon', 'litmus'),
                        'desc' => esc_html__('Icon for Apple iPad Retina (144px X 144px)', 'litmus'),
                    ),
                ),
            ),       
            'preloader_options' => array(
                'title'   => esc_html__( 'Preloader', 'litmus' ),
                'type'    => 'tab',
                'options' => array(
                    'preloader_option' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'preloader' => array(
                                'label'    => esc_html__( 'Preloader', 'litmus' ),
                                'desc' => esc_html__( 'You can off/on prelaoder from here', 'litmus' ),
                                'type'         => 'switch',
                                'right-choice' => array(
                                    'value' => '1',
                                    'label' => esc_html__( 'On', 'litmus' )
                                ),
                                'left-choice'  => array(
                                    'value' => '0',
                                    'label' => esc_html__( 'Off', 'litmus' )
                                ),
                                'value'        => '0',
                            ),
                        ),
                        'choices'      => array(
                            '1'  => array( 
                                'per_img'   => array(
                                    'type'  => 'upload', 
                                    'label' => esc_html__('Preloader Image', 'litmus'), 
                                ),  
                            ),
                        ),
                    ),
                )
            ),   
            'color_scheme_settings_tab' => array(
                'title'   => esc_html__( 'Color Scheme', 'litmus' ),
                'type'    => 'tab',
                'options' => array(              
                    'color_scheme' => array(
                        'label'   => esc_html__( 'Select Theme Color', 'litmus' ), 
                        'value'   => '#03dedf',
                        'choices' => array(
                            '1' => '#03dedf',
                            '2' => '#d12a5c',
                            '3' => '#49ca9f',
                            '4' => '#1f1f1f',
                            '5' => '#808080',
                            '6' => '#ebebeb'
                        ),
                        'type'    => 'color-palette'
                    ),   
                )
            ), 
            'contact_form_control' => array(
                'title'   => esc_html__( 'Get A Quote Form', 'litmus' ),
                'type'    => 'tab',
                'options' => array(
                    'get_a_quote_email'  => array(
                        'label' => esc_html__('Receving Email', 'litmus'),
                        'desc' => esc_html__('In whiich id you will receice email you need to define here', 'litmus'),
                        'type'  => 'text',
                    ),
                    'get_a_quote_service_list' => array(
                        'label'  => esc_html__( 'Service List', 'litmus' ),
                        'type'   => 'addable-option',
                        'option' => array(
                            'type' => 'text',
                        ),
                        'value' => array('UX/UI Design', 'WordPress Theme Development', 'Custom Website Build'),
                        'desc'   =>  esc_html__( 'You can add get a quote service list one by one from here', 'litmus' ),
                    ),
                    'get_a_quote_form' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'quote_form_visibility' => array(
                                'label'    => esc_html__( 'Contact Form', 'litmus' ),
                                'desc' => esc_html__( 'You can off/on Default Quote From or Dynamic quote from with contact form 7here', 'litmus' ),
                                'type'         => 'switch',
                                'right-choice' => array(
                                    'value' => '1',
                                    'label' => esc_html__( 'On', 'litmus' )
                                ),
                                'left-choice'  => array(
                                    'value' => '0',
                                    'label' => esc_html__( 'Off', 'litmus' )
                                ),
                                'value'        => '0',
                            ),
                        ),
                        'choices'      => array(
                            '1'  => array( 
                                'contact_shortcode'   => array(
                                    'type'  => 'text', 
                                    'label' => esc_html__('Contact Form Shortcode', 'litmus'), 
                                ),  
                            ),
                        ),
                    ),
                )
            ), 
            'other_settings_tab' => array(
                'title'   => esc_html__( 'Others', 'litmus' ),
                'type'    => 'tab',
                'options' => array(   
                    'custom_css_code'   => array(
                        'type'  => 'textarea', 
                        'label' =>  esc_html__( 'Custom CSS Code', 'litmus' ),
                        'desc' => esc_html__( 'Paste your CSS code here without <style></style> tag', 'litmus' ),
                    ),
                    'custom_js'   => array(
                        'type'  => 'textarea', 
                        'label' =>  esc_html__( 'Custom JS Code', 'litmus' ),
                        'desc' => esc_html__( 'Paste your JS code here without <script></script> tag', 'litmus' ),
                    ),
                )
            ),
        ),
    ), 
    'typography_settings' => array(
        'title' => esc_html__( 'Typography', 'litmus' ),
        'type'    => 'tab',
        'options' => array(
            'global_body_fonts'  => array(
                'label' => esc_html__( 'Body Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'subset'    => 'latin-ext',
                    'variation' => 'regular',
                    'size'      => '14px',
                    'line-height' => '1.45em',
                    'letter-spacing' => '',
                    'color'     => '#999999'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the body font properties.','litmus' ),
            ),
            'section_heading_font'  => array(
                'label' => esc_html__( 'Section Heading Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'size'      => '40px',
                    'color'     => '#1d1d1f'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => false,
                    'letter-spacing' => false,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h1 Font font properties.','litmus' ),
            ),
            'hading_one_font'  => array(
                'label' => esc_html__( 'Heading h1 Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'subset'    => 'latin-ext',
                    'variation' => '600',
                    'size'      => '2.441em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#1d1d1f'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h1 Font font properties.','litmus' ),
            ),
            'hading_two_font'  => array(
                'label' => esc_html__( 'Heading h2 Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'subset'    => 'latin-ext',
                    'variation' => '600',
                    'size'      => '1.953em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#1d1d1f'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h2 font font properties.','litmus' ),
            ),
            'hading_three_font'  => array(
                'label' => esc_html__( 'Heading h3 Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'subset'    => 'latin-ext',
                    'variation' => '600',
                    'size'      => '1.563em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#1d1d1f'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h3 font font properties.','litmus' ),
            ),
            'hading_four_font'  => array(
                'label' => esc_html__( 'Heading h4 Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'subset'    => 'latin-ext',
                    'variation' => '600',
                    'size'      => '1.25em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#1d1d1f'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h4 font font properties.','litmus' ),
            ),
            'hading_five_font'  => array(
                'label' => esc_html__( 'Heading h5 Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'subset'    => 'latin-ext',
                    'variation' => '600',
                    'size'      => '1em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#1d1d1f'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h5 font font properties.','litmus' ),
            ),
            'hading_six_font'  => array(
                'label' => esc_html__( 'Heading h6 Font', 'litmus' ),
                'type'  => 'typography-v2',
                'value'      => array(
                    'family'    => 'Montserrat',
                    'subset'    => 'latin-ext',
                    'variation' => '600',
                    'size'      => '0.8em',
                    'line-height' => '',
                    'letter-spacing' => '',
                    'color'     => '#1d1d1f'
                ),
                'components' => array(
                    'family'         => true,
                    'size'           => true,
                    'line-height'    => true,
                    'letter-spacing' => true,
                    'color'          => true
                ),
                'desc'  => esc_html__( 'You can specify the heading h6 Font font properties.','litmus' ),
            ),
        ),
    ),
    'header_section' => array(
        'title' => esc_html__( 'Header Section', 'litmus' ),
        'type' => 'tab',
        'options' => array( 
            'logo_settings'        => array(
                'type'          => 'multi',
                'label'         => false,
                'inner-options' => array(
                    'logo' => array(
                        'type'         => 'multi-picker',
                        'label'        => false,
                        'desc'         => false,
                        'picker'       => array(
                            'selected_value' => array(
                                'label'   => esc_html__( 'Logo Type', 'litmus' ),
                                'desc'    => esc_html__( 'Select the logo type', 'litmus' ),
                                'attr'    => array( 'class' => 'fw-checkbox-float-left' ),
                                'value'   => 'text',
                                'type'    => 'radio',
                                'choices' => array(
                                    'text'  => esc_html__( 'Text', 'litmus' ),
                                    'image' => esc_html__( 'Image', 'litmus' ),
                                ),
                            )
                        ),
                        'choices'      => array(
                            'text'  => array(
                                'title'              => array(
                                    'label' => esc_html__( 'Title', 'litmus' ),
                                    'desc'  => esc_html__( 'Enter the title', 'litmus' ),
                                    'type'  => 'text',
                                    'value' => get_bloginfo( 'name' )
                                ),
                                'logo_title_font'    => array(
                                    'label' => esc_html__( '', 'litmus' ),
                                    'desc'  => esc_html__( 'Choose the title font', 'litmus' ),
                                    'type'  => 'typography-v2',
                                    'value' => array(
                                        'family'         => 'Playfair Display',
                                        'size'           => '2.5em',
                                        'color'           => '#ffffff',
                                        'line-height'    => '1.35em',
                                        'style'          => '400',
                                        'letter-spacing' => '',
                                    )
                                ),
                                'subtitle'           => array(
                                    'label' => esc_html__( 'Subtitle', 'litmus' ),
                                    'desc'  => esc_html__( 'Enter the subtitle', 'litmus' ),
                                    'type'  => 'text',
                                    'value' => get_bloginfo( 'description', 'display' ),
                                ),
                                'logo_subtitle_font' => array(
                                    'label' => esc_html__( '', 'litmus' ),
                                    'desc'  => esc_html__( 'Choose the subtitle font', 'litmus' ),
                                    'type'  => 'typography-v2',
                                    'value' => array(
                                        'family'         => 'Montserrat',
                                        'size'           => '1em',
                                        'color'           => '#dddddd',
                                        'line-height'    => '1.3em',
                                        'style'          => '400',
                                        'letter-spacing' => '',
                                    )
                                ),
                            ),
                            'image' => array(
                                'image_logo'  => array(
                                    'label' => esc_html__( '', 'litmus' ),
                                    'desc'  => esc_html__( 'Upload logo image', 'litmus' ),
                                    'type'  => 'upload',
                                    'show_borders' => true,
                                ),
                                'retina_logo' => array(
                                    'type'         => 'multi-picker',
                                    'label'        => esc_html__('', 'litmus' ),
                                    'desc'         => false,
                                    'picker'       => array(
                                        'retina_logo_image' => array(
                                            'label'    => false,
                                            'desc' => esc_html__( 'Use logo as retina?', 'litmus' ),
                                            'attr'    => array( 'class' => 'fw-multi-switch-margin' ),
                                            'type'         => 'switch',
                                            'right-choice' => array(
                                                'value' => '1',
                                                'label' => esc_html__( 'On', 'litmus' )
                                            ),
                                            'left-choice'  => array(
                                                'value' => '0',
                                                'label' => esc_html__( 'Off', 'litmus' )
                                            ),
                                            'value'        => '0',
                                        ),
                                    ),
                                    'choices' => array(
                                        '1'  => array(
                                            'retina_image_main'      => array(
                                                'type'       => 'upload', 
                                                'label' => false,                            
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                        'show_borders' => false,
                    ),
                ),
            ),
            'header_style_option' => array(
                'type'         => 'multi-picker',
                'label'        => false,
                'desc'         => false,
                'picker'       => array( 
                    'header_style'   => array(
                        'type'  => 'image-picker',
                        'value' => 'creative', 
                        'label' => esc_html__('Header Style', 'litmus'),
                        'choices' => array( 
                            'creative'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-one-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-one-large.jpg'
                                ),
                            ),
                            'restaurant'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-two-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-two-large.jpg'
                                ),
                            ),
                            'ecommerce'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-three-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-three-large.jpg'
                                ),
                            ),
                            'creative2'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-three-custom.jpg'
                                ),
                            ),
                            'header_builder'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/header-three-custom.jpg'
                                ),
                            ),
                        ), 
                    ),
                ),
                'choices'      => array(
                    'ecommerce'  => array( 
                        'header_top_title'  => array(
                            'label' => esc_html__('Header Top Title', 'litmus'), 
                            'value'        => 'Welcome to Litmus online store!',
                            'type'  => 'text',
                        ),
                        'header_top_support_text'  => array(
                            'label' => esc_html__('Header Top Support Text', 'litmus'), 
                            'value'        => 'Support:',
                            'type'  => 'text',
                        ),
                        'header_top_phone'  => array(
                            'label' => esc_html__('Header Top Phone', 'litmus'), 
                            'value'        => '01-234-888',
                            'type'  => 'text',
                        ),
                    ),                       
                    'header_builder'  => array( 
                        'header_layout_page'  => array(
                            'type'  => 'multi-select', 
                            'label' => esc_html__('Select Header Layout Page', 'litmus'), 
                            'population' => 'array', 
                            'population' => 'posts',
                            'source' => 'layout', 
                            'prepopulate' => 50, 
                            'limit' => 1,
                        ),
                    ),                    
                    'creative2'  => array( 
                        'background_color' => array(
                            'type'  => 'color-picker',
                            'value' => '#25A65C',
                            'label' => esc_html__('Header Background Color', 'litmus'),
                            'desc'  => esc_html__('Select Your Header Background Color', 'litmus'),
                        ),                        
                        'website_bg'       => array(
                            'type'    => 'background-image',
                            'value'   => 'none',
                            'label'   => __( '', 'the-core' ),
                            'desc'    => __( 'Select the patern overlay', 'the-core' ),
                            'choices' => array(
                                'none' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/no_pattern.jpg',
                                    'css'  => array(
                                        'background-image' => 'none'
                                    ),
                                ),
                                'bg-1' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/diagonal_bottom_to_top_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/diagonal_bottom_to_top_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                                'bg-2' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/diagonal_top_to_bottom_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/diagonal_top_to_bottom_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                                'bg-3' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/dots_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/dots_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                                'bg-4' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/noise_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/noise_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                                'bg-5' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/romb_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/romb_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                                'bg-6' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/square_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/square_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                                'bg-7' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/vertical_lines_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/vertical_lines_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                                'bg-8' => array(
                                    'icon' => get_template_directory_uri() . '/assets/custom/backend/patterns/waves_pattern_preview.jpg',
                                    'css'  => array(
                                        'background-image'  => 'url("' . get_template_directory_uri() . '/assets/custom/backend/patterns/waves_pattern.png' . '")',
                                        'background-repeat' => 'repeat',
                                    )
                                ),
                            )
                        ),
                        'color' => array(
                            'type'  => 'color-picker',
                            'value' => '#ffffff',
                            'label' => esc_html__('Header Text Color', 'litmus'),
                            'desc'  => esc_html__('Select Your Header Text Color', 'litmus'),
                        ),
                    ),

                ),
            ),
            'sticky_header' => array(
                'label'    => esc_html__( 'Sticky Header', 'litmus' ), 
                'type'         => 'switch',
                'right-choice' => array(
                    'value' => '1',
                    'label' => esc_html__( 'On', 'litmus' )
                ),
                'left-choice'  => array(
                    'value' => '0',
                    'label' => esc_html__( 'Off', 'litmus' )
                ),
                'value'        => '0',
            ),
            'search_menu' => array(
                'label'    => esc_html__( 'Search Menu', 'litmus' ), 
                'desc'    => esc_html__( 'Enable Or Disable Search.', 'litmus' ), 
                'type'         => 'switch',
                'right-choice' => array(
                    'value' => '1',
                    'label' => esc_html__( 'On', 'litmus' )
                ),
                'left-choice'  => array(
                    'value' => '0',
                    'label' => esc_html__( 'Off', 'litmus' )
                ),
                'value'        => '1',
            ),            
            'user_reg_menu' => array(
                'label'    => esc_html__( 'User Menu', 'litmus' ), 
                'desc'    => esc_html__( 'Enable Or Disable User Menu.', 'litmus' ), 
                'type'         => 'switch',
                'right-choice' => array(
                    'value' => '1',
                    'label' => esc_html__( 'On', 'litmus' )
                ),
                'left-choice'  => array(
                    'value' => '0',
                    'label' => esc_html__( 'Off', 'litmus' )
                ),
                'value'        => '1',
            ),
            'cart_widget' => array(
                'label'    => esc_html__( 'Cart Widget', 'litmus' ), 
                'desc'    => esc_html__( 'Enable/Disable WooCommerce Cart Icon', 'litmus' ), 
                'type'         => 'switch',
                'right-choice' => array(
                    'value' => '1',
                    'label' => esc_html__( 'On', 'litmus' )
                ),
                'left-choice'  => array(
                    'value' => '0',
                    'label' => esc_html__( 'Off', 'litmus' )
                ),
                'value'        => '0',
            ),
            'hamburger_block' => array(
                'label'    => esc_html__( 'Hamburger Menu', 'litmus' ), 
                'desc'    => esc_html__( 'Enable Or Disable Hamburger Menu', 'litmus' ), 
                'type'         => 'switch',
                'right-choice' => array(
                    'value' => '1',
                    'label' => esc_html__( 'On', 'litmus' )
                ),
                'left-choice'  => array(
                    'value' => '0',
                    'label' => esc_html__( 'Off', 'litmus' )
                ),
                'value'        => '1',
            ),
            'header_bg_img'   => array(
                'type'  => 'upload', 
                'label' => esc_html__('Background Pattern Image', 'litmus'),  
            ),
        ),
    ),  
    'blog_page' => array(
        'title'   => esc_html__( 'Blog Page', 'litmus' ),
        'type'    => 'tab',
        'options' => array(   
            'page_header_bg_img'   => array(
                'type'  => 'upload', 
                'label' => esc_html__('Page Header Background Image', 'litmus'),  
            ),  
            'blog_post_excerpt_length'   => array(
                'type'  => 'short-text', 
                'label' => esc_html__('Blog Post Excerpt Length', 'litmus'),  
                'value' => 55,  
            ), 
        )
    ),
    (LITMUS_IS_ACTIVE_WC) ? 
    array(
        'woo_settings' => array(
            'title'   => esc_html__( 'Woo Commerce Settings', 'litmus' ),
            'type'    => 'tab',
            'options' => array(
                'woo_archive' => array(
                    'title'   => esc_html__( 'Woo Commerce Archive', 'litmus' ),
                    'type'    => 'tab',
                    'options' => array(   
                        'wc_archive_style_option' => array(
                            'type'         => 'multi-picker',
                            'label'        => false,
                            'desc'         => false,
                            'picker'       => array( 
                                'wc_archive_style'   => array(
                                    'type'  => 'image-picker',
                                    'value' => 'one', 
                                    'label' => esc_html__('Archive Style', 'litmus'),
                                    'choices' => array( 
                                        'one'  => array(
                                            'small' => array(
                                                'height' => 70,
                                                'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/wc-pro-one-small.jpg'
                                            ),
                                        ), 
                                        'two'  => array(
                                            'small' => array(
                                                'height' => 70,
                                                'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/wc-pro-two-small.jpg'
                                            ),
                                        ),
                                    ), 
                                ),
                            ),
                            'choices'      => array(
                                'two'  => array( 
                                    'header_bg_img'   => array(
                                        'type'  => 'upload', 
                                        'label' => esc_html__('Background Image', 'litmus'),  
                                    ),
                                ),
                            ),
                        ), 
                        'wc_archive_sidebar' => array(
                            'label'    => esc_html__( 'WC Archive Sidebar', 'litmus' ),
                            'desc' => esc_html__( 'You can show or hide sidebar on WC archive page', 'litmus' ),
                            'type'         => 'switch',
                            'right-choice' => array(
                                'value' => '1',
                                'label' => esc_html__( 'On', 'litmus' )
                            ),
                            'left-choice'  => array(
                                'value' => '0',
                                'label' => esc_html__( 'Off', 'litmus' )
                            ),
                            'value'        => '1',
                        ),
                    )
                ),  
                'woo_single' => array(
                    'title'   => esc_html__( 'Woo Commerce Single', 'litmus' ),
                    'type'    => 'tab',
                    'options' => array(  
                        'wc_single_style'   => array(
                            'type'  => 'image-picker',
                            'value' => 'one', 
                            'label' => esc_html__('WC Single Page Style', 'litmus'),
                            'choices' => array( 
                                'one'  => array(
                                    'small' => array(
                                        'height' => 70,
                                        'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/woo-single-gallery-two.jpg'
                                    ),
                                ), 
                                'two'  => array(
                                    'small' => array(
                                        'height' => 70,
                                        'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/woo-single-gallery-one.jpg'
                                    ),
                                ),
                            ), 
                        ),
                        'wc_related_post_option' => array(
                            'type'         => 'multi-picker',
                            'label'        => false,
                            'desc'         => false,
                            'picker'       => array(
                                'wc_related_post' => array(
                                    'label'    => esc_html__( 'Related Posts', 'litmus' ),
                                    'desc' => esc_html__( 'You can show or hide related posts', 'litmus' ),
                                    'type'         => 'switch',
                                    'right-choice' => array(
                                        'value' => '1',
                                        'label' => esc_html__( 'On', 'litmus' )
                                    ),
                                    'left-choice'  => array(
                                        'value' => '0',
                                        'label' => esc_html__( 'Off', 'litmus' )
                                    ),
                                    'value'        => '0',
                                ),
                            ),

                            'choices' => array(
                                '1'  => array(
                                    'wc_related_title'  => array(
                                        'label' => esc_html__('Title', 'litmus'), 
                                        'value' => '<span>Litmus</span> Suggest',
                                        'type'  => 'text',
                                    ),
                                    'wc_related_desc'  => array(
                                        'label' => esc_html__('Short Description', 'litmus'), 
                                        'value' => 'Etiam congue rhoncus gravida. Sed vel sodales tortor. Donec eget dictum enim. Donec tempus euismod metus ac maximus, aenean mattis.',
                                        'type'  => 'textarea',
                                    ),
                                    'wc_related_query' => array(
                                        'label'    => esc_html__( 'Related Post Query Type', 'litmus' ),
                                        'desc' => esc_html__( 'You can change related post query type', 'litmus' ),
                                        'type'    => 'radio',
                                        'choices' => array(
                                            'tag' => esc_html__( 'Tag', 'litmus' ),
                                            'category' => esc_html__( 'Category', 'litmus' ),
                                            'author' => esc_html__( 'Author', 'litmus' ),
                                            'best_saller' => esc_html__( 'Best Saller', 'litmus' ),
                                        ),                                    
                                        'value'   => 'best_saller',
                                    ),
                                    'wc_related_query_number' => array(
                                        'label' => esc_html__('Post Per Slider', 'litmus'),
                                        'desc' => esc_html__('How many post you want to show', 'litmus'),
                                        'type'  => 'slider',
                                        'value' => 6,
                                        'properties' => array(                        
                                            'min' => 4,
                                            'max' => 20,
                                            'step' => 1,
                                        ),
                                    ),
                                ),
                            ),
                        ),  
                    ),
                ),
            ),
        ), 
    ) : array(), 
    'footer_section' => array(
        'title' => esc_html__( 'Footer Section', 'litmus' ),
        'type' => 'tab',
        'options' => array(  
            'footer_style_option' => array(
                'type'         => 'multi-picker',
                'label'        => false,
                'desc'         => false,
                'picker'       => array( 
                    'footer_style'   => array(
                        'type'  => 'image-picker',
                        'value' => 'creative', 
                        'label' => esc_html__('Footer Style', 'litmus'),
                        'choices' => array( 
                            'creative'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-one-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-one-large.jpg'
                                ),
                            ),
                            'restaurant'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-two-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-two-large.jpg'
                                ),
                            ),
                            'ecommerce'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-three-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-three-large.jpg'
                                ),
                            ),                            
                            'creative2'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-three-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-three-large.jpg'
                                ),
                            ),
                            'layout_footer'  => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-three-small.jpg'
                                ),
                                'large' => array(
                                    'height' => 94,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/header-variation/footer-three-large.jpg'
                                ),
                            ),
                        ), 
                    ),
                ),
                'choices'      => array(
                    'ecommerce'  => array( 
                        'service_offer_option' => array(
                            'type'         => 'multi-picker',
                            'label'        => false,
                            'desc'         => false,
                            'picker'       => array(
                                'service_offer' => array(
                                    'label'    => esc_html__( 'Service Offer', 'litmus' ), 
                                    'type'         => 'switch',
                                    'right-choice' => array(
                                        'value' => '1',
                                        'label' => esc_html__( 'On', 'litmus' )
                                    ),
                                    'left-choice'  => array(
                                        'value' => '0',
                                        'label' => esc_html__( 'Off', 'litmus' )
                                    ),
                                    'value'        => '0',
                                ),
                            ),
                            'choices'      => array(
                                '1'  => array( 
                                    'service_offers'   => array(
                                        'type'          => 'addable-popup',
                                        'label'       => esc_html__( 'Service Offer List', 'litmus' ),
                                        'template'      => '{{- title }}',
                                        'popup-options' => array(
                                            'icon'  => array(
                                                'label' => esc_html__('Icon', 'litmus'),
                                                'type'  => 'icon-v2',
                                            ),
                                            'title'  => array(
                                                'label' => esc_html__('Title', 'litmus'),
                                                'type'  => 'text',
                                            ),
                                            'short_desc'  => array(
                                                'label' => esc_html__('Short Description', 'litmus'),
                                                'type'  => 'text',
                                            ),
                                        ), 
                                        'value' => array(
                                            array(
                                                'icon' => array(
                                                    'type' => 'icon-font',  
                                                    'icon-class' => 'unycon unycon-head-phones',  
                                                ),
                                                'title' => 'Support 24/7',
                                                'short_desc' => 'Tiam congue rhoncus gravida. Sed vel sodales tortor.',
                                            ), 
                                            array(
                                                'icon' => array(
                                                    'type' => 'icon-font',  
                                                    'icon-class' => 'unycon unycon-plane',  
                                                ),
                                                'title' => 'Shipping Wordlwide',
                                                'short_desc' => 'Tiam congue rhoncus gravida. Sed vel sodales tortor.',
                                            ),
                                            array(
                                                'icon' => array(
                                                    'type' => 'icon-font',  
                                                    'icon-class' => 'fa fa-recycle',  
                                                ),
                                                'title' => 'Money Back Guarantee',
                                                'short_desc' => 'Tiam congue rhoncus gravida. Sed vel sodales tortor.',
                                            ), 
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),                    
                    'creative2'  => array( 
                        'service_offer_option' => array(
                            'type'         => 'multi-picker',
                            'label'        => false,
                            'desc'         => false,
                            'picker'       => array(
                                'service_offer' => array(
                                    'label'    => esc_html__( 'Service Offer', 'litmus' ), 
                                    'type'         => 'switch',
                                    'right-choice' => array(
                                        'value' => '1',
                                        'label' => esc_html__( 'On', 'litmus' )
                                    ),
                                    'left-choice'  => array(
                                        'value' => '0',
                                        'label' => esc_html__( 'Off', 'litmus' )
                                    ),
                                    'value'        => '0',
                                ),
                            ),
                            'choices'      => array(
                                '1'  => array( 
                                    'service_offers'   => array(
                                        'type'          => 'addable-popup',
                                        'label'       => esc_html__( 'Service Offer List', 'litmus' ),
                                        'template'      => '{{- title }}',
                                        'popup-options' => array(
                                            'icon'  => array(
                                                'label' => esc_html__('Icon', 'litmus'),
                                                'type'  => 'icon-v2',
                                            ),
                                            'title'  => array(
                                                'label' => esc_html__('Title', 'litmus'),
                                                'type'  => 'text',
                                            ),
                                            'short_desc'  => array(
                                                'label' => esc_html__('Short Description', 'litmus'),
                                                'type'  => 'text',
                                            ),
                                        ), 
                                        'value' => array(
                                            array(
                                                'icon' => array(
                                                    'type' => 'icon-font',  
                                                    'icon-class' => 'unycon unycon-head-phones',  
                                                ),
                                                'title' => 'Support 24/7',
                                                'short_desc' => 'Tiam congue rhoncus gravida. Sed vel sodales tortor.',
                                            ), 
                                            array(
                                                'icon' => array(
                                                    'type' => 'icon-font',  
                                                    'icon-class' => 'unycon unycon-plane',  
                                                ),
                                                'title' => 'Shipping Wordlwide',
                                                'short_desc' => 'Tiam congue rhoncus gravida. Sed vel sodales tortor.',
                                            ),
                                            array(
                                                'icon' => array(
                                                    'type' => 'icon-font',  
                                                    'icon-class' => 'fa fa-recycle',  
                                                ),
                                                'title' => 'Money Back Guarantee',
                                                'short_desc' => 'Tiam congue rhoncus gravida. Sed vel sodales tortor.',
                                            ), 
                                        ),
                                    ),
                                ),
                            ),
                        ),
                        'background_color' => array(
                            'type'  => 'color-picker',
                            'value' => '#229955',
                            'label' => esc_html__('Footer Background Color', 'litmus'),
                            'desc'  => esc_html__('Select Your Footer Background Color', 'litmus'),
                        ),                        
                        'color' => array(
                            'type'  => 'color-picker',
                            'value' => '#ffffff',
                            'label' => esc_html__('Footer Text Color', 'litmus'),
                            'desc'  => esc_html__('Select Your Footer Text Color', 'litmus'),
                        ),
                    ),
                    'layout_footer'  => array( 
                        'footer_layout_page'  => array(
                            'type'  => 'multi-select', 
                            'label' => esc_html__('Select Footer Layout Page', 'litmus'), 
                            'population' => 'array', 
                            'population' => 'posts',
                            'source' => 'layout', 
                            'prepopulate' => 50, 
                            'limit' => 1,
                        ),
                    ), 
                ),
            ),  
            'footer_newsletter' => array(
                'label'    => esc_html__( 'Newsletter', 'litmus' ), 
                'type'         => 'switch',
                'right-choice' => array(
                    'value' => '1',
                    'label' => esc_html__( 'On', 'litmus' )
                ),
                'left-choice'  => array(
                    'value' => '0',
                    'label' => esc_html__( 'Off', 'litmus' )
                ),
                'value'        => '0',
            ),
            'newsletter_form_action_url'  => array(
                'label' => esc_html__('Newslatter Form Action URL', 'litmus'), 
                'type'  => 'text',
            ), 
            'footer_option' => array(
                'type'         => 'multi-picker',
                'label'        => false,
                'desc'         => false,
                'picker'       => array(
                    'widgets_columns'   => array(
                        'label'   => esc_html__( 'Footer Widget Column', 'litmus' ),
                        'type'    => 'image-picker',
                        'value'   => '4',
                        'choices' => array(
                            '1' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/footer/one-column.png'
                                ),
                            ),
                            '2' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/footer/two-columns.png'
                                ),
                            ),
                            '3' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/footer/three-columns.png'
                                ),
                            ),
                            '4' => array(
                                'small' => array(
                                    'height' => 70,
                                    'src'    => get_template_directory_uri() . '/assets/custom/backend/footer/four-columns.png'
                                ),
                            ),
                        ),
                    ),
                ),
                'choices'      => array(
                    '4'  => array( 
                        'middle_col_small' => array(
                            'label'    => esc_html__( 'Middle Two Column Small', 'litmus' ), 
                            'type'         => 'switch',
                            'right-choice' => array(
                                'value' => '1',
                                'label' => esc_html__( 'On', 'litmus' )
                            ),
                            'left-choice'  => array(
                                'value' => '0',
                                'label' => esc_html__( 'Off', 'litmus' )
                            ),
                            'value'        => '0',
                        ), 
                    ),
                ),
            ), 
            'copyright_text'  => array(
                'label' => esc_html__('Copyright Info', 'litmus'),
                'value' => wp_kses( __('Copyright 2018 <a href="#">Litmus</a> All rights Reserved.', 'litmus'), LITMUS_Static::html_allow() ),
                'type'  => 'text',
            ),
        ),
    ),
    '404_page' => array(
        'title'   => esc_html__( '404 Page', 'litmus' ),
        'type'    => 'tab',
        'options' => array(   
            '404_bg_img'   => array(
                'type'  => 'upload', 
                'label' => esc_html__('404 Page Background Image', 'litmus'),  
            ), 
        )
    ),
);