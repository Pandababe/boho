<?php
/**
 * Load and register widgets
 *
 * @package litmus
 */

require_once LITMUS_TEMPLATE_DIR . '/inc/widgets/popular-posts.php';
require_once LITMUS_TEMPLATE_DIR . '/inc/widgets/latest-posts.php';
require_once LITMUS_TEMPLATE_DIR . '/inc/widgets/newsletter.php';

/**
 * Register widgets
 */
add_action('widgets_init','sh_lm_register_widgets');
function sh_lm_register_widgets() {
	register_widget('Litmus_Popular_Posts');
	register_widget('Litmus_Latest_Posts'); 
	register_widget('Litmus_Newsletter');
}