<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package Litmus
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function sh_lm_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'sh_lm_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function sh_lm_jetpack_setup
add_action( 'after_setup_theme', 'sh_lm_jetpack_setup' );

function sh_lm_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/post/content', get_post_format() );
	}
} // end function sh_lm_infinite_scroll_render