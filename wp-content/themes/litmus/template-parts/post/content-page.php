<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Litmus
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ( has_post_thumbnail() ) { ?>
    <figure class="post-thumb">
        <?php 
            the_post_thumbnail( 'sh-lm-single-full', array( 'class' => " img-responsive", 'alt' => get_the_title() ));
        ?>
    </figure> <!-- /.post-thumb -->
    <?php } ?>
    <header class="entry-header">            
        <?php the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); ?>
    </header> <!-- /.entry-header -->

    <div class="entry-content">
    <?php 
        the_content(); 
        sh_lm_wp_link_pages();
    ?>
    </div> <!-- .entry-content -->
    <footer class="entry-footer clearfix">                          
        <?php
            if (sh_lm_get_customizer_field('page_social_share')) {
                sh_lm_page_footer(); 
            }
        ?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->
