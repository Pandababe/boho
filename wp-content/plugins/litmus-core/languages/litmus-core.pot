msgid ""
msgstr ""
"Project-Id-Version: litmus\n"
"POT-Creation-Date: 2018-01-02 15:31+0600\n"
"PO-Revision-Date: 2018-01-02 15:32+0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_x;_ex;_n;_nx;_n_noop;_nx_noop;"
"translate_nooped_plural;esc_html__;esc_html_e;esc_html_x;esc_attr__;"
"esc_attr_e;esc_attr_x\n"
"X-Poedit-SearchPath-0: .\n"

#: custom-post.php:11 custom-post.php:12 custom-post.php:13 custom-post.php:26
#: custom-post.php:27 kc-shortcodes/addons/our-portfilio.php:22
msgid "Portfolio"
msgstr ""

#: custom-post.php:14
msgid "Parent Portfolio:"
msgstr ""

#: custom-post.php:15
msgid "All Portfolio"
msgstr ""

#: custom-post.php:16
msgid "View Portfolio"
msgstr ""

#: custom-post.php:17
msgid "Add New Portfolio"
msgstr ""

#: custom-post.php:18
msgid "New Portfolio"
msgstr ""

#: custom-post.php:19
msgid "Edit Portfolio"
msgstr ""

#: custom-post.php:20
msgid "Update Portfolio"
msgstr ""

#: custom-post.php:21
msgid "Search Portfolio"
msgstr ""

#: custom-post.php:22
msgid "No portfolio found"
msgstr ""

#: custom-post.php:23
msgid "No portfolio found in Trash"
msgstr ""

#: custom-post.php:49 custom-post.php:50
msgid "Portfolio Categories"
msgstr ""

#: custom-post.php:51
msgid "Search Portfolio Category"
msgstr ""

#: custom-post.php:52
msgid "All Portfolio Category"
msgstr ""

#: custom-post.php:53
msgid "Parent Portfolio Category"
msgstr ""

#: custom-post.php:54
msgid "Parent Portfolio Category:"
msgstr ""

#: custom-post.php:55
msgid "Edit Portfolio Category"
msgstr ""

#: custom-post.php:56
msgid "Update Portfolio Category"
msgstr ""

#: custom-post.php:57
msgid "Add New Portfolio Category"
msgstr ""

#: custom-post.php:58
msgid "New Portfolio Category Name"
msgstr ""

#: custom-post.php:59
msgid "Portfolio Category"
msgstr ""

#: custom-post.php:73 custom-post.php:74
msgid "Portfolio Tags"
msgstr ""

#: custom-post.php:75
msgid "Search Portfolio Tag"
msgstr ""

#: custom-post.php:76
msgid "All Portfolio Tag"
msgstr ""

#: custom-post.php:77
msgid "Parent Portfolio Tag"
msgstr ""

#: custom-post.php:78
msgid "Parent Portfolio Tag:"
msgstr ""

#: custom-post.php:79
msgid "Edit Portfolio Tag"
msgstr ""

#: custom-post.php:80
msgid "Update Portfolio Tag"
msgstr ""

#: custom-post.php:81
msgid "Add New Portfolio Tag"
msgstr ""

#: custom-post.php:82
msgid "New Portfolio Tag Name"
msgstr ""

#: custom-post.php:83
msgid "Portfolio Tag"
msgstr ""

#: functions.php:32
msgid "Sorry, spaces not allowed in usernames"
msgstr ""

#: functions.php:36
msgid "Username already exist."
msgstr ""

#: functions.php:38
msgid "Email already exist."
msgstr ""

#: functions.php:40
msgid "Password do not match."
msgstr ""

#: functions.php:51
msgid "Error on user creation."
msgstr ""

#: functions.php:55
msgid "You're successfully register"
msgstr ""

#: functions.php:90
#, php-format
msgid "Reservatition Form %s Website"
msgstr ""

#: functions.php:91
msgid "Person: "
msgstr ""

#: functions.php:91
msgid "Date: "
msgstr ""

#: functions.php:91
msgid "Time: "
msgstr ""

#: functions.php:91
msgid "Phone: "
msgstr ""

#: functions.php:109
msgid "Litmus"
msgstr ""

#: functions.php:111
msgid "Product Registration"
msgstr ""

#: functions.php:112
msgid "Support"
msgstr ""

#: functions.php:113
msgid "Auto Setup"
msgstr ""

#: functions.php:114
msgid "System Status"
msgstr ""

#: functions.php:115
msgid "Theme Settings"
msgstr ""

#: functions.php:119
msgid "Welcome"
msgstr ""

#: functions.php:139
msgid "Share on Facebook"
msgstr ""

#: functions.php:142
msgid "Share on Twitter"
msgstr ""

#: functions.php:146
msgid "Share on GooglePlus"
msgstr ""

#: functions.php:150
msgid "Share on Linkedin"
msgstr ""

#: functions.php:153
msgid "Share on Pinterest"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:64
#: kc-shortcodes/kc-functions.php:61
msgid "Show All"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:84
msgid "Best Seller Slider"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:85
msgid "Product Best Seller Slider"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:91
#: kc-shortcodes/addons/call-to-action.php:51
#: kc-shortcodes/addons/coming-soon.php:61
#: kc-shortcodes/addons/page-header.php:44
#: kc-shortcodes/addons/testimonials.php:70
msgid "Background Image"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:97
#: kc-shortcodes/addons/best-seller-two-slider.php:75
#: kc-shortcodes/addons/call-to-action.php:58
#: kc-shortcodes/addons/coming-soon.php:75
#: kc-shortcodes/addons/dining-menu.php:75
#: kc-shortcodes/addons/new-arrivals.php:149
#: kc-shortcodes/addons/newsletter-form.php:56
#: kc-shortcodes/addons/our-blog-two.php:111
#: kc-shortcodes/addons/our-blog.php:64 kc-shortcodes/addons/our-policy.php:75
#: kc-shortcodes/addons/our-portfilio.php:166
#: kc-shortcodes/addons/our-services.php:56
#: kc-shortcodes/addons/our-services.php:96
#: kc-shortcodes/addons/our-skills.php:86
#: kc-shortcodes/addons/our-skills.php:123 kc-shortcodes/addons/our-team.php:69
#: kc-shortcodes/addons/page-header.php:58
#: kc-shortcodes/addons/reservation-form.php:108
#: kc-shortcodes/addons/restaurant-welcome.php:87
#: kc-shortcodes/addons/restaurant-welcome.php:151
#: kc-shortcodes/addons/submit-project.php:67
#: kc-shortcodes/addons/submit-project.php:94
#: kc-shortcodes/addons/working-process.php:52
#: kc-shortcodes/addons/working-process.php:83
msgid "Title"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:104
#: kc-shortcodes/addons/dining-menu.php:82 kc-shortcodes/addons/our-blog.php:71
#: kc-shortcodes/addons/our-policy.php:70
#: kc-shortcodes/addons/our-services.php:91
#: kc-shortcodes/addons/reservation-form.php:114
#: kc-shortcodes/addons/restaurant-welcome.php:146
msgid "Icon"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:109
#: kc-shortcodes/addons/category-with-thumb.php:71
#: kc-shortcodes/addons/dining-menu.php:87
#: kc-shortcodes/addons/our-portfilio.php:182
msgid "Select Category"
msgstr ""

#: kc-shortcodes/addons/best-seller-slider.php:119
msgid "Total Post In Tab"
msgstr ""

#: kc-shortcodes/addons/best-seller-two-slider.php:66
msgid "Best Seller Two Slider"
msgstr ""

#: kc-shortcodes/addons/best-seller-two-slider.php:67
msgid "Product Best Seller Two Slider"
msgstr ""

#: kc-shortcodes/addons/best-seller-two-slider.php:82
#: kc-shortcodes/addons/call-to-action.php:65
#: kc-shortcodes/addons/new-arrivals.php:156
#: kc-shortcodes/addons/our-blog-two.php:118
#: kc-shortcodes/addons/our-policy.php:81
#: kc-shortcodes/addons/our-portfilio.php:173
#: kc-shortcodes/addons/our-services.php:102
#: kc-shortcodes/addons/our-skills.php:93
#: kc-shortcodes/addons/reservation-form.php:121
#: kc-shortcodes/addons/working-process.php:89
msgid "Short Description"
msgstr ""

#: kc-shortcodes/addons/best-seller-two-slider.php:87
#: kc-shortcodes/addons/dining-menu.php:107
#: kc-shortcodes/addons/our-blog.php:76
#: kc-shortcodes/addons/our-portfilio.php:192
msgid "Total Post"
msgstr ""

#: kc-shortcodes/addons/call-to-action.php:44
#: kc-shortcodes/addons/call-to-action.php:45
msgid "Call To Action"
msgstr ""

#: kc-shortcodes/addons/call-to-action.php:72
#: kc-shortcodes/addons/newsletter-form.php:63
msgid "Button Title"
msgstr ""

#: kc-shortcodes/addons/call-to-action.php:78
msgid "Button URL"
msgstr ""

#: kc-shortcodes/addons/category-with-thumb.php:64
#: kc-shortcodes/addons/category-with-thumb.php:65
msgid "Category With Thumb"
msgstr ""

#: kc-shortcodes/addons/category-with-thumb.php:81
msgid "Thumbnail"
msgstr ""

#: kc-shortcodes/addons/category-with-thumb.php:87
msgid "URL"
msgstr ""

#: kc-shortcodes/addons/coming-soon.php:54
#: kc-shortcodes/addons/coming-soon.php:55
msgid "Coming Soon"
msgstr ""

#: kc-shortcodes/addons/coming-soon.php:68
#: kc-shortcodes/addons/newsletter-form.php:49
#: kc-shortcodes/addons/reservation-form.php:101
msgid "Title Top"
msgstr ""

#: kc-shortcodes/addons/coming-soon.php:82
msgid "Subtitle"
msgstr ""

#: kc-shortcodes/addons/coming-soon.php:88
msgid "Time Top Text"
msgstr ""

#: kc-shortcodes/addons/coming-soon.php:93
#: kc-shortcodes/addons/reservation-form.php:37
msgid "Date"
msgstr ""

#: kc-shortcodes/addons/coming-soon.php:99
#: kc-shortcodes/addons/reservation-form.php:41
msgid "Time"
msgstr ""

#: kc-shortcodes/addons/dining-menu.php:67
#: kc-shortcodes/addons/dining-menu.php:68
msgid "Dining Menu"
msgstr ""

#: kc-shortcodes/addons/dining-menu.php:97
msgid "Category Tab"
msgstr ""

#: kc-shortcodes/addons/dining-menu.php:100
msgid "Show"
msgstr ""

#: kc-shortcodes/addons/dining-menu.php:101
msgid "Hide"
msgstr ""

#: kc-shortcodes/addons/new-arrivals.php:141
msgid "New Arrivals"
msgstr ""

#: kc-shortcodes/addons/new-arrivals.php:142
msgid "Product New Arrivals"
msgstr ""

#: kc-shortcodes/addons/new-arrivals.php:161
msgid "Select Product"
msgstr ""

#: kc-shortcodes/addons/newsletter-form.php:41
msgid "Newsletter Form"
msgstr ""

#: kc-shortcodes/addons/newsletter-form.php:42
msgid "Newsletter Form Section"
msgstr ""

#: kc-shortcodes/addons/our-blog-two.php:75
msgid "Read More"
msgstr ""

#: kc-shortcodes/addons/our-blog-two.php:103
msgid "Our Blog Two"
msgstr ""

#: kc-shortcodes/addons/our-blog-two.php:104
msgid "Our Blog Style Two"
msgstr ""

#: kc-shortcodes/addons/our-blog.php:56 kc-shortcodes/addons/our-blog.php:57
msgid "Our Blog"
msgstr ""

#: kc-shortcodes/addons/our-blog.php:72
msgid "If you set icon title will show in center"
msgstr ""

#: kc-shortcodes/addons/our-client.php:33
#: kc-shortcodes/addons/our-client.php:34
#: kc-shortcodes/addons/our-client.php:41
msgid "Our Client"
msgstr ""

#: kc-shortcodes/addons/our-client.php:42
msgid "Add new client"
msgstr ""

#: kc-shortcodes/addons/our-client.php:46
msgid "Company Logo"
msgstr ""

#: kc-shortcodes/addons/our-client.php:52
msgid "Company Name"
msgstr ""

#: kc-shortcodes/addons/our-client.php:58
msgid "Company URL"
msgstr ""

#: kc-shortcodes/addons/our-policy.php:38
#: kc-shortcodes/addons/our-policy.php:39
#: kc-shortcodes/addons/our-policy.php:46
msgid "Our Policy"
msgstr ""

#: kc-shortcodes/addons/our-policy.php:47
msgid "Add new policy"
msgstr ""

#: kc-shortcodes/addons/our-portfilio.php:19
#: kc-shortcodes/addons/page-header.php:15
msgid "Home"
msgstr ""

#: kc-shortcodes/addons/our-portfilio.php:38
#: kc-shortcodes/addons/our-portfilio.php:69
msgid "All"
msgstr ""

#: kc-shortcodes/addons/our-portfilio.php:67
msgid "Filter +"
msgstr ""

#: kc-shortcodes/addons/our-portfilio.php:148
#: kc-shortcodes/addons/our-portfilio.php:149
msgid "Our Portfolio"
msgstr ""

#: kc-shortcodes/addons/our-portfilio.php:155
msgid "Style"
msgstr ""

#: kc-shortcodes/addons/our-portfilio.php:158
msgid "One"
msgstr ""

#: kc-shortcodes/addons/our-portfilio.php:159
msgid "Two"
msgstr ""

#: kc-shortcodes/addons/our-services.php:48
#: kc-shortcodes/addons/our-services.php:49
#: kc-shortcodes/addons/our-services.php:63
msgid "Our Services"
msgstr ""

#: kc-shortcodes/addons/our-services.php:64
msgid "Add new service"
msgstr ""

#: kc-shortcodes/addons/our-skills.php:53
msgid "skill iamge"
msgstr ""

#: kc-shortcodes/addons/our-skills.php:78
#: kc-shortcodes/addons/our-skills.php:79
#: kc-shortcodes/addons/our-skills.php:99
msgid "Our Skills"
msgstr ""

#: kc-shortcodes/addons/our-skills.php:100
msgid "Add new skill"
msgstr ""

#: kc-shortcodes/addons/our-skills.php:129
msgid "Percent"
msgstr ""

#: kc-shortcodes/addons/our-skills.php:136
msgid "Skill Images"
msgstr ""

#: kc-shortcodes/addons/our-team.php:31
msgid "team iamge"
msgstr ""

#: kc-shortcodes/addons/our-team.php:61 kc-shortcodes/addons/our-team.php:62
#: kc-shortcodes/addons/our-team.php:76
msgid "Our Team"
msgstr ""

#: kc-shortcodes/addons/our-team.php:77
msgid "Add new member"
msgstr ""

#: kc-shortcodes/addons/our-team.php:81
#: kc-shortcodes/addons/restaurant-welcome.php:156
#: kc-shortcodes/addons/testimonials.php:81
msgid "Image"
msgstr ""

#: kc-shortcodes/addons/our-team.php:87
#: kc-shortcodes/addons/restaurant-welcome.php:100
#: kc-shortcodes/addons/testimonials.php:87
msgid "Name"
msgstr ""

#: kc-shortcodes/addons/our-team.php:93
#: kc-shortcodes/addons/testimonials.php:93
msgid "Position"
msgstr ""

#: kc-shortcodes/addons/page-header.php:37
#: kc-shortcodes/addons/page-header.php:38
msgid "Page Header"
msgstr ""

#: kc-shortcodes/addons/page-header.php:51
msgid "Short Title"
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:33
msgid "Person"
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:45
#: kc-shortcodes/addons/reservation-form.php:127
msgid "Phone Number"
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:57
msgid "We have received your mail, We will get back to you soon!"
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:58
msgid "Sorry, Message could not send! Please try again."
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:76
#: kc-shortcodes/addons/reservation-form.php:77
msgid "Reservation Form"
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:94
msgid "Receiver Email ID"
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:95
msgid "In which email you want to receive booking mail?"
msgstr ""

#: kc-shortcodes/addons/reservation-form.php:133
msgid "Submit Order"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:59
msgid "chef image"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:79
msgid "Restaurant Welcome"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:80
msgid "Restaurant Welcome Section"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:94
msgid "Description"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:105
msgid "Sign Image"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:111
msgid "Cuisine Text"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:117
msgid "Dish of our cuisine"
msgstr ""

#: kc-shortcodes/addons/restaurant-welcome.php:118
msgid "Add new cuisine"
msgstr ""

#: kc-shortcodes/addons/submit-project.php:58
#: kc-shortcodes/addons/submit-project.php:59
msgid "Submit Project"
msgstr ""

#: kc-shortcodes/addons/submit-project.php:74
msgid "Pricing List"
msgstr ""

#: kc-shortcodes/addons/submit-project.php:75
msgid "Add new price"
msgstr ""

#: kc-shortcodes/addons/submit-project.php:101
msgid "Project Summit CF7 Shortcode"
msgstr ""

#: kc-shortcodes/addons/testimonials.php:63
#: kc-shortcodes/addons/testimonials.php:64
#: kc-shortcodes/addons/testimonials.php:76
msgid "Testimonials"
msgstr ""

#: kc-shortcodes/addons/testimonials.php:77
msgid "Add new testimonial"
msgstr ""

#: kc-shortcodes/addons/testimonials.php:98
msgid "Rating Star"
msgstr ""

#: kc-shortcodes/addons/testimonials.php:106
msgid "Testimonial"
msgstr ""

#: kc-shortcodes/addons/working-process.php:44
#: kc-shortcodes/addons/working-process.php:45
#: kc-shortcodes/addons/working-process.php:59
msgid "Working Process"
msgstr ""

#: kc-shortcodes/addons/working-process.php:60
msgid "Add new process"
msgstr ""

#: kc-shortcodes/kc-functions.php:66
msgid "Order"
msgstr ""
