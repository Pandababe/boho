<?php 

/**
 * Sync common LITMUS Settings and Customizer options db values
 * @internal
 */
class LITMUS_Sync_Customizer_Options {
    public static function init() {
        add_action('customize_save_after', array(__CLASS__, 'after_customizer_save'));
        add_action('fw_settings_form_saved', array(__CLASS__, 'after_settings_save'));
        add_action('fw_settings_form_reset', array(__CLASS__, 'after_settings_save'));
    }

    /**
     * If a customizer option also exists in settings options, copy its value to settings option value
     */
    public static function after_customizer_save()
    {
        $settings_options = fw_extract_only_options(fw()->theme->get_settings_options());

        foreach (
            array_intersect_key(
                fw_extract_only_options(fw()->theme->get_customizer_options()),
                $settings_options
            )
            as $option_id => $option
        ) {
            if ($option['type'] === $settings_options[$option_id]['type']) {
                fw_set_db_settings_option(
                    $option_id, fw_get_db_customizer_option($option_id)
                );
            }
        }
    }

    /**
     * If a settings option also exists in customizer options, copy its value to customizer option value
     */
    public static function after_settings_save()
    {
        $customizer_options = fw_extract_only_options(fw()->theme->get_customizer_options());

        foreach (
            array_intersect_key(
                fw_extract_only_options(fw()->theme->get_settings_options()),
                $customizer_options
            )
            as $option_id => $option
        ) {
            if ($option['type'] === $customizer_options[$option_id]['type']) {
                fw_set_db_customizer_option(
                    $option_id, fw_get_db_settings_option($option_id)
                );
            }
        }
    }
}
LITMUS_Sync_Customizer_Options::init();

/* Unyson Custom Font Family Name*/
function sh_lm_typography_v2_custom_fonts( $fonts ) {
    array_push( $fonts, "BazarMedium" );
    return $fonts;
}
add_filter( 'fw_option_type_typography_v2_standard_fonts', 'sh_lm_typography_v2_custom_fonts' );

/* litmus fw post meta */
if ( ! function_exists( 'sh_lm_fw_post_meta' ) ) :
    function sh_lm_fw_post_meta() {
        $result = get_post_meta( get_the_ID(), 'fw_options', true); 
        foreach (func_get_args() as $arg) {
            if(is_array($arg)) {
                if (!empty($result[$arg[0]])) {
                    $result = $result[$arg[0]];
                } else {  
                  $result = $arg[1];
                }
            } else {
                if (!empty($result[$arg])) {
                    $result = $result[$arg];
                } else { 
                    $result = null;
                }
            }
        }
        return $result;
    }
endif;

/* litmus theme return  */
if ( ! function_exists( 'sh_lm_return' ) ) :
    function sh_lm_return($value) {
       return $value;
    }
endif;

if ( ! function_exists( 'sh_lm_get_customizer_field' ) ) :
    function sh_lm_get_customizer_field() {
        $result = get_theme_mod('fw_options');
        foreach (func_get_args() as $arg) {
            if(is_array($arg)) {
                if (!empty($result[$arg[0]])) {
                    $result = $result[$arg[0]];
                } else {  
                  $result = $arg[1];
                }
            } else {
                if (!empty($result[$arg])) {
                    $result = $result[$arg];
                } else { 
                    $result = null;
                }
            }
        }
        return $result;
    }
endif;

/* Typography Field */
/*
 * GOOGLE FONT
 * since 1.0
 */
if(!function_exists('_sh_lm_process_google_fonts')) {
    function _sh_lm_process_google_fonts()
    {
        $include_from_google = array();
        $google_fonts = fw_get_google_fonts();

        $typo_fiels = array(
            fw_get_db_settings_option('global_body_fonts'), 
            fw_get_db_settings_option('hading_one_font'),   
            fw_get_db_settings_option('hading_two_font'),   
            fw_get_db_settings_option('hading_three_font'),   
            fw_get_db_settings_option('hading_four_font'),   
            fw_get_db_settings_option('hading_five_font'),   
            fw_get_db_settings_option('hading_six_font'),   
            fw_get_db_settings_option('logo_settings')['logo']['text']["logo_title_font"],   
            fw_get_db_settings_option('logo_settings')['logo']['text']['logo_subtitle_font'],  
        );

        foreach ($typo_fiels as $value) {
            if( isset($google_fonts[$value['family']]) ){
                $include_from_google[$value['family']] =   $google_fonts[$value['family']];
            }
        }

        $google_fonts_links = sh_lm_get_remote_fonts($include_from_google);
        // set a option in db for save google fonts link
        update_option( 'sh_lm_google_fonts_link', $google_fonts_links );
    }
    add_action('fw_settings_form_saved', '_sh_lm_process_google_fonts', 999, 2);
    add_action('customize_save_after', '_sh_lm_process_google_fonts', 999, 2);
    add_action('fw_settings_form_reset', '_sh_lm_process_google_fonts', 999, 2);
}

if (!function_exists('sh_lm_get_remote_fonts')) :
    function sh_lm_get_remote_fonts($include_from_google) {
        /**
         * Get remote fonts
         * @param array $include_from_google
         */
        if ( ! sizeof( $include_from_google ) ) {
            return '';
        }

        $html = "//fonts.googleapis.com/css?family=";

        foreach ( $include_from_google as $font => $styles ) {
            $html .= str_replace( ' ', '+', $font ) . ':' . implode( ',', $styles['variants'] ) . '%7C';
        }

        $html = substr( $html, 0, - 1 );

        return $html;
    }
endif;

if (!function_exists('_sh_lm_print_google_fonts_link')) :
    function _sh_lm_print_google_fonts_link() {
        /**
         * Print google fonts link
         */
        wp_enqueue_style('sh-lm-custom-google-font', get_option('sh_lm_google_fonts_link'), false, false, 'all');
    }
    add_action( 'wp_enqueue_scripts', '_sh_lm_print_google_fonts_link' );
endif;
//end goole font

if ( ! function_exists( 'sh_lm_builder_field' ) ) :
    function sh_lm_builder_field($value) {
        if(isset($value)) {
            return $value;
        }
    }
endif;

if ( ! function_exists( 'sh_lm_includes_additional_option_types' ) ) :
    /**
     * Include addition option types
     */
    function sh_lm_includes_additional_option_types() {
        $sh_lm_template_directory = get_template_directory(); 
        load_template( $sh_lm_template_directory . '/inc/custom-option-types/color-palette/class-fw-color-palette-new.php' ); 
    }

    add_action( 'fw_option_types_init', 'sh_lm_includes_additional_option_types' );
endif;

/**
 * Include the auto Installer
 */
if ( is_admin() && current_user_can('switch_themes') && !class_exists( 'FW_Theme_Auto_Setup' )) {
    require LITMUS_TEMPLATE_DIR . '/framework-customizations/extensions/auto-setup/class-fw-auto-install.php';
}

/**
 * @param FW_Ext_Backups_Demo[] $demos
 * @return FW_Ext_Backups_Demo[]
 */
function sh_lm_demo_contetn_setup($demos) {
    $demos_array = array(
        'demo-one' => array(
            'title' => esc_html__('Creative Demo', 'litmus'),
            'screenshot' => esc_url('http://demo.tortoizthemes.com/litmus-demo-content/screenshot-1.png'),
            'preview_link' => esc_url('http://demo.tortoizthemes.com/litmus-wp/creative/'),
        ),
        'demo-restaurant' => array(
            'title' => esc_html__('Restaurant Demo', 'litmus'),
            'screenshot' => esc_url('http://demo.tortoizthemes.com/litmus-demo-content/demo-restaurant.jpg'),
            'preview_link' => esc_url('http://demo.tortoizthemes.com/litmus-wp/restaurant/'),
        ),
        'demo-wooshop' => array(
            'title' => esc_html__('WooCommerce Demo', 'litmus'),
            'screenshot' => esc_url('http://demo.tortoizthemes.com/litmus-demo-content/demo-wooshop.jpg'),
            'preview_link' => esc_url('http://demo.tortoizthemes.com/litmus-wp/wooshop/'),
        ),
    );

    $download_url = esc_url('http://demo.tortoizthemes.com/litmus-demo-content/');

    foreach ( $demos_array as $id => $data ) {
        $demo = new FW_Ext_Backups_Demo($id, 'piecemeal', array(
            'url' => $download_url,
            'file_id' => $id,
        ));
        $demo->set_title($data['title']);
        $demo->set_screenshot($data['screenshot']);
        $demo->set_preview_link($data['preview_link']);

        $demos[ $demo->get_id() ] = $demo;

        unset($demo);
    }

    return $demos;
}
add_filter('fw:ext:backups-demo:demos', 'sh_lm_demo_contetn_setup');