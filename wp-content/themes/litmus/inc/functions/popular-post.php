<?php
function sh_lm_set_post_views($postID) {
    $count_key = 'sh_lm_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ( $count == '' ){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching

function sh_lm_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id ) ) {
        global $post;
        $post_id = $post->ID;    
    }
    sh_lm_set_post_views($post_id);
}
add_action( 'wp_head', 'sh_lm_track_post_views');

function sh_lm_get_post_views($postID){
    $count_key = 'sh_lm_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if( $count == 0 ) {
        return esc_html__('0','litmus');
    } elseif( $count == 1 ) {
        return esc_html__('1','litmus');
    }
    return $count;
}

