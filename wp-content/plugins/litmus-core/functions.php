<?php 
/**
 * Some theme and plugin funcitons goes here
 */

if ( ! defined( 'LITMUS_PLUGIN_DIR_URL' ) ) {
    define( 'LITMUS_PLUGIN_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! function_exists( 'sh_lm_registar_form' ) ) :
    function sh_lm_registar_form() { ?>
        <?php 
        $header_style = sh_lm_get_customizer_field('header_style_option','header_style');
        if( $header_style == 'restaurant' ) {
            $hamburger_class = 'citrus-theme';
        } elseif( $header_style == 'ecommerce' ) {
            $hamburger_class = 'orange-theme';
        } elseif( $header_style == 'creative2' ) {
            $hamburger_class = '';
        } else {
            $hamburger_class = '';
        }
        ?>
        <div id="login-register" class="login-register-modal modal fade <?php echo esc_attr($hamburger_class); ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="pe-7s-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php if ( ! is_user_logged_in() ) { ?>
                        <ul class="bsm-tabs">
                            <li class="bsm-tab"><a class="active" href="#login"><?php esc_html_e('Login', 'litmus'); ?></a></li>
                            <li class="bsm-tab"><a href="#register"><?php esc_html_e('Register', 'litmus'); ?></a></li>
                        </ul>
                        <div id="login" class="bsm-tab-content">
                            <form action="<?php echo esc_url(home_url('/')); ?>wp-login.php" method="post" class="login-form">
                                <div class="bsm-input-field">
                                    <input id="login_user_name" required="required" name="log" type="text" class="validate">
                                    <label for="login_user_name"><?php esc_html_e('User Name', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field -->
                                <div class="bsm-input-field">
                                    <input id="login_passwords" required="required" name="pwd"  type="password" class="validate">
                                    <label for="login_passwords"><?php esc_html_e('Passwords', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field -->
                                <div class="remember-field">   
                                    <input type="checkbox" name="rememberme" id="remember" />
                                    <label for="remember"><?php esc_html_e('Remember Me', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field -->
                                <button class="bsm-btn waves-effect moody-blue-bg" type="submit"><?php esc_html_e('Submit', 'litmus'); ?></button>
                                <?php do_action('login_form'); ?>
                                <input type="hidden" name="redirect_to" value="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>" />
                                <input type="hidden" name="user-cookie" value="1" />
                                
                            </form><!-- /.login-form -->
                        </div><!-- /.bsm-tab-content -->

                        <div id="register" class="bsm-tab-content">
                            <form method="post" class="register-form">
                                <div class="bsm-input-field">
                                    <input id="full_name" required="required"  name="display_name" type="text" class="validate">
                                    <label for="full_name"><?php esc_html_e('Full Name *', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field -->

                                <div class="bsm-input-field">
                                    <input id="user_name" required="required"  name="username" type="text" class="validate">
                                    <label for="user_name"><?php esc_html_e('User Name *', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field -->

                                <div class="bsm-input-field">
                                    <input id="emai" required="required"  name="email" type="email" class="validate">
                                    <label for="emai"><?php esc_html_e('Email *', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field --> 

                                <div class="bsm-input-field">
                                    <input id="passwords" required="required"  type="password" name="pwd1" class="validate">
                                    <label for="passwords"><?php esc_html_e('Password *', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field -->

                                <div class="bsm-input-field">
                                    <input id="passwords-two" required="required"  type="password" name="pwd2" class="validate">
                                    <label for="passwords"><?php esc_html_e('Confirm Password *', 'litmus'); ?></label>
                                </div><!-- /.bsm-input-field -->

                                <button class="bsm-btn waves-effect moody-blue-bg" type="submit" name="user-submit"><?php esc_html_e('Register', 'litmus'); ?></button>
                                <input type="hidden" name="task" value="register" />
                            </form><!-- /.login-form -->
                        </div><!-- /.bsm-tab-content -->
                        <?php } else { ?>
                            <div class="user-board">  
                                <p><?php esc_html_e( "Welcome:", "litmus" );?> <strong><?php sh_lm_author_name(); ?></strong></p>
                                <ul>
                                    <li><i class="fa fa-tachometer"></i><a href="<?php echo home_url() ; ?>/wp-admin"><?php esc_html_e( "Dashboard","litmus" );?></a></li>
                                    <li><i class="fa fa-user"></i><a href="<?php echo get_edit_user_link( get_the_author_meta('ID') ) ?>"><?php esc_html_e( "Profile", "litmus" );?></a></li>
                                    <li><i class="fa fa-power-off"></i><a href="<?php echo wp_logout_url( home_url('/') ); ?>"><?php esc_html_e( "Logout", "litmus" );?></a></li>
                                    <?php 
                                        $tem_base_name = wp_basename( get_page_template() );
                                        $profile_active = ( $tem_base_name == 'template-user-profile.php' ) ? 'active' : null; 
                                        $favorites_id = sh_lm_page_id_by_tem_name('templates/template-user-favorites.php');
                                        if($favorites_id) :
                                    ?>
                                    <li><i class="fa fa-heart"></i><a href="<?php echo get_permalink( $favorites_id ); ?>"><?php echo get_the_title( $favorites_id ); ?></a></li>
                                    <?php endif; ?>
                                </ul>  
                            </div> <!-- / .user-board -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php }
endif;

if ( ! function_exists( 'sh_lm_reg_from' ) ) :
    function sh_lm_reg_from() { 
        $err = '';
        $success = '';

        global $wpdb, $PasswordHash, $current_user, $user_ID;

        if(isset($_POST['task']) && $_POST['task'] == 'register' ) { 

            $pwd1 = esc_sql($_POST['pwd1']);
            $pwd2 = esc_sql($_POST['pwd2']);
            $display_name = esc_sql($_POST['display_name']); 
            $username = esc_sql($_POST['username']);
            $email = esc_sql($_POST['email']);

            if( $email == "" || $pwd1 == "" || $pwd2 == "" || $username == "" || $display_name == "" ) {
                $err = 'Please don\'t leave the required fields.';
            } else if ( strpos($username, ' ') !== false ) {    
                $err = esc_html__( 'Sorry, spaces not allowed in usernames', 'litmus' );
            } else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $err = 'Invalid email address.';
            } else if(username_exists($username) ) {
                $err = esc_html__( 'Username already exist.', 'litmus' );
            } else if(email_exists($email) ) {
                $err = esc_html__( 'Email already exist.', 'litmus' );
            } else if($pwd1 <> $pwd2 ){
                $err = esc_html__( 'Password do not match.', 'litmus' );
            } else {

                $user_id = wp_insert_user( array (
                    'display_name' => apply_filters('pre_user_display_name', $display_name), 
                    'user_pass' => apply_filters('pre_user_user_pass', $pwd1),
                    'user_login' => apply_filters('pre_user_user_login', $username),
                    'user_email' => apply_filters('pre_user_user_email', $email),
                    'role' => 'subscriber' 
                ) );
                if( is_wp_error($user_id) ) {
                    $err = esc_html__( 'Error on user creation.', 'litmus' );
                } else {
                    do_action('user_register', $user_id);

                    $success = esc_html__( 'You\'re successfully register', 'litmus' );
                } 
            } 
        } else if(isset($_POST['task']) && $_POST['task'] == 'login' ) { 
 
        }
        ?>

        <!--display error/success message-->
        <div id="message">
            <?php
                if(! empty($err) ) :
                    echo '<p class="error">'.$err.'</p>';
                endif;
            ?>

            <?php
                if(! empty($success) ) :
                    echo '<p class="error">'.$success.'</p>';
                endif;
            ?>
        </div>
        <?php
    }
endif;

//this function for book a table shortcode
function sh_lm_reservation_mail_form() { 
    $person = ($_REQUEST['person']) ? $_REQUEST['person'] : ""; 
    $date = ($_REQUEST['date']) ? $_REQUEST['date'] : "";
    $time = ($_REQUEST['time']) ? $_REQUEST['time'] : "";
    $phone = ($_REQUEST['phone']) ? $_REQUEST['phone'] : "";
    $receving_mail_id = ($_REQUEST['receiver_mail_id']) ? $_REQUEST['receiver_mail_id'] : "";
    $book_message = ($_REQUEST['message']) ? $_REQUEST['message'] : "";
    $to = $receving_mail_id;
    $subject = sprintf(esc_html__('Reservatition Form %s Website', 'litmus' ), get_bloginfo('name'));
    $message = esc_html__('Person: ', 'litmus' ) . $person . "\n". esc_html__('Date: ', 'litmus') . $date . "\n". esc_html__('Time: ', 'litmus') . $time . "\n". esc_html__('Phone: ', 'litmus') . $phone . "\n";
    if( wp_verify_nonce( $_REQUEST['lm_reservation_form_field'], 'lm_reservation_form_action' ) ) {
	    wp_mail($to, $subject, $message);
    }
    die();
}
add_action('wp_ajax_nopriv_sh_lm_reservation_mail_form', 'sh_lm_reservation_mail_form');
add_action('wp_ajax_sh_lm_reservation_mail_form', 'sh_lm_reservation_mail_form');


//this function for quote form shortcode
if ( ! function_exists( 'sh_lm_get_quote_from_clients' ) ) :
    function sh_lm_get_quote_from_clients($submit_btn_title = "", $form_title = "", $btn_title = "", $only_form = true) { 
    $submit_btn_title = ($submit_btn_title != "") ? $submit_btn_title : esc_html__("Get a Quote", "litmus");
    $form_title = ($form_title != "") ? $form_title : esc_html__("Get a Quote", "litmus");
    $btn_title = ($btn_title != "") ? $btn_title : esc_html__("Get a Quote", "litmus");
    ?>
    <div class="quote-form-block">    
        <div class="form-popup-wrap">
            <div class="form-popup">
                <div class="modal-close">×</div>
                <?php if(sh_lm_get_customizer_field('get_a_quote_form')['quote_form_visibility'] == "0") { ?>
                <form class="quote-form" method="post">
                    <h3 class="form-title"><?php echo esc_html($form_title); ?></h3>
                    <div class="successform successQuote">
                        <p><?php esc_html_e('Your message was sent successfully!', 'litmus'); ?></p>
                    </div>
                    <div class="errorform errorQuote">
                        <p><?php esc_html_e('Something went wrong, try refreshing and submitting the form again.', 'litmus'); ?></p>
                    </div>
                    <input type="hidden" name="action" value="sh_lm_theme_get_a_quote_form" />
                    <div class="bsm-input">
                        <input type="text" name="name" class="form-control" value="" placeholder="<?php esc_attr_e('Your Name', 'litmus'); ?>" required>
                    </div>
                    <div class="bsm-input">
                        <input type="text" name="email" class="form-control" value="" placeholder="<?php esc_attr_e('Your E-mail', 'litmus'); ?>" required>
                    </div>

                    <div class="bsm-input">
                        <div class="select-wrapper">
                            <select name="service" class="select-service" required>
                                <option value="Service you need"><?php esc_html_e('Service you need','litmus') ?></option>
                                <?php 
                                    if( sh_lm_get_customizer_field('get_a_quote_service_list') != "") {
                                        foreach ( sh_lm_get_customizer_field('get_a_quote_service_list') as $value) { ?>
                                        <option value="<?php echo esc_html($value); ?>"><?php echo esc_html($value); ?></option>
                                        <?php }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="bsm-input">
                        <textarea name="message" class="form-control bsm-textarea" placeholder="<?php esc_attr_e('Details', 'litmus'); ?>" required></textarea>
                    </div>
                    <button type="submit" class="bsm-btn btn-creative"><i class="icon icon-nature-2"></i> <?php echo esc_html($btn_title); ?></button>
                </form>
                <?php } else { ?>
                    <div class="quote-form">
                    <?php if ( sh_lm_get_customizer_field('get_a_quote_form')['quote_form_visibility'] == "1" ) {
                        echo do_shortcode( sh_lm_get_customizer_field('get_a_quote_form')['1']["contact_shortcode"] ); 
                    }  ?>
                    </div><!--  /.quote-form -->
                <?php } ?>
        
            </div>
        </div>
    </div><!--  /.quote-form-block -->
    <?php }
endif;

//this function for get a quote contact form
function sh_lm_theme_get_a_quote_form() {
    $name = ($_REQUEST['name']) ? $_REQUEST['name'] : "";
    $email = ($_REQUEST['email']) ? $_REQUEST['email'] : "";
    $service = ($_REQUEST['service']) ? $_REQUEST['service'] : "";
    $user_message = ($_REQUEST['message']) ? $_REQUEST['message'] : "";  
    $to = (sh_lm_get_customizer_field('get_a_quote_email')) ? sh_lm_get_customizer_field('get_a_quote_email') : "" ; 
    $subject = sprintf(esc_html__('Get A Quote Form %s Website', 'litmus' ), get_bloginfo('name')); 
    $message = esc_html__('Name: ', 'litmus' ) . $name . "\n".esc_html__('Email: ', 'litmus' ) . $email . "\n" .esc_html__('Service: ', 'litmus' ) . $service . "\n".esc_html__('Message: ', 'litmus' ) . $user_message . "\n";
    wp_mail($to, $subject, $message);
    die(); // never forget to die() your AJAX reuqests
}
add_action('wp_ajax_nopriv_sh_lm_theme_get_a_quote_form', 'sh_lm_theme_get_a_quote_form');
add_action('wp_ajax_sh_lm_theme_get_a_quote_form', 'sh_lm_theme_get_a_quote_form');

/**
 * register our theme panel via the hook
 */
function sh_lm_register_theme_panel() {
    $user = wp_get_current_user();
    if ( !in_array( 'administrator', (array) $user->roles ) ) {
        return;
    }
    /* wp doc: add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position ); */
    add_menu_page(esc_html__('Litmus', 'litmus'), esc_html__('Litmus', 'litmus'), "manage_options", "sh_lm_welcome", 'sh_lm_welcome', null, 2);

    add_submenu_page( "sh_lm_welcome", esc_html__('Product Registration', 'litmus'), esc_html__('Product Registration', 'litmus'), 'manage_options', 'admin.php?page=sh_lm_welcome&tab=registration', null);
    add_submenu_page( "sh_lm_welcome", esc_html__('Support', 'litmus'), esc_html__('Support', 'litmus'), 'manage_options', 'admin.php?page=sh_lm_welcome&tab=support', null);
    add_submenu_page( "sh_lm_welcome", esc_html__('Auto Setup', 'litmus'), esc_html__('Auto Setup', 'litmus'), 'manage_options', 'admin.php?page=sh_lm_welcome&tab=auto_setup', null);
    add_submenu_page( "sh_lm_welcome", esc_html__('System Status', 'litmus'), esc_html__('System Status', 'litmus'), 'manage_options', 'admin.php?page=sh_lm_welcome&tab=system_status', null);
    add_submenu_page( "sh_lm_welcome", esc_html__('Theme Settings', 'litmus'), esc_html__('Theme Settings', 'litmus'), 'manage_options', 'themes.php?page=fw-settings', null);

    // shit hack for welcome menu
    global $submenu; // this is a global from WordPress
    $submenu['sh_lm_welcome'][0][0] = esc_html__('Welcome', 'litmus');
}

add_action('admin_menu', 'sh_lm_register_theme_panel');

function sh_lm_welcome() {
    return false;
}


if ( ! function_exists( 'sh_lm_social_share_link' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function sh_lm_social_share_link($before_text = "") { ?>
 
    <?php if($before_text != ""): ?> 
    <span class="meta-name"><?php echo esc_html($before_text); ?></span>    
    <?php endif; ?>               
    <!-- facebook share -->
    <a class="customer share facebook" rel="nofollow" title="<?php esc_html_e(  'Share on Facebook', 'litmus' ); ?>" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" onclick="sh_lm_PopupWindow(this.href, 'facebook-share', 580, 400); return false;">
       <i class="fa fa-facebook"></i>
    </a>
    <a class="customer share twitter" rel="nofollow" title="<?php esc_html_e(  'Share on Twitter', 'litmus' ); ?>" href="https://twitter.com/home?status=<?php the_permalink(); ?>"  onclick="sh_lm_PopupWindow(this.href, 'facebook-share', 580, 400); return false;">
        <i class="fa fa-twitter"></i>
    </a>

    <a class="customer share google-plus" rel="nofollow" title="<?php esc_html_e(  'Share on GooglePlus', 'litmus' ); ?>" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="sh_lm_PopupWindow(this.href, 'facebook-share', 580, 400); return false;">
        <i class="fa fa-google-plus"></i>
    </a>

    <a class="customer share linkedin" rel="nofollow" title="<?php esc_html_e(  'Share on Linkedin', 'litmus' ); ?>" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" onclick="sh_lm_PopupWindow(this.href, 'facebook-share', 580, 400); return false;">
        <i class="fa fa-linkedin"></i>
    </a>
    <a rel="nofollow" class="customer share pinterest" title="<?php esc_html_e(  'Share on Pinterest', 'litmus' ); ?>" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
        <i class="fa fa-pinterest-p"></i>
    </a> 
    <?php
}
endif;

/**
 * Remove Query String
 */
function sh_lm_remove_query_string_one( $src ){   
    $rqs = explode( '?ver', $src );
    return $rqs[0];
}
if ( is_admin() ) {

} else {
    add_filter( 'script_loader_src', 'sh_lm_remove_query_string_one', 15, 1 );
    add_filter( 'style_loader_src', 'sh_lm_remove_query_string_one', 15, 1 );
}

function sh_lm_remove_query_string_two( $src ){
    $rqs = explode( '&ver', $src );
    return $rqs[0];
}
if ( is_admin() ) {

} else {
    add_filter( 'script_loader_src', 'sh_lm_remove_query_string_two', 15, 1 );
    add_filter( 'style_loader_src', 'sh_lm_remove_query_string_two', 15, 1 );
}

if(LITMUS_PG_IS_ACTIVE_KC) {
    global $kc;
    // add single content type
    $kc->add_content_type( 'layout' );
}