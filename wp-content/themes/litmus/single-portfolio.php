<?php
/**
 * The template for displaying all single posts.
 *
 * @package Litmus
 */
get_header(); 
sh_lm_page_header( esc_html__('Portfolio Details', 'litmus') ); ?>

<!-- Portfolio Details Block
================================================== -->
<section class="portfolio-details-block pd-t-60-b-90">
    <div class="container">
        <!-- Portfolio Details Area -->
        <div class="row">
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-md-7">
                <figure class="portfolio-thumb">
                    <?php sh_lm_post_featured_image(650, 500, false); ?>
                </figure><!--  /.portfolio-thumb -->
            </div><!--  /.col-md-7 -->
            <div class="col-md-5">
                <div class="portfolio-header">
                    <h2 class="portfolio-title"><?php the_title(); ?></h2>
                    <div class="portfolio-meta">
                        <div class="portfolio-cat">
                            <?php 
                                $categories = get_the_terms( get_the_ID(), 'portfolio-category' );
                                if ( ! empty( $categories ) ) {
                                    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                }
                            ?>
                        </div><!--  /.portfolio-cat -->
                        <div class="portfolio-other">
                            <span class="time-period"><?php the_time( get_option( 'date_format' ) ); ?></span>
                            <span class="devider">|</span>
                            <span class="rating">
                                <?php echo LITMUS_Static::num_to_star(sh_lm_fw_post_meta('portfolio_rating')); ?>
                            </span>
                        </div><!--  /.portfolio-other -->
                    </div><!--  /.portfolio-meta -->
                </div><!--  /.portfolio-header -->

                <div class="portfolio-details">
                    <?php the_content(); ?>
                    <table class="project-details">
                        <?php if(sh_lm_fw_post_meta('portfolio_cus_name') != '') { ?>
                        <tr>
                            <th><?php esc_html_e('Customer:', 'litmus'); ?></th>
                            <td><?php echo esc_html(sh_lm_fw_post_meta('portfolio_cus_name')); ?></td>
                        </tr> 
                        <?php } if(sh_lm_fw_post_meta('portfolio_live_url') != '') { ?> 
                        <tr>
                            <th><?php esc_html_e('Live Demo:', 'litmus'); ?></th>
                            <td><a href="<?php echo esc_url(sh_lm_fw_post_meta('portfolio_live_url')); ?>"><?php echo esc_url(sh_lm_fw_post_meta('portfolio_live_url')); ?></a></td>
                        </tr>   
                        <?php }  ?>    

                        <?php 
                        $portfolio_cats = get_the_terms( get_the_ID(), 'portfolio-category' );
                        if(!empty($portfolio_cats)) { ?>                      
                        <tr>
                            <th><?php esc_html_e('Category:', 'litmus'); ?></th>
                            <td>  
                                <?php foreach ($portfolio_cats as $portfolio_cat) { ?>
                                <a href="<?php echo esc_url(get_term_link($portfolio_cat->slug, 'portfolio-category')); ?>" rel="category tag"><?php echo esc_html($portfolio_cat->name); ?></a>  
                                <?php } ?>
                            </td>
                        </tr> 
                        <?php } ?> 

                        <?php if(sh_lm_fw_post_meta('portfolio_skills') != '') { ?> 
                        <tr>
                            <th><?php esc_html_e('Skill:', 'litmus'); ?></th>
                            <td><?php echo esc_html(sh_lm_fw_post_meta('portfolio_skills')); ?></td>
                        </tr> 
                        <?php } ?>      

                        <?php if(sh_lm_fw_post_meta('portfolio_date') != '') { ?> 
                        <tr>
                            <th><?php esc_html_e('Project Date:', 'litmus'); ?></th>
                            <td><?php echo esc_html(sh_lm_fw_post_meta('portfolio_date')); ?></td>
                        </tr>  
                        <?php } ?>

                        <?php 
                        $portfolio_tags = get_the_terms( get_the_ID(), 'portfolio-tag' );
                        if(!empty($portfolio_tags)) { ?>                           
                        <tr>
                            <th><?php esc_html_e('Tag:', 'litmus'); ?></th>
                            <td> 
                                <?php foreach ($portfolio_tags as $portfolio_tag) { ?>
                                <a href="<?php echo esc_url(get_term_link($portfolio_tag->slug, 'portfolio-tag')); ?>" rel="category tag"><?php echo esc_html($portfolio_tag->name); ?></a>  
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table><!--  /.project-details -->
                    <div class="project-demo-btn">
                        <?php if(sh_lm_fw_post_meta('portfolio_download_url') != '') { ?> 
                        <a href="<?php esc_url(sh_lm_fw_post_meta('portfolio_download_url')); ?>" class="btn project-btn"><?php esc_html_e('Download', 'litmus'); ?></a>
                        <?php } if(sh_lm_fw_post_meta('portfolio_project_url') != '') { ?> 
                        <a href="<?php esc_url(sh_lm_fw_post_meta('portfolio_project_url')); ?>" class="btn project-btn"><?php esc_html_e('Lunch Project', 'litmus'); ?></a>
                        <?php } ?> 
                    </div><!--  /.project-demo-btn -->
                </div><!--  /.portfolio-details -->
            </div><!--  /.col-md-5 -->
            <?php endwhile; ?>
        </div><!--  /.row -->
        
        <div class="clearfix"></div><!--  /.clearfix -->
        <div class="pd-tb-15"></div><!--  /.pd-tb-30 -->

        <!-- Related Portfolio Area -->
        <?php  
        $related_query = new WP_Query(array( 
            'post_type' => 'portfolio', 
            'post__not_in' => array(get_the_ID()),
            'posts_per_page'=> 8, 
        )); 

        if ( $related_query->have_posts() ) : ?> 
        <div class="row">
            <div class="col-12">
                <h2 class="portfolio-related-title"><?php esc_html_e('You may also like:', 'litmus'); ?></h2><!--  /.related-title -->
                <div class="portfolio-related-slider owl-carousel">

                    <?php  
                    while ($related_query->have_posts()) : $related_query->the_post(); ?> 
                    <div class="item">
                        <a href="<?php the_permalink(); ?>"><?php sh_lm_post_featured_image(265, 205, true); ?></a>
                    </div><!--  /.item -->  
                    <?php
                    endwhile; 
                    wp_reset_postdata(); 
                    ?> 

                </div><!--  /.related-slider -->
            </div><!--  /.col-12 -->
        </div><!--  /.row -->
        <?php endif; ?>

    </div><!--  /.container -->
</section><!--  /.portfolio-block -->
<?php get_footer(); ?>