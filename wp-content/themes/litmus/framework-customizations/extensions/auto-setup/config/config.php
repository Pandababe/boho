<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}
return array(
	/**
	 * Array for demos
	 */
	'plugins'            => array( 
		array(
		    'name'      => esc_html__('Unyson', 'litmus'),
		    'slug'      => 'unyson',
		    'required'  => true,
		), 
		array(
		    'name'      => esc_html__('King Composer', 'litmus'),
		    'slug'      => 'kingcomposer',
		    'required'  => true,
		),
		array(
			'name'               => esc_html__('Litmus Core', 'litmus'), // The plugin name.
			'slug'               => 'litmus-core', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/libs/tgm-plugin-activation/plugins/litmus-core.zip', 
		),
		array(
			'name'               => esc_html__('Slider Revolution', 'litmus'), // The plugin name.
			'slug'               => 'revslider', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/libs/tgm-plugin-activation/plugins/revslider.zip', // The plugin source.
		),  
		array(
		    'name'      => esc_html__('Contact Form 7', 'litmus'),
		    'slug'      => 'contact-form-7', 
		), 
	),
	'plugins_restaurant'            => array( 
		array(
		    'name'      => esc_html__('Unyson', 'litmus'),
		    'slug'      => 'unyson',
		    'required'  => true,
		), 
		array(
		    'name'      => esc_html__('King Composer', 'litmus'),
		    'slug'      => 'kingcomposer',
		    'required'  => true,
		),
		array(
			'name'               => esc_html__('Litmus Core', 'litmus'), // The plugin name.
			'slug'               => 'litmus-core', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/libs/tgm-plugin-activation/plugins/litmus-core.zip', 
		),
		array(
			'name'               => esc_html__('Slider Revolution', 'litmus'), // The plugin name.
			'slug'               => 'revslider', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/libs/tgm-plugin-activation/plugins/revslider.zip', // The plugin source.
		),  
		array(
		    'name'      => esc_html__('Contact Form 7', 'litmus'),
		    'slug'      => 'contact-form-7', 
		), 
	),
	'theme_id'           => 'litmus',
	'child_theme_source' => esc_url('http://demo.tortoizthemes.com/litmus-demo-content/litmus-child.zip'),
	'has_demo_content'   => true,
);
