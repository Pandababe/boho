<?php
/**
 * Custom conditional tags for this theme.
 *
 * @package Litmus
 */

/* It's license key for King Composer Plugin */
define('KC_LICENSE', 'sgwaz9nr-vhzj-l2zl-0g6v-tktr-btjcerh07xmq');

/* It's to check if plugin active */
define( 'LITMUS_IS_ACTIVE_CF7', in_array( 'contact-form-7/wp-contact-form-7.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'LITMUS_IS_ACTIVE_WC', in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );