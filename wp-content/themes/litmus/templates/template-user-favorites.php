<?php
/**
 * Template Name: User - Favorites
 */
get_header(); ?>
<?php sh_lm_page_header( get_the_title() ); ?>

<div class="tl-main-content pd-tb-90 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="product-user-favourites row item-3">
            	    <?php 
                    if( is_user_logged_in() ) {
            	        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                        $fav_ids = unserialize( get_user_meta( get_current_user_id(), 'sh_lm_user_favorites', true ) );
                        if(!empty($fav_ids)) {
                            $query_args = array(
                               'post_type' => 'product',
                               'paged'     => $paged,
                               'posts_per_page' => 5, //sh_lm_get_customizer_field('product_fav_per_page')
                               'post__in' => $fav_ids, 
                            );
                            $i = 1;
                            $loop = new WP_Query($query_args);
                	        while ( $loop->have_posts() ) : $loop->the_post(); 
                                if(sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') {
                                    require get_template_directory().'/woocommerce/template-parts/content-product.php'; 
                                } else {
                                    require get_template_directory().'/woocommerce/template-parts/content-product-two.php'; 
                                }
                	        endwhile; 
                	        wp_reset_postdata(); ?>
                            <div class="col-md-12">
                                <?php sh_lm_posts_pagination_nav($loop); ?>
                            </div> <!-- /.col-md-12 -->
                            <?php 
                        } else {
                            echo "<div class='alert alert-info'>".esc_html__('You have not added any favorite product yet', 'litmus')."</div>";
                        }
            	    ?>
                    <?php } else { ?> 
                        <p class="alert alert-info"><?php esc_html_e( 'You have to be logged-in to see this page content.', 'litmus' ); ?></p> 
                    <?php } ?>
            	</div><!-- /.row -->
            </div><!-- /.col-md-12 --> 
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.favorite-area-->
<?php get_footer(); ?>