<?php
/**
 * This template for displaying default layout
 *
 * @package Litmus
 */

sh_lm_page_header( esc_html__('Our Latest Story', 'litmus') ); ?>

<!-- Blog Page Content
================================================== -->
<section class="blog-section-content pd-tb-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 order-lg-9 blog-post-ctn-area">
                <?php if( !is_archive() && !is_search() ) : ?>
                <div class="posts-sorting">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-5 col-md-3">
                                    <label><?php esc_html_e('Sort By:', 'litmus'); ?></label>
                                </div><!--  /.col-3 -->
                                <div class="col-7 col-md-4">                                
                                    <select id="post_short_by">
                                        <option value="newest"><?php esc_html_e('Newest', 'litmus'); ?></option>
                                        <option value="popular"><?php esc_html_e('Popular', 'litmus'); ?></option>
                                        <option value="trending"><?php esc_html_e('Treading', 'litmus'); ?></option>
                                    </select>
                                </div><!--  /.col-9 -->
                            </div><!--  /.row -->
                        </div><!--  /.col-6 -->
                        <div class="col-6">
                            <ul class="blog-layout-tab-menu bsm-tabs align-items-center">
                                <li class="bsm-tab">
                                    <a href="#" data-filter="grid"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                                </li>
                                <li class="bsm-tab">
                                    <a href="#" data-filter="list"><i class="fa fa-list" aria-hidden="true"></i></a>
                                </li>
                            </ul><!--  /.blog-layout-tab-menu -->
                        </div><!--  /.col-6 -->
                    </div><!--  /.row -->
                </div><!--  /.posts-sorting -->
                <?php endif; ?>

                <div id="blog-style-grid">
                    <div class="grid-of-blog">    
                        <input type="hidden" class="post-style" value="grid" />  
                        <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>              
                        <input type="hidden" class="current-page" value="<?php echo esc_html($paged); ?>" />                
                        <div class="row ajax-content item-2">
                            <?php 
                            $archive = $cat_id = false;
                            if( is_archive() ) {
                                $taxonomy = get_query_var( 'taxonomy' );
                                if( $taxonomy == 'portfolio-category' || $taxonomy == 'portfolio-tag' ) {
                                    $archive = true;
                                    $queried_object = get_queried_object();
                                    $cat_id = $queried_object->term_id;
                                }
                            } 
                            // the query
                            $args = array(  
                                'post_type' => 'portfolio',  
                                'tax_query' => array(
                                    array (
                                        'taxonomy' => (isset($taxonomy)) ? $taxonomy : '',
                                        'field' => 'id',
                                        'terms' => $cat_id,
                                    )
                                ),   
                            );

                            if($archive) {
                                $the_query = new WP_Query( $args ); 
                                if ( $the_query->have_posts() ) :
                                    while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                        get_template_part( 'template-parts/post/content', get_post_format() );
                                    endwhile;  
                                else :  
                                    get_template_part( 'template-parts/post/content', 'none' ); 
                                endif; 
                            } else { 
                                if ( have_posts() ) :
                                    if (is_search() && get_search_query() == '') {
                                        get_template_part( 'template-parts/post/content', 'none' ); 
                                    } elseif( is_search() ) {
                                        while ( have_posts() ) : the_post(); 
                                        get_template_part( 'template-parts/post/content', 'search' );
                                        endwhile; 
                                    } else {
                                        while ( have_posts() ) : the_post(); 
                                            get_template_part( 'template-parts/post/content', get_post_format() );
                                        endwhile; 
                                    }
                                else :  
                                    get_template_part( 'template-parts/post/content', 'none' ); 
                                endif; 
                            } 
                            
                            if ( !is_search() || get_search_query() != '') { sh_lm_posts_pagination_nav(); } ?>

                        </div><!--  /.row -->
                    </div><!--  /.grid-of-blog -->
                </div>
            </div><!--  /.col-lg-9 -->
            <div class="col-lg-3 order-lg-3">
                <?php get_sidebar(); ?>
            </div><!--  /.col-lg-3 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.blog-section-content -->
    