<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'post' => array(
        'title'   => esc_html__( 'Page Extra Options', 'litmus' ),
        'type'    => 'box',
        'options' => array( 
            'custom_css'   => array(
                'type'  => 'textarea', 
                'label' =>  esc_html__( 'Custom CSS Code', 'litmus' ),
                'desc' => esc_html__( 'Paste your CSS code here without <style></style> tag', 'litmus' ),
            ),
            'custom_js'   => array(
                'type'  => 'textarea', 
                'label' =>  esc_html__( 'Custom JS Code', 'litmus' ),
                'desc' => esc_html__( 'Paste your JS code here without <script></script> tag', 'litmus' ),
            ),
        ),
    ),
);