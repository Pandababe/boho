<?php 


/* Favorite Property */
add_action('wp_ajax_sh_lm_property_favorite', 'sh_lm_property_favorite');
add_action('wp_ajax_nopriv_sh_lm_property_favorite', 'sh_lm_property_favorite');
function sh_lm_property_favorite() {
    $curent_user_id = get_current_user_id(); 
    $data_nonce = isset($_GET['data_nonce']) ? $_GET['data_nonce'] : '';
    $data_type = isset($_GET['data_type']) ? $_GET['data_type'] : '';
    $fav_id = isset($_GET['fav_id']) ? $_GET['fav_id'] : '';

    if ( wp_verify_nonce( $data_nonce, "sh-lm-favorite" ) && is_user_logged_in() ) {
        $fav_ids = unserialize( get_user_meta( $curent_user_id, 'sh_lm_user_favorites', true ) );  
        if( $data_type == 'add' ) {
            if ( !in_array($fav_id, $fav_ids)) {
                $fav_ids[] = $fav_id;
                update_user_meta( $curent_user_id, 'sh_lm_user_favorites', serialize($fav_ids) ); 
            }
        } else {
            if ( in_array($fav_id, $fav_ids)) { 
                $fav_ids = array_diff($fav_ids, array($fav_id));
                update_user_meta( $curent_user_id, 'sh_lm_user_favorites', serialize($fav_ids) ); 
            }
        }
    }    
    die();
}



add_action('wp_ajax_sh_lm_update_registration', 'sh_lm_update_registration');
add_action('wp_ajax_nopriv_sh_lm_update_registration', 'sh_lm_update_registration');
function sh_lm_update_registration() { 
    $tf_token = (isset($_REQUEST['tf_token'])) ? $_REQUEST['tf_token'] : '';
    $tf_purchase_code = (isset($_REQUEST['tf_purchase_code'])) ? $_REQUEST['tf_purchase_code'] : '';

    if( $tf_token && $tf_purchase_code ) {
        $url = 'https://api.envato.com/v3/market/buyer/purchase?code='.$tf_purchase_code; 
        $defaults = array(
            'headers' => array(
                'Authorization' => 'Bearer '.$tf_token,
                'User-Agent' => 'SoftHopper Litmus Theme',
            ),
            'timeout' => 300,
        );

        $theme_info = wp_get_theme();
        $theme_info = ( $theme_info->parent() ) ? $theme_info->parent() : $theme_info;
        $theme_name = $theme_info->get('Name'); 
        $author_name = $theme_info->get('Author'); 

        $verification = false;
        $raw_response = wp_remote_get( $url, $defaults );
        $response = json_decode( $raw_response['body'], true );
        if(isset($response['item']['wordpress_theme_metadata'])) {
            $tf_theme_name = (isset($response['item']['wordpress_theme_metadata']['theme_name'])) ? $response['item']['wordpress_theme_metadata']['theme_name'] : '';
            $tf_author_name = (isset($response['item']['wordpress_theme_metadata']['author_name'])) ? $response['item']['wordpress_theme_metadata']['author_name'] : '';
            if($theme_name == $tf_theme_name && $author_name == $tf_author_name) {
                update_option( 'sh_lm_verification_key', array(
                    'tf_token'         => $tf_token, 
                    'tf_purchase_code' => $tf_purchase_code,
                ) ); 
                $verification = true;
            } 
        } 

        if ($verification == true) {
            echo('true');
        } else {
            echo('false');
        }
    }

    die(); 
}

add_action('wp_ajax_sh_lm_blog_post', 'sh_lm_blog_post');
add_action('wp_ajax_nopriv_sh_lm_blog_post', 'sh_lm_blog_post');
if ( ! function_exists( 'sh_lm_blog_post' ) ) :
    function sh_lm_blog_post() { 
        $post_style = (isset($_REQUEST['post_style'])) ? $_REQUEST['post_style'] : ''; 
        $short_by = (isset($_REQUEST['short_by'])) ? $_REQUEST['short_by'] : ''; 

        $paged = ( isset($_GET['paged']) ? $_GET['paged'] : 1 ); 

        if ( $short_by == 'trending') {
            $the_query = new WP_Query(
                array( 
                   'paged' => $paged,
                   'post_status' => 'publish',
                   'meta_key' => 'sh_lm_post_views_count',
                   'ignore_sticky_posts' => true,
                   'orderby' => 'meta_value_num',
                   'order' => 'DESC',
                   'date_query' => array(
                        array(
                            'after' => "365 day ago"
                        )
                    ),
                ) 
            );
        } else if ( $short_by == 'popular')  {
            $the_query = new WP_Query(
                array( 
                    'paged' => $paged,
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => true,
                    'date_query' => array(
                        array(
                            'after' => "365 day ago"
                        )
                    ),
                    'meta_query'      => array(
                        'relation'    => 'AND',
                        'post_views_count'    => array(
                            'key'     => 'sh_lm_post_views_count',
                            'type'    => 'NUMERIC',
                            'compare' => 'EXISTS',
                        ),
                    ),
                    'orderby' => array(
                        'comment_count' => 'DESC',
                        'post_views_count'    => 'DESC',
                    ),
                ) 
            );
        } else {
            $the_query = new WP_Query(
                array( 
                    'paged' => $paged,
                    'post_status' => 'publish',
                    'post_type' => 'post',
                    'ignore_sticky_posts' => true
                ) 
            );
            
        }  

        if ( $the_query->have_posts() ) :
            $sticky = get_option( 'sticky_posts' );
            if ($paged == 1 && isset($sticky[0])) {
                $sticky_query = new WP_Query(
                    array( 
                        'paged' => $paged,
                        'post_status' => 'publish',
                        'post__in'  => $sticky,
                        'ignore_sticky_posts' => 1
                    ) 
                );
                while ( $sticky_query->have_posts() ) : $sticky_query->the_post();  
                    require get_template_directory().'/template-parts/post/content.php'; 
                endwhile;  
            }
            while ( $the_query->have_posts() ) : $the_query->the_post();  
                require get_template_directory().'/template-parts/post/content.php'; 
            endwhile;  
        else : 
            echo "<p class='no-post-found'>";
            if ( $short_by == 'trending') {
                esc_html_e( 'No Trending Post Found', 'litmus' );
            } else if ( $short_by == 'popular')  {
                esc_html_e( 'No Popular Post Found', 'litmus' );
            } else {
                esc_html_e( 'No Latest Post Found', 'litmus' );                 
            }  
            echo "</p>";
        endif;  
        sh_lm_posts_pagination_nav($the_query, 'ajax-post', true);
    die(); }
endif;

add_action('wp_ajax_sh_lm_portfolio_single', 'sh_lm_portfolio_single');
add_action('wp_ajax_nopriv_sh_lm_portfolio_single', 'sh_lm_portfolio_single');
if ( ! function_exists( 'sh_lm_portfolio_single' ) ) :
    function sh_lm_portfolio_single() { 
        $post_id = (isset($_REQUEST['post_id'])) ? $_REQUEST['post_id'] : ''; ?>
        <div class="container-portfolio">
            <div class="single-content bg-gray">
                <?php 
                // the query
                $args = array(
                  'p'         => $post_id, // ID of a page, post, or custom type
                  'post_type' => 'portfolio'
                );
                $the_query = new WP_Query( $args ); ?> 
                <?php if ( $the_query->have_posts() ) : ?> 
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <article class="single-post">
                    <div class="entry-header"> 
                        <h2 class="entry-title"><?php the_title(); ?></h2>
                    </div><!-- /.entry-header -->

                    <?php if ( has_post_thumbnail() ) { ?>
                    <figure class="post-thumb"> 
                        <?php sh_lm_post_featured_image(850, 500, true); ?> 
                    </figure> <!-- /.post-thumb -->
                    <?php } ?>

                    <div class="signle-post-content"> 
                        <div class="entry-meta clearfix">
                            <div class="entry-date">
                                <div class="meta-title"><?php esc_html_e('Date', 'litmus'); ?></div> 
                                <?php the_time( get_option( 'date_format' ) ); ?>
                            </div>
                            <?php if(sh_lm_fw_post_meta('portfolio_url') != "") { ?>
                            <div class="entry-url">
                                <div class="meta-title"><?php esc_html_e('URL', 'litmus'); ?></div> 
                                <a href="<?php echo esc_url(sh_lm_fw_post_meta('portfolio_url')); ?>"><?php echo esc_url(sh_lm_fw_post_meta('portfolio_url')); ?></a> 
                            </div>
                            <?php } ?>

                            <?php $portfolio_cats = wp_get_post_terms( get_the_ID(), 'portfolio-category' );
                            if(!empty($portfolio_cats)) { ?> 
                            <div class="entry-category">
                                <div class="meta-title"><?php esc_html_e('Category', 'litmus'); ?></div> 
                                <?php foreach ($portfolio_cats as $portfolio_cat) { ?>
                                <a href="<?php echo esc_url(get_term_link($portfolio_cat->slug, 'portfolio-category')); ?>" rel="category tag"><?php echo esc_html($portfolio_cat->name); ?></a>
                                <?php } ?>
                            </div>  
                            <?php } ?>

                            <?php $portfolio_tags = wp_get_post_terms( get_the_ID(), 'portfolio-tag' );
                            if(!empty($portfolio_tags)) { ?> 
                            <div class="entry-tag">
                                <div class="meta-title"><?php esc_html_e('Tag', 'litmus'); ?></div> 
                                <?php foreach ($portfolio_tags as $portfolio_tag) { ?>
                                <a href="<?php echo esc_url(get_term_link($portfolio_tag->slug, 'portfolio-tag')); ?>" rel="category tag"><?php echo esc_html($portfolio_tag->name); ?></a>
                                <?php } ?>
                            </div>  
                            <?php } ?>
                                   
                        </div><!-- /.entry-meta -->

                        <div class="entry-content">
                            <?php the_content(); ?>
                        </div><!-- /.entry-content -->
                            
                        <div class="entry-share">
                            <?php if ( function_exists( 'sh_lm_social_share_link' ) ) {
                                sh_lm_social_share_link( esc_html__('Share:', 'litmus') );
                            } ?>    
                        </div><!-- /.entry-share -->
                    </div><!-- /.post -->                             
                </article><!-- /.single-post -->
                    <?php endwhile; ?> <?php wp_reset_postdata(); ?> 
                <?php endif; ?>
            </div><!-- /.single-main -->  
        </div><!-- /.container -->
    <?php die(); }
endif;

