<?php

function sh_lm_hex_2_rgba($color, $opacity = false) {
    $default = 'rgb(0,0,0)';
    //Return default if no color provided
    if(empty($color))
        return $default; 
 
    //Sanitize $color if "#" is provided 
    if ($color[0] == '#' ) {
        $color = substr( $color, 1 );
    }
 
    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
        $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
        $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
        return $default;
    }
 
    //Convert hexadec to rgb
    $rgb =  array_map('hexdec', $hex);
 
    //Check if opacity is set(rgba or rgb)
    if($opacity){
        if(abs($opacity) > 1)
            $opacity = 1.0;
        $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
    } else {
        $output = 'rgb('.implode(",",$rgb).')';
    }
 
    //Return rgb(a) color string
    return $output;
}

function sh_lm_color() {  
    $setting_color = sh_lm_get_customizer_field('color_scheme', array('id', '1'));
    switch( $setting_color ) {
        case '1':  
            // add a condition to show demo color scheme by url
            ( isset($_GET["color_scheme_color"]) ) ? $color_scheme_color = $_GET["color_scheme_color"]  : $color_scheme_color = "" ;
            if (preg_match('/^[A-Z0-9]{6}$/i', $color_scheme_color)) {
              $demo_color_scheme = $_GET['color_scheme_color'];
            }
            else {
               $demo_color_scheme = "03dedf";
            }
            $sh_lm_color = "#".$demo_color_scheme; 
            break;
        case '2': 
            $sh_lm_color = "#d12a5c";
            break;
        case '3': 
            $sh_lm_color = "#49ca9f";
            break;
        case '4': 
            $sh_lm_color = "#1f1f1f";
            break;
        case '5': 
            $sh_lm_color = "#808080";
            break;
        case '6': 
            $sh_lm_color = "#ebebeb";
            break;
        case 'fw-custom': 
            $sh_lm_color = sh_lm_get_customizer_field('color_scheme', 'color');
            break; 
    }
    //rgba color
    $sh_lm_rgba_color = sh_lm_hex_2_rgba($sh_lm_color, 0.8);
?>
::selection {background: <?php echo esc_attr($sh_lm_color); ?> none repeat scroll 0 0; } *::-moz-selection {background: <?php echo esc_attr($sh_lm_color); ?> none repeat scroll 0 0; } a:hover, a:focus, a:active {color: <?php echo esc_attr($sh_lm_color); ?>; } label a {color: <?php echo esc_attr($sh_lm_color); ?>; } blockquote {border-left: 5px solid <?php echo esc_attr($sh_lm_color); ?>; } .blog-single-page a { color: <?php echo esc_attr($sh_lm_color); ?>;} .blog-single-page a:hover {color: <?php echo esc_attr($sh_lm_color); ?>;} .article-content .page-links > span {border-color: <?php echo esc_attr($sh_lm_color); ?>; color: <?php echo esc_attr($sh_lm_color); ?>; } .blog-layout-tab-menu .bsm-tab a.active {color: <?php echo esc_attr($sh_lm_color); ?> !important; } .post .article-content .read-more:hover {border-color: <?php echo esc_attr($sh_lm_color); ?> !important; color: <?php echo esc_attr($sh_lm_color); ?>;} .comment-respond #submit, comment-respond .submit {background-color: <?php echo esc_attr($sh_lm_color); ?>;} .right-menu li .user-registration {color: <?php echo esc_attr($sh_lm_color); ?> !important;} @media only screen and (min-width: 1199px) {.navigation .mainmenu > li > a:after {border-bottom-color: <?php echo esc_attr($sh_lm_color); ?>; }} .bsm-btn:hover {background-color: <?php echo esc_attr($sh_lm_color); ?> !important; } .owl-nav > div, .portfolio-details-block .portfolio-header .portfolio-cat a, .portfolio-details-block .portfolio-header .portfolio-other .rating i, .breadcrumbs .last-item { color: <?php echo esc_attr($sh_lm_color); ?> !important; } .expand-block .hamburger-content ul li a::after { color: <?php echo esc_attr($sh_lm_color); ?>; } .purchase-btn { border: 2px solid <?php echo esc_attr($sh_lm_color); ?>; color: <?php echo esc_attr($sh_lm_color); ?>; } .policy-container.policy-xs-slider .owl-dots .owl-dot.active {border-color: <?php echo esc_attr($sh_lm_color); ?>; } .overlay-search .search-footer-title span { color: <?php echo esc_attr($sh_lm_color); ?>;
} .contact-form-area .contact-form .btn-contact:hover { border-color: <?php echo esc_attr($sh_lm_color); ?>; color: <?php echo esc_attr($sh_lm_color); ?>; } .login-register-modal .bsm-tabs .bsm-tab a:hover, .login-register-modal .bsm-tabs .bsm-tab a.active { color: <?php echo esc_attr($sh_lm_color); ?>; } input:not([type]):focus:not([readonly]), input[type="text"]:not(.browser-default):focus:not([readonly]), input[type="password"]:not(.browser-default):focus:not([readonly]), input[type="email"]:not(.browser-default):focus:not([readonly]), input[type="url"]:not(.browser-default):focus:not([readonly]), input[type="time"]:not(.browser-default):focus:not([readonly]), input[type="date"]:not(.browser-default):focus:not([readonly]), input[type="datetime"]:not(.browser-default):focus:not([readonly]), input[type="datetime-local"]:not(.browser-default):focus:not([readonly]), input[type="tel"]:not(.browser-default):focus:not([readonly]), input[type="number"]:not(.browser-default):focus:not([readonly]), input[type="search"]:not(.browser-default):focus:not([readonly]), textarea.bsm-textarea:focus:not([readonly]) { border-bottom: 1px solid <?php echo esc_attr($sh_lm_color); ?>;  -webkit-box-shadow: 0 1px 0 0 <?php echo esc_attr($sh_lm_color); ?>; box-shadow: 0 1px 0 0 <?php echo esc_attr($sh_lm_color); ?>;} [type="checkbox"]:checked + label::before {   border-right: 2px solid <?php echo esc_attr($sh_lm_color); ?>; border-bottom: 2px solid <?php echo esc_attr($sh_lm_color); ?>; } .login-register-modal .modal-content {background: <?php echo esc_attr(sh_lm_get_customizer_field('header_style_option','creative2','background_color')); ?>;} .login-register-modal .bsm-tabs { background: <?php echo esc_attr(sh_lm_get_customizer_field('header_style_option','creative2','background_color')); ?>; } .overlay-search { background: <?php echo esc_attr(sh_lm_get_customizer_field('header_style_option','creative2','background_color')); ?>; opacity: 0.9; } .portfolio-details-block .project-demo-btn .project-btn:hover {border-color: <?php echo esc_attr($sh_lm_color); ?>; color: <?php echo esc_attr($sh_lm_color); ?>;}.woo-menu-item.woo-list form.cart .single_add_to_cart_button { border-color: <?php echo esc_attr($sh_lm_color); ?> !important; color: <?php echo esc_attr($sh_lm_color); ?> !important; } .woo-menu-item.woo-list form.cart .wishlist-btn { color: <?php echo esc_attr($sh_lm_color); ?> !important;} .woo-menu-item.woo-list form.cart .wishlist-btn:hover,
.new-arrival-item .wishlist-btn:hover {border-color: <?php echo esc_attr($sh_lm_color); ?> !important;background: <?php echo esc_attr($sh_lm_color); ?> !important;} .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover { background-color: <?php echo esc_attr($sh_lm_color); ?> !important; } form.cart .single_add_to_cart_button {border-color: <?php echo esc_attr($sh_lm_color); ?> !important; color: <?php echo esc_attr($sh_lm_color); ?> !important;}.site-footer a:hover,.site-footer .footer-bottom .copyright-text a, .error-subtitle {color: <?php echo esc_attr($sh_lm_color); ?>;} #review_form input[type="submit"] {background: <?php echo esc_attr($sh_lm_color); ?> !important; } input:not([type]):focus:not([readonly]), input[type=text]:not(.browser-default):focus:not([readonly]), input[type=password]:not(.browser-default):focus:not([readonly]), input[type=email]:not(.browser-default):focus:not([readonly]), input[type=url]:not(.browser-default):focus:not([readonly]), input[type=time]:not(.browser-default):focus:not([readonly]), input[type=date]:not(.browser-default):focus:not([readonly]), input[type=datetime]:not(.browser-default):focus:not([readonly]), input[type=datetime-local]:not(.browser-default):focus:not([readonly]), input[type=tel]:not(.browser-default):focus:not([readonly]), input[type=number]:not(.browser-default):focus:not([readonly]), input[type=search]:not(.browser-default):focus:not([readonly]), textarea.bsm-textarea:focus:not([readonly]) {border-bottom: 1px solid <?php echo esc_attr($sh_lm_color); ?>; -webkit-box-shadow: 0 1px 0 0 <?php echo esc_attr($sh_lm_color); ?>; box-shadow: 0 1px 0 0 <?php echo esc_attr($sh_lm_color); ?>;} .user-cart .budge { background: <?php echo esc_attr($sh_lm_color); ?> !important; }.grid-of-blog .post .read-more, .list-of-blog .post .read-more {background: <?php echo esc_attr($sh_lm_color); ?> !important; } .posts-sorting .bsm-dropdown-content li > a, .posts-sorting .bsm-select-wrapper input.select-dropdown, 
.posts-sorting .bsm-dropdown-content li > span { color: <?php echo esc_attr($sh_lm_color); ?>;} .portfolio-menu.orange-theme .dropdown-menu li a:hover, .portfolio-menu.orange-theme .dropdown-menu li a:focus, .portfolio-menu.orange-theme .dropdown-menu li a:active { background-color: <?php echo esc_attr($sh_lm_color); ?> !important; }.woocommerce .return-to-shop .wc-backward  { background-color: <?php echo esc_attr($sh_lm_color); ?> !important;}.litmus-theme-newsletter-box .newsletter-area .mc-embedded-subscribe-form .mc-embedded-subscribe { background-color: <?php echo esc_attr($sh_lm_color); ?>;
} .login-register-modal .bsm-btn {border-color: <?php echo esc_attr($sh_lm_color); ?>; color: <?php echo esc_attr($sh_lm_color); ?>; }.client-thumb:hover img { border-color: <?php echo esc_attr($sh_lm_color); ?>;
}.entry-title a:hover {color: <?php echo esc_attr($sh_lm_color); ?>;}.woo-menu-item .hover-content .hover-wrap a:hover {color: <?php echo esc_attr($sh_lm_color); ?>;
}.restu-menu-item .menu-price .order-block .order-btn:hover {background-color: <?php echo esc_attr($sh_lm_color); ?>;}.policy-container .thumb-icon,.policy-container .item:hover .policy-title,.progress-mark span.percent,.call-to-content h2 span {color: <?php echo esc_attr($sh_lm_color); ?>;}.service-slider .item:hover {background: <?php echo esc_attr($sh_lm_color); ?>;border: 1px solid <?php echo esc_attr($sh_lm_color); ?>;}.hexagonal:hover .hexagonal-img-container, .hexagonal:hover .hexagonal-img-container:before, .hexagonal:hover .hexagonal-img-container:after {   background-color: <?php echo esc_attr($sh_lm_color); ?>; border-top: 1px solid <?php echo esc_attr($sh_lm_color); ?>; border-bottom: 1px solid <?php echo esc_attr($sh_lm_color); ?>;}.call-to-content .call-to-link {border: 2px solid <?php echo esc_attr($sh_lm_color); ?>;
color: <?php echo esc_attr($sh_lm_color); ?>;} .owl-dots .owl-dot.active {border-color: <?php echo esc_attr($sh_lm_color); ?>;} .dish-tab-title, .dish-tab-nab .nav-item a.active, .resturent-title-border .lit-restaurent-knife, .restu-menu-item .menu-price .order-block .order-btn {color: <?php echo esc_attr($sh_lm_color); ?>;}.color-citrus {color: <?php echo esc_attr($sh_lm_color); ?> !important;}.restu-menu-item .menu-cat:before {
    background: <?php echo esc_attr($sh_lm_color); ?>;}.restu-menu-item .menu-price .order-block .wishlist-btn:hover,.resturent-tab-v1 > li > a.active, .resturent-tab-v1 > li > a:hover { background: <?php echo esc_attr($sh_lm_color); ?> !important; border-color: <?php echo esc_attr($sh_lm_color); ?>;}.newsletter-block-v2 button, .newsletter-block-v2 input[type="button"], .newsletter-block-v2 input[type="reset"], .newsletter-block-v2 input[type="submit"] { border: 2px solid <?php echo esc_attr($sh_lm_color); ?>; background: <?php echo esc_attr($sh_lm_color); ?>; }.reservation-btn {border-color: <?php echo esc_attr($sh_lm_color); ?>;}.reservation-btn:hover { background: <?php echo esc_attr($sh_lm_color); ?> !important;
    border-color: <?php echo esc_attr($sh_lm_color); ?> !important;} .show-all-btn:hover { border-color: <?php echo esc_attr($sh_lm_color); ?> !important;color: <?php echo esc_attr($sh_lm_color); ?> !important; } .post .article-v2 .article-footer .more-link {color: <?php echo esc_attr($sh_lm_color); ?>}
<?php
} // end sh_lm_color function
sh_lm_color(); // here print the function


function sh_lm_backgound_image_cover_bg() {
$sh_lm_page_header = sh_lm_get_customizer_field('page_header_bg_img', array('url', get_template_directory_uri()."/assets/images/client-overlay-bg.jpg" ));
$sh_lm_error_page_cover = sh_lm_get_customizer_field('404_bg_img', array('url', get_template_directory_uri()."/assets/images/404-bg.jpg" ));
$sh_lm_restaurent_banner = sh_lm_get_customizer_field('wc_archive_style_option', 'two', 'header_bg_img', array('url', get_template_directory_uri()."/assets/images/restaurant/banner/banner-2.jpg" ));

?>
.page-header.litmus-page-header { background-image: url(<?php echo esc_attr($sh_lm_page_header); ?>);}.error-page.bg-cover { background-image: url(<?php echo esc_attr($sh_lm_error_page_cover); ?>);}.restaurant-banner.overlay-bg {background-image: url(<?php echo esc_attr($sh_lm_restaurent_banner); ?>);}
.creative-2 {color: <?php echo esc_attr(sh_lm_get_customizer_field('header_style_option','creative2','color')); ?>}
.site-title {
    
}
<?php
}
sh_lm_backgound_image_cover_bg();