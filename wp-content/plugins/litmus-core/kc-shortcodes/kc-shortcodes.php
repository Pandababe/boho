<?php 
/**
 * All KC Custom Functions
 */
if ( LITMUS_PG_IS_ACTIVE_KC ) {  
    require_once LITMUS_PLUGIN_DIR_URL . 'kc-shortcodes/kc-functions.php';
    //Require all PHP files in the /kc-shortcodes/addons directory
    foreach( glob( plugin_dir_path( __FILE__ ) . "addons/*.php") as $file ) {
        require $file; 
    }
}
