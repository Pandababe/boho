<?php
add_shortcode( 'sh_lm_our_client', function($atts, $content = null) {
	extract(shortcode_atts(array(   
		'our_client' => '',  
	), $atts));  
	ob_start(); ?>    
	<div class="company-with-us pd-tb-45 border-top">
	    <div class="litmus-container">        
	        <div class="company-slider owl-carousel">
	        	<?php 
	        	if(!empty($our_client)) {
	        	foreach ($our_client as $client) { 
	        	?>
	            <div class="item">
	                <a href="<?php echo esc_attr($client->url); ?>">
	                    <img src="<?php echo esc_url(sh_lm_get_image_crop_size($client->img, 285, 120)); ?>" alt="<?php echo esc_attr($client->name); ?>" />
	                </a>
	            </div><!--  /.item -->
	            <?php } } ?>
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</div><!--  /.company-with-us -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_client', 105 );
function sh_lm_kc_our_client() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_client' => array(
	                'name' => esc_html__('Our Client', 'litmus'),
	                'description' => esc_html__('Our Client', 'litmus'),
	                'icon' => 'sl-drawar',
	                'category' => 'Litmus',
	                'params' => array(  
	                    array(
	                    	'name'			=> 'our_client',
	                    	'type'			=> 'group',
	                    	'label'			=> esc_html__('Our Client', 'litmus'), 
	                    	'options'		=> array('add_text' => esc_html__('Add new client', 'litmus')),  
	                    	'params' => array(  
	                    		array(
	                    			'name' => 'img',
	                    			'label' => esc_html__('Company Logo', 'litmus'), 
	                    			'type' => 'attach_image',  
	                    		),
	                    		array(
	                    			'name' => 'name',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Company Name', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    		array(
	                    			'name' => 'url',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Company URL', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    	)
	                    ), 
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}