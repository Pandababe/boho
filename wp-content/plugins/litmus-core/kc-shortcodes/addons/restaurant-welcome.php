<?php
add_shortcode( 'sh_lm_restaurant_welcome', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'title' => '', 
		'desc' => '', 
		'name' => '', 
		'sign' => '', 
		'name' => '', 
		'cuisine_title' => '', 
		'cuisine' => '', 
	), $atts));  
	ob_start(); ?>   
	<section class="restaurent-welcome">
	    <div class="litmus-container">
	        <div class="row no-gutters align-items-center">
	            <div class="col-lg-6">
	                <div class="welcome-block clearfix">                    
	                    <h2 class="welcome-title"><?php echo esc_html($title); ?></h2>
	                    <p class="welcome-des">“<?php echo esc_html($desc); ?>”</p>
	                    <div class="owner-area row align-items-center">
	                        <div class="col-6">
	                            <h3 class="owner-name">- <?php echo esc_html($name); ?> -</h3><!--  /.class-owner-name -->
	                        </div><!--  /.float-left -->
	                        <?php if($sign != '') { ?>
	                        <div class="col-6 text-right">
	                            <img src="<?php echo esc_url(sh_lm_get_image_crop_size($sign, 200, 200)); ?>" alt="<?php echo esc_attr($name); ?>" />
	                        </div><!--  /.float-right -->
	                        <?php } ?>
	                    </div><!--  /.owner-area -->
	                </div><!--  /.welcome-block -->

	                <div class="litmus-dish-tab clearfix d-none d-xl-block">
	                    <h2 class="dish-tab-title"><?php echo esc_html($cuisine_title); ?></h2>
	                    <ul class="dish-tab-nab" role="tablist">
	                    	<?php 
	                    	if(!empty($cuisine)) {
	                    	$i = 1;
	                    	foreach ($cuisine as $cuisines) { 
	                    	?>
	                        <li class="nav-item">
	                            <a class="nav-link <?php if($i == 1) echo 'active'; ?>" data-toggle="tab" href="#cuision-item-<?php echo esc_attr($i); ?>" role="tab" aria-selected="false">
	                                <i class="<?php if(isset($cuisines->icon)) echo esc_html($cuisines->icon); ?>"></i>
	                                <span><?php if(isset($cuisines->title)) echo esc_html($cuisines->title); ?></span>
	                            </a>
	                        </li> 
	                        <?php $i++; } } ?>
	                    </ul><!--  /.dish-tab-nab -->
	                </div><!--  /.litmus-dish-tab -->
	            </div><!--  /.col-md-6 -->
	            
	            <div class="col-lg-6 d-none d-xl-block">
	                <div class="tab-content litmus-dish-content">
						<?php 
						if(!empty($cuisine)) {
						$i = 1;
						foreach ($cuisine as $cuisines) { ?>
	                    	<?php if($cuisines->img != '') { ?>
		                    <div class="tab-pane fade <?php if($i == 1) echo 'show active'; ?>" id="cuision-item-<?php echo esc_attr($i); ?>" role="tabpanel"> 
		                        <img src="<?php echo esc_url(sh_lm_get_image_crop_size($cuisines->img, 520, 720)); ?>" alt="<?php esc_attr_e( 'chef image', 'litmus' ); ?>" />
		                    </div>
	                        <?php } ?>
	                    <?php $i++; } } ?>

	                </div>
	            </div><!--  /.col-md-6 -->
	        </div><!--  /.row -->
	    </div><!-- /.container -->
	</section><!-- /.restaurant-welcome -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_restaurant_welcome', 108 );
function sh_lm_kc_restaurant_welcome() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_restaurant_welcome' => array(
	                'name' => esc_html__('Restaurant Welcome', 'litmus'),
	                'description' => esc_html__('Restaurant Welcome Section', 'litmus'),
	                'icon' => 'sl-pin',
	                'category' => 'Litmus',
	                'params' => array(
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'Welcome to Litmus!',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'desc',
	                		'type' => 'textarea',
	                		'label' => esc_html__('Description', 'litmus'),  
	                		'value' => 'Welcome to Litmus’s website. We want to bring feeling best for our customer. Our mission is to make everything from scratch. This means bringing in great product and treating it with the most care and creativity that it deserves.',   
	                	),
	                	array(
	                		'name' => 'name',
	                		'type' => 'text',
	                		'label' => esc_html__('Name', 'litmus'),  
	                		'value' => 'Richard Haelon',  
	                	),
	                	array(
                			'name' => 'sign',
                			'label' => esc_html__('Sign Image', 'litmus'), 
                			'type' => 'attach_image',  
                		),
	                	array(
	                		'name' => 'cuisine_title',
	                		'type' => 'text',
	                		'label' => esc_html__('Cuisine Text', 'litmus'),  
	                		'value' => 'Dish of our cuisine',  
	                	),
	                    array(
	                    	'name'			=> 'cuisine',
	                    	'type'			=> 'group',
	                    	'label'			=> esc_html__('Dish of our cuisine', 'litmus'), 
	                    	'options'		=> array('add_text' => esc_html__('Add new cuisine', 'litmus')),
	                    	// default values when create new group
	                    	'value' => base64_encode( json_encode( array(
	                    		"1" => array(
	                    			"icon" => "lit lit-appetizers", 
	                    			"title" => "Appetizers", 
	                    		),
	                    		"2" => array(
	                    			"icon" => "lit lit-main-dish",
	                    			"title" => "Main Dish",
	                    		),
	                    		"3" => array(
	                    			"icon" => "lit lit-salads",
	                    			"title" => "Salads",
	                    		),
	                    		"4" => array(
	                    			"icon" => "lit lit-desserts",
	                    			"title" => "Desserts",
	                    		),
	                    		"5" => array(
	                    			"icon" => "lit lit-drinks",
	                    			"title" => "Drinks",
	                    		)
	                    	))), 
	                    	'params' => array( 
	                    		array(
	                    			'name' => 'icon',
	                    			'type' => 'icon_picker',
	                    			'label' => esc_html__('Icon', 'litmus'),   
	                    		),
	                    		array(
	                    			'name' => 'title',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Title', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
			                	array(
		                			'name' => 'img',
		                			'label' => esc_html__('Image', 'litmus'), 
		                			'type' => 'attach_image',  
		                		),
	                    	)
	                    )
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}