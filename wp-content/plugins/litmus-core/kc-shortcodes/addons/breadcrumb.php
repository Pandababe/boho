<?php
add_shortcode( 'sh_lm_breadcrumbs_block', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'bg_img' => '',  
		'short_desc' => '',  
		'username' => '',  
		'title' => '', 
	), $atts));  
	ob_start(); ?>     
	<div class="restaurant-banner bg-snow">
	    <div class="container">
	        <div class="row">
	            <div class="col-12"> 
	                <?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
	                    <?php echo fw_ext_get_breadcrumbs('<i class="fa fa-angle-right"></i>'); ?>
	                <?php endif; ?>
	            </div><!--  /.col-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</div><!--  /.restaurant-banner -->  
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_breadcrumbs_block', 129 );
function sh_lm_kc_breadcrumbs_block() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_breadcrumbs_block' => array(
	                'name' => esc_html__('Breadcrumbs', 'litmus'),
	                'description' => esc_html__('Page Breadcrumbs', 'litmus'),
	                'icon' => 'sl-direction',
	                'category' => 'Litmus',
	                'params' => array( ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}