<?php
/*
Plugin Name: WooCommerce Instagram Shop
Plugin URI: http://www.softhopper.net
Description: WooCommerce Instagram Shop plugin allows to create "shoppable" instagram feed page on your woocommerce. Visitors from your instagram will be able to click a link in your profile and land on "shoppable" instagram page on your site, from which they will be able to add to cart your instagram items.
Author: SoftHopper
Author URI: http://www.softhopper.net
Version: 1.0.0
*/
/**
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    if ( ! class_exists( 'WC_Instashop' ) ) {
        /**
         * Localisation
         **/
        load_plugin_textdomain( 'wc_instashop', false, dirname( plugin_basename( __FILE__ ) ) . '/' );

        class WC_Instashop {
            public function __construct() {
                // called only after woocommerce has finished loading
                add_action( 'woocommerce_init', array( &$this, 'woocommerce_loaded' ) );

                // called after all plugins have loaded
                add_action( 'plugins_loaded', array( &$this, 'plugins_loaded' ) );

                // called just before the woocommerce template functions are included
                add_action( 'init', array( &$this, 'include_template_functions' ), 20 );

                /* Add instashop tab to the main WP menu */
                add_action( 'admin_menu', array(&$this, 'register_instashop_menu_page') );

                /* Load translation file */
                add_action('init', array($this, 'wc_instahop_init'));

                // indicates we are running the admin
                if ( is_admin() ) {
                    // ...
                }

                // indicates we are being served over ssl
                if ( is_ssl() ) {
                    // ...
                }

            }

            function wc_instahop_init() {
                $domain = 'wc_instashop';
                $locale = apply_filters('plugin_locale', get_locale(), $domain);
                load_textdomain($domain, WP_LANG_DIR.'/WooCommerce_Instagram_Shop/'.$domain.'-'.$locale.'.mo');
                load_plugin_textdomain($domain, FALSE, dirname(plugin_basename(__FILE__)).'/languages/');
            }

            /**
             * Take care of anything that needs woocommerce to be loaded.
             * For instance, if you need access to the $woocommerce global
             */
            public function woocommerce_loaded() {
                // ...
            }

            /**
             * Take care of anything that needs all plugins to be loaded
             */
            public function plugins_loaded() {
                // ...
            }

            public function register_instashop_menu_page() {
                add_menu_page( 'Instagram Shop', 'Instagram Shop', 'manage_options', 'WooCommerce_Instagram_Shop', 'instashop_render', plugin_dir_url( __FILE__ ).'assets/images/icon.png', 6 );
                if(isset($_REQUEST['instashop_access_token'])) {
                    // Grab userID from token
                    $tempArr = explode('.',$_REQUEST['instashop_access_token']);
                    update_option('instashop_userID', $tempArr[0]);
                    update_option('instashop_access_token', $_REQUEST['instashop_access_token']);
                    if(isset($_REQUEST['instashop_sig']) && !empty($_REQUEST['instashop_sig'])) {
                        update_option('instashop_sig', $_REQUEST['instashop_sig']);
                    }
                }

                function instashop_render() {
                    global $title;
                    global $wpdb;
                    ?><h2><?php echo $title;?></h2><?php
                    if(isset($_REQUEST['processDisconnect']) && $_REQUEST['processDisconnect'] == 1) {
                        update_option('instashop_access_token', "");
                        update_option('instashop_userID', "");
                        update_option('instashop_sig', "");
                        ?>
                        <div class="updated"><p><strong><?php esc_html_e('Instagram account was disconnected.', "wc_instashop" ); ?></strong></p></div>
                        <?php
                    } else {
                        if(isset($_REQUEST['processClearCache']) && $_REQUEST['processClearCache'] == 1) {
                            $files = glob(plugin_dir_path(__FILE__).'cache/*');
                            foreach($files as $file) {
                                if(is_file($file)) {
                                    unlink($file);
                                }
                            }
                            ?>
                            <div class="updated"><p><strong><?php esc_html_e('Cache cleared.', "wc_instashop" ); ?></strong></p></div>
                            <?php
                        } else {
                            if(isset($_POST['processSave']) && $_POST['processSave'] == 1) {
                                update_option('instashop_access_token', $_POST['instashop_access_token']);
                                update_option('instashop_redirect_url', $_POST['instashop_redirect_url']);
                                update_option('instashop_number_of_items', $_POST['instashop_number_of_items']);
                                update_option('instashop_feed_limit', $_POST['instashop_feed_limit']);
                                update_option('instashop_feed_caching', $_POST['instashop_feed_caching']);
                                update_option('instashop_cache_renew', $_POST['instashop_cache_renew']);
                                update_option('instashop_add_to_cart_text', $_POST['instashop_add_to_cart_text']);
                                update_option('instashop_product_details_text', $_POST['instashop_product_details_text']);
                                update_option('instashop_feed_layout', $_POST['instashop_feed_layout']);
                                update_option('instashop_button_layout', $_POST['instashop_button_layout']);
                                update_option('instashop_image_quality', $_POST['instashop_image_quality']);
                                ?>
                                <div class="updated"><p><strong><?php esc_html_e('Options saved.', "wc_instashop" ); ?></strong></p></div>
                                <?php
                            }
                        }
                    }
                    settings_fields('plugin_options');
                    do_settings_sections('WooCommerce_Instagram_Shop');
                    ?>
                    <div class="wrap woocommerce" id="instashop-admin">
                        <form method="post" id="mainform" action="" enctype="multipart/form-data">
                            <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
                                <a href="#" rel="1" class="nav-tab nav-tab1 nav-tab-active"><?php esc_html_e('General Settings', "wc_instashop" ); ?></a>
                                <a href="#" rel="2" class="nav-tab nav-tab2"><?php esc_html_e('Statistics', "wc_instashop" ); ?></a>
                            </h2>
                            <div class="tab tab1">
                                <h3><?php esc_html_e("Instagram Shop Settings", "wc_instashop"); ?></h3>
                                <table class="form-table">
                                <tbody>
                                <?php
                                $access_token = get_option('instashop_access_token');
                                if(!empty($access_token)) {
                                    ?>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                            <a class="disconnect button button-primary"><?php esc_html_e("Disconnect your Instagram account", "wc_instashop"); ?></a>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_access_token"><?php esc_html_e("Access Token:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                            <input name="instashop_access_token" id="instashop_access_token" type="text" value="<?php echo $access_token; ?>" size="60" placeholder="Click the link above to get your Access Token" readonly />
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_redirect_url"><?php esc_html_e("Redirect URI:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                            <input name="instashop_redirect_url" id="instashop_redirect_url" type="text" value="<?php echo get_option('instashop_redirect_url');  ?>" size="60" placeholder="<?php echo esc_html__('Added Redirect URI','wc_instashop') ?>" />
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_number_of_items"><?php esc_html_e("Number of Items:", "wc_instashop"); ?></label>
                                            <img class="help_tip" data-tip="<?php esc_html_e("How many buy-able instashots to show for shortcode.", "wc_instashop"); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png' ); ?>" height="16px" width="16px" />
                                        </th>
                                        <td class="forminp forminp-text">
                                            <select name="instashop_number_of_items" id="instashop_number_of_items" class="wc-enhanced-select">
                                                <?php
                                                for($i = 10; $i <= 100; $i+=10) {
                                                    ?><option value="<?php echo $i; ?>" <?php echo (get_option('instashop_number_of_items')==$i?'selected="selected"':''); ?>><?php echo $i; ?></option><?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_feed_limit"><?php esc_html_e("Instagram Feed Limit:", "wc_instashop"); ?></label>
                                            <img class="help_tip" data-tip="<?php esc_html_e("Script will not fetch Instagram items older than the value selected.", "wc_instashop"); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png' ); ?>" height="16px" width="16px" />
                                        </th>
                                        <td class="forminp forminp-text">
                                            <select name="instashop_feed_limit" id="instashop_feed_limit" class="wc-enhanced-select">
                                                <option value="-1 week" <?php echo (get_option('instashop_feed_limit')=="-1 week"?'selected="selected"':''); ?>><?php esc_html_e("One Week","wc_instashop");?></option>
                                                <option value="-2 weeks" <?php echo (get_option('instashop_feed_limit')=="-2 weeks"?'selected="selected"':''); ?>><?php esc_html_e("Two Weeks","wc_instashop");?></option>
                                                <option value="-1 month" <?php echo (get_option('instashop_feed_limit')=="-1 month"?'selected="selected"':''); ?>><?php esc_html_e("One Month","wc_instashop");?></option>
                                                <option value="-2 month" <?php echo (get_option('instashop_feed_limit')=="-2 month"?'selected="selected"':''); ?>><?php esc_html_e("Two Months","wc_instashop");?></option>
                                                <option value="-3 month" <?php echo (get_option('instashop_feed_limit')=="-3 month"?'selected="selected"':''); ?>><?php esc_html_e("Three Months","wc_instashop");?></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="top" id="show_renew_cache" <?php echo (get_option('instashop_feed_caching')=="no"?'style="display:none;"':''); ?>>
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_cache_renew"><?php esc_html_e("Cache Renew Time:", "wc_instashop"); ?></label>
                                            <img class="help_tip" data-tip="<?php esc_html_e("This option will use WordPress internal cron system.", "wc_instashop"); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png' ); ?>" height="16" width="16" />
                                        </th>
                                        <td class="forminp forminp-select">
                                            <select name="instashop_cache_renew" id="instashop_cache_renew" class="wc-enhanced-select">
                                                <option value="1" <?php echo (get_option('instashop_cache_renew')==1?'selected="selected"':''); ?>><?php esc_html_e("Every Hour", "wc_instashop"); ?></option>
                                                <option value="2" <?php echo (get_option('instashop_cache_renew')==2?'selected="selected"':''); ?>><?php esc_html_e("Every 12 Hours", "wc_instashop"); ?></option>
                                                <option value="3" <?php echo (get_option('instashop_cache_renew')==3?'selected="selected"':''); ?>><?php esc_html_e("Every 24 Hours", "wc_instashop"); ?></option>
                                                <option value="4" <?php echo (get_option('instashop_cache_renew')==4?'selected="selected"':''); ?>><?php esc_html_e("Every 48 Hours", "wc_instashop"); ?></option>
                                                <option value="5" <?php echo (get_option('instashop_cache_renew')==5?'selected="selected"':''); ?>><?php esc_html_e("Every Week", "wc_instashop"); ?></option>
                                            </select>
                                            <a class="clearCache button button-primary"><?php esc_html_e("Clear cache", "wc_instashop"); ?></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                    </table>
                                    <h3><?php esc_html_e("Layout & Design Settings", "wc_instashop"); ?></h3>
                                    <table class="form-table">
                                    <tbody>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_add_to_cart_text"><?php esc_html_e("Add to Cart Button Text:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                            <input name="instashop_add_to_cart_text" id="instashop_add_to_cart_text" type="text" value="<?php echo get_option('instashop_add_to_cart_text'); ?>" />
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_product_details_text"><?php esc_html_e("Product Details Button Text:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                            <input name="instashop_product_details_text" id="instashop_product_details_text" type="text" value="<?php echo get_option('instashop_product_details_text'); ?>" />
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_feed_layout"><?php esc_html_e("Instagram Feed Layout:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-select">
                                            <select name="instashop_feed_layout" id="instashop_feed_layout" class="wc-enhanced-select">
                                                <option value="1" <?php echo (get_option('instashop_feed_layout')==1?'selected="selected"':''); ?>><?php esc_html_e("1 Column", "wc_instashop"); ?></option>
                                                <option value="2" <?php echo (get_option('instashop_feed_layout')==2?'selected="selected"':''); ?>><?php esc_html_e("2 Columns", "wc_instashop"); ?></option>
                                                <option value="3" <?php echo (get_option('instashop_feed_layout')==3?'selected="selected"':''); ?>><?php esc_html_e("3 Columns", "wc_instashop"); ?></option>
                                                <option value="4" <?php echo (get_option('instashop_feed_layout')==4?'selected="selected"':''); ?>><?php esc_html_e("4 Columns", "wc_instashop"); ?></option>
                                                <option value="5" <?php echo (get_option('instashop_feed_layout')==5?'selected="selected"':''); ?>><?php esc_html_e("5 Columns", "wc_instashop"); ?></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_button_layout"><?php esc_html_e("Action Buttons Position:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-select">
                                            <select name="instashop_button_layout" id="instashop_button_layout" class="wc-enhanced-select">
                                                <option value="1" <?php echo (get_option('instashop_button_layout')==1?'selected="selected"':''); ?>><?php esc_html_e("Below Image", "wc_instashop"); ?></option>
                                                <option value="2" <?php echo (get_option('instashop_button_layout')==2?'selected="selected"':''); ?>><?php esc_html_e("On Hover", "wc_instashop"); ?></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label for="instashop_image_quality"><?php esc_html_e("Instagram Image Quality:", "wc_instashop"); ?></label>
                                            <img class="help_tip" data-tip="<?php esc_html_e("Higher image quality may take longer to load", "wc_instashop"); ?>" src="<?php echo plugins_url( 'woocommerce/assets/images/help.png' ); ?>" height="16px" width="16px" />
                                        </th>
                                        <td class="forminp forminp-select">
                                            <select name="instashop_image_quality" id="instashop_image_quality" class="wc-enhanced-select">
                                                <option value="thumbnail" <?php echo (get_option('instashop_image_quality')=="thumbnail"?'selected="selected"':''); ?>><?php esc_html_e("Lowest (150x150)", "wc_instashop"); ?></option>
                                                <option value="low_resolution" <?php echo (get_option('instashop_image_quality')=="low_resolution"?'selected="selected"':''); ?>><?php esc_html_e("Standard (320x320)", "wc_instashop"); ?></option>
                                                <option value="standard_resolution" <?php echo (get_option('instashop_image_quality')=="standard_resolution"?'selected="selected"':''); ?>><?php esc_html_e("High (640x640)", "wc_instashop"); ?></option>
                                                <option value="1080" <?php echo (get_option('instashop_image_quality')=="1080"?'selected="selected"':''); ?>><?php esc_html_e("Highest (1080x1080)", "wc_instashop"); ?></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <?php
                                } else {
                                    ?>                                     
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label><?php esc_html_e("Instagram Client Id:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                            <input name="instashop_cleint_id" id="instashop_cleint_id" type="text" value="" size="60" placeholder="Add Your Instagram Client Id" onkeyup="instaShopAccesGenerator()" />
                                        </td>
                                    </tr>   
                                    <tr valign="top">
                                        <th scope="row" class="titledesc">
                                            <label><?php esc_html_e("Instagram Redirect URI:", "wc_instashop"); ?></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                            <input name="instashop_redirect_uri" id="instashop_redirect_uri" type="text" value="" size="60" placeholder="Add Your Website URL" onkeyup="instaShopAccesGenerator()" />
                                        </td>
                                    </tr>                 
                                    <tr valign="bottom">
                                        <th scope="row" class="titledesc">
                                            <label></label>
                                        </th>
                                        <td class="forminp forminp-text">
                                        	<script>
                                        		function instaShopAccesGenerator() {
                                        		    var x = document.getElementById("instashop_cleint_id").value;
                                        		    var y = document.getElementById("instashop_redirect_uri").value;
                                        		    document.getElementById("instagram_access_generator").href = 'https://instagram.com/oauth/authorize/?client_id='+x+'&redirect_uri='+y+'/instagram/oauth-instagram.php?return_uri=<?php echo admin_url('admin.php?page=WooCommerce_Instagram_Shop'); ?>&response_type=code';
                                        		}
                                        	</script>
                                            <a class="button button-primary" id="instagram_access_generator" href="https://instagram.com/oauth/authorize/?client_id=&redirect_uri=/instagram/oauth-instagram.php?return_uri=<?php echo admin_url('admin.php?page=WooCommerce_Instagram_Shop'); ?>&response_type=code"><?php esc_html_e("Click here to generate your Access Token and User ID", "wc_instashop"); ?></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                                </table>
                                <?php
                                if(!empty($access_token)) {
                                    ?>
                                    <?php submit_button('Save all changes', 'primary','saveAll', TRUE); ?>
                                    <input type="hidden" name="processSave" id="processSave" value="1" />
                                    <input type="hidden" name="processDisconnect" id="processDisconnect" value="" />
                                    <input type="hidden" name="processClearCache" id="processClearCache" value="" />
                                    <input type="hidden" name="instashop_feed_caching" id="instashop_feed_caching" value="yes" />
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="tab tab2">
                                <?php
                                if(!empty($access_token)) {
                                    ?>
                                    <h3><?php esc_html_e("Instagram Shop Visitors", "wc_instashop"); ?></h3>
                                    <table class="form-table visitors-table">
                                        <tr>
                                            <td>
                                                <div><?php echo instashop_getStatistics("instashop_visit", "today"); ?><span><?php esc_html_e("Today", "wc_instashop"); ?></span></div>
                                            </td>
                                            <td>
                                                <div><?php echo instashop_getStatistics("instashop_visit", "month"); ?><span><?php esc_html_e("This Month", "wc_instashop"); ?></span></div>
                                            </td>
                                            <td>
                                                <div><?php echo instashop_getStatistics("instashop_visit", "year"); ?><span><?php esc_html_e("This Year", "wc_instashop"); ?></span></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <h3><?php esc_html_e("Instagram Shop Page Views", "wc_instashop"); ?></h3>
                                    <table class="form-table visitors-table">
                                        <tr>
                                            <td>
                                                <div><?php echo instashop_getStatistics("instashop_view", "today"); ?><span><?php esc_html_e("Today", "wc_instashop"); ?></span></div>
                                            </td>
                                            <td>
                                                <div><?php echo instashop_getStatistics("instashop_view", "month"); ?><span><?php esc_html_e("This Month", "wc_instashop"); ?></span></div>
                                            </td>
                                            <td>
                                                <div><?php echo instashop_getStatistics("instashop_view", "year"); ?><span><?php esc_html_e("This Year", "wc_instashop"); ?></span></div>
                                            </td>
                                        </tr>
                                    </table>
                                    <h3><?php esc_html_e("Hashtags Click-Through Stats", "wc_instashop"); ?></h3>
                                    <table class="form-table hashtag-table">
                                        <thead>
                                        <th><?php esc_html_e("Hashtag", "wc_instashop"); ?></th>
                                        <th><?php esc_html_e("Today", "wc_instashop"); ?></th>
                                        <th><?php esc_html_e("This Month", "wc_instashop"); ?></th>
                                        <th><?php esc_html_e("This Year", "wc_instashop"); ?></th>
                                        </thead>
                                        <tbody>
                                        <?php
                                        // Get all hashtags that were clicked
                                        $sql = "SELECT DISTINCT(post_title) FROM {$wpdb->posts} WHERE post_type = 'instashop_click'";
                                        $results = $wpdb->get_results($sql);
                                        if(!empty($results)) {
                                            foreach($results as $result) {
                                                ?>
                                                <tr>
                                                    <td>#<?php echo $result->post_title; ?></td>
                                                    <td><?php echo instashop_getStatistics("instashop_click", "today", $result->post_title); ?></td>
                                                    <td><?php echo instashop_getStatistics("instashop_click", "month", $result->post_title); ?></td>
                                                    <td><?php echo instashop_getStatistics("instashop_click", "year", $result->post_title); ?></td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <tr>
                                                <td colspan="4" align="center"><?php esc_html_e("No clicks recorded yet", "wc_instashop"); ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <?php
                                } else {
                                    ?>
                                    <p class="notification"><?php esc_html_e("Sorry, no data collected yet. Please come back later!", "wc_instashop"); ?></p>
                                    <?php
                                }
                                ?>
                            </div>
                        </form>
                    </div>
                    <?php
                }
            }

            public function options_update() {
                register_setting($this->plugin_name, $this->plugin_name, array($this, 'validate'));
            }

            public function validate($input) {
                $valid = array();

                $valid['instashop_username'] = (isset($input['instashop_username']) && !empty($input['instashop_username'])) ? $input['instashop_username'] : '';
                $valid['instashop_number_of_items'] = (isset($input['instashop_number_of_items']) && !empty($input['instashop_number_of_items'])) ? $input['instashop_number_of_items'] : '';

                return $valid;
            }


            /**
             * Override any of the template functions from functions file
             * with our own template functions file
             */
            public function include_template_functions() {
                include( 'functions.php' );
            }
        }

        // finally instantiate our plugin class and add it to the set of globals
        $GLOBALS['wc_instashop'] = new WC_Instashop();

        // Add shortcodes
        add_shortcode('cg_instashop', 'display_instashop');

        // Add settings link on plugin page

        $plugin = plugin_basename(__FILE__);

        function instashop_getStatistics($type, $timeframe, $hashtag = false) {
            global $wpdb;
            $searchHash = "";
            if($timeframe == 'today') {
                $dateFormat = "%Y-%m-%d";
                $date = date('Y-m-d', time());
            } elseif($timeframe == 'month') {
                $dateFormat = "%Y-%m";
                $date = date('Y-m', time());
            } elseif($timeframe == 'year') {
                $dateFormat = "%Y";
                $date = date('Y', time());
            }
            if($hashtag) {
                $searchHash = " AND post_title = '".$hashtag."'";
            }
            $sql = "SELECT COUNT(ID) count FROM {$wpdb->posts} WHERE post_type = '".$type."' AND DATE_FORMAT(post_date, '$dateFormat') = '".$date."' $searchHash";
            $result = $wpdb->get_results($sql);
            return $result[0]->count;
        }

        function instashop_get_http_response_code($url) {
            $headers = get_headers($url);
            return substr($headers[0], 9, 3);
        }

        function display_instashop($atts) {
            global $woocommerce;
            $a = shortcode_atts(array(
                'type' => 'buyable'
            ), $atts);
            $userID = get_option('instashop_userID');
            $accessToken = get_option('instashop_access_token');
            $instashop_userID = get_option('instashop_userID');
            $instashop_redirect_URL = get_option('instashop_redirect_url');
            $sig = get_option('instashop_sig');
            $is_proceed = true;
            $jsonData = '';
            if(!empty($userID) && !empty($accessToken)) {
                $url = 'https://api.instagram.com/v1/users/'.$userID.'/media/recent/?access_token='.$accessToken.'&sig='.$sig;
                // Get plugin options to use
                $feed_caching = get_option('instashop_feed_caching');
                $cache_renew = get_option('instashop_cache_renew');
                $num_of_items = get_option('instashop_number_of_items');
                $feed_limit = get_option('instashop_feed_limit');

                $num_of_columns = get_option('instashop_feed_layout');
                $action_button = get_option('instashop_button_layout'); // 1 - under image / 2 - in popup
                $product_button = get_option('instashop_product_details_text'); // text for "product details" link
                $cart_button = get_option('instashop_add_to_cart_text'); // text for "add to cart" link
                $instashop_image_quality = get_option('instashop_image_quality');

                // Cache the results as the instagram API is slow
                $cacheFolder = plugin_dir_path(__FILE__).'cache/';
                if (!file_exists($cacheFolder)) {
                    mkdir($cacheFolder);
                }
                $cache = $cacheFolder.sha1($url).'.json';
                // Specify cache file time difference in seconds, depending on settings
                if($cache_renew == 1) {
                    // Every hour
                    $renew = 60*60;
                } elseif($cache_renew == 2) {
                    // Every 12 hours
                    $renew = 60*60*12;
                } elseif($cache_renew == 3) {
                    // Every 24 hours
                    $renew = 60*60*24;
                } elseif($cache_renew == 4) {
                    // Every 48 hours
                    $renew = 60*60*48;
                } elseif($cache_renew == 5) {
                    // Every Week
                    $renew = 60*60*24*7;
                } else {
                    // Every hour
                    $renew = 60*60;
                }
                if($feed_caching=="yes" && file_exists($cache) && filemtime($cache) > time() - $renew) {
                    // If a cache file exists, and it is newer than hourly difference, use it
                    $jsonData = json_decode(file_get_contents($cache));
                    $jsonDataConcat['data'] = $jsonData->data;
                } else {
                    if(instashop_get_http_response_code($url) != "200") {
                        $is_proceed = false;
                        ?><p><?php esc_html_e("Couldn't contact Instagram API - please check your access token (try disconnecting api from instagram and connecting again)", "wc_instashop")?></p><?php
                    } else {
                        $jsonDataConcat = array('data' => array());
                        while($url !== NULL && instashop_get_http_response_code($url) == "200") {
                            $jsonData = json_decode((file_get_contents($url)));
                            foreach($jsonData->data as $data) {
                                if($data->created_time > strtotime('now '.$feed_limit)) {
                                    array_push($jsonDataConcat['data'], $data);
                                } else {
                                    break 2;
                                }
                            }

                            if(isset($jsonData->pagination->next_url) && !empty($jsonData->pagination->next_url)) {
                                // Generate next url
                                $sigGenURL = $instashop_redirect_URL."/instagram/oauth-instagram.php?action=siggen&accessToken=".$accessToken."&userID=".$instashop_userID."&maxID=".$jsonData->pagination->next_max_id;
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $sigGenURL);
                                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                                curl_setopt($ch, CURLOPT_USERAGENT, "Instagram Shop WP Signature Generator");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                $ch_data = curl_exec($ch);
                                curl_close($ch);
                                if (!empty($ch_data)) {
                                    $url = $ch_data;
                                } else {
                                    $url = NULL;
                                }
                            } else {
                                $url = NULL;
                            }
                        }
                        file_put_contents($cache,json_encode($jsonDataConcat));
                    }
                }
                ob_start();
                ?>
                <div id="instashop_feed" class="woocommerce" style="width: 100%;">
                    <?php
                    if($num_of_columns == 1) {
                        $feedClass = "one-col";
                    } elseif($num_of_columns == 2) {
                        $feedClass = "two-col";
                    } elseif($num_of_columns == 3) {
                        $feedClass = "three-col";
                    } elseif($num_of_columns == 4) {
                        $feedClass = "four-col";
                    } elseif($num_of_columns == 5) {
                        $feedClass = "five-col";
                    } else {
                        $feedClass = "one-col";
                    }
                    ?>
                    <ul class="<?php echo $feedClass; ?>">
                        <?php
                        if($is_proceed) {
                            $count = 0;
                            $exception = array();
                            foreach ($jsonDataConcat['data'] as $key=>$value) {
                                if($count < $num_of_items) {
                                    // Check if this image has tags that match any product's hashtag
                                    if(isset($value->caption->text)) {
                                        preg_match_all('/(?<!\w)#\w+/',$value->caption->text,$hashArr);
                                        $hashArr = str_replace('#','',$hashArr[0]);
                                        if(!empty($hashArr)) {
                                            $argsInsta = array(
                                                'posts_per_page'    => 1,
                                                'post_type'         => 'product',
                                                'post__not_in'      => $exception,
                                                'fields'            => 'ids',
                                                'meta_query'        => array(
                                                    array(
                                                        'key'       => '_instashop_hashtag',
                                                        'value'     => $hashArr,
                                                        'compare'   => 'IN'
                                                    )
                                                )
                                            );
                                            $instashopProducts = get_posts($argsInsta);
                                            if(count($instashopProducts)>0){
                                                foreach($instashopProducts as $productID) {
                                                    $variableProduct = false;
                                                    $exception[] = $productID;
                                                    $productHashtags = get_post_meta($productID, '_instashop_hashtag', false);
                                                    // Match one hash-tag
                                                    $hash = "";
                                                    foreach($hashArr as $hashInsta) {
                                                        if(in_array($hashInsta, $productHashtags)) {
                                                            $hash = $hashInsta;
                                                            break;
                                                        }
                                                    }
                                                    // Get action button
                                                    $action = get_post_custom_values("_instashop_action", $productID);
                                                    if($action[0] == "redirect") {
                                                        $redirect_to = get_post_custom_values("_instashop_link", $productID);
                                                        $redirect_text = get_post_custom_values("_instashop_link_text", $productID);
                                                        $redirect_nw = get_post_custom_values("_instashop_new_window", $productID);
                                                        $link_to = $redirect_to[0];;
                                                        $link_target = ($redirect_nw[0]==1?'target="_blank"':'');
                                                        $link_text = $redirect_text[0];
                                                    } elseif($action[0] == "product") {
                                                        $link_to = get_permalink($productID);
                                                        $link_target = '';
                                                        $link_text = $product_button;
                                                    } elseif($action[0] == "cart") {
                                                        $link_to = $woocommerce->cart->get_cart_url().'?add-to-cart='.$productID;
                                                        $link_target = '';
                                                        $link_text = $cart_button;
                                                        // Check for variations
                                                        $productObject = get_product($productID);
                                                        if($productObject->is_type('variable')) {
                                                            $variableProduct = true;
                                                            $variations = $productObject->get_available_variations();
                                                        }
                                                    }
                                                    if(empty($link_text)) {
                                                        $link_text = "&nbsp;";
                                                    }
                                                    ?>
                                                    <li>
                                                        <?php
                                                        // Variables in the popup
                                                        if($variableProduct && !empty($variations)) {
                                                            ?>
                                                            <div class="instashopVariableOptions overlay_<?php echo $productID; ?>">
                                                                <i class="fa fa-close instashopCloseOverlay"></i>
                                                                <?php
                                                                ?>
                                                                <div class="instashopVarCont">
                                                                    <label><?php esc_html_e("Select options:", "wc_instashop")?></label>
                                                                    <select>
                                                                        <?php
                                                                        foreach ($variations as $key => $varValue) {
                                                                            if($varValue['variation_is_visible'] == 1 && $varValue['variation_is_active'] == 1 && $varValue['is_purchasable'] == 1 && $varValue['is_in_stock'] == 1) {
                                                                                $addToCartLink = "";
                                                                                $addToCartLink = $woocommerce->cart->get_cart_url().'?add-to-cart='.$productID.'&variation_id='.$varValue['variation_id'];
                                                                                foreach ($varValue['attributes'] as $attr_key => $attr_value) {
                                                                                    $addToCartLink .= '&'.$attr_key.'='.$attr_value;
                                                                                }
                                                                                ?>
                                                                                <option value="<?php echo $addToCartLink; ?>">
                                                                                    <?php
                                                                                    $variationNameArr = array();
                                                                                    foreach ($varValue['attributes'] as $attr_key => $attr_value) {
                                                                                        $variationNameArr[] = ucwords(str_replace('-', ' ', $attr_value));
                                                                                    }
                                                                                    echo implode(', ',$variationNameArr);
                                                                                    ?>
                                                                                </option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select><br />
                                                                    <a class="button instashopVariableAddToCartAction"><?php echo $link_text; ?></a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                        if($action_button != 2) {
                                                            ?><a data-instashop-productID="<?php echo $productID; ?>" <?php echo($variableProduct?'class="instashopVariableAddToCart"':''); ?> href="<?php echo $link_to; ?>" <?php echo $link_target; ?> rel="<?php echo $hash; ?>"><?php
                                                        }
                                                        // Image path depending on quality
                                                        if(!empty($instashop_image_quality)) {
                                                            if($instashop_image_quality == "1080") {
                                                                $imageURL = str_replace("s640x640/", "", $value->images->standard_resolution->url);
                                                            } else {
                                                                $imageURL = $value->images->$instashop_image_quality->url;
                                                            }
                                                        } else {
                                                            $imageURL = $value->images->standard_resolution->url;
                                                        }
                                                        ?>
                                                        <img src="<?php echo $imageURL; ?>" alt="<?php echo $value->caption->text; ?>" />
                                                        <?php
                                                        if($action_button != 2) {
                                                            ?></a><?php
                                                        }
                                                        ?>
                                                        <div class="action <?php echo ($action_button == 2?'overlay':'')?>">
                                                            <a data-instashop-productID="<?php echo $productID; ?>" class="button <?php echo($variableProduct?'instashopVariableAddToCart':''); ?>" href="<?php echo $link_to; ?>" <?php echo $link_target; ?> rel="<?php echo $hash; ?>"><?php echo $link_text; ?></a>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    $count++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // Record view
                            $my_view = array(
                                'post_title' => 'instashop view',
                                'post_type' => 'instashop_view',
                            );
                            $my_view_id = wp_insert_post($my_view);
                        }
                        ?>
                    </ul>
                </div>
                <script>
                    jQuery(document).ready(function() {
                        <?php
                        if ($is_proceed && !isset($_COOKIE['instashop_visit'])) {
                            ?>
                            jQuery.ajax({
                                url: "<?php echo plugin_dir_url(__FILE__).'assets/setVisit.php'; ?>"
                            });
                            <?php
                        }
                        ?>
                        jQuery('#instashop_feed li a').click(function() {
                            var hash = jQuery(this).attr('rel');
                            jQuery.ajax({
                                url: "<?php echo plugin_dir_url(__FILE__).'assets/setClick.php'; ?>",
                                data: {hash:hash}
                            });
                        });
                        jQuery('.instashopVariableAddToCart').click(function(e) {
                            e.preventDefault();
                            var instashopProductID = jQuery(this).attr('data-instashop-productID');
                            jQuery('.instashopVariableOptions.overlay_'+instashopProductID).fadeIn('fast');
                        });
                        jQuery('.instashopCloseOverlay').click(function() {
                            jQuery(this).parent().fadeOut('fast');
                        });
                        jQuery('.instashopVariableAddToCartAction').click(function() {
                            var instashopCartLocation = jQuery(this).parent().find('select').val();
                            window.location.href = instashopCartLocation;
                        });
                        <?php
                        if($action_button != 2) {
                            // If there is a link under the image, we have to add a padding for variable pricing overlay
                            ?>
                            var instashopAddedPadding = jQuery('#instashop_feed ul li .action').height()+20; // +20px top/bottom padding
                            jQuery('.instashopVariableOptions').each(function() {
                                jQuery(this).css('paddingBottom', instashopAddedPadding+'px');
                            });
                            <?php
                        }
                        ?>
                    });
                </script>
                <?php
                $output_string=ob_get_contents();
                ob_end_clean();
                return $output_string;
            } else {
                ?><p><?php esc_html_e("Instagram account disconnected. Please login to administration and authenticate through Instagram.", "wc_instashop")?></p><?php
            }
        }
    }
}
?>