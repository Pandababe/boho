<?php
add_shortcode( 'sh_lm_our_expertised', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'our_policy' => '', 
	), $atts));  
	extract($atts);
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>   
	<div class="policy-block">
	    <div class="litmus-container">
			<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">			
		        <div class="policy-container expertized-area policy-xs-slider item-4 row"> 
		        	<?php 
		        	if(!empty($our_policy)) {
		        	foreach ($our_policy as $policy) { 
		        	?>
		            <div class="item col-lg-4">
		                <div class="thumb-icon">
		                	<?php if($policy->imageicon) { ?>
		                	<img class="brand-images" src="<?php echo esc_url(sh_lm_get_image_crop_size($policy->imageicon, 128, 128)); ?>" alt="<?php echo esc_html__('Service Icon','litmus'); ?>" />
		                	<?php } else { ?>
		                    <i class="<?php echo esc_attr($policy->icon); ?>"></i>
		                    <?php } ?>
		                </div><!-- /.thumb-icon -->
		                <div class="policy-desc">
		                    <h3 class="policy-title"><?php echo esc_html($policy->title); ?></h3>
		                    <p class="policy-content"><?php echo esc_html($policy->desc); ?></p>
		                </div><!-- /.work-desc -->
		                <?php 
		                	$linkurl = sh_lm_theme_kc_custom_link($policy->url);
		                	$button_attr = array();
		                	if(  $linkurl["url"] != "" ) {
		                		$button_attr[] = 'href="'. esc_attr($linkurl["url"]) .'"';
		                	} else {
		                		$button_attr[] = 'href="#"';
		                	}
		                	if( $linkurl["target"] != "" ){
		                		$button_attr[] = 'target="'. esc_attr($linkurl["target"]) .'"';
		                	}
		                	if(  $linkurl["title"] != "" ){
		                		$button_attr[] = 'title="'. esc_attr($linkurl["title"]) .'"';
		                	}
		                 ?>
		                <?php 
		                	if($policy->url) { ?>
		                		<a class="bsm-btn btn-creative" <?php echo implode(' ', $button_attr); ?>><?php echo esc_html($linkurl["title"]); ?></a>
		                	<?php }
		                ?>
		            </div><!-- /.col-lg-4 -->
					<?php } } ?>
		        </div><!-- /.row -->
			</div>
	    </div><!-- /.container -->
	</div><!-- /.policy-block -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_expertise_area', 124 );
function sh_lm_kc_our_expertise_area() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_expertised' => array(
	                'name' => esc_html__('Our Expertized Area', 'litmus'),
	                'description' => esc_html__('Our Expertized Area', 'litmus'),
	                'icon' => 'et-megaphone',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array( 
		                    array(
		                    	'name'			=> 'our_policy',
		                    	'type'			=> 'group',
		                    	'label'			=> esc_html__('Our Expertized Area', 'litmus'), 
		                    	'options'		=> array('add_text' => esc_html__('Add new policy', 'litmus')),
		                    	// default values when create new group
		                    	'value' => base64_encode( json_encode( array(
		                    		"1" => array(
		                    			"icon" => "pe-7s-tools", 
		                    			"title" => "AWESOME SUPPORT",
		                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
		                    		),
		                    		"2" => array(
		                    			"icon" => "pe-7s-cup",
		                    			"title" => "FAST DELIVERY",
		                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
		                    		),
		                    		"3" => array(
		                    			"icon" => "pe-7s-light",
		                    			"title" => "CREATIVE DESIGN",
		                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
		                    		)
		                    	))), 
		                    	'params' => array( 
		                    		array(
		                    			'name' => 'icon',
		                    			'type' => 'icon_picker',
		                    			'label' => esc_html__('Icon', 'litmus'),   
		                    		),
		                    		array(
		                    			'name' => 'imageicon',
		                    			'type' => 'attach_image',
		                    			'label' => esc_html__('Or Image Icon', 'litmus'),   
		                    		),
		                    		array(
		                    			'name' => 'title',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Title', 'litmus'),  
		                    			'admin_label' => true,
		                    		),
		                    		array(
		                    			'name' => 'desc',
		                    			'type' => 'textarea',
		                    			'label' => esc_html__('Short Description', 'litmus'),  
		                    		),
		                    		array(
		                    			'name' => 'url',
		                    			'type' => 'link',
		                    			'label' => esc_html__('Link', 'litmus'),  
		                    		),
		                    	)
		                    ),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
				                		"screens" => "any,1024,999,767,479",
				                		'Heading'	=> array(
				                			array(
				                				'property' => 'color',
				                				'label' => esc_html__('Heading Color','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'font-size', 
				                				'label' => esc_html__('Heading Font Size','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'font-weight', 
				                				'label' => esc_html__('Heading Font Weight','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'font-style', 
				                				'label' => esc_html__('Heading Font Style','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'font-family', 
				                				'label' => esc_html__('Heading Font Family','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'text-align', 
				                				'label' => esc_html__('Heading Text Align','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'text-shadow', 
				                				'label' => esc_html__('Heading Text Shadow','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'text-transform', 
				                				'label' => esc_html__('Heading Text Transform','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'text-decoration', 
				                				'label' => esc_html__('Heading Text Decoration','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'line-height', 
				                				'label' => esc_html__('Heading Line Height','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                			array(
				                				'property' => 'letter-spacing', 
				                				'label' => esc_html__('Heading Letter Spacing','litmus'),
				                				'selector' => '.expertized-area .policy-title'
				                			),
				                		),
				                		'Description'	=> array(
				                			array(
				                				'property' => 'color',
				                				'label' => esc_html__('Description Color','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'font-size', 
				                				'label' => esc_html__('Description Font Size','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'font-weight', 
				                				'label' => esc_html__('Description Font Weight','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'font-style', 
				                				'label' => esc_html__('Description Font Style','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'font-family', 
				                				'label' => esc_html__('Description Font Family','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'text-align', 
				                				'label' => esc_html__('Description Text Align','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'text-shadow', 
				                				'label' => esc_html__('Description Text Shadow','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'text-transform', 
				                				'label' => esc_html__('Description Text Transform','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'text-decoration', 
				                				'label' => esc_html__('Description Text Decoration','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'line-height', 
				                				'label' => esc_html__('Description Line Height','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                			array(
				                				'property' => 'letter-spacing', 
				                				'label' => esc_html__('Description Letter Spacing','litmus'),
				                				'selector' => '.expertized-area .policy-content'
				                			),
				                		),
				                		'Button'	=> array(
				                			array(
				                				'property' => 'color',
				                				'label' => esc_html__('Button Color','litmus'),
				                				'selector' => '.expertized-area .btn-creative'
				                			),	                						
				                			array(
				                				'property' => 'color',
				                				'label' => esc_html__('Button Hover Color','litmus'),
				                				'selector' => '.expertized-area .btn-creative:hover'
				                			),	                						
				                			array(
				                				'property' => 'background',
				                				'label' => esc_html__('Button Background Color','litmus'),
				                				'selector' => '.expertized-area .btn-creative'
				                			),
				                			array(
				                				'property' => 'background',
				                				'label' => esc_html__('Button Hover Background','litmus'),
				                				'selector' => '.expertized-area .btn-creative:hover'
				                			),
				                		),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}