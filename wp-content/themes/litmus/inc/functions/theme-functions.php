<?php 
/* This template for theme function */

if ( ! function_exists( 'sh_lm_preloader' ) ) :
    function sh_lm_preloader() { 
        if (sh_lm_get_customizer_field('preloader_option','preloader')) { ?>
        <div id="preloader-wrap" class="preloader-wrap"> 
            <div class="preloader-container text-center">        
                <div class="preloader-logo">
                    <img src="<?php echo sh_lm_get_customizer_field('preloader_option','1','per_img', array('url', get_template_directory_uri()."/assets/images/logo.png" )); ?>" alt="<?php esc_attr_e( 'Preloader', 'litmus' ); ?>" />
                </div><!-- /.preloader-logo -->    
      
                <div class="preloader-wrapper big active">
                  <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                      <div class="circle"></div>
                    </div><div class="gap-patch">
                      <div class="circle"></div>
                    </div><div class="circle-clipper right">
                      <div class="circle"></div>
                    </div>
                  </div>
                </div>
            </div><!-- /.preloader-container -->
        </div><!-- /#preloader-wrap -->
        <?php }
    }
endif; 

if ( ! function_exists( 'sh_lm_get_template_part' ) ) :
function sh_lm_page_header( $title = '') { ?>
    <section class="page-header litmus-page-header overlay-bg-snow <?php if (sh_lm_get_customizer_field('sticky_header')) echo 'pd-t-120-b-90' ?>">
        <div class="container">
            <div class="row"> 
                <?php if ( is_archive() ) {
                    sh_lm_archive_title( '<div class="col-md-12"><h2 class="section-title center-title no-border">', '</h2></div>' );
                } elseif ( is_search() ) { ?>
                    <div class="col-md-12"><h2 class="section-title center-title no-border"><?php printf( '<span>'.esc_html__( 'Search Results for: ', 'litmus' ).'</span>%s', get_search_query() ); ?></h2></div>
                <?php } else { ?>   
                    <?php if(!is_front_page()) { ?>
                    <div class="col-md-12">
                        <?php if( function_exists('fw_ext_breadcrumbs') ) { fw_ext_breadcrumbs('<i class="fa fa-angle-right"></i>'); } ?>
                    </div><!--  /.col-md-12 -->
                    <?php } ?>
                    <div class="col-md-12">
                        <h2 class="section-title center-title no-border"><?php echo esc_html($title); ?></h2>
                    </div><!--  /.col-md-12 -->     
                <?php } ?>
            </div><!--  /.row -->
        </div><!--  /.container -->
    </section><!--  /.page-header -->
<?php }
endif;

if ( ! function_exists( 'sh_lm_product_favorite_btn' ) ) :
function sh_lm_product_favorite_btn($fav_id) {
    $curent_user_id = get_current_user_id(); 
    $fav_ids = unserialize( get_user_meta( $curent_user_id, 'sh_lm_user_favorites', true ) );
    if($fav_ids == null) {
        $fav_ids = array();
    }
    $data_type =  ( !in_array($fav_id, $fav_ids) ) ? "add" : "remove";  
    $fav_class = ( !in_array($fav_id, $fav_ids) ) ? "pe-7s-like" : "pe-7s-like full";   
    if(is_user_logged_in()) {
        $tooltip_title = ( !in_array($fav_id, $fav_ids) ) ? esc_html__("Add To Favorites", 'litmus') : esc_html__("Remove From Favorites", 'litmus');
    } else {
        $tooltip_title = esc_html__("You are not logged in", 'litmus');
    }
    ?>  
    <i class="<?php echo esc_attr($fav_class); ?> property-favorites" data-nonce="<?php echo wp_create_nonce("sh-lm-favorite"); ?>" data-fav-id="<?php echo esc_attr($fav_id); ?>" data-type="<?php echo esc_attr($data_type); ?>" data-placement="bottom" data-toggle="tooltip" data-original-title="<?php echo esc_attr($tooltip_title); ?>"></i>
<?php }
endif;
/**
 * Get page ID by Template Name
 *
 */
if ( ! function_exists( 'sh_lm_page_id_by_tem_name' ) ) {
    function sh_lm_page_id_by_tem_name( $template_name = null ) {
        $template_page_id = null;
        $template_array = get_pages( array (
            'meta_key'   => '_wp_page_template',
            'meta_value' => $template_name
        ) );
        if ( $template_array ) {
            $template_page_id = $template_array[0]->ID;
        }
        return $template_page_id;
    }
}

if ( ! function_exists( 'sh_lm_author_name' ) ) :
    function sh_lm_author_name() {
        global $current_user;
        $name = get_user_meta( get_current_user_id(), 'first_name', true )." ".get_user_meta( get_current_user_id(), 'last_name', true ); 
        if (get_user_meta( get_current_user_id(), 'first_name', true )) {
            echo esc_html($name);  
        } else {
            echo esc_html($current_user->display_name);
        }
    }
endif;

/*
 * Template parts functions
*/
if ( ! function_exists( 'sh_lm_get_template_part' ) ) :
    function sh_lm_get_template_part($part_name = "") {
        $part_style_url = (isset($_GET["{$part_name}_style"])) ? $_GET["{$part_name}_style"] : '';
        switch($part_style_url) { 
            case 'one': 
            case 'two': 
                $part_style = $part_style_url; break;
            default: $part_style = "one"; break;
        }  
        return get_template_part( "template-parts/$part_name/$part_style" );
    }
endif;

/*
 * Get social_link
*/
if ( ! function_exists( 'sh_lm_social_link' ) ) :
    function sh_lm_social_link($social_link = "", $li_tag = false) { 
        if ( $social_link != "" ) {
            if($li_tag != false ) echo '<li class="social-icon">';
            foreach ( $social_link as $value ) { 
                if ($value['icon']['type'] == "icon-font") { ?>
                    <a target="_blank" href="<?php echo esc_url($value['url']); ?>"><i class="<?php echo esc_attr($value['icon']['icon-class']);?>"></i></a>
                    <?php
                } else { ?>
                    <a target="_blank" href="<?php echo esc_url($value['url']); ?>"><img src="<?php  echo esc_url($value['icon']['url']); ?>" alt="<?php esc_attr_e('img icon','litmus' ); ?>" /></a>
                <?php } 
            }
            if($li_tag != false ) echo '</li>';
        } 
    }
endif;

function sh_lm_excerpt_length($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

function sh_lm_string_to_excerpt($text = "", $limit = null) {
    $excerpt = explode(' ', $text, $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

// custom post excerpt with charecter
function sh_lm_custom_post_excerpt($string, $length, $dots = "&hellip;") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

// custom post excerpt with words
function sh_lm_custom_string_limit_words($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if(count($words) > $word_limit)
    array_pop($words);
    return implode(' ', $words);
}

