<?php
add_shortcode( 'sh_lm_our_services', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'title' => '', 
		'our_services' => '', 
	), $atts));  
	ob_start(); ?>   
	<section class="our-service">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-12">
	                <h2 class="section-title"><?php echo esc_html($title); ?></h2>
	            </div><!-- /.col-12 -->
	        </div><!-- /.row -->

	        <div class="row">
	            <div class="col-12">                
	                <div class="service-slider owl-carousel"> 
	                    <?php 
			        	if(!empty($our_services)) {
			        	foreach ($our_services as $services) { 
			        	?>
			            <div class="item">
			                <div class="thumb-icon">
			                    <i class="<?php echo esc_attr($services->icon); ?>"></i>
			                </div><!-- /.thumb-icon -->
			                <div class="service-desc">
			                    <h3 class="service-title"><?php echo esc_html($services->title); ?></h3>
			                    <p class="service-content"><?php echo esc_html($services->desc); ?></p>
			                </div><!-- /.work-desc -->
			            </div><!-- /.item --> 
						<?php } } ?>
	                </div><!-- /.service-slider -->
	            </div><!-- /.col-12 -->
	        </div><!-- /.row -->
	    </div>
	</section><!-- /.our-service -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_services', 99 );
function sh_lm_kc_our_services() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_services' => array(
	                'name' => esc_html__('Our Services', 'litmus'),
	                'description' => esc_html__('Our Services', 'litmus'),
	                'icon' => 'sl-equalizer',
	                'category' => 'Litmus',
	                'params' => array(
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'Our Service',  
	                		'admin_label' => true,
	                	),
	                    array(
	                    	'name'			=> 'our_services',
	                    	'type'			=> 'group',
	                    	'label'			=> esc_html__('Our Services', 'litmus'), 
	                    	'options'		=> array('add_text' => esc_html__('Add new service', 'litmus')), 
	                    	'value' => base64_encode( json_encode( array(
	                    		"1" => array(
	                    			"icon" => "pe-7s-pen", 
	                    			"title" => "Graphic Design",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"2" => array(
	                    			"icon" => "pe-7s-share",
	                    			"title" => "WordPress Theme",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"3" => array(
	                    			"icon" => "pe-7s-phone",
	                    			"title" => "App Mobile",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"4" => array(
	                    			"icon" => "pe-7s-light",
	                    			"title" => "Creative Design",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		)
	                    	))), 
	                    	'params' => array( 
	                    		array(
	                    			'name' => 'icon',
	                    			'type' => 'icon_picker',
	                    			'label' => esc_html__('Icon', 'litmus'),   
	                    		),
	                    		array(
	                    			'name' => 'title',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Title', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    		array(
	                    			'name' => 'desc',
	                    			'type' => 'textarea',
	                    			'label' => esc_html__('Short Description', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    	)
	                    )
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}