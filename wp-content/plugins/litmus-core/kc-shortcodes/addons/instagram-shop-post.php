<?php
add_shortcode( 'sh_lm_insta_feed_shop_slider', function($atts, $content = null) {
	extract(shortcode_atts(array(        
		'products' => '',  
		'image_size' => '',  
	), $atts));  
	ob_start(); ?>  
	<?php 
		extract($atts);
		$master_class = apply_filters( 'kc-el-class', $atts );
	?>
	<?php 
	if(!empty($products)) {
		$product_ids = array();
    	$products = explode(",", $products);  
    	foreach ( $products as $product ) {  
        	$product = explode(":", $product); 
        	$product_ids[] = (int) $product[0];
	 	}  
	?>
	<?php 
	$argsInsta = array(
	    'posts_per_page'    => 10,
	    'post_type'         => 'product',
	    'posts_per_page' => count($product_ids),
	    'post__in' => $product_ids,
	    'meta_query'        => array(
	        array(
	            'key'       => '_instashop_hashtag',
	            'compare'   => 'IN'
	        )
	    ),
	);
	$instashopProducts = get_posts($argsInsta);
	if(count($instashopProducts)>0){ ?>
		<div class="insta-shop-block">
	    	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">    		
		        <div class="row">
		            <div class="col-12">
		            	<?php foreach ($instashopProducts as $key) {?>	            		
			                <div class="row instashop-item">
			                    <div class="col-md-12 col-xl-12">
			                        <a href="<?php echo esc_attr($key->guid); ?>" class="insta-item">
			                        	<?php if($image_size == '255x255') { ?>
											<img src="<?php echo esc_url(sh_lm_get_image_crop_size(get_post_thumbnail_id( $key->ID ), 255, 255, true)); ?>" alt="<?php echo esc_html($key->post_title); ?>" />
			                        	<?php } elseif($image_size == '540x540') {?>
			                        		<img src="<?php echo esc_url(sh_lm_get_image_crop_size(get_post_thumbnail_id( $key->ID ), 540, 540, true)); ?>" alt="<?php echo esc_html($key->post_title); ?>" />
			                        	<?php } elseif($image_size == '255x538') { ?>
			                        		<img src="<?php echo esc_url(sh_lm_get_image_crop_size(get_post_thumbnail_id( $key->ID ), 255, 538, true)); ?>" alt="<?php echo esc_html($key->post_title); ?>" />
			                        	<?php } else { ?>
											<img src="<?php echo esc_url(sh_lm_get_image_crop_size(get_post_thumbnail_id( $key->ID ), 600, 600)); ?>" alt="<?php echo esc_html($key->post_title); ?>" />
			                        	<?php } ?>
			                            <span class="hover-content">
			                                <i class="pe-7s-camera"></i>
			                            </span>
			                        </a>
			                    </div><!--  /.col-md-3 -->
			                </div><!--  /.row -->
		            	<?php } ?>
		            	<?php ?> 
		            </div><!--  /.col-12 -->
		        </div><!--  /.row -->
	    	</div>
		</div><!--  /.insta-shop-block -->
	<?php }
	}?>
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_insta_shop_feed', 127 );
function sh_lm_kc_insta_shop_feed() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_insta_feed_shop_slider' => array(
	                'name' => esc_html__('Instagram Shop Post', 'litmus'),
	                'description' => esc_html__('The instagram Post which you published soon', 'litmus'),
	                'icon' => 'et-documents',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array(
		                	array(
		                	    'type'          => 'autocomplete',
		                	    'label'         => esc_html__('Select Product', 'litmus'),
		                	    'name'          => 'products',
		                	    'description'   => esc_html__('Select the products which you recently publish in Instagram', 'litmus'),
		                	    'options'       => array( 
		                	        'multiple'      => false, 
		                	        'post_type'     => 'product',
		                	    ), 
		                	), 
		                	array(
		                		'name'          => 'image_size',
		                		'label'         => 'Image Size',
		                		'type'          => 'text',
		                		'value'         => '255x255',
		                		'description'   => esc_html__(' Set the image size: "full", "255x538", "255x255" or "540x540"', 'litmus'),
		                	),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
	                					"screens" => "any,1024,999,767,479",
	                					'Hover'	=> array(
	                						array(
	                							'property' => 'color', 
	                							'label' => 'Select Highlighted Text Color', 
	                							'selector' => '.section-title span'
	                						),
	                					),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}