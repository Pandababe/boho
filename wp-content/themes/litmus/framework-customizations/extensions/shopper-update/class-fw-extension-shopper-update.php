<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

class FW_Extension_Shopper_Update extends FW_Extension {

	/**
	 * Transient ID, the length must be 40 characters or less
	 * @var string
	 */
	private $items_details_transient_id = 'fw_ext_sh_lm_update';

	/**
	 * How long to cache server responses
	 * @var int seconds
	 */
	private $transient_expiration = DAY_IN_SECONDS;

	private $api_timeout = 300;


	public function _filter_pre_set_site_transient_update_themes( $updates ) {

		if ( isset( $updates->checked ) ) { 
			$theme_info = wp_get_theme();
			$theme_info = ( $theme_info->parent() ) ? $theme_info->parent() : $theme_info; 
            $theme_name = $theme_info->get('Name'); 
            $author_name = $theme_info->get('Author'); 
            $theme_version = $theme_info->get('Version'); 

            $sh_lm_options = get_option( 'sh_lm_verification_key' );
            $registration_complete = false;
            $tf_token = isset( $sh_lm_options[ 'tf_token' ] ) ? $sh_lm_options[ 'tf_token' ] : '';
            $tf_api = isset( $sh_lm_options[ 'tf_api' ] ) ? $sh_lm_options[ 'tf_api' ] : '';
            $tf_purchase_code = isset( $sh_lm_options[ 'tf_purchase_code' ] ) ? $sh_lm_options[ 'tf_purchase_code' ] : '';
            if ( $tf_token != ""  && $tf_purchase_code != "" ) {
                $registration_complete = true;
            }
            
            if( $registration_complete ) {
                $url_download = 'https://api.envato.com/v3/market/buyer/download?purchase_code='.$tf_purchase_code; 
                $url_for_meta = 'https://api.envato.com/v3/market/buyer/purchase?code='.$tf_purchase_code; 
                $defaults = array(
                    'headers' => array(
                        'Authorization' => 'Bearer '.$tf_token,
                        'User-Agent' => 'SoftHopper Litmus Theme',
                    ),
                    'timeout' => 300,
                ); 

                $raw_response_url = wp_remote_get( $url_download, $defaults );
                $raw_response_meta = wp_remote_get( $url_for_meta, $defaults );
                $response_meta = json_decode( $raw_response_meta['body'], true );
                $response_url = json_decode( $raw_response_url['body'], true );
                $download_url = (isset($response_url['wordpress_theme'])) ? $response_url['wordpress_theme'] : '';

                if(isset($response_meta['item']['wordpress_theme_metadata'])) {
                    $tf_theme_name = (isset($response_meta['item']['wordpress_theme_metadata']['theme_name'])) ? $response_meta['item']['wordpress_theme_metadata']['theme_name'] : '';
                    $tf_author_name = (isset($response_meta['item']['wordpress_theme_metadata']['author_name'])) ? $response_meta['item']['wordpress_theme_metadata']['author_name'] : '';
                    $tf_theme_version = (isset($response_meta['item']['wordpress_theme_metadata']['version'])) ? $response_meta['item']['wordpress_theme_metadata']['version'] : ''; 
                    $tf_theme_url = (isset($response_meta['item']['url'])) ? $response_meta['item']['url'] : '';  
                } 

                if($theme_name == $tf_theme_name && $author_name == $tf_author_name) {
                    if( version_compare($tf_theme_version, $theme_version) === 1) {
                        //remove all persistent-notices
                        fw_set_db_extension_data( $this->get_name(), 'persistent-notices', array() );
                        $update  = array(
                            "url"         => $tf_theme_url,
                            "new_version" => $tf_theme_version,
                            "package"     => $download_url
                        );
                        $updates->response[ $theme_info->get_stylesheet() ] = $update;
                    }
                } 
            }
		}
		return $updates;
	}

	/**
	 *  increase timeout for api request
	 *
	 * @param $request
	 *
	 * @return mixed
	 */
	public function _filter_http_timeout( $request ) {
		$request["timeout"] = $this->api_timeout;

		return $request;
	}

	/**
	 * @internal
	 */
	protected function _init() {

		if ( ! current_user_can( 'update_themes' ) ) {
			return;
		}

		$this->add_filters();
		$this->add_actions();
	}

	private function add_filters() {
		add_filter( 'pre_set_site_transient_update_themes', array(
			$this,
			'_filter_pre_set_site_transient_update_themes'
		) );
	}

	private function add_actions() {
		add_action( 'admin_notices', array( $this, '_action_admin_notices' ) );
		add_action( 'wp_ajax_fw_ext_sh_lm_update_dismiss_notice', array(
			$this,
			'_action_wp_ajax_fw_ext_sh_lm_update_dismiss_notice'
		) );
		add_action( 'admin_enqueue_scripts', array( $this, '_action_admin_enqueue_scripts' ) ); 
	}

	public function _action_admin_enqueue_scripts() {
		wp_enqueue_script(
			'fw-extension-' . $this->get_name() . '-scripts',
			$this->get_uri( '/static/js/scripts.js' ),
			array( 'jquery' ),
			$this->manifest->get_version()
		);
	}

	/*
	 * Type of notices: info, error, success, warning
	 */
	public function _action_admin_notices() {
		// todo: temporary is disabled the licence key
		/*$notices = fw_get_db_extension_data( $this->get_name(), 'persistent-notices', array() );

		$update_licence_key = fw_get_db_settings_option( apply_filters( 'fw_ext_sh_lm_update_licence_key_option_name', $this->licence_key_option_name ), false );
		{ //Notification of the need to add a license key to receive updates.

			if ( empty( $update_licence_key ) ) {
				$notices['need_licence_key'] = array(
					'dismissed' => ! empty( $notices['need_licence_key']['dismissed'] ),
					'data'      => array(
						'message' => '<strong>' . sprintf( esc_html__( 'It needs to fill Licence key field from <a href="%s">Theme Settings</a> page, that you received by email so you can receive updates.', 'the-core' ), admin_url( 'themes.php?page=fw-settings' ) ) . '</strong>',
						'type'    => 'warning'
					)
				);
			} elseif ( ! empty( $notices['need_licence_key'] ) && $notices['need_licence_key']['dismissed'] !== true ) {
				unset( $notices['need_licence_key'] );
			}
		}

		foreach ( $notices as $id => $notice ) {
			if ( $notice['dismissed'] ) {
				continue;
			}
			echo '<div id="' . $id . '" data-source="fw-litmus-update" class="notice notice-' . $notice['data']['type'] . ' is-dismissible"><p>' . $notice['data']['message'] . '</p></div>';
		}

		fw_set_db_extension_data( $this->get_name(), 'persistent-notices', $notices );*/
	}


	public function _action_wp_ajax_fw_ext_sh_lm_update_dismiss_notice() {
		if ( ! current_user_can( 'update_themes' ) || empty( $_POST['notice_id'] ) ) {
			die( '0' );
		}

		$id = $_POST['notice_id'];

		$notices = fw_get_db_extension_data( $this->get_name(), 'persistent-notices', array() );

		if ( in_array( $id, array_keys( $notices ) ) ) {
			$notices[ $id ]['dismissed'] = true;
			//No need to store message in database
			unset( $notices[ $id ]['data']['message'] );
		}
		fw_set_db_extension_data( $this->get_name(), 'persistent-notices', $notices );
		wp_send_json_success( $notices );
	}
}
