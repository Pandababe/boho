<?php
/*
    Plugin Name: Litmus Core
    Plugin URI: http://tortoizthemes.com/plugins/litmus-core
    Description: This plugin contains essential data for litmus WordPress theme, To use Litmus theme properly you must install this plugin.
    Author: Tortoizthemes
    Version: 1.1
    Author URI: http://tortoizthemes.com/
*/

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$theme = wp_get_theme(); // gets the current theme
if ( 'Litmus' == $theme->name || 'Litmus' == $theme->parent_theme ) :

	define( 'LITMUS_PLUGIN_DIR_URL', plugin_dir_path( __FILE__ ) );
	define( 'LITMUS_PG_IS_ACTIVE_KC', in_array( 'kingcomposer/kingcomposer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );

	require_once LITMUS_PLUGIN_DIR_URL . 'functions.php';
	require_once LITMUS_PLUGIN_DIR_URL . 'kc-shortcodes/kc-shortcodes.php';
	require_once LITMUS_PLUGIN_DIR_URL . 'custom-post.php';

endif; //LITMUS_Master

