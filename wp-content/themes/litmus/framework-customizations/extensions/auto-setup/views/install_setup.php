<?php if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' ); ?>
<div class="wrap about-wrap shopper-wrap">
<?php 
$sh_lm_options = get_option( 'sh_lm_verification_key' );
$registration_complete = false;
$tf_token = isset( $sh_lm_options[ 'tf_token' ] ) ? $sh_lm_options[ 'tf_token' ] : '';
$tf_api = isset( $sh_lm_options[ 'tf_api' ] ) ? $sh_lm_options[ 'tf_api' ] : '';
$tf_purchase_code = isset( $sh_lm_options[ 'tf_purchase_code' ] ) ? $sh_lm_options[ 'tf_purchase_code' ] : '';
if ( $tf_token !== ""  && $tf_purchase_code !== "" ) {
    $registration_complete = true;
}
?>
    <h1><?php esc_html_e( "Welcome to Litmus!", "litmus" ); ?></h1>
    <div class="about-text"><?php esc_html_e( 'Litmus is now installed and ready to use! Get ready to build something beautiful. Please register your purchase to get support and automatic theme updates. Read below for additional information. We hope you enjoy it!', 'litmus' ); ?></div>
    <h2 class="nav-tab-wrapper">
        <?php
        $active_tab = (isset( $_GET['tab'] )) ? $_GET['tab'] : 'registration'; 
        $registration_tab = ($active_tab == 'registration') ? 'nav-tab-active' : '';
        $support_tab = ($active_tab == 'support') ? 'nav-tab-active' : '';
        $auto_setup_tab = ($active_tab == 'auto_setup') ? 'nav-tab-active' : '';
        $system_status_tab = ($active_tab == 'system_status') ? 'nav-tab-active' : '';

        printf( '<a href="%s" class="nav-tab %s">%s</a>', admin_url( 'admin.php?page=sh_lm_welcome&tab=registration'), $registration_tab,  esc_html__( "Product Registration", "litmus" ) );
        printf( '<a href="%s" class="nav-tab %s">%s</a>', admin_url( 'admin.php?page=sh_lm_welcome&tab=support'), $support_tab,  esc_html__( "Support", "litmus" ) );
        printf( '<a href="%s" class="nav-tab %s">%s</a>', admin_url( 'admin.php?page=sh_lm_welcome&tab=auto_setup'), $auto_setup_tab,  esc_html__( "Auto Setup", "litmus" ) );
        printf( '<a href="%s" class="nav-tab %s">%s</a>', admin_url( 'admin.php?page=sh_lm_welcome&tab=system_status'), $system_status_tab,  esc_html__( "System Status", "litmus" ) );
        if ( defined( 'FW' ) ) : 
        printf( '<a href="%s" class="nav-tab %s">%s</a>', admin_url( 'themes.php?page=fw-settings'), '',  esc_html__( "Theme Settings", "litmus" ) );
        endif;
        ?>
    </h2>
    <?php if($active_tab == 'registration') { ?>
        <p class="registration-notice"><?php esc_html_e( "Your purchase must be registered to receive theme support, auto setup & auto updates. Please follow the step.", "litmus" ); ?></p>
        <div class="litmus-registration-steps">
            <div class="feature-section col three-col">
                <div class="col">
                    <h3><?php esc_html_e( "Step 1 - Generate a ThemeForest token", "litmus" ); ?></h3>
                    <p><a href="<?php echo esc_url('https://build.envato.com/create-token/?purchase:download=t&purchase:verify=t&purchase:list=t'); ?>" target="_blank"><?php esc_html_e( "Click here", "litmus" ); ?></a> <?php esc_html_e( "To get token from ThemeForest. View a tutorial", "litmus" ); ?> <a href="<?php echo esc_url('https://www.youtube.com/watch?v=vjP0glIIjiA'); ?>" target="_blank"><?php esc_html_e( "here", "litmus" ); ?></a><?php esc_html_e( ". This gives you access to get theme auto updates", "litmus" ); ?></p>
                </div>

                <div class="col">
                    <h3><?php esc_html_e( "Step 2 - Get ThemeForest purchase code", "litmus" ); ?></h3> 
                    <p><?php esc_html_e( "After gatting token you need to get purchase code. ", "litmus" ); ?><a href="<?php echo esc_url('https://themeforest.net/downloads'); ?>" target="_blank"><?php esc_html_e( "Click here", "litmus" ); ?></a> <?php esc_html_e( "to get purchase code from ThemeForest. View a tutorial", "litmus" ); ?> <a href="<?php echo esc_url('https://www.youtube.com/watch?v=JxA5j9PuBRc'); ?>" target="_blank"><?php esc_html_e( "here", "litmus" ); ?></a><?php esc_html_e( ". This will verify your purchase item", "litmus" ); ?></p>
                </div>
                <div class="col last-feature">
                    <h3><?php esc_html_e( "Step 3 - Verify purchase", "litmus" ); ?></h3>
                    <p><?php esc_html_e( "Enter your ThemeForest token and purchase code into the fields below. This will give you access to automatic theme updates.", "litmus" ); ?></p>
                </div>
            </div> 
        </div>
        <div class="feature-section">
            <div class="litmus-important-notice registration-form-container">
                <?php 
                if ( $registration_complete ) {
                    echo '<p class="about-description"><span class="dashicons dashicons-yes litmus-icon-key"></span>' . esc_html__("Registration Complete! You can now receive Theme Support, Auto Setup, Auto Updates and future goodies.", "litmus") . '</p>';
                } else { ?>
                    <p class="about-description-faild-msg"><?php esc_html_e( "Sorry, Your given information not match. Please try agian", "litmus" ); ?></p> 
                    <p class="about-description-success-before"><?php esc_html_e( "After Steps 1-2 are complete, enter your credentials below to complete product registration.", "litmus" ); ?></p> 
                    <p class="about-description-success"><span class="dashicons dashicons-yes avada-icon-key"></span><?php esc_html_e('Registration Complete! Thank you for registering your purchase, you can now receive automatic updates, theme support and future goodies.', 'litmus'); ?></p>
                    <div class="litmus-registration-form">
                        <form id="sh_lm_product_registration">
                            <input type="hidden" name="action" value="sh_lm_update_registration" />
                            <input 
                                type="text" 
                                required="required" 
                                name="tf_token" 
                                value="<?php echo esc_attr($tf_token); ?>"
                                placeholder="<?php esc_html_e( "Themeforest Token", "litmus" ); ?>" />
                            <input 
                                type="text" 
                                required="required" 
                                name="tf_purchase_code" 
                                value="<?php echo esc_attr($tf_purchase_code); ?>"
                                placeholder="<?php esc_html_e( "Enter Themeforest Purchase Code", "litmus" ); ?>" />
                            <button type="submit" class="button button-large button-primary litmus-large-button"><?php esc_html_e( "Submit", "litmus" ); ?></button>
                            <span class="litmus-loader"><i class="dashicons dashicons-update loader-icon"></i><span></span></span>
                        </form>
                    </div> 
                <?php } ?> 
            </div>
        </div>
    <?php } elseif($active_tab == 'support') { ?>
    <div class="litmus-support-section">
        <div class="litmus-important-notice">
            <?php if ( !$registration_complete ) { ?> 
            <p class="about-description"><?php esc_html_e( 'To access our support forum and resources, you first must register your purchase.', 'litmus' ); ?></p>
            <p class="about-description"><?php printf( __( 'See the <a href="%s">Product Registration</a> tab for instructions on how to complete registration.', 'litmus' ), admin_url('admin.php?page=sh_lm_welcome&tab=registration') ); ?></p>
            <?php } ?>
        </div>
        <div class="feature-section col three-col">
            <div class="col">
                <h3><span class="dashicons dashicons-sos"></span><?php esc_html_e( "Submit A Ticket", "litmus" ); ?></h3>
                <p><?php esc_html_e( "We offer excellent support through our advanced ticket system. Make sure to register your purchase first to access our support services and other resources.", "litmus" ); ?></p>
                <a href="<?php echo esc_url('http://softhopper.net/support') ?>" class="button button-large button-primary litmus-large-button" target="_blank"><?php esc_html_e( 'Submit a ticket', 'litmus' ); ?></a>
            </div>
            <div class="col">
                <h3><span class="dashicons dashicons-book"></span><?php esc_html_e( "Documentation", "litmus" ); ?></h3>
                <p><?php esc_html_e( "This is the place to go to reference different aspects of the theme. Our online documentaiton is an incredible resource for learning the ins and outs of using Litmus.", "litmus" ); ?></p>
                <a href="<?php echo esc_url('#') ?>" class="button button-large button-primary litmus-large-button" target="_blank"><?php esc_html_e( 'Documentation', 'litmus' ); ?></a>
            </div>
            <div class="col last-feature">
                <h3><span class="dashicons dashicons-portfolio"></span><?php esc_html_e( "Knowledgebase", "litmus" ); ?></h3>
                <p><?php esc_html_e( "Our knowledgebase contains additional content that is not inside of our documentation. This information is more specific and unique to various versions or aspects of Litmus.", "litmus" ); ?></p>
                <a href="<?php echo esc_url('#') ?>" class="button button-large button-primary litmus-large-button" target="_blank"><?php esc_html_e( 'Knowledgebase', 'litmus' ); ?></a>
            </div>
        </div> 
    </div>
    <?php } elseif($active_tab == 'auto_setup') { ?>
    <p class="sub-header"><?php esc_html_e( 'Choose one of the install methods below.', 'litmus' ) ?></p>
    <br/>
    <?php if($has_demo_content): ?>
    <!-- START INSTALL PLUGINS AND DEMO CONTENT -->
    <div class="postbox auto-setup-box plugins-and-demo">
        <div class="header hndle">
            <h3><span><?php esc_html_e( 'Plugins & Demo Content', 'litmus' ) ?></span></h3>
        </div>
        <div class="content">

            <p>
                <?php echo($messages['plugins_and_demo']); ?>
            </p>
            <ul>
                <li>
                    <div class="dashicons dashicons-yes"></div>
                    <span><?php esc_html_e( 'Unyson Framework', 'litmus' ) ?></span></li>
                <?php foreach ( $plugins_list as $plugin_name ): ?>
                    <li>
                        <div class="dashicons dashicons-yes"></div>
                        <span><?php printf( esc_html__( '%s Plugin', 'litmus' ), $plugin_name ); ?></span></li>
                <?php endforeach; ?>
                <li>
                    <div class="dashicons dashicons-yes"></div>
                    <span><?php esc_html_e( 'Demo Content', 'litmus' ) ?></span></li>
            </ul>
        </div>
        <div class="actions">
            <a class="button button-primary"
               href="<?php echo($import_demo_content_url); ?>"><?php esc_html_e( 'Install Plugins & Demo Content', 'litmus' ) ?></a>

        </div>
    </div>
    <!-- END INSTALL PLUGINS AND DEMO CONTENT -->
    <?php endif; ?>
    <!-- START INSTALL PLUGINS ONLY CONTENT -->
    <div class="postbox auto-setup-box plugins-only">
        <div class="header hndle">
            <h3><span><?php esc_html_e( 'Plugins Only', 'litmus' ) ?></span></h3>
        </div>
        <div class="content">

            <p>
                <?php echo($messages['plugins_only']); ?>
            </p>
            <ul>
                <li>
                    <div class="dashicons dashicons-yes"></div>
                    <span><?php esc_html_e( 'Unyson Framework', 'litmus' ) ?></span></li>
                <?php foreach ( $plugins_list as $plugin_name ): ?>
                    <li>
                        <div class="dashicons dashicons-yes"></div>
                        <span><?php printf( esc_html__( '%s Plugin', 'litmus' ), $plugin_name ); ?></span></li>
                <?php endforeach; ?>
                <li>
                    <div class="dashicons dashicons-no-alt"></div>
                    <span><?php esc_html_e( 'Demo Content', 'litmus' ) ?></span></li>
            </ul>
        </div>
        <div class="actions">
            <a class="button button-primary"
               href="<?php echo($install_dependencies_url); ?>"><?php esc_html_e( 'Install Plugins Only', 'litmus' ) ?></a>

        </div>
    </div>
    <!-- END INSTALL PLUGINS ONLY CONTENT -->

    <!-- START SKIP AUTO SETUP -->
    <div class="postbox auto-setup-box skip-auto-setup">
        <div class="header hndle">
            <h3><span><?php esc_html_e( 'Skip Auto Setup', 'litmus' ) ?></span></h3>
        </div>
        <div class="content">

            <p>
                <?php echo($messages['skip_auto_install']); ?>
            </p>
            <ul>
                <li>
                    <div class="dashicons dashicons-no-alt"></div>
                    <span><?php esc_html_e( 'Unyson Framework', 'litmus' ) ?></span></li>
                <?php foreach ( $plugins_list as $plugin_name ): ?>
                    <li>
                        <div class="dashicons dashicons-no-alt"></div>
                        <span><?php printf( esc_html__( '%s Plugin', 'litmus' ), $plugin_name ); ?></span></li>
                <?php endforeach; ?>
                <li>
                    <div class="dashicons dashicons-no-alt"></div>
                    <span><?php esc_html_e( 'Demo Content', 'litmus' ) ?></span></li>
            </ul>
        </div>
        <div class="actions">
            <a class="button button-secondary"
               href="<?php echo($skip_auto_install_url); ?>"><?php esc_html_e( 'Skip Auto Setup', 'litmus' ) ?></a>

        </div>
    </div>
    <!-- END SKIP AUTO SETUP -->
    <?php } elseif ($active_tab == 'system_status') { ?>
        <br />
        <br />
        <?php 
        class sh_system_status {
            static $system_status = array();
            static function add($section, $status_array) {
                self::$system_status[$section] []= $status_array;
            }
            static function render_tables() {
                foreach (self::$system_status as $section_name => $section_statuses) {
                    ?>
                    <table class="widefat sh-system-status-table" cellspacing="0">
                        <thead>
                            <tr>
                               <th colspan="4"><?php echo esc_html($section_name); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php

                        foreach ($section_statuses as $status_params) {
                            ?>
                            <tr>
                                <td class="sh-system-status-name"><?php echo esc_html($status_params['check_name']); ?></td>
                                <td class="sh-system-status-help"><!--<a href="#" class="help_tip">[?]</a>--></td>
                                <td class="sh-system-status-status">
                                    <?php
                                        switch ($status_params['status']) {
                                            case 'green':
                                                echo '<div class="sh-system-status-led sh-system-status-green sh-tooltip" data-position="right" title="' . $status_params['tooltip'] . '"></div>';
                                                break;
                                            case 'yellow':
                                                echo '<div class="sh-system-status-led sh-system-status-yellow sh-tooltip" data-position="right" title="' . $status_params['tooltip'] . '"></div>';
                                                break;
                                            case 'red' :
                                                echo '<div class="sh-system-status-led sh-system-status-red sh-tooltip" data-position="right" title="' . $status_params['tooltip'] . '"></div>';
                                                break;
                                            case 'info':
                                                echo '<div class="sh-system-status-led sh-system-status-info sh-tooltip" data-position="right" title="' . $status_params['tooltip'] . '">i</div>';
                                                break;

                                        }


                                    ?>
                                </td>
                                <td class="sh-system-status-value"><?php echo($status_params['value']); ?></td>
                            </tr>
                            <?php
                        }

                    ?>
                        </tbody>
                    </table>
                    <?php
                }
            }

            static function wp_memory_notation_to_number( $size ) {
                $l   = substr( $size, -1 );
                $ret = substr( $size, 0, -1 );
                switch ( strtoupper( $l ) ) {
                    case 'P':
                        $ret *= 1024;
                    case 'T':
                        $ret *= 1024;
                    case 'G':
                        $ret *= 1024;
                    case 'M':
                        $ret *= 1024;
                    case 'K':
                        $ret *= 1024;
                }
                return $ret;
            }
        } 

        /*  ----------------------------------------------------------------------------
            Theme config
         */
        $theme_info = wp_get_theme();
        $theme_info = ( $theme_info->parent() ) ? $theme_info->parent() : $theme_info; 
        $theme_name = $theme_info->get('Name'); 
        $author_name = $theme_info->get('Author'); 
        $theme_version = $theme_info->get('Version'); 

        // Theme name
        sh_system_status::add('Theme config', array(
            'check_name' => 'Theme name',
            'tooltip' => 'Theme name',
            'value' =>  $theme_name,
            'status' => 'info'
        ));

        // Theme version
        sh_system_status::add('Theme config', array(
            'check_name' => 'Theme version',
            'tooltip' => 'Theme current version',
            'value' =>  $theme_version,
            'status' => 'info'
        )); 

        // Theme author name
        sh_system_status::add('Theme config', array(
            'check_name' => 'Theme author name',
            'tooltip' => 'Theme author name',
            'value' =>  $author_name,
            'status' => 'info'
        )); 

        // Theme remote http channel used by the theme
        $td_remote_http = '';

        if (empty($td_remote_http['test_status'])) {
        } elseif ($td_remote_http['test_status'] == 'all_fail') {
            // all the http tests failed to run!
            sh_system_status::add('Theme config', array(
                'check_name' => 'HTTP channel test',
                'tooltip' => 'The theme cannot connect to other data sources. We are unable to get the number of likes, video information, tweets etc. This is usually due to a
                misconfigured server or firewall',
                'value' =>  $td_remote_http['test_status'],
                'status' => 'red'
            ));
        } else {
            // we have a http channel test that works
            sh_system_status::add('Theme config', array(
                'check_name' => 'HTTP channel test',
                'tooltip' => 'The theme has multiple ways to get information (like count, tweet count etc) from other sites and this is the channel that was detected to work with your host.',
                'value' =>  $td_remote_http['test_status'],
                'status' => 'green'
            ));
        }

        // server info
        sh_system_status::add('php.ini configuration', array(
            'check_name' => 'Apache Version',
            'tooltip' => 'Server apache version',
            'value' =>  apache_get_version(),
            'status' => 'info'
        ));


        // php version
        sh_system_status::add('php.ini configuration', array(
            'check_name' => 'PHP version',
            'tooltip' => 'You should have PHP version 5.2.4 or greater (recommended: PHP 5.4 or greater)',
            'value' => phpversion(),
            'status' => 'info'
        ));

        // mysql_version
        global $wpdb;
        sh_system_status::add('php.ini configuration', array(
            'check_name' => 'MySQL version',
            'tooltip' => 'The version of MySQL installed on your hosting server',
            'value' =>  $wpdb->db_version(),
            'status' => 'info'
        ));

        // post_max_size
        sh_system_status::add('php.ini configuration', array(
            'check_name' => 'post_max_size',
            'tooltip' => 'Sets max size of post data allowed. This setting also affects file upload. To upload large files you have to increase this value and in some cases you also have to increase the upload_max_filesize value.',
            'value' =>  ini_get('post_max_size') . '<span class="sh-status-small-text"> - You cannot upload images, themes and plugins that have a size bigger than this value. To see how you can change this please check our guide <a target="_blank" href="http://forum.tagdiv.com/system-status-parameters-guide/">here</a>.</span>',
            'status' => 'info'
        ));

        // php time limit
        $max_execution_time = ini_get('max_execution_time');
        if ($max_execution_time == 0 or $max_execution_time >= 60) {
            sh_system_status::add('php.ini configuration', array(
                'check_name' => 'max_execution_time',
                'tooltip' => 'This parameter is properly set',
                'value' =>  $max_execution_time,
                'status' => 'green'
            ));
        } else {
            sh_system_status::add('php.ini configuration', array(
                'check_name' => 'max_execution_time',
                'tooltip' => 'This sets the maximum time in seconds a script is allowed to run before it is terminated by the parser. The theme demos download images from our servers and depending on the connection speed this process may require a longer time to execute. We recommend that you should increase it 60 or more.',
                'value' =>  $max_execution_time . '<span class="sh-status-small-text"> - the execution time should be bigger than 60 if you plan to use the demos. To see how you can change this please check our guide <a target="_blank" href="http://forum.tagdiv.com/system-status-parameters-guide/">here</a>.</span>',
                'status' => 'yellow'
            ));
        }


        // php max input vars
        $max_input_vars = ini_get('max_input_vars');
        if ($max_input_vars == 0 or $max_input_vars >= 2000) {
            sh_system_status::add('php.ini configuration', array(
                'check_name' => 'max_input_vars',
                'tooltip' => 'This parameter is properly set',
                'value' =>  $max_input_vars,
                'status' => 'green'
            ));
        } else {
            sh_system_status::add('php.ini configuration', array(
                'check_name' => 'max_input_vars',
                'tooltip' => 'This sets how many input variables may be accepted (limit is applied to $_GET, $_POST and $_COOKIE superglobal separately). By default this parameter is set to 1000 and this may cause issues when saving the menu, we recommend that you increase it to 2000 or more. ',
                'value' =>  $max_input_vars . '<span class="sh-status-small-text"> - the max_input_vars should be bigger than 2000, otherwise it can cause incomplete saves in the menu panel in WordPress. To see how you can change this please check our guide <a target="_blank" href="http://forum.tagdiv.com/system-status-parameters-guide/">here</a>.</span>',
                'status' => 'yellow'
            ));
        }

        // suhosin
        if (extension_loaded('suhosin') !== true) {
            sh_system_status::add('php.ini configuration', array(
                'check_name' => 'SUHOSIN installed',
                'tooltip' => 'Suhosin is not installed on your server.',
                'value' => 'false',
                'status' => 'green'
            ));
        } else {
            sh_system_status::add('php.ini configuration', array(
                'check_name' => 'SUHOSIN Installed',
                'tooltip' => 'Suhosin is an advanced protection system for PHP installations. It was designed to protect servers and users from known and unknown flaws in PHP applications and the PHP core. If it\'s installed on your host you have to increase the suhosin.post.max_vars and suhosin.request.max_vars parameters to 2000 or more.',
                'value' =>  'SUHOSIN is installed - <span class="sh-status-small-text">it may cause problems with saving the theme panel if it\'s not properly configured. You have to increase the suhosin.post.max_vars and suhosin.request.max_vars parameters to 2000 or more. To see how you can change this please check our guide <a target="_blank" href="http://forum.tagdiv.com/system-status-parameters-guide/">here</a>.</span>',
                'status' => 'yellow'
            ));

            // suhosin.post.max_vars
            if (ini_get( "suhosin.post.max_vars" ) >= 2000){
                sh_system_status::add('php.ini configuration', array(
                    'check_name' => 'suhosin.post.max_vars',
                    'tooltip' => 'This parameter is properly set',
                    'value' => ini_get("suhosin.post.max_vars"),
                    'status' => 'green'
                ));
            } else {
                sh_system_status::add('php.ini configuration', array(
                    'check_name' => 'suhosin.post.max_vars',
                    'tooltip' => 'You may encounter issues when saving the menu, to avoid this increase suhosin.post.max_vars parameter to 2000 or more.',
                    'value' => ini_get("suhosin.post.max_vars"),
                    'status' => 'yellow'
                ));
            }

            // suhosin.request.max_vars
            if (ini_get( "suhosin.request.max_vars" ) >= 2000){
                sh_system_status::add('php.ini configuration', array(
                    'check_name' => 'suhosin.request.max_vars',
                    'tooltip' => 'This parameter is properly set',
                    'value' => ini_get("suhosin.request.max_vars"),
                    'status' => 'green'
                ));
            } else {
                sh_system_status::add('php.ini configuration', array(
                    'check_name' => 'suhosin.request.max_vars',
                    'tooltip' => 'You may encounter issues when saving the menu, to avoid this increase suhosin.request.max_vars parameter to 2000 or more.',
                    'value' => ini_get("suhosin.request.max_vars"),
                    'status' => 'yellow'
                ));
            }
        }
 
        /*  ----------------------------------------------------------------------------
            WordPress
        */
        // home url
        sh_system_status::add('WordPress and plugins', array(
            'check_name' => 'WP Home URL',
            'tooltip' => 'WordPress Address (URL) - the address where your WordPress core files reside',
            'value' => home_url(),
            'status' => 'info'
        ));

        // site url
        sh_system_status::add('WordPress and plugins', array(
            'check_name' => 'WP Site URL',
            'tooltip' => 'Site Address (URL) - the address you want people to type in their browser to reach your WordPress blog',
            'value' => site_url(),
            'status' => 'info'
        ));

        // home_url == site_url
        if (home_url() != site_url()) {
            sh_system_status::add('WordPress and plugins', array(
                'check_name' => 'Home URL - Site URL',
                'tooltip' => 'Home URL not equal to Site URL, this may indicate a problem with your WordPress configuration.',
                'value' => 'Home URL != Site URL <span class="sh-status-small-text">Home URL not equal to Site URL, this may indicate a problem with your WordPress configuration.</span>',
                'status' => 'yellow'
            ));
        }

        // version
        sh_system_status::add('WordPress and plugins', array(
            'check_name' => 'WP version',
            'tooltip' => 'WordPress version',
            'value' => get_bloginfo('version'),
            'status' => 'info'
        ));


        // is_multisite
        sh_system_status::add('WordPress and plugins', array(
            'check_name' => 'WP multisite enabled',
            'tooltip' => 'WP multisite',
            'value' => is_multisite() ? 'Yes' : 'No',
            'status' => 'info'
        ));


        // language
        sh_system_status::add('WordPress and plugins', array(
            'check_name' => 'WP Language',
            'tooltip' => 'WP Language - can be changed from Settings -> General',
            'value' => get_locale(),
            'status' => 'info'
        ));



        // memory limit
        $memory_limit = sh_system_status::wp_memory_notation_to_number(WP_MEMORY_LIMIT);
        if ( $memory_limit < 67108864 ) {
            sh_system_status::add('WordPress and plugins', array(
                'check_name' => 'WP Memory Limit',
                'tooltip' => 'By default in wordpress the PHP memory limit is set to 40MB. With some plugins this limit may be reached and this affects your website functionality. To avoid this increase the memory limit to at least 64MB.',
                'value' => size_format( $memory_limit ) . '/request <span class="sh-status-small-text">- We recommend setting memory to at least 64MB. The theme is well tested with a 40MB/request limit, but if you are using multiple plugins that may not be enough. See: <a target="_blank" href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP">Increasing memory allocated to PHP</a>. You can also check our guide <a target="_blank" href="http://forum.tagdiv.com/system-status-parameters-guide/">here</a>.</span>',
                'status' => 'yellow'
            ));
        } else {
            sh_system_status::add('WordPress and plugins', array(
                'check_name' => 'WP Memory Limit',
                'tooltip' => 'This parameter is properly set.',
                'value' => size_format( $memory_limit ) . '/request',
                'status' => 'green'
            ));
        }


        // wp debug
        if (defined('WP_DEBUG') and WP_DEBUG === true) {
            sh_system_status::add('WordPress and plugins', array(
                'check_name' => 'WP_DEBUG',
                'tooltip' => 'The debug mode is intended for development and it may display unwanted messages. You should disable it on your side.',
                'value' => 'WP_DEBUG is enabled. <span class="sh-status-small-text">It may display unwanted messages. To see how you can change this please check our guide <a target="_blank" href="http://forum.tagdiv.com/system-status-parameters-guide/">here</a>.</span>',
                'status' => 'yellow'
            ));
        } else {
            sh_system_status::add('WordPress and plugins', array(
                'check_name' => 'WP_DEBUG',
                'tooltip' => 'The debug mode is disabled.',
                'value' => 'False',
                'status' => 'green'
            ));
        }

        // caching
        $caching_plugin_list = array(
            'wp-super-cache/wp-cache.php' => array(
                'name' => 'WP super cache - <span class="sh-status-small-text">for best performance please check the plugin configuration guide <a target="_blank" href="http://forum.tagdiv.com/cache-plugin-install-and-configure/">here</a>.</span>',
                'status' => 'green',
            ),
            'w3-total-cache/w3-total-cache.php' => array(
                'name' => 'W3 total cache - <span class="sh-status-small-text">we recommend <a target="_blank" href="https://ro.wordpress.org/plugins/wp-super-cache/">WP super cache</a></span>',
                'status' => 'yellow',
            ),
            'wp-fastest-cache/wpFastestCache.php' => array(
                'name' => 'WP Fastest Cache - <span class="sh-status-small-text">we recommend <a target="_blank" href="https://ro.wordpress.org/plugins/wp-super-cache/">WP super cache</a></span>',
                'status' => 'yellow',
            ),
        );
        $active_plugins = get_option('active_plugins');
        $caching_plugin = 'No caching plugin detected - <span class="sh-status-small-text">for best performance we recommend using <a target="_blank" href="https://wordpress.org/plugins/wp-super-cache/">WP Super Cache</a></span>';
        $caching_plugin_status = 'yellow';
        foreach ($active_plugins as $active_plugin) {
            if (isset($caching_plugin_list[$active_plugin])) {
                $caching_plugin = $caching_plugin_list[$active_plugin]['name'];
                $caching_plugin_status = $caching_plugin_list[$active_plugin]['status'];
                break;
            }
        }
        sh_system_status::add('WordPress and plugins', array(
            'check_name' => 'Caching plugin',
            'tooltip' => 'A cache plugin generates static pages and improves the site pagespeed. The cached pages are stored in the memory and when a user makes a request the pages are delivered from the cache. By this the php execution and the database requests are skipped.',
            'value' =>  $caching_plugin,
            'status' => $caching_plugin_status
        ));

        sh_system_status::render_tables(); ?>
        <h3 class="screen-reader-text"><?php esc_html_e( 'Active Plugins', 'litmus' ); ?></h3>
        <table class="widefat sh-system-status-table" cellspacing="0">
            <thead>
                <tr>
                    <th colspan="4" data-export-label="Active Plugins (<?php echo count( (array) get_option( 'active_plugins' ) ); ?>)"><?php esc_html_e( 'Active Plugins', 'litmus' ); ?> (<?php echo count( (array) get_option( 'active_plugins' ) ); ?>)</th>
                </tr>
            </thead>
            
            <tbody>
                <?php
                $active_plugins = (array) get_option( 'active_plugins', array() );

                if ( is_multisite() ) {
                    $active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
                }

                foreach ( $active_plugins as $plugin ) {
                    
                    $plugin_data    = @get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin );
                    $dirname        = dirname( $plugin );
                    $version_string = '';
                    $network_string = '';

                    if ( ! empty( $plugin_data['Name'] ) ) {

                        // link the plugin name to the plugin url if available
                        $plugin_name = esc_html( $plugin_data['Name'] );

                        if ( ! empty( $plugin_data['PluginURI'] ) ) {
                            $plugin_name = '<a href="' . esc_url( $plugin_data['PluginURI'] ) . '" title="' . __( 'Visit plugin homepage' , 'litmus' ) . '">' . $plugin_name . '</a>';
                        }
                        ?>
                        <tr>
                            <td class="sh-system-status-name"><?php echo($plugin_name); ?></td>
                            <td class="sh-system-status-help"></td>
                            <td class="sh-system-status-status">
                                <div class="sh-system-status-led sh-system-status-green sh-tooltip" data-position="right" title="This plugin is now active."></div> 
                            </td>
                            <td><?php printf( _x( 'by %s', 'by author', 'litmus' ), $plugin_data['Author'] ) . ' &ndash; ' . esc_html( $plugin_data['Version'] ) . $version_string . $network_string; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table> 
    <?php } // end stystem status ?>  

    <div class="litmus-thanks">
        <p class="description"><?php esc_html_e( "Thank you for choosing Litmus. We are honored and are fully dedicated to making your experience perfect.", "litmus" ); ?></p>
    </div><!-- /.litmus-thanks -->
</div><!-- /. wrap about-wrap shopper-wrap -->

