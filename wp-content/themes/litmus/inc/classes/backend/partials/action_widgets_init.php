<?php 
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

register_sidebar(  array(
    'name'          => esc_html__( 'Sidebar Blog', 'litmus' ),
    'description'   => esc_html__( 'This sidebar will show in blog page', 'litmus' ),
    'id'            => 'sidebar-blog',
    'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h4 class="widget-title">',
    'after_title'   => '</h4>',
) );

register_sidebar(  array(
    'name'          => esc_html__( 'Sidebar Hamburger', 'litmus' ),
    'description'   => esc_html__( 'This sidebar will show in hamburger menu popup right', 'litmus' ),
    'id'            => 'sidebar-hamburger',
    'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h4 class="widget-title">',
    'after_title'   => '</h4>',
) );


for ($i=1; $i <= sh_lm_get_customizer_field('footer_option', 'widgets_columns'); $i++) { 
	register_sidebar(  
		array(
			'name'          => esc_html__( 'Footer Widget ', 'litmus' ).$i,
			'id'            => 'sidebar-footer-'.$i,
			'description'   => esc_html__( 'This sidebar will show in Footer', 'litmus' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) 
	);
}
 
register_sidebar(  array(
    'name'          => esc_html__( 'Sidebar Shop', 'litmus' ),
    'description'   => esc_html__( 'This sidebar will show in WooCommerce Page', 'litmus' ),
    'id'            => 'sidebar-shop',
    'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
) );