( function( $ ) {
    $(document).ready(function() {
        $('select[name=_instashop_action]').change(function() {
            var select = $(this).val();
            if(select == "redirect") {
                $('.show-redirect').show();
            } else {
                $('.show-redirect').hide();
            }
        });
        if($("input[name=_instashop_hashtag]").length) {
            $("input[name=_instashop_hashtag]").tagit();
        }
        $('.help_tip').tipTip( {
            'attribute': 'data-tip',
            'fadeIn': 50,
            'fadeOut': 50,
            'delay': 200
        });
        $('input[name="instashop_feed_caching"]').on("change", function() {
            if($(this).val() == "yes") {
                $('#show_renew_cache').show();
            } else {
                $('#show_renew_cache').hide();
            }
        });
        $('.nav-tab').click(function() {
            var tab = $(this).attr("rel");
            $('.tab').hide();
            $('.tab'+tab).show();
            $('.nav-tab').removeClass('nav-tab-active');
            $('.nav-tab'+tab).addClass('nav-tab-active');
        });
        $('a.disconnect').click(function() {
            if(confirm("Are you sure you'd like to disconnect your Instagram account?")) {
                $('#processDisconnect').val('1');
                $('#mainform').submit();
            }
        });
        $('a.clearCache').click(function() {
            $('#processClearCache').val('1');
            $('#mainform').submit();
        });
    });
})(jQuery);
