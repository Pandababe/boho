<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'boho' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-jwI!S/7|k30C~1wy0NLU6d 7*0Y_us:T0=SNbr-D#|(_3tAA. 8e=/wN=&)xz]_');
define('SECURE_AUTH_KEY',  'Ypo|72i$>BGK&(:Eq4SkJ`M>C60]GDu$/;GwI(!OE.;7W;|_0q$+TBnaPj^Ba*] ');
define('LOGGED_IN_KEY',    'z[R_!ubfEP;lc_T#1b))pW$FS5BgQt^X=G90N(Gc?f@Zdpb#E-+W!;PkovF`NF`i');
define('NONCE_KEY',        '~LXD-:pP^&!6>QPC+yMj21;&T_bZ)sKGW~f>g3{G|[4zE|=bjoO6(xM$qf|8_-(#');
define('AUTH_SALT',        'jFzZH|ex.Bt-4iA$4G/=btgdqtI_.yOJ,dJ6+a^{!2Gce|gc0l0}nXF^b+?ibg@g');
define('SECURE_AUTH_SALT', '9-6HN?@3H?Pq?/2[(bl+ED{L*H+lHw(<e,M|>x45| F-74,>|SlE{5)4@9vyd7|D');
define('LOGGED_IN_SALT',   'vd9X[?bx]<Pw}Tc;RjKuRz_X5t1nX=Wf`^6vPkgP##/,2eWK$un).pB:EL2B@ Z3');
define('NONCE_SALT',       'W:#gS832jQJ3ma}$~aIG-eDK}W[/FWj(-TMh..mvEY>eQ^aBLL8pSJYgtz3|n8|5');


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */
define('WP_DEBUG', true);

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
