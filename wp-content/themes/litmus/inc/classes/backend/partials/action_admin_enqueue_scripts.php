<?php 
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

wp_enqueue_style('sh-lm-admin-css', LITMUS_TEMPLATE_DIR_URL . "/assets/custom/admin.css");

// enqueue scripts
wp_enqueue_script('sh-lm-backend-js', LITMUS_TEMPLATE_DIR_URL . '/assets/custom/admin.js', array("jquery"), false, true);
wp_localize_script("sh-lm-backend-js", "litmus", array (
        'ajaxurl' => admin_url( "admin-ajax.php" ),
        'root' => esc_url_raw( rest_url() ),
        'home_uri' => esc_url( home_url( '/' ) ), 
    )
);

if(defined( 'FW' )) {
    fw()->backend->option_type('icon-v2')->packs_loader->enqueue_frontend_css();
}