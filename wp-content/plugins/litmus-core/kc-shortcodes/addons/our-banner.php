<?php
add_shortcode( 'sh_lm_our_banner', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'title' => '',  
		'desc' => '',   
		'mascotimg' => '',  
		'showscroll' => '',  
	), $atts));

	extract($atts);
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?> 

	<div class="creative-banner-1 <?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		<div class="banner-background">		
			<div class="litmus-container">
				<div class="row align-items-center">
					<div class="col-md-7">
						<h2 class="creative-title">
							<?php echo esc_html($title); ?>
						</h2><!--  /.creative-title -->
						<p class="creative-description">
							<?php echo esc_html($desc); ?>
						</p>
						<?php 
							$linkurl = sh_lm_theme_kc_custom_link($showscroll);
							$button_attr = array();
							if(  $linkurl["url"] != "" ) {
								$button_attr[] = 'href="'. esc_attr($linkurl["url"]) .'"';
							} else {
								$button_attr[] = 'href="#"';
							}
							if( $linkurl["target"] != "" ){
								$button_attr[] = 'target="'. esc_attr($linkurl["target"]) .'"';
							}
							if(  $linkurl["title"] != "" ){
								$button_attr[] = 'title="'. esc_attr($linkurl["title"]) .'"';
							}
						?>
						<?php 
							if($showscroll) { ?>
								<a class="bsm-btn btn-banner" <?php echo implode(' ', $button_attr); ?>><?php echo esc_html($linkurl["title"]); ?></a>
							<?php }
						?>
					</div><!--  /.col-md-7 -->
					<div class="col-md-5">
						<img class="brand-images" src="<?php echo esc_url(sh_lm_get_image_crop_size($mascotimg, 535, 586)); ?>" alt="<?php echo esc_html__('Brand Image','litmus'); ?>" />
					</div><!--  /.col-md-5 -->
				</div><!--  /.row -->
				<?php if($showscroll) : ?>
				<?php endif ?>
			</div><!--  /.litmus-container -->	
		</div>
	</div><!--  /.creative-banner-1 -->

	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_banner', 123 );
function sh_lm_kc_our_banner() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_banner' => array(
	                'name' => esc_html__('Creative Banner', 'litmus'),
	                'description' => esc_html__('It\'s For Creative Agency Banner', 'litmus'),
	                'icon' => 'et-ribbon',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array(               	
	                		array(
	                			'name' => 'title',
	                			'type' => 'text',
	                			'label' => esc_html__('Title', 'litmus'),  
	                			'value' => 'We Create Design That never dies',   
	                			'admin_label' => true,
	                		),
	                		array(
	                			'name' => 'desc',
	                			'type' => 'textarea',
	                			'label' => esc_html__('Short Description', 'litmus'), 
	                			'value' => 'We are passionate about UX/UI Design &amp; Development and can help bring your ideas beyond awesomeness',
	                		),
	                		array(
	                			'name' => 'mascotimg',
	                			'label' => esc_html__('Mascot element', 'litmus'),  
	                			'description' => esc_html__('It\'s for mascot element or the agency CEO Image', 'litmus'),
	                			'type' => 'attach_image',  
	                		),	                	
	                		array(
	                			'name' => 'showscroll',
	                			'label' => esc_html__('Button', 'litmus'),  
	                			'type' => 'link',  
	                			'description' => esc_html__('Add the Button url with hash(#) for going on specific section', 'litmus'),
	                		),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
	                					"screens" => "any,1024,999,767,479",
	                					'Background' => array(
	                						array(
	                							'property' => 'background',
	                							'selector' => '.banner-background'
	                						),
	                					),
	                					'Heading'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Heading Color','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'font-size', 
	                							'label' => esc_html__('Heading Font Size','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'font-weight', 
	                							'label' => esc_html__('Heading Font Weight','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'font-style', 
	                							'label' => esc_html__('Heading Font Style','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'font-family', 
	                							'label' => esc_html__('Heading Font Family','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'text-align', 
	                							'label' => esc_html__('Heading Text Align','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'text-shadow', 
	                							'label' => esc_html__('Heading Text Shadow','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'text-transform', 
	                							'label' => esc_html__('Heading Text Transform','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'text-decoration', 
	                							'label' => esc_html__('Heading Text Decoration','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'line-height', 
	                							'label' => esc_html__('Heading Line Height','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                						array(
	                							'property' => 'letter-spacing', 
	                							'label' => esc_html__('Heading Letter Spacing','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                					),
	                					'Description'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Description Color','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'font-size', 
	                							'label' => esc_html__('Description Font Size','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'font-weight', 
	                							'label' => esc_html__('Description Font Weight','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'font-style', 
	                							'label' => esc_html__('Description Font Style','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'font-family', 
	                							'label' => esc_html__('Description Font Family','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'text-align', 
	                							'label' => esc_html__('Description Text Align','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'text-shadow', 
	                							'label' => esc_html__('Description Text Shadow','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'text-transform', 
	                							'label' => esc_html__('Description Text Transform','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'text-decoration', 
	                							'label' => esc_html__('Description Text Decoration','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'line-height', 
	                							'label' => esc_html__('Description Line Height','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                						array(
	                							'property' => 'letter-spacing', 
	                							'label' => esc_html__('Description Letter Spacing','litmus'),
	                							'selector' => '.creative-description'
	                						),
	                					),
	                					'Button'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Color','litmus'),
	                							'selector' => '.btn-banner'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Hover Color','litmus'),
	                							'selector' => '.btn-banner:hover'
	                						),	                						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Button Background Color','litmus'),
	                							'selector' => '.btn-banner'
	                						),
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Button Hover Background','litmus'),
	                							'selector' => '.btn-banner:hover'
	                						),
	                					),
	                					'Spacing'	=> array(
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Banner Padding','litmus'),
	                							'selector' => '.banner-background'
	                						),            						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Mascot Image Margin','litmus'),
	                							'selector' => '.brand-images'
	                						),                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Banner Heading Margin','litmus'),
	                							'selector' => '.creative-title'
	                						),
	                					),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 
	        )
	    ); // End add map
	
	} // End if
}