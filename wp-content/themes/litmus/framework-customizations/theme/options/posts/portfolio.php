<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'post' => array(
        'title' => esc_html__( 'Portfolio Options', 'litmus' ), 
        'type'  => 'box',
        'options' => array( 
            'portfolio_img_size'  => array(
                'type'      => 'short-select',
                'title'     => esc_html__( 'Image Size', 'litmus' ), 
                'desc'      => esc_html__( 'Which is the image size of this post in shortcode', 'litmus' ), 
                'choices'   => array( 
                    'square' => esc_html__( 'Square', 'litmus' ), 
                    'vertical' => esc_html__( 'vertical', 'litmus' ),  
                ),
                'value' => 'square'
            ),
            'portfolio_rating'  => array(
                'type'      => 'short-select',
                'title'     => esc_html__( 'Project Rating', 'litmus' ),   
                'choices'   => LITMUS_Static::star_option(),
                'value'   => '5'
            ),
            'portfolio_cus_name' => array(
                'type'  => 'text', 
                'label' => esc_html__('Portfolio Customer Name', 'litmus'),  
            ),
            'portfolio_live_url' => array(
                'type'  => 'text', 
                'label' => esc_html__('Portfolio Live URL', 'litmus'),  
            ),
            'portfolio_skills' => array(
                'type'  => 'text', 
                'label' => esc_html__('Portfolio Skills', 'litmus'),  
            ),
            'portfolio_date' => array(
                'type'  => 'text', 
                'label' => esc_html__('Portfolio Date', 'litmus'),  
            ),
            'portfolio_download_url' => array(
                'type'  => 'text', 
                'label' => esc_html__('Portfolio Download URL', 'litmus'),  
            ),
            'portfolio_project_url' => array(
                'type'  => 'text', 
                'label' => esc_html__('Portfolio Lunch Project URL', 'litmus'),  
            ),
        ),
    ),
); 