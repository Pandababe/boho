<?php
add_shortcode( 'sh_lm_category_with_thumb', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'cat' => '',  
		'thumb' => '',  
		'url' => '',  
	), $atts));  
	ob_start(); ?> 
	<?php 
	$is_cat = false;
    if(!empty($cat)) {
        $cats = explode(",", $cat);  

        foreach ( $cats as $cat ) {  
            $cat = explode(":", $cat); 
            $cat_url = get_term_link($cat[0], 'product_cat');
            $title = $cat[1];
            $is_cat = true;
        } 
	} 
	?>
	<?php 
		$linkurl = sh_lm_theme_kc_custom_link($url);
		$button_attr = array();
		if(  $linkurl["url"] != "" ) {
			$button_attr[] = 'href="'. esc_attr($linkurl["url"]) .'"';
		} else {
			$button_attr[] = 'href="#"';
		}
		if( $linkurl["target"] != "" ){
			$button_attr[] = 'target="'. esc_attr($linkurl["target"]) .'"';
		}
		if(  $linkurl["title"] != "" ){
			$button_attr[] = 'title="'. esc_attr($linkurl["title"]) .'"';
		}
	 ?>
	<?php 
		if($url) { ?>
			<a class="category-link" <?php echo implode(' ', $button_attr); ?>>
		<?php } elseif($is_cat) { ?>
			<a href="<?php if($is_cat) { echo esc_url($cat_url); } ?>" class="category-link">
		<?php }
	?>
 	    <figure class="category-thumb">
 	        <img src="<?php echo esc_url(sh_lm_get_image_crop_size($thumb, 1200, 1200)); ?>" alt="<?php if($is_cat) echo esc_attr($title); ?>" />
 	    </figure>

 	    <?php if($is_cat) { ?>
 	    <div class="cat-box">
 	        <h4 class="category-name"><?php echo esc_html($title); ?></h4>
 	    </div><!--  /.cat-box -->
 	    <?php } ?>
 	</a>
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_category_with_thumb', 117 );
function sh_lm_kc_category_with_thumb() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_category_with_thumb' => array(
	                'name' => esc_html__('Category With Thumb', 'litmus'),
	                'description' => esc_html__('Category With Thumb', 'litmus'),
	                'icon' => 'et-picture',
	                'category' => 'Litmus',
	                'params' => array(  
	                	array(
	                	    'type'          => 'autocomplete',
	                	    'label'         => esc_html__('Select Category', 'litmus'),
	                	    'name'          => 'cat',
	                	    'options'       => array( 
	                	        'multiple'      => false, 
	                	        'post_type'     => 'product', 
	                	        'taxonomy'      => 'product_cat', 
	                	    ), 
	                	),
	                	array(
                			'name' => 'thumb',
                			'label' => esc_html__('Thumbnail', 'litmus'), 
                			'type' => 'attach_image',  
                		),
	                    array(
	                    	'name' => 'url',
	                    	'type' => 'link',
	                    	'label' => esc_html__('URL', 'litmus'),  
	                    	'description' => 'If you give URL it will show as banner',  
	                    ),
	                )
	            ),  // End of elemnt kc_icon 
	        )
	    ); // End add map
	
	} // End if
}