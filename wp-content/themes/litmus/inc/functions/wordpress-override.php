<?php
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

class LITMUS_WP_Override {
 
    public function __construct() {  
        // filter read more
        add_filter( 'the_content_more_link', array($this, 'content_more') ); 

        // remove bracket from category widget
        add_filter( 'wp_list_categories', array($this, 'cats_postcount_filter') ); 

        // remove bracket from archive widget
        add_filter( 'get_archives_link', array($this, 'archive_postcount_filter') ); 

        // remove unnecessary p and br tag from short code
        add_filter( 'the_content', array($this, 'remove_p_n_br_tag') ); 
    }

    /** 
     * @since 1.0
     */
    public function content_more() {
        return esc_html__( '&hellip; ', 'litmus');
    }

    /** 
     * @since 1.0
     */
    public function cats_postcount_filter($args) {
        $args = str_replace('(', '<span class="count"> ', $args);
        return str_replace(')', ' </span>', $args); 
    }

    /** 
     * @since 1.0
     */
    public function archive_postcount_filter($args) {
        $args = str_replace('</a>&nbsp;(', '</a> <span class="count">', $args);
        return str_replace(')', ' </span>', $args); 
    }

    /** 
     * @since 1.0
     */
    public function remove_p_n_br_tag($content) {
        $array = array (
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );
        $content = strtr($content, $array);
        return $content;   
    } 

    /** 
     * @since 1.0
     */
    public function add_property_attribute( $tag, $handle ) {
        if ( strpos( $tag, 'mediaelement' ) !== FALSE ) {
            $tag = str_replace( "/>", "property='stylesheet' />", $tag );
        }
        return $tag;
    }

    /** 
     * @since 1.0
     */
    public function cf7_dequeue_scripts() {
        $load_scripts = false;
        $post = get_post();
        if( is_singular() ) {
            if( has_shortcode($post->post_content, 'contact-form-7') ) {
                $load_scripts = true;
            }
        }

        if( ! $load_scripts ) {
            wp_dequeue_script( 'contact-form-7' );
            wp_dequeue_style( 'contact-form-7' );
        }
    }
} 
new LITMUS_WP_Override; 