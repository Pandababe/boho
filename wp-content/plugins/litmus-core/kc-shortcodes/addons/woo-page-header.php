<?php
add_shortcode( 'sh_lm_woo_page_header', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'bg_img' => '',  
		'short_desc' => '',  
		'username' => '',  
		'title' => '', 
	), $atts));  
	ob_start(); ?>     
	<div class="insta-title-content">
    	<?php 
    		$array_from_to = array(
    			'&lt;' => '<', 
    			'&gt;' => '>', 
    		);
    		$title = strtr($title, $array_from_to);
    	?>
	    <h2 class="section-title no-border v-3"><?php echo wp_kses( $title, LITMUS_Static::html_allow() ); ?></h2>
	    <p class="sub-section-title v-2"><?php echo esc_html($short_desc); ?></p>
	    <div class="insta_user"><?php echo esc_html($username); ?></div><!--  /.insta_user -->
	</div><!--  /.insta-title-content -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_woo_page_header', 128 );
function sh_lm_kc_woo_page_header() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_woo_page_header' => array(
	                'name' => esc_html__('Woo Page Header', 'litmus'),
	                'description' => esc_html__('Woo Page Header', 'litmus'),
	                'icon' => 'fa-header',
	                'category' => 'Litmus',
	                'params' => array( 
	                    array(
	                    	'name' => 'title',
	                    	'type' => 'text',
	                    	'value' => '<span>Latest</span> Form Instagram',
	                    	'label' => esc_html__('Short Title', 'litmus'),  
	                    	'admin_label' => true,
	                    ), 
	                    array(
	                    	'name' => 'short_desc',
	                    	'type' => 'textarea',
	                    	'value' => 'Etiam congue rhoncus gravida. Sed vel sodales tortor. Donec eget dictum enim. Donec tempus euismod metus ac maximus.',
	                    	'label' => esc_html__('Title', 'litmus'),  
	                    ), 
	                    array(
	                    	'name' => 'username',
	                    	'type' => 'text',
	                    	'value' => '@sh_lm_container',
	                    	'label' => esc_html__('User Name', 'litmus'),  
	                    ), 
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}