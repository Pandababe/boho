<?php
add_shortcode( 'sh_lm_our_portfilio', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'style' => '',  
		'title' => '',  
		'desc' => '',  
		'cats' => '',  
		'url' => '',  
		'number' => '',  
	), $atts));  
	extract($atts);
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?> 

	<?php if($style == 'two'): ?>
	<section class="page-header bg-snow">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-12">
	                <div class="breadcrumbs">
	                    <span class="first-item">
	                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'domain'); ?></a>
	                    </span>
	                    <span class="separator"><i class="fa fa-angle-right"></i></span>
	                    <span class="last-item"><?php esc_html_e('Portfolio', 'domain'); ?></span>
	                </div>
	            </div><!--  /.col-md-12 -->
	            <div class="col-md-12">
	                <h2 class="section-title center-title"><?php echo esc_html($title); ?></h2> 
                	<?php 
                		$array_from_to = array(
                			'&lt;' => '<', 
                			'&gt;' => '>', 
                		); 
                		$desc = strtr($desc, $array_from_to);
                	?> 
                    <p class="section-subtitle"><?php echo wp_kses( $desc, LITMUS_Static::html_allow() ); ?></p>
	            </div><!--  /.col-md-12 -->
	            <div class="col-md-12">
	                <ul class="portfolio-menu style-two">
                        <li><a href="#" data-filter="*"><?php esc_html_e('All', 'litmus'); ?></a></li>
                        <?php  
                        if(!empty($cats)) {
                        $cats = explode(",", $cats);  
                        $port_cat_slugs = array();
                        foreach ( $cats as $cat_slug_title ) {  
                        $cat_slug_title = explode(":", $cat_slug_title); 
                        $port_cat_slugs[] = $cat_slug_title[0];
                        ?>
                        <li><a href="#" data-filter=".<?php echo esc_attr($cat_slug_title[0]); ?>"><?php echo esc_html($cat_slug_title[1]); ?></a></li> 
                        <?php } } ?>
	                </ul>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.page-header -->
	<?php endif; ?>

	<div class="portfolio-block">
		<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		    <div class="litmus-container">
		    	<?php if($style == 'one'): ?>
		        <div class="row"> 
		            <div class="col-md-6">
		                <h2 class="section-title"><?php echo esc_html($title); ?></h2>
		            </div><!-- /.col-md-6 -->
		            <div class="col-md-6">
		                <div class="float-right mb-fl-none">                    
		                    <ul class="portfolio-menu">
		                        <li>
		                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php esc_html_e('Filter +', 'litmus'); ?></a>
		                            <ul class="dropdown-menu dropdown-menu-right">
		                                <li><a href="#" data-filter="*"><?php esc_html_e('All', 'litmus'); ?></a></li>
		                                <?php  
		                                if(!empty($cats)) {
		                                $cats = explode(",", $cats);  
		                                $port_cat_slugs = array();
	                                    foreach ( $cats as $cat_slug_title ) {  
	                                    $cat_slug_title = explode(":", $cat_slug_title); 
	                                    $port_cat_slugs[] = $cat_slug_title[0];
	                                    ?>
		                                <li><a href="#" data-filter=".<?php echo esc_attr($cat_slug_title[0]); ?>"><?php echo esc_html($cat_slug_title[1]); ?></a></li> 
		                                <?php } } ?>
		                            </ul><!--  /.dropdown-menu -->
		                        </li>
		                    </ul>
		                </div><!--  /.float-right -->
		            </div><!--  /.col-md-6 -->
		        </div><!-- /.row -->
			    <?php endif; ?>

		        <div class="row">
		            <div class="col-md-12">
		            	<?php if($style == 'one' || $style == 'two'): ?>
		                <div class="row item-4 portfolio-grid">
		                	<?php 
		                	$portfolio_query = new WP_Query(
		                	    array(
		                	        'post_type' => 'portfolio', 
		                	        'posts_per_page' => $number,
		                	        'tax_query' => array(
		                	            array (
		                	                'taxonomy' => 'portfolio-category',
		                	                'field' => 'slug',
		                	                'terms' => $port_cat_slugs,
		                	            )
		                	        ),  
		                	    )
		                	); 
		                	while ($portfolio_query->have_posts()) : $portfolio_query->the_post(); ?> 
		                    <div class="item col-sm-6 col-md-6 col-lg-4 <?php $portfilio_cats = get_the_terms(get_the_ID(), 'portfolio-category'); 
		                    	foreach($portfilio_cats as $cat_name) {    
		                    		echo esc_attr($cat_name->slug).' '; } ?>">
		                        <figure class="portfolio-thumb">
		                            <?php 
		                            if( sh_lm_fw_post_meta('portfolio_img_size') == 'vertical') {
		                                sh_lm_post_featured_image(365, 573, true);
		                            } else {
		                                sh_lm_post_featured_image(365, 280, true);
		                            }
		                            ?> 
		                            <div class="hover-content">
		                                <div class="hover-details">      
		                                    <a href="<?php the_permalink(); ?>" class="ajax-single-link">
		                                        <h5><?php the_title(); ?></h5>
		                                        <span>- <?php 
							                        if ( ! empty( $portfilio_cats ) ) {
							                            echo esc_html( $portfilio_cats[0]->name );
							                        }
							                    ?> -</span>
		                                    </a>
		                                </div><!--  /.hover-details -->
		                            </div><!-- /.hover-content -->
		                        </figure><!-- /.portfolio-thumb -->
		                    </div><!-- /.col-md-3 -->
		                    <?php endwhile; ?> <?php wp_reset_postdata(); ?> 

		                </div><!-- /.row -->
		                <?php endif ?>
		            </div><!--  /.col-md-12 -->
		        </div><!--  /.row -->

		        <?php if($style == 'three'): ?>
		        	<div class="row item-2 portfolio-grid ver-3">
		        		<?php 
			        		$portfolio_query = new WP_Query(
			        		    array(
			        		        'post_type' => 'portfolio', 
			        		        'posts_per_page' => $number,
			        		    )
			        		);
			        		while ($portfolio_query->have_posts()) : $portfolio_query->the_post(); ?>
		                    <div class="item col-sm-6 col-md-6 col-lg-6 <?php $portfilio_cats = get_the_terms(get_the_ID(), 'portfolio-category'); 
		                    	foreach($portfilio_cats as $cat_name) {    
		                    		echo esc_attr($cat_name->slug).' '; } ?>">
		                        <figure class="portfolio-thumb">
		                            <?php 
		                                sh_lm_post_featured_image(545, 410, true);
		                            ?> 
		                            <div class="hover-content">
		                                <div class="hover-details">      
		                                    <a href="<?php the_permalink(); ?>" class="ajax-single-link">
		                                        <h5><?php the_title(); ?></h5>
		                                        <span>- <?php 
							                        if ( ! empty( $portfilio_cats ) ) {
							                            echo esc_html( $portfilio_cats[0]->name );
							                        }
							                    ?> -</span>
		                                    </a>
		                                </div><!--  /.hover-details -->
		                            </div><!-- /.hover-content -->
		                        </figure><!-- /.portfolio-thumb -->
		                    </div><!-- /.col-md-3 -->
			        	<?php endwhile; ?> <?php wp_reset_postdata(); ?> 
		        	</div><!--  /.row -->
		        	<div class="row">
		        		<div class="col-md-12 text-center">
		        			<?php 
		        				$linkurl = sh_lm_theme_kc_custom_link($url);
		        				$button_attr = array();
		        				if(  $linkurl["url"] != "" ) {
		        					$button_attr[] = 'href="'. esc_attr($linkurl["url"]) .'"';
		        				} else {
		        					$button_attr[] = 'href="#"';
		        				}
		        				if( $linkurl["target"] != "" ){
		        					$button_attr[] = 'target="'. esc_attr($linkurl["target"]) .'"';
		        				}
		        				if(  $linkurl["title"] != "" ){
		        					$button_attr[] = 'title="'. esc_attr($linkurl["title"]) .'"';
		        				}
		        			 ?>
		        			<?php 
		        				if($url) { ?>
		        					<a class="bsm-btn btn-creative" <?php echo implode(' ', $button_attr); ?>><?php echo esc_html($linkurl["title"]); ?></a>
		        				<?php }
		        			?>
		        		</div><!--  /.col-md-12 -->
		        	</div><!--  /.row -->
		        <?php endif ?>
		    </div><!--  /.litmus-container -->
	    </div>
	</div><!--  /.portfolio-block -->   
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_portfilio', 102 );
function sh_lm_kc_our_portfilio() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_portfilio' => array(
	                'name' => esc_html__('Our Portfolio', 'litmus'),
	                'description' => esc_html__('Our Portfolio', 'litmus'),
	                'icon' => 'sl-grid',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array( 
		                	array(
		                		'name' => 'style',
		                		'label' => esc_html__('Style', 'litmus'),  
		                		'type' => 'select',  
		                		'options' => array(  
		                			'one' => esc_html__('One', 'litmus'), 
		                			'two' => esc_html__('Two', 'litmus'),  
		                			'three' => esc_html__('Three', 'litmus'),  
		                		),
		                		'value' => 'one',
		                	),
		                	array(
		                		'name' => 'title',
		                		'type' => 'text',
		                		'label' => esc_html__('Title', 'litmus'),  
		                		'value' => 'Projects',   
		                		'admin_label' => true,
		                	),
		                	array(
		                		'name' => 'desc',
		                		'type' => 'textarea',
		                		'label' => esc_html__('Short Description', 'litmus'), 
		                		'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley',  
		                		'relation' => array(
	                		        'parent'    => 'style',
	                		        'show_when' => 'two' 
	                		    )
		                	),
		                	array(
		                	    'type'          => 'autocomplete',
		                	    'label'         => esc_html__('Select Category', 'litmus'),
		                	    'name'          => 'cats',
		                	    'options'       => array( 
		                	        'multiple'      => true, 
		                	        'post_type'     => 'portfolio', 
		                	        'taxonomy'      => 'portfolio-category', 
		                	    ),
    	                		'relation' => array(
                    		        'parent'    => 'style',
                    		        'show_when' => 'one,two' 
                    		    )
		                	),
		                    array(
		                    	'name' => 'number',
		                    	'label' => esc_html__('Total Post', 'litmus'), 
		                    	'type' => 'number_slider',  
		                    	'options' => array(  
		                    		'min' => 3,
		                    		'max' => 50, 
		                    		'show_input' => true
		                    	), 
		                    	'value' => 16
		                    ),
		                	array(
		                	    'type'          => 'link',
		                	    'label'         => esc_html__('Select Category', 'litmus'),
		                	    'name'          => 'url',
    	                		'relation' => array(
                    		        'parent'    => 'style',
                    		        'show_when' => 'three' 
                    		    )
		                	),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
	                					"screens" => "any,1024,999,767,479",
	                					'Hover'	=> array(
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Hover Background','litmus'),
	                							'selector' => '.portfolio-thumb .hover-content'
	                						),	                						
	                					),
	                					'Button'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Color','litmus'),
	                							'selector' => '.btn-creative'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Hover Color','litmus'),
	                							'selector' => '.btn-creative:hover'
	                						),	                						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Button Background Color','litmus'),
	                							'selector' => '.btn-creative'
	                						),
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Button Hover Background','litmus'),
	                							'selector' => '.btn-creative:hover'
	                						),
	                					),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 
	        )
	    ); // End add map
	
	} // End if
}