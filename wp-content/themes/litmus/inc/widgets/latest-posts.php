<?php
class Litmus_Latest_Posts extends WP_Widget {

	function __construct() {
        $title_n_description = esc_html__('Litmus: Latest Posts', 'litmus');
        parent::__construct(
            __CLASS__, //class name
            $title_n_description, // widget title
            array (
                'description' => $title_n_description // widget description
            )
        );
    }

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'litmus'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo esc_attr($this->get_field_id('title')); ?>"
				name="<?php echo esc_attr($this->get_field_name('title')); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('sh_lm_theme_latest_post_limit')); ?>"><?php esc_html_e('Number of posts to show:', 'litmus'); ?></label>
			<input 
				id="<?php echo esc_attr($this->get_field_id('sh_lm_theme_latest_post_limit')); ?>" 
				type="text" 
				name="<?php echo esc_attr($this->get_field_name('sh_lm_theme_latest_post_limit')); ?>"
				value="<?php if( isset($sh_lm_theme_latest_post_limit) ) echo esc_attr($sh_lm_theme_latest_post_limit); ?>"
				size="3" />
		</p>
		<?php
	} // end form function

	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['sh_lm_theme_latest_post_limit'] = intval( $new_instance['sh_lm_theme_latest_post_limit'] );
     
        return $instance;
    }

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		$sh_lm_theme_latest_post_limit = apply_filters('widget_sh_lm_theme_latest_post_limit',$sh_lm_theme_latest_post_limit);
		if ( empty($sh_lm_theme_latest_post_limit) ) $sh_lm_theme_latest_post_limit = 5;

		echo sh_lm_return($before_widget);
			if ( !empty( $title ) ) {
				echo sh_lm_return( $before_title . $title . $after_title );
			}
			?>
			
				<?php 
					$sh_lm_theme_latest_post = new WP_Query( array( 'posts_per_page' => $sh_lm_theme_latest_post_limit,  'order' => 'DESC', 'ignore_sticky_posts' => true  ) );
				?>
				<div class="widget-feed">
                    <ul>
						<?php while ( $sh_lm_theme_latest_post->have_posts() ) : $sh_lm_theme_latest_post->the_post(); ?>

                    	<li class="feed-wrapper">
                            <div class="content">      
                            	<?php if ( has_post_thumbnail() ) {?>                           
                                <div class="image-area">
                                    <?php sh_lm_post_featured_image(60, 60, true); ?>
                                </div> <!-- /.image-area -->
                                <?php } ?>

                                <div class="item-text">
                                    <h5><a href="<?php the_permalink(); ?>"><?php echo sh_lm_custom_post_excerpt( get_the_title(), 70, '&hellip;'); ?></a></h5>
                                    <span class="item-meta"><?php the_time( 'j M, Y' ); ?></span>
                                </div> <!-- /.item-text -->
                            </div>  <!-- /.content --> 
                        </li>

		                <?php
							endwhile;
						?>
					</ul> 
                </div> <!-- /.widget-feed -->  
			<?php
		echo sh_lm_return( $after_widget );
	}
}