<?php
add_shortcode( 'sh_lm_resume_banner', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'sayhello' => '',  
		'sayname' => '',  
		'sayme' => '',
		'sayinfo' => '',  
		'socialarea' => '',  
		'resumebtn' => '',  
	), $atts));  
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>
	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		<div class="litmus-resume-banner-info">
			<h2 class="say-hello"><?php echo esc_html($sayhello); ?></h2>
			<?php 
				$array_from_to = array(
					'&lt;' => '<', 
					'&gt;' => '>', 
				);
				$title = strtr($sayname, $array_from_to);
			?>

			<h2 class="say-name"><?php echo wp_kses( $title, LITMUS_Static::html_allow() ); ?></h2>
			<h3 class="say-myself"><?php echo esc_html($sayme); ?></h3>
			<p class="say-someinfo"><?php echo esc_html($sayinfo); ?></p>
			<ul class="introduce-socialarea">
				<?php foreach ( $socialarea as $item ) {?>
				<?php if(isset($item->icon) || isset($item->imageicon)) : ?>
				<?php 
					$linkurl = sh_lm_theme_kc_custom_link($item->url);
					$button_attr = array();
					if(  $linkurl["url"] != "" ) {
						$button_attr[] = 'href="'. esc_attr($linkurl["url"]) .'"';
					} else {
						$button_attr[] = 'href="#"';
					}
					if( $linkurl["target"] != "" ){
						$button_attr[] = 'target="'. esc_attr($linkurl["target"]) .'"';
					}
					if(  $linkurl["title"] != "" ){
						$button_attr[] = 'title="'. esc_attr($linkurl["title"]) .'"';
					}
				?>
				<li>
					<a <?php echo implode(' ', $button_attr); ?> >
						<?php if(!$item->imageicon) { ?>
						<i class="<?php echo esc_attr($item->icon); ?>"></i>
						<?php } else { ?>
						<img src="<?php echo sh_lm_get_image_crop_size($item->imageicon, 80, 80); ?>" alt="<?php echo esc_attr('Social Url', 'litmus');?>" />
						<?php } ?>
					</a>
				</li>
				<?php endif; ?>
				<?php } ?>
			</ul>
			<?php 
				$btnUrl = sh_lm_theme_kc_custom_link($resumebtn);
				$button_attr = array();
				if(  $btnUrl["url"] != "" ) {
					$button_attr[] = 'href="'. esc_attr($btnUrl["url"]) .'"';
				} else {
					$button_attr[] = 'href="#"';
				}
				if( $btnUrl["target"] != "" ){
					$button_attr[] = 'target="'. esc_attr($btnUrl["target"]) .'"';
				}
				if(  $btnUrl["title"] != "" ){
					$button_attr[] = 'title="'. esc_attr($btnUrl["title"]) .'"';
				}
			?>
			<a <?php echo implode(' ', $button_attr); ?> class="download-resume btn"><?php echo  esc_html($btnUrl['title']); ?></a>
		</div>
	</div>
	<?php return ob_get_clean();
});

// King Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_resume_banner_block', 135 );
function sh_lm_kc_resume_banner_block() {
	if (function_exists('kc_add_map') ) { 
	    kc_add_map(
	        array( 
	            'sh_lm_resume_banner' => array(
	                'name' => esc_html__('Resume Banner Info', 'litmus'),
	                'description' => esc_html__('This Component For Litmus Resume Banner Info', 'litmus'),
	                'icon' => 'et-laptop',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array(
        		    		array(
		                        'name' => 'sayhello',
		                        'label' => esc_html__('Grettings','litmus'),
								'type' => 'text',
								'value' => 'Hello',
		                        'description' => esc_html__('Say Hello for greetings','litmus'),
		                    ),        		    		
		                    array(
		                        'name' => 'sayname',
		                        'label' => esc_html__('Your Name','litmus'),
								'type' => 'text',
								'value' => '<strong>I am Jack</strong> Smith',
		                        'description' => esc_html__('Added your name at here.','litmus'),
							),
							array(
		                        'name' => 'sayme',
		                        'label' => esc_html__('Your Position','litmus'),
								'type' => 'text',
								'value' => 'Web Designer & Developer',
		                        'description' => esc_html__('Added what you doing?','litmus'),
		                    ),        		    		
		                    array(
		                        'name' => 'sayinfo',
		                        'label' => esc_html__('Short Description','litmus'),
								'type' => 'textarea',
								'value' => 'Showcase your best work with powerful tools that take your online portfolio to the next level.',
		                        'description' => esc_html__('Tell short description about you.','litmus'),
							),
							array(
		                        'name' => 'socialarea',
		                        'label' => esc_html__('About Social Media','litmus'),
		                        'type' => 'group',
								'description' => esc_html__('Tell people about your social info','litmus'),
								'params' => array( 
		                    		array(
		                    			'name' => 'icon',
		                    			'type' => 'icon_picker',
		                    			'label' => esc_html__('Icon', 'litmus'),   
		                    		),
		                    		array(
		                    			'name' => 'imageicon',
		                    			'type' => 'attach_image',
		                    			'label' => esc_html__('Or Image Icon', 'litmus'),   
		                    		),
		                    		array(
		                    			'name' => 'url',
		                    			'type' => 'link',
		                    			'label' => esc_html__('Link', 'litmus'),  
		                    		),
		                    	)
		                    ),        		    		
		                    array(
		                        'name' => 'resumebtn',
		                        'label' => esc_html__('Resume Button','litmus'),
		                        'type' => 'link',
		                        'description' => esc_html__('Added you button for resume or anylink','litmus'),
							),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
										"screens" => "any,1024,999,767,479",
										'Hello'	=> array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	 	                						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),              						
										),
										'Name'	=> array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),	 	                						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-name'
	                						),              						
										),
										'MySelf' => array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),	 	                						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-myself'
	                						),              						
										),
										'Description' => array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),	 	                						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-someinfo'
	                						),              						
										),
										'Social' => array(
											array(
	                							'property' => 'color',
	                							'label' => esc_html__('Icon Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'background',
	                							'label' => esc_html__('Icon Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Icon Size','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Icon Aligment','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'line-height',
	                							'label' => esc_html__('Icon Line Height','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
	                						),
											array(
	                							'property' => 'width',
	                							'label' => esc_html__('Icon Width','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'height',
	                							'label' => esc_html__('Icon Height','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'box-shadow',
	                							'label' => esc_html__('Icon Box Shadow','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'boder',
	                							'label' => esc_html__('Icon Border','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'border-radius',
	                							'label' => esc_html__('Icon Border Radius','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
											),
											array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Icon Padding','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a'
	                						),
										),
										'Social:hover' => array(
											array(
	                							'property' => 'color',
	                							'label' => esc_html__('Icon Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a:hover'
											),
											array(
	                							'property' => 'background',
	                							'label' => esc_html__('Icon Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a:hover'
											),
											array(
	                							'property' => 'box-shadow',
	                							'label' => esc_html__('Icon BOx Shadow','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a:hover'
											),
											array(
	                							'property' => 'boder',
	                							'label' => esc_html__('Icon Border Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a:hover'
											),
											array(
	                							'property' => 'border-radius',
	                							'label' => esc_html__('Icon Border Radius','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea li a:hover'
	                						),
										),
										'ResumeBtn' => array(
											array(
	                							'property' => 'color',
	                							'label' => esc_html__('Icon Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'background',
	                							'label' => esc_html__('Icon Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Icon Size','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Icon Aligment','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'line-height',
	                							'label' => esc_html__('Icon Line Height','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
	                						),
											array(
	                							'property' => 'width',
	                							'label' => esc_html__('Icon Width','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'height',
	                							'label' => esc_html__('Icon Height','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'box-shadow',
	                							'label' => esc_html__('Icon Box Shadow','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'boder',
	                							'label' => esc_html__('Icon Border','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'border-radius',
	                							'label' => esc_html__('Icon Border Radius','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Icon Padding','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
	                						),
										),
										'ResumeBtn:hover' => array(
											array(
	                							'property' => 'color',
	                							'label' => esc_html__('Icon Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'background',
	                							'label' => esc_html__('Icon Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'box-shadow',
	                							'label' => esc_html__('Icon BOx Shadow','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'boder',
	                							'label' => esc_html__('Icon Border Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
											),
											array(
	                							'property' => 'border-radius',
	                							'label' => esc_html__('Icon Border Radius','litmus'),
	                							'selector' => '.litmus-resume-banner-info .introduce-socialarea .download-resume'
	                						),
										),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}