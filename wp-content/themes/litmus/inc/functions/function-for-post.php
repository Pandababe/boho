<?php

if ( ! function_exists( 'sh_lm_post_featured_image' ) ) :
function sh_lm_post_featured_image($width = 900, $height = 600, $crop = false) {
    if ( wp_is_mobile() ) {
        $featured_image = sh_lm_aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ,'full' ), 400, 300, false ); 
        if( $featured_image == false ) {
            the_post_thumbnail( 'full', array( 'alt' => get_the_title() ));
        } else { ?>
        <img src="<?php echo esc_url($featured_image); ?>" alt="<?php the_title(); ?>" />
        <?php }
    } else {
        $featured_image = sh_lm_aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ,'full' ), $width, $height, $crop ); 
        if( $featured_image == false ) {
            the_post_thumbnail( 'full', array( 'alt' => esc_attr(get_the_title()) ));
        } else { ?>
        <img src="<?php echo esc_url($featured_image); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
        <?php }
    }
}
endif;

if ( ! function_exists( 'sh_lm_get_image_crop_size' ) ) :
function sh_lm_get_image_crop_size($img_id = false, $width = 900, $height = 600, $crop = false) {
    $url = wp_get_attachment_url( $img_id ,'full' );
    if ( wp_is_mobile() ) {
        $crop_image = sh_lm_aq_resize( $url, 305, 175, false ); 
        if( $crop_image == false ) {
            return $url;
        } else { 
            return $crop_image;
        }
    } else {
        $crop_image = sh_lm_aq_resize( $url, $width, $height, $crop ); 
        if( $crop_image == false ) {
            return $url;
        } else { 
            return $crop_image;
        }
    }
}
endif;

if ( ! function_exists( 'sh_lm_get_image_crop_size_by_id' ) ) :
function sh_lm_get_image_crop_size_by_id($post_id = false, $width = 900, $height = 600, $crop = false) {
    $url = get_the_post_thumbnail_url($post_id, 'full');
    if ( wp_is_mobile() ) { 
        $crop_image = sh_lm_aq_resize( $url, 400, 300, false ); 
        if( $crop_image == false ) {
            return $url;
        } else { 
            return $crop_image;
        }
    } else {
        $crop_image = sh_lm_aq_resize( $url, $width, $height, $crop ); 
        if( $crop_image == false ) {
            return $url;
        } else { 
            return $crop_image;
        }
    }
}
endif;

if ( ! function_exists( 'sh_lm_theme_kc_custom_link' ) ) :
function sh_lm_theme_kc_custom_link( $link, $default = array( 'url' => '', 'title' => '', 'target' => '' ) ) {
    $result = $default;
    $params_link = explode('|', $link);

    if( !empty($params_link) ){
        $result['url'] = rawurldecode(isset($params_link[0])?$params_link[0]:'#');
        $result['title'] = isset($params_link[1])?$params_link[1]:'';
        $result['target'] = isset($params_link[2])?$params_link[2]:'';
    }

    return $result;
}
endif;

//Adding Custom language menu for WPML
if ( ! function_exists( 'sh_km_wpml_custom_language_var' ) ) :
function sh_km_wpml_custom_language_var() {
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)) { ?> 
        <ul class="language-submeu">
        <?php foreach($languages as $lang) { ?>
            <li class="menu-item menu-item-language">
                <a href="<?php echo esc_attr($lang['url']);?>">
                    <img src="<?php echo esc_attr($lang['country_flag_url']); ?>" alt="<?php echo esc_html($lang['native_name']); ?>" />
                    <?php echo esc_html($lang['native_name']); ?>
                </a>
            </li>;
        <?php } ?>
        </ul>
    <?php }
}
endif;