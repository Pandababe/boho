<?php 
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

$protocol = is_ssl() ? 'https' : 'http';

// enqueue style
if ( !defined( 'FW' ) ) :
wp_enqueue_style( 'sh-lm-google-font', $this->google_fonts_url(), array(), '1.0' );
endif;
// All Plug-in CSS  
wp_enqueue_style('bootstrap', LITMUS_TEMPLATE_DIR_URL . "/assets/css/bootstrap.min.css");
wp_enqueue_style('bsmaterial', LITMUS_TEMPLATE_DIR_URL . "/assets/css/bsmaterial.min.css");
wp_enqueue_style('owl-carousel', LITMUS_TEMPLATE_DIR_URL . "/assets/css/owl.carousel.min.css");
wp_enqueue_style('magnific-popup', LITMUS_TEMPLATE_DIR_URL . "/assets/css/magnific-popup.css");

wp_enqueue_style('sh-lm-icons', LITMUS_TEMPLATE_DIR_URL . "/assets/css/icons.min.css");

wp_deregister_style('rs-icon-set-pe-7s-css');

wp_enqueue_style('sh-lm-pe-icon-stroke', LITMUS_TEMPLATE_DIR_URL . "/assets/fonts/pe-icon-stroke/css/pe-icon-stroke.css");
wp_enqueue_style('sh-lm-linea-arrows', LITMUS_TEMPLATE_DIR_URL . "/assets/css/linea-arrows.css");
wp_enqueue_style('sh-lm-litmus-icon', LITMUS_TEMPLATE_DIR_URL . "/assets/css/litmus-icon.css");

wp_enqueue_style('sh-lm-main', LITMUS_TEMPLATE_DIR_URL . "/assets/css/style.css");
wp_enqueue_style( 'sh-lm-style', get_stylesheet_uri() ); 

// enqueue scripts
wp_enqueue_script('modernizr-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/modernizr.min.js', array("jquery"), false, true);
wp_enqueue_script('popper-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/popper.min.js', array("jquery"), false, true);
wp_enqueue_script('bootstrap-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/bootstrap.min.js', array("jquery"), false, true);
wp_enqueue_script('bsmkit-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/bsmkit.min.js', array("jquery"), false, true);
wp_enqueue_script('owl-carousel-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/owl.carousel.min.js', array("jquery"), false, true);
wp_enqueue_script('jquery-fitvids-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/isotope.pkgd.min.js', array("jquery"), false, true);
wp_enqueue_script('magnific-popup-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/jquery.magnific-popup.min.js', array("jquery"), false, true);

wp_enqueue_script('hoverdir', LITMUS_TEMPLATE_DIR_URL . '/assets/js/jquery.hoverdir.js', array("jquery"), false, true);
wp_enqueue_script('fitvids', LITMUS_TEMPLATE_DIR_URL . '/assets/js/jquery.fitvids.js', array("jquery"), false, true);
wp_enqueue_script('ajaxchimp', LITMUS_TEMPLATE_DIR_URL . '/assets/js/ajaxchimp.js', array("jquery"), false, true);
wp_enqueue_script('syoTimer', LITMUS_TEMPLATE_DIR_URL . '/assets/js/syoTimer.js', array("jquery"), false, true);

wp_enqueue_script("jquery-form");
wp_enqueue_script("form-validate", LITMUS_TEMPLATE_DIR_URL .'/assets/js/jquery.validate.min.js', array('jquery'), false, true);

wp_enqueue_script('sh-lm-function-js', LITMUS_TEMPLATE_DIR_URL . '/assets/js/litmus.js', array("jquery"), false, true);
wp_enqueue_script('sh-lm-custom-js', LITMUS_TEMPLATE_DIR_URL . '/assets/custom/custom.js', array("jquery"), false, true);

wp_localize_script("sh-lm-function-js", "litmus", array (
        'ajaxurl' => admin_url( "admin-ajax.php" ), 
        'newsletter_form_action_url' => sh_lm_get_customizer_field('newsletter_form_action_url'), 
    )
);

if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
}