<?php
add_shortcode( 'sh_lm_dining_menu', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'title' => '',  
		'icon' => '',  
		'cats' => '',  
		'tab' => 'hide',  
		'number' => '',  
	), $atts));  
	ob_start(); ?>  
	<section class="our-menu-block">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content mrb-60">
	                <h2 class="section-title no-border"><?php echo esc_html($title); ?></h2>
	                <div class="resturent-title-border">
	                    <i class="<?php echo esc_attr($icon); ?>"></i>
	                </div>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	        <div class="row">
	            <div class="col-md-12">
	            	<?php 
                    $product_cat_slugs = array(); 
                    if(!empty($cats)) {
                        $catss = explode(",", $cats);  

                        if($tab == 'show') { echo '<ul class="resturent-tab-v2 bsm-tabs">'; }

                        foreach ( $catss as $cat ) {  
                            $cat = explode(":", $cat); 
                            $product_cat_slugs[] = $cat[0]; 
                            if($tab == 'show') { ?>
								<li class="bsm-tab"><a href="#dining-menu-<?php echo esc_attr($cat[0]); ?>"><?php echo esc_html($cat[1]); ?></a></li>
                            <?php }
                        } 

                        if($tab == 'show') { echo '</ul>'; }
                	}    

                	if($tab == 'show') { 
                        if(!empty($cats)) { 
                            $cats_two = explode(",", $cats);  
                            foreach ( $cats_two as $cat_two ) {  
                                $cat_two = explode(":", $cat_two); 
                                echo '<div class="bsm-tab-content" id="dining-menu-'.$cat_two[0].'">';
                                sh_lm_dining_menu($cat_two[0], $number, $tab); 
                                echo '</div>';
                            }  
                    	}     
	            	} else { sh_lm_dining_menu($product_cat_slugs, $number, $tab); } ?>

	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.our-menu-block -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_dining_menu', 111 );
function sh_lm_kc_dining_menu() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_dining_menu' => array(
	                'name' => esc_html__('Dining Menu', 'litmus'),
	                'description' => esc_html__('Dining Menu', 'litmus'),
	                'icon' => 'sl-book-open',
	                'category' => 'Litmus',
	                'params' => array( 
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'Breakfast',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'icon',
	                		'type' => 'icon_picker',
	                		'label' => esc_html__('Icon', 'litmus'),  
	                		'value' => 'lit lit-restaurent-knife',  
	                	),
	                	array(
	                	    'type'          => 'autocomplete',
	                	    'label'         => esc_html__('Select Category', 'litmus'),
	                	    'name'          => 'cats',
	                	    'options'       => array( 
	                	        'multiple'      => true, 
	                	        'post_type'     => 'product', 
	                	        'taxonomy'      => 'product_cat', 
	                	    ), 
	                	),
	                	array(
	                		'name' => 'tab',
	                		'label' => esc_html__('Category Tab', 'litmus'),  
	                		'type' => 'select',  
	                		'options' => array(  
	                			'show' => esc_html__('Show', 'litmus'), 
	                			'hide' => esc_html__('Hide', 'litmus'),  
	                		),
	                		'value' => 'hide',
	                	),
	                    array(
	                    	'name' => 'number',
	                    	'label' => esc_html__('Total Post', 'litmus'), 
	                    	'type' => 'number_slider',  
	                    	'options' => array(  
	                    		'min' => 1,
	                    		'max' => 20, 
	                    		'show_input' => true
	                    	), 
	                    	'value' => 6
	                    ),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}