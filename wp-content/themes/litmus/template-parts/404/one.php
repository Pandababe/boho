<?php
/**
 * This template for displaying 404 page part
 *
 * @package Litmus
 */
?>
<!-- 404 Page Content
================================================== -->
<section class="error-page bg-cover">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 text-center">
                <h2 class="error-title"><?php esc_html_e('404 Not Found', 'litmus'); ?></h2><!--  /.error-title -->
                <h3 class="error-subtitle"><?php esc_html_e('Something appears to be missing', 'litmus'); ?></h3>
                <p class="error-desc"><?php esc_html_e('We\'re sorry for the inconvenience, please use the button below to go back', 'litmus'); ?></p>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="go-back-btn"><?php esc_html_e('Go back to home', 'litmus'); ?></a>
            </div><!--  /.col-12 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.error-page -->