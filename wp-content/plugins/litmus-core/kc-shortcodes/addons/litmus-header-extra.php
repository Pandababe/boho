<?php
add_shortcode( 'sh_lm_header_extra', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'searchmenu' => '',  
		'searchicon' => '',  
		'showlogin' => '',
		'loginicon' => '',  
		'showwoomenu' => '',  
		'woomenuicon' => '',  
		'showhamburger' => '',   
		'hamburgericon' => '',  
	), $atts));  
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>
	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		<div class="header-right header-extra-block">                    
			<ul class="right-menu">
				<?php if( $searchmenu == 'yes' ) { ?>
				<li><a href="#" class="search-extend"><i class="<?php echo esc_attr($searchicon); ?>"></i></a></li>	
				<?php } ?>
				<?php if( $showlogin == 'yes' ) { ?>	
				<li><a href="#login-register" class="user-registration" data-toggle="modal" data-target="#login-register"><i class="<?php echo esc_attr($loginicon); ?>"></i></a></li>
				<?php } ?>
				<?php if( $showwoomenu == 'yes' ) { ?>		
				<li>
				<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
					<a class="user-cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart','litmus' ); ?>"><i class="<?php echo esc_attr($woomenuicon); ?>"></i><span class="budge"><?php echo sprintf( _n( '%d', '%d', WC()->cart->get_cart_contents_count(), 'litmus' ), WC()->cart->get_cart_contents_count() ); ?></span></a>
				<?php } ?>
				</li>
				<?php } ?>
				<?php if( $showhamburger == 'yes' ) { ?>
				<li><a href="#" class="hamburger-menu"><i class="<?php echo esc_attr($hamburgericon); ?>"></i></a></li>
				<?php } ?>
			</ul>
		</div>

		<!-- Hamburger Menu
		================================================== -->
		<?php if( $showhamburger == 'yes' ) { ?>
		<div class="header-extra-block">
			<div class="hamburger-block"> 
				<div class="overlaybg"></div>
				<div class="hamburger-block-container expand-block">
					<div class="hamburger-search-form">
						<?php get_search_form(); ?>
					</div><!--  /.hamburger-search-form -->
					<!-- Mobile Nav Close -->
					<div class="close-menu">
						<i class="pe-7s-close"></i>
					</div>
					
					<!-- Main Menu Mobile -->
					<div class="main-mobile-menu" id="mobile-main-menu"></div>

					<!-- Hamburger Menu Wrapper -->
					<div class="hamburger-wrapper">
						<!-- Hamburger Menu Content -->
						<div class="hamburger-content">
							<?php 
								wp_nav_menu ( array(
									'menu_class' => 'hamburger-mainmenu',
									'container'=> 'ul',
									'theme_location' => 'hamburger-menu', 
									'walker'    => new LITMUS_Walker_Nav_Menu(), 
									'fallback_cb'    => 'LITMUS_Walker_Nav_Menu::menu_callback' 
								));
							?>
						</div> <!-- /.hours-content-->
					</div> <!-- /.menu-wrapper --> 
				</div><!-- /.site-navigation -->
			</div><!--  /.hamburger-block -->
		</div>
		<?php } ?>

		<!-- Header Modal
		================================================== -->
		<?php if( $searchmenu == 'yes' ) { ?>
		<div class="header-extra-block">
			<div class="overlay-search">
				<button type="button" class="overlay-close"><i class="pe-7s-close"></i></button>
				<div class="header-search-content">
					<div class="container">            
						<div class="row justify-content-md-center">            
							<div class="col-sm-12 col-md-8">
								<h4 class="search-title"><?php esc_html_e('What are you looking for?', 'litmus'); ?></h4>
								<?php get_search_form(); ?>
								<h5 class="search-footer-title">
									<?php echo wp_kses( __('Press <span>enter</span> to confirm search term', 'litmus'), LITMUS_Static::html_litmusw() ); ?>
								</h5><!-- /.search-footer-title -->
							</div>
						</div>
					</div><!-- /.container -->
				</div><!-- /.header-search-content -->
			</div><!-- /.header-search-content --> 
		</div>
		<?php } ?>
	</div>
	<?php return ob_get_clean();
});

// King Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_header_extra_block', 132 );
function sh_lm_kc_header_extra_block() {
	if (function_exists('kc_add_map') ) { 
	    kc_add_map(
	        array( 
	            'sh_lm_header_extra' => array(
	                'name' => esc_html__('Litmus Header Extra', 'litmus'),
	                'description' => esc_html__('This Component For Extra Header Component For Layout Builder', 'litmus'),
	                'icon' => 'sl-key',
	                'category' => 'Litmus Layout',
	                'params' => array(
	                	'general' => array(
        		    		array(
		                        'name' => 'searchmenu',
		                        'label' => esc_html__('Search','litmus'),
    			    			'relation' => array( 
	    			    			'parent'    => 'showhamburger',
	    			    			'show_when' => 'yes',
    			    			),
		                    ),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
										"screens" => "any,1024,999,767,479",
										'Basic'	=> array(
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.header-extra-block.header-right'
	                						),                						
	                						array(
	                							'property' => 'float',
	                							'label' => esc_html__('Float','litmus'),
	                							'selector' => '.header-extra-block.header-right'
											),
											array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Content Padding','litmus'),
	                							'selector' => '.header-extra-block.header-right'
											),	 
											array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Content Margin','litmus'),
	                							'selector' => '.header-extra-block.header-right'
											), 
											array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Content Item Padding','litmus'),
	                							'selector' => '.header-extra-block.header-right li a'
											),	 
											array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Content Item Margin','litmus'),
	                							'selector' => '.header-extra-block.header-right li a'
	                						),               						
	                					),
	                					'Search'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Search Icon Color','litmus'),
	                							'selector' => '.header-extra-block .right-menu a.search-extend'
	                						),                						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Search Overlay Background','litmus'),
	                							'selector' => '.header-extra-block .overlay-search'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Close Button Color','litmus'),
	                							'selector' => '.header-extra-block .overlay-search .overlay-close'
	                						),	 	                						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Input Background','litmus'),
	                							'selector' => '.header-extra-block .overlay-search .form-controller'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Search Title Color','litmus'),
	                							'selector' => '.header-extra-block .overlay-search .search-title'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Search Footer Title Color','litmus'),
	                							'selector' => '.header-extra-block .search-footer-title'
	                						),						
	                					),
	                					'Login'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Icon Color','litmus'),
	                							'selector' => '.header-extra-block .user-registration'
	                						),                						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Modal Background','litmus'),
	                							'selector' => '.header-extra-block .login-register-modal .modal-content, .login-register-modal .bsm-tabs'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Text Color','litmus'),
	                							'selector' => '.header-extra-block input, .header-extra-block label'
	                						),	                 						
	                					),	                					
	                					'WooCommerce'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Icon Color','litmus'),
	                							'selector' => '.header-extra-block .right-menu li a'
	                						),	 
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Badge Background','litmus'),
	                							'selector' => '.header-extra-block .user-cart .budge'
	                						),              									
	                					),
	                					'Hamburger'	=> array(
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.header-extra-block .hamburger-menu.mobile-hamburger .expand-block'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Float','litmus'),
	                							'selector' => '.header-extra-block .hamburger-menu.mobile-hamburger .close-menu, .header-extra-block .hamburger-menu.mobile-hamburger .hamburger-search-form .btn.btn-default i, .header-extra-block .hamburger-menu.mobile-hamburger .expand-block .hamburger-content ul li a'
	                						),              						
	                					),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}