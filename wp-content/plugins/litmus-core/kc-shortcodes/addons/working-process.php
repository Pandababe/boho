<?php
add_shortcode( 'sh_lm_working_progess', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'title' => '', 
		'working_progess' => '', 
	), $atts));  
	ob_start(); ?>   
	<section class="work-in-progress bg-snow">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-12">
	                <h2 class="section-title"><?php echo esc_html($title); ?></h2>
	            </div><!--  /.col-12 -->
	        </div><!--  /.row -->

	        <div class="row">
				<?php 
	        	if(!empty($working_progess)) {
        		$i = 1;
	        	foreach ($working_progess as $services) { 
	        	?>
	            <div class="col-md-3 process-item">
	                <h2 class="process-serial"><?php if($i < 10) echo '0'; ?><?php echo esc_html($i); ?></h2><!--  /.process-serial -->
	                <div class="process-box">
	                    <h3 class="process-title"><?php echo esc_html($services->title); ?></h3><!--  /.process-title -->
	                    <p><?php echo esc_html($services->desc); ?></p>
	                </div><!--  /.process-box -->
	            </div><!--  /.col-md-3 --> 
	            <?php $i++; } } ?>

	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.work-in-progress -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_working_progess', 107 );
function sh_lm_kc_working_progess() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_working_progess' => array(
	                'name' => esc_html__('Working Process', 'litmus'),
	                'description' => esc_html__('Working Process', 'litmus'),
	                'icon' => 'sl-loop',
	                'category' => 'Litmus',
	                'params' => array(
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'Working Process',  
	                		'admin_label' => true,
	                	),
	                    array(
	                    	'name'			=> 'working_progess',
	                    	'type'			=> 'group',
	                    	'label'			=> esc_html__('Working Process', 'litmus'), 
	                    	'options'		=> array('add_text' => esc_html__('Add new process', 'litmus')), 
	                    	'value' => base64_encode( json_encode( array(
	                    		"1" => array( 
	                    			"title" => "Customer Request",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"2" => array( 
	                    			"title" => "Up the design idea",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"3" => array( 
	                    			"title" => "Customer acceptance",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"4" => array( 
	                    			"title" => "Complete",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		)
	                    	))), 
	                    	'params' => array(  
	                    		array(
	                    			'name' => 'title',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Title', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    		array(
	                    			'name' => 'desc',
	                    			'type' => 'textarea',
	                    			'label' => esc_html__('Short Description', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    	)
	                    )
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}