<?php
add_shortcode( 'sh_lm_best_seller_two_slider', function($atts, $content = null) {
	extract(shortcode_atts(array(   
		'title' => '',     
		'desc' => '',     
		'number' => '',  
	), $atts));  
	ob_start(); ?>  
	<?php 
		extract($atts);
		// Create master class, return as an array
		$master_class = apply_filters( 'kc-el-class', $atts );
	?>
	<section class="woo-shop-best-seller <?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content">
	            	<?php 
	            		$array_from_to = array(
	            			'&lt;' => '<', 
	            			'&gt;' => '>', 
	            		);
	            		$title = strtr($title, $array_from_to);
	            		$desc = strtr($desc, $array_from_to);
	            	?>
	                <h2 class="section-title no-border v-3"><?php 
                            echo wp_kses( $title, LITMUS_Static::html_allow() );
                        ?></h2>
	                <p class="sub-section-title v-2"><?php 
                            echo wp_kses( $desc, LITMUS_Static::html_allow() );
                        ?></p>
	                <div class="mrb-45"></div><!--  /.mrb -->
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	        <div class="row">
	            <div class="col-md-12">
	                <div class="woo-best-seller-slider owl-carousel">
	                	<?php  
	                	$product_query = new WP_Query(
	                	    array(
	                	        'post_type' => 'product', 
	                	        'meta_key' => 'total_sales',
	                	        'orderby' => 'meta_value_num',
	                	        'posts_per_page' => $number,   
	                	    )
	                	); 
	                	while ($product_query->have_posts()) : $product_query->the_post(); 
	                		$only_item_class = true; 
	                	    require get_template_directory().'/woocommerce/template-parts/content-product.php'; 
						endwhile; wp_reset_postdata(); ?> 
	                </div><!--  /.woo-best-seller-slider -->
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.woo-shop-best-seller -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_best_seller_two_slider', 110 );
function sh_lm_kc_best_seller_two_slider() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_best_seller_two_slider' => array(
	                'name' => esc_html__('Best Seller Two Slider', 'litmus'),
	                'description' => esc_html__('Product Best Seller Two Slider', 'litmus'),
	                'icon' => 'sl-pie-chart',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array(
                			array(
                				'name' => 'title',
                				'type' => 'text',
                				'label' => esc_html__('Title', 'litmus'),  
                				'value' => '<span>Best</span> Seller',  
                				'admin_label' => true,
                			),
                			array(
                				'name' => 'desc',
                				'type' => 'text',
                				'label' => esc_html__('Short Description', 'litmus'),  
                				'value' => 'Etiam congue rhoncus gravida. Sed vel sodales tortor. Donec eget dictum enim. Donec tempus euismod metus ac maximus, aenean mattis.',  
                			),
                		    array(
                		    	'name' => 'number',
                		    	'label' => esc_html__('Total Post', 'litmus'), 
                		    	'type' => 'number_slider',  
                		    	'options' => array(  
                		    		'min' => 1,
                		    		'max' => 20, 
                		    		'show_input' => true
                		    	), 
                		    	'value' => 6
                		    ),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
	                					"screens" => "any,1024,999,767,479",
	                					'HeadingSpan'	=> array(
	                						array(
	                							'property' => 'color', 
	                							'label' => 'Select Highlighted Text Color', 
	                							'selector' => '.section-title span'
	                						),
	                						array(
	                							'property' => 'font-family', 
	                							'label' => 'Select Highlighted Font Family', 
	                							'selector' => '.section-title span'
	                						),
	                					),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}