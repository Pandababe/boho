<?php 
/**
 * Some theme and plugin funcitons goes here
 */

function sh_lm_dining_menu($cat_slugs, $number, $tab) { ?>
    <div class="regular-menu">
        <div class="row">
            <?php  
            $product_query = new WP_Query(
                array(
                    'post_type' => 'product', 
                    'posts_per_page' => $number,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field' => 'slug',
                            'terms' => $cat_slugs,
                        )
                    ),  
                )
            ); 
            while ($product_query->have_posts()) : $product_query->the_post(); ?>
            <div class="col-lg-6">
                <div class="restu-menu-item menu-v2">                                   
                    <?php if ( has_post_thumbnail() ) { ?> 
                    <figure class="menu-thumbnail">
                        <a href="<?php the_permalink(); ?>">
                            <?php sh_lm_post_featured_image(120, 120); ?>            
                        </a> 
                    </figure>
                    <?php } ?> 
                    <div class="menu-desc row">
                        <div class="left-part col-6 col-md-6 col-xl-7">
                            <h2 class="menu-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <div class="menu-meta">
                                <div class="menu-cat">
                                    <?php 
                                    $product_cats = get_the_terms( get_the_ID(), 'product_cat' );
                                    if(!empty($product_cats)) { ?>
                                    <a href="<?php echo esc_url(get_term_link($product_cats[0]->slug, 'product_cat')); ?>" rel="category tag"><?php echo esc_html($product_cats[0]->name); ?></a>  
                                    <?php } ?> 
                                </div><!--  /.menu-cat -->
                            </div><!--  /.menu-meta -->
                        </div><!--  /.left-part -->
                        <div class="right-part col-6 col-md-6 col-xl-5">
                            <div class="menu-price">
                                <div class="price"><?php sh_lm_product_price(); ?></div><!--  /.price -->
                                <div class="order-block">
                                    <?php sh_lm_order_n_favorite_btn( get_the_ID() ); ?>
                                </div><!--  /.order-block -->
                            </div><!--  /.menu-price -->
                        </div><!--  /.right-part -->
                    </div><!--  /.menu-desc -->
                </div><!--  /.restu-menu-item --> 
            </div><!--  /.col-lg-6 -->   
            <?php endwhile; wp_reset_postdata(); ?> 
        </div><!--  /.row -->
    </div><!--  /.regular-menu -->
<?php 
    if($tab == 'show') { echo '<a href="'.esc_url( get_term_link($cat_slugs, 'product_cat') ).'" class="show-all-btn gray-border">'.esc_html__( 'Show All', 'litmus' ).'</a>'; }
}


function sh_lm_order_n_favorite_btn($id = null) { ?>
    <a rel="nofollow" href="#" data-quantity="1" data-product_id="<?php echo esc_attr($id); ?>" data-product_sku="" class="order-btn button product_type_simple add_to_cart_button ajax_add_to_cart"><?php esc_html_e('Order', 'litmus'); ?></a>
     
    <a href="#" class="wishlist-btn"><?php sh_lm_product_favorite_btn( get_the_ID() ); ?></a>
<?php }
