<?php

/* Add Instagram Store tab */
add_filter( 'woocommerce_product_data_tabs', 'woo_instashop_product_tab' );
function woo_instashop_product_tab( $tabs ) {
    $tabs['instashop'] = array(
        'label' 	=> __( 'Instagram Shop', 'woocommerce' ),
        'target' 	=> 'woo_instashop_content',
        'class'  => array()
    );

    return $tabs;
}


/* Add Instagram Store tab content */
add_action('woocommerce_product_data_panels', 'instashop_tab_options');
function instashop_tab_options() {
    global $post;

    $custom_tab_options = array(
        '_instashop_hashtag' => get_post_meta($post->ID, '_instashop_hashtag', false),
        '_instashop_action' => get_post_meta($post->ID, '_instashop_action', true),
        '_instashop_link' => get_post_meta($post->ID, '_instashop_link', true),
        '_instashop_link_text' => get_post_meta($post->ID, '_instashop_link_text', true),
        '_instashop_new_window' => get_post_meta($post->ID, '_instashop_new_window', true),
    );
    $custom_tab_options['_instashop_hashtag'] = implode(',',$custom_tab_options['_instashop_hashtag']);
    ?>
    <div id="woo_instashop_content" class="panel woocommerce_options_panel">
        <div class="options_group instashop_tab_options">
            <p class="form-field">
                <label for="_instashop_hashtag"><?php _e('Hash Tag(s):', 'wc_instashop'); ?><br /><?php _e('(without # symbol)', 'wc_instashop'); ?></label>
                <input type="text" size="5" name="_instashop_hashtag" value="<?php echo @$custom_tab_options['_instashop_hashtag']; ?>" placeholder="<?php _e('Enter hash tag for this product (# will be added automatically)', 'wc_instashop'); ?>" />
            </p>
            <p class="form-field">
                <label><?php _e('Action Button:', 'wc_instashop'); ?></label>
                <select name="_instashop_action" class="select short">
                    <option value="product" <?php echo (@$custom_tab_options['_instashop_action']=="product"?'selected="selected"':''); ?>><?php _e("Redirect to product page","wc_instashop"); ?></option>
                    <option value="cart" <?php echo (@$custom_tab_options['_instashop_action']=="cart"?'selected="selected"':''); ?>><?php _e("Add to cart action","wc_instashop");?></option>
                    <option value="redirect" <?php echo (@$custom_tab_options['_instashop_action']=="redirect"?'selected="selected"':''); ?>><?php _e("Redirect to custom link","wc_instashop");?></option>
                </select>
            </p>
            <p class="form-field show-redirect" <?php echo (@$custom_tab_options['_instashop_action']!="redirect"?'style="display:none;"':''); ?>>
                <label><?php _e('Link:', 'wc_instashop'); ?></label>
                <input type="text" size="5" name="_instashop_link" value="<?php echo @$custom_tab_options['_instashop_link']; ?>" placeholder="<?php _e('Where to redirect the user', 'wc_instashop'); ?>" />
            </p>
            <p class="form-field show-redirect" <?php echo (@$custom_tab_options['_instashop_action']!="redirect"?'style="display:none;"':''); ?>>
                <label><?php _e('Link Text:', 'wc_instashop'); ?></label>
                <input type="text" size="5" name="_instashop_link_text" value="<?php echo @$custom_tab_options['_instashop_link_text']; ?>" placeholder="<?php _e('Link text', 'wc_instashop'); ?>" />
            </p>
            <p class="form-field show-redirect" <?php echo (@$custom_tab_options['_instashop_action']!="redirect"?'style="display:none;"':''); ?>>
                <label><?php _e('Open in New Window:', 'wc_instashop'); ?></label>
                <select name="_instashop_new_window" class="select short">
                    <option value="1" <?php echo (@$custom_tab_options['_instashop_new_window']==1?'selected="selected"':''); ?>><?php _e("Yes","wc_instashop"); ?></option>
                    <option value="0" <?php echo (@$custom_tab_options['_instashop_new_window']==0?'selected="selected"':''); ?>><?php _e("No","wc_instashop"); ?></option>
                </select>
            </p>
        </div>
    </div>
    <?php
}

/* Add action to save field in WooCommerce Instagram Shop tab */
function process_product_meta_instashop( $post_id ) {
    $tagsArr = explode(',',$_POST['_instashop_hashtag']);
    delete_post_meta($post_id, '_instashop_hashtag');
    foreach($tagsArr as $tag) {
        if(!empty($tag)) {
            add_post_meta( $post_id, '_instashop_hashtag', $tag);
        }
    }
    update_post_meta( $post_id, '_instashop_action', $_POST['_instashop_action']);
    update_post_meta( $post_id, '_instashop_link', $_POST['_instashop_link']);
    update_post_meta( $post_id, '_instashop_link_text', $_POST['_instashop_link_text']);
    update_post_meta( $post_id, '_instashop_new_window', $_POST['_instashop_new_window']);
}
add_action('woocommerce_process_product_meta', 'process_product_meta_instashop', 10, 2);

/* Add stylesheets for front end*/
wp_register_style('instashop', plugin_dir_url(__FILE__).'assets/css/instashop.css');
wp_enqueue_style('instashop');

function instashop_tagit_tiptip_admin_scripts() {
    /* Add stylesheets */
    wp_enqueue_style( 'woocommerce_admin_styles', WC()->plugin_url() . '/assets/css/admin.css', array(), WC_VERSION );

    wp_register_style('tagit', plugin_dir_url(__FILE__).'assets/css/jquery.tagit.css');
    wp_enqueue_style('tagit');
    wp_register_style('tagit-zendesk', plugin_dir_url(__FILE__).'assets/css/tagit.ui-zendesk.css');
    wp_enqueue_style('tagit-zendesk');

    /* Add javascript */
    wp_register_script( 'tagit', plugins_url( '/assets/js/tag-it.min.js', __FILE__ ) );
    wp_enqueue_script( 'tagit' );
    wp_register_script( 'tiptip', plugins_url( '../woocommerce/assets/js/jquery-tiptip/jquery.tipTip.min.js', __FILE__ ) );
    wp_enqueue_script( 'tiptip' );
    wp_register_script( 'instashop', plugins_url( '/assets/js/main.js', __FILE__ ) );
    wp_enqueue_script( 'instashop' );
}

add_action( 'admin_enqueue_scripts', 'instashop_tagit_tiptip_admin_scripts' );
?>