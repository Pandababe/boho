<?php
/**
 * The template for displaying all single posts.
 *
 * @package Litmus
 */
get_header(); 
sh_lm_page_header( esc_html__('Read Our Full Stories', 'litmus') ); ?>

<!-- Blog Page Content
================================================== -->
<section class="blog-section-content pd-tb-90">
    <div class="container">
        <div class="row">
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-lg-9 order-lg-9 blog-post-ctn-area">
                <div class="list-of-blog blog-single-page">
                    <div class="row">
                        <div class="col-md-12">
                            <article class="post"> 
                                <?php if ( has_post_thumbnail() ) { ?>
                                <figure class="article-thumb"> 
                                    <?php sh_lm_post_featured_image(850, 500, true); ?> 
                                </figure> <!-- /.article-thumb -->
                                <?php } ?>
                                <div class="article-header">
                                    <div class="entry-meta">
                                        <div class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></div><!-- /.entry-date -->
                                        <div class="entry-cat"><?php the_category(', ' ); ?></div><!-- /.entry-cat -->
                                        <?php if( has_tag() ): ?>    
                                        <div class="entry-tag"> <?php the_tags('',', ',''); ?></div><!--  /.entry-tag -->
                                        <?php endif; ?> 
                                    </div><!-- /.entry-meta -->
                                    <h2 class="entry-title"><?php the_title(); ?></h2>
                                </div><!-- /.article-header -->
                                <div class="article-content">
                                    <?php 
                                        the_content(); 
                                        sh_lm_wp_link_pages(); 
                                    ?>
                                </div><!--  /.article-content -->
                                <div class="entry-share"> 
                                    <?php if ( function_exists( 'sh_lm_social_share_link' ) ) {
                                        sh_lm_social_share_link( esc_html__('Share:', 'litmus') );
                                    } ?>
                                </div>
                            </article><!--  /.post -->

                            <?php
                                // If comments are open or we have at least one comment, load up the comment template
                                if ( comments_open() || get_comments_number() ) :
                                  comments_template();
                                endif;
                            ?> 
                        </div><!--  /.col-md-12 -->
                    </div><!--  /.row -->
                </div><!--  /.list-of-blog -->
            </div><!--  /.col-lg-9 -->
            <?php endwhile; ?>
            <div class="col-lg-3 order-lg-3">
                <?php get_sidebar(); ?>
            </div><!--  /.col-lg-3 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.blog-section-content -->
<?php get_footer(); ?>