<?php 
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

class LITMUS_Backend_Master {

    public static $partial_path;

    public function __construct() {  
        self::$partial_path = LITMUS_TEMPLATE_DIR . '/inc/classes/backend/partials/';

        // Add after_setup_theme() for specific functions. 
        add_action( 'after_setup_theme', array( $this, 'theme_setup_core' ) );

        // Register all admin scripts and styles
        add_action( 'admin_enqueue_scripts', array($this, 'scripts_and_styles') );

        // Register our Widgets
        add_action( 'widgets_init', array( $this, 'widgets_init' ) );
    }

    /** 
     * @since 1.0
     */
    public function theme_setup_core() {
        require self::$partial_path . 'action_after_setup_theme.php';
    }

    /** 
     * @since 1.0
     */
    public function scripts_and_styles($hook) {
        require self::$partial_path . 'action_admin_enqueue_scripts.php';
    }

    /** 
     * @since 1.0
     */
    public function widgets_init() {
        require self::$partial_path . 'action_widgets_init.php';
    }

}
//if( is_admin() ) { 
    new LITMUS_Backend_Master; 
//}
