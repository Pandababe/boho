<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['id'] = 'litmus';

$manifest['supported_extensions'] = array(
    //'page-builder' => array(), 
	'backups' => array(),
    'breadcrumbs' => array(),
);
