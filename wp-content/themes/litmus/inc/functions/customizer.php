<?php
/**
 * Litmus Theme Customizer
 *
 * @package Litmus
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function sh_lm_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

    $wp_customize->selective_refresh->add_partial( 'blogname', array(
        'selector' => '.site-title a',
        //'render_callback' => 'twentyseventeen_customize_partial_blogname',
    ) );
   
    $wp_customize->selective_refresh->add_partial( 'fw_options[pagination]', array(
        'selector' => '.site-description',
    ) );

    $wp_customize->remove_control('header_textcolor');

}
add_action( 'customize_register', 'sh_lm_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function sh_lm_customize_preview_js() {
	wp_enqueue_script( 'sh_lm_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'sh_lm_customize_preview_js' );
