<?php
add_shortcode( 'sh_lm_reservation_form', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'rcv_email_id' => '',  
		'title_top' => '',  
		'title' => '',  
		'icon' => '',  
		'desc' => '',  
		'phone' => '',  
		'btn_title' => '',  
	), $atts));  
	ob_start(); ?>    
	<section class="make-reservation pd-tb-90 bg-nero">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content">
	                <h3 class="sub-section-title color-citrus"><?php echo esc_html($title_top); ?></h3>
	                <h2 class="section-title no-border color-white"><?php echo esc_html($title); ?></h2>
	                <div class="resturent-title-border dark-border">
	                    <i class="<?php echo esc_attr($icon); ?>"></i>
	                </div>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	        <div class="row">
	            <div class="col-md-12">
	                <form action="#" class="reservation-form citrus-theme" id="reservation-form">
	                    <div class="row">
	                    	<input type="hidden" name="action" value="sh_lm_reservation_mail_form" />
		                    <input type="hidden" name="receiver_mail_id" value="<?php echo esc_attr($rcv_email_id); ?>" />
		                    <?php wp_nonce_field( 'lm_reservation_form_action', 'lm_reservation_form_field' ); ?>
	                        <div class="col-md-3">
	                            <i class="fa fa-user-o"></i>
	                            <input type="text" name="person" class="form-contol" required="required" id="book-person" placeholder="<?php esc_attr_e('Person', 'litmus'); ?>" />
	                        </div><!--  /.col-md-3 -->
	                        <div class="col-md-3">
	                            <i class="fa fa-calendar"></i>
	                            <input type="text" value="" name="date" class="form-contol date-selector" required="required" id="book-date" placeholder="<?php esc_attr_e('Date', 'litmus'); ?>" />
	                        </div><!--  /.col-md-3 -->
	                        <div class="col-md-3">
	                            <i class="fa fa-clock-o"></i>
	                            <input type="time" name="time" class="form-contol time-selector" required="required" id="book-time" placeholder="<?php esc_attr_e('Time', 'litmus'); ?>" />
	                        </div><!--  /.col-md-3 -->
	                        <div class="col-md-3">
	                            <i class="fa fa-phone-square"></i>
	                            <input type="number" name="phone" class="form-contol" id="book-phone" required="required" placeholder="<?php esc_attr_e('Phone Number', 'litmus'); ?>" />
	                        </div><!--  /.col-md-3 -->
	                    </div><!--  /.row -->
	                    <div class="row text-center">
	                        <div class="col-12">
	                            <p class="text-center"><?php echo esc_html($desc); ?></p>
	                            <h2 class="color-white"><?php echo esc_html($phone); ?></h2>
	                        </div><!--  /.col-md-12 -->
	                        <div class="col-12">
	                            <button type="submit" class="reservation-btn"><?php echo esc_html($btn_title); ?></button>
	                        </div><!--  /.col-md-12 -->
	                        <div class="col-12 mrt-30">
	                            <p class="input-success alert alert-success"><?php esc_html_e('We have received your mail, We will get back to you soon!', 'litmus'); ?></p>
	                            <p class="input-error alert alert-danger"><?php esc_html_e('Sorry, Message could not send! Please try again.', 'litmus'); ?></p>
	                        </div><!-- /.col-md-12 -->
	                    </div><!--  /.row -->
	                </form>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.make-reservation -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_reservation_form', 113 );
function sh_lm_kc_reservation_form() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_reservation_form' => array(
	                'name' => esc_html__('Reservation Form', 'litmus'),
	                'description' => esc_html__('Reservation Form', 'litmus'),
	                'icon' => 'fa-wpforms',
	                'category' => 'Litmus',
	                'assets' => array(
    	                'scripts' => array(
    	            	    'sh-lm-datepicker' => LITMUS_TEMPLATE_DIR_URL . '/assets/js/jquery.datetimepicker.full.min.js',
     	            	    'jquery' => '', // Leave empty to call the registered scripts          	            
    	                ),
    	                'styles' => array(
    	            	    'sh-lm-datepicker' => LITMUS_TEMPLATE_DIR_URL . '/assets/css/jquery.datetimepicker.css',
     	            	    //'jquery-ui' => '', // Leave empty to call the registered styles          	            
    	                )
    	            ),
	                'params' => array( 
	                	array(
	                    	'name' => 'rcv_email_id',
	                    	'type' => 'text', 
	                    	'label' => esc_html__('Receiver Email ID', 'litmus'),  
	                    	'description' => esc_html__('In which email you want to receive booking mail?', 'litmus'),   
	                    ), 
	                    array(
	                    	'name' => 'title_top',
	                    	'type' => 'text',
	                    	'value' => 'Always happy to see you',
	                    	'label' => esc_html__('Title Top', 'litmus'),  
	                    	'admin_label' => true,
	                    ),
	                    array(
	                    	'name' => 'title',
	                    	'type' => 'text',
	                    	'value' => 'Make A Reservation',
	                    	'label' => esc_html__('Title', 'litmus'),  
	                    	'admin_label' => true,
	                    ),
	                    array(
	                    	'name' => 'icon',
	                    	'type' => 'icon_picker',
	                    	'label' => esc_html__('Icon', 'litmus'),
	                    	'value' => 'lit lit-restaurent-knife',  
	                    ),
	                    array(
	                    	'name' => 'desc',
	                    	'type' => 'text',
	                    	'value' => 'Or five us a call now to reserve your table to experience your best dining experience ever.',
	                    	'label' => esc_html__('Short Description', 'litmus'),  
	                    ),
	                    array(
	                    	'name' => 'phone',
	                    	'type' => 'text',
	                    	'value' => 'Hotline: 012 345 6789',
	                    	'label' => esc_html__('Phone Number', 'litmus'),   
	                    ),
	                    array(
	                    	'name' => 'btn_title',
	                    	'type' => 'text', 
	                    	'value' => 'Submit Order',
	                    	'label' => esc_html__('Submit Order', 'litmus'),   
	                    ), 
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}