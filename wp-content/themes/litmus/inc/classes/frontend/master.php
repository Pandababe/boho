<?php 
if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );
 
class LITMUS_Master {

    public static $partial_path;

    public function __construct() {  

    	self::$partial_path = LITMUS_TEMPLATE_DIR . '/inc/classes/frontend/partials/';

        // Set content width for custom media information
        global $content_width;
        if ( ! isset( $content_width ) ) $content_width = 1020;

        // template_redirect
        add_action( 'template_redirect', array($this, 'coming_soon_page') );

        // Register all wp scripts and styles
        add_action( 'wp_enqueue_scripts', array($this, 'scripts_and_styles') );

        // Post title filter.
        add_filter( "wp_title", array( $this, "page_title" ) );

        // Change excerpt lenght
        add_filter( 'excerpt_length', array( $this, 'excerpt_length' ), 10 );

        // Add read more link instead of [...]
        add_filter( 'excerpt_more', array( $this, 'excerpt_more' ) );
    }
 
    public function coming_soon_page() {
        $coming_soon = sh_lm_get_customizer_field('coming_soon_page','coming_soon');
        if ($coming_soon == "1" && !current_user_can('administrator')) {
            $curent_page_id = get_the_ID();
            $page_id = sh_lm_get_customizer_field('coming_soon_page','1','coming_soon_page_id', '0');
            header('HTTP/1.0 503 Service Unavailable');
            get_header();
            $post_content = get_post($page_id); 
            echo '<div class="sh-kc-content clearfix">';
            echo($post_content->post_content);
            echo '</div>';
            get_footer();
            exit();
        }
    }

    /** 
     * @since 1.0
     */
    public function scripts_and_styles() { 
        require self::$partial_path . 'action_wp_enqueue_scripts.php';
    } 

    /** 
     * @since  1.0
     */
    public function google_fonts_url() {
        $font_url = ''; 
        if ( 'off' !== esc_html_x( 'on', 'Google font: on or off', 'litmus' ) ) {
            $font_url = add_query_arg( 'family', urlencode('Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cPoppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i'), "//fonts.googleapis.com/css" );
        }
        return $font_url;
    }


    /**
     * Write the theme title. It doesnt return anything.
     * The simple name comes, because its very natural when call it:
     * Header::title();
     * 
     * @uses get_bloginfo()
     * @uses wp_title()
     * @uses is_home()
     * @uses is_front_page();
     * 
     * @since  1.0
     */
    public function page_title( $title, $sep = ' | ' ) {
        global $page, $paged;

        if ( is_feed() )
            return $title;

        $site_description = get_bloginfo( 'description' );

        $filtered_title = $title . get_bloginfo( 'name' );
        $filtered_title .= ( ! empty( $site_description ) && ( is_home() || is_front_page() ) ) ? $sep . $site_description: '';
        $filtered_title .= ( 2 <= $paged || 2 <= $page ) ? $sep . sprintf( __( 'Page %s', 'litmus' ), max( $paged, $page ) ) : '';

        return $filtered_title;
    }
    /**
     * @since 1.0
     */
    public function excerpt_length( $length ) {
        return sh_lm_get_customizer_field( array('blog_post_excerpt_length', 55) );
    }

    /**
     * @since 1.0
     */
    public function excerpt_more( $more ) {
        return '<a class="read-more" href="'. get_permalink( get_the_ID() ) . '">'.esc_html__('Read More', 'litmus').'</a>';
    }

} 
new LITMUS_Master;
 