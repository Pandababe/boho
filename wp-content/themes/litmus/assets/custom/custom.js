(function ($) {
    "use strict"; 
    var ajaxurl = litmus.ajaxurl;

    $(".blog-layout-tab-menu a").on('click', function(e) {
        e.preventDefault();
        var data_filter = $(this).data("filter"); 
        
        if( data_filter == 'list' || data_filter == 'grid') {
            $("input.post-style").attr("value", data_filter);
        } 
        if( data_filter !== 'list' || data_filter !== 'grid') {
            $("input.filter-type").attr("value", data_filter);
        }

        var post_style = $("input.post-style").val();
        var short_by = $("select#post_short_by").val();
        var paged = $("input.current-page").val();
        
        $.ajax({
            type: "GET",
            url: ajaxurl, 
            dataType: 'html',
            data: ({ 
                action: 'sh_lm_blog_post',
                post_style: post_style,
                short_by: short_by, 
                paged: paged, 
            }),
            beforeSend: function () {
                $(".row.ajax-content").html("<div class='text-center full-width'><i class='fa fa-refresh fa-spin fa-3x fa-fw'></i></div>"); 
            },
            success : function( response ) { 
                $('.grid-of-blog .row.ajax-content').html(response); 
                console.log(response);
                item_ajax_pagination();
            },
        });
    });

    $("#post_short_by").on('change', function(e) {
        e.preventDefault();  

        var post_style = $("input.post-style").val();
        var paged = $("input.current-page").val();
        var short_by = this.value; 
        
        $.ajax({
            type: "GET",
            url: ajaxurl, 
            dataType: 'html',
            data: ({ 
                action: 'sh_lm_blog_post',
                post_style: post_style, 
                short_by: short_by, 
                paged: paged, 
            }),
            beforeSend: function () {
            },
            success : function( response ) { 
                $('.grid-of-blog .row.ajax-content').html(response); 
                item_ajax_pagination();
            },
        });
    });
 
    function item_ajax_pagination() {
        jQuery('.ajax-post .pagination li a').on('click', function(e) {
            e.preventDefault();  
            var href = jQuery(this).attr('href');
            jQuery.ajax({
                type: "GET",
                url: href, 
                dataType: 'html', 
                beforeSend: function () {
                },
                success : function( response ) { 
                    $('.grid-of-blog .row.ajax-content').html(response);
                    item_ajax_pagination(); 
                },
            });
        });
    }
    item_ajax_pagination();

    //reservation form
    var $contactForm = $("#reservation-form");
    $contactForm.on('submit', function (e) {
        e.preventDefault();
        var data = $contactForm.serialize(); 
        $.ajax({
            type: "POST",
            dataType: "html",
            url: ajaxurl,
            data: data,
            beforeSend: function() {
                //$contactForm.find('.reservation-btn').append('<i class="fa fa-spinner fa-spin"></i>'); 
            },
            success: function () {
                //console.log(data);
                $('#reservation-form .input-success').fadeIn(500); 
            },
            error: function () { 
                $('#reservation-form .input-error').fadeIn(500);
            }
        }); 

        return false;
    });

    // favorite property
    $(".property-favorites").on('click', function(e) {
        e.preventDefault();
        var this_button = $(this), 
            data_type = $(this).data("type"), 
            fav_id = $(this).data("fav-id"), 
            data_nonce = $(this).data("nonce"); 
        $.ajax({
            type: "GET",
            url: ajaxurl, 
            dataType: 'html',
            data: ({ 
                action: 'sh_lm_property_favorite', 
                data_type: data_type,
                fav_id: fav_id,
                data_nonce: data_nonce,
            }),
            beforeSend: function () {
                if(data_type == "remove") {
                    this_button.data("type", "add"); 
                    this_button.attr("data-original-title", "Add To Favorites"); 
                    this_button.removeClass("fa-heart").addClass("fa-heart-o"); 
                } else {
                    this_button.data("type", "remove");
                    this_button.attr("data-original-title", "Remove From Favorites"); 
                    this_button.removeClass("fa-heart-o").addClass("fa-heart");
                }

            },
            success : function( response ) {
                if(data_type == "remove") {
                    this_button.data("type", "add"); 
                    this_button.attr("data-original-title", "Add To Favorites"); 
                    this_button.removeClass("fa-heart").addClass("fa-heart-o"); 
                } else {
                    this_button.data("type", "remove");
                    this_button.attr("data-original-title", "Remove From Favorites"); 
                    this_button.removeClass("fa-heart-o").addClass("fa-heart");
                }
            },
        });
    });

})(jQuery);
