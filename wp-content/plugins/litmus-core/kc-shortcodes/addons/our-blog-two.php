<?php
add_shortcode( 'sh_lm_our_blog_two', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'title' => '',  
		'desc' => '',  
		'number' => '',  
	), $atts));  
	ob_start(); ?> 
	<section class="blog-block">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content">
	            	<?php 
	            		$array_from_to = array(
	            			'&lt;' => '<', 
	            			'&gt;' => '>', 
	            		);
	            		$title = strtr($title, $array_from_to);
	            		$desc = strtr($desc, $array_from_to);
	            	?>
	                <h2 class="section-title no-border v-3"><?php 
                            echo wp_kses( $title, LITMUS_Static::html_allow() );
                        ?></h2>
	                <p class="sub-section-title v-2"><?php 
                            echo wp_kses( $desc, LITMUS_Static::html_allow() );
                        ?></p>
	                <div class="mrb-45"></div><!--  /.mrb -->
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->

	        <div class="row">
	            <div class="col-12">
	                <div class="row">
						<?php  
						$args = array(
						  'posts_per_page' => 3,  
						  'ignore_sticky_posts' => true,
						  'post_type' => 'post'
						);
						$the_query = new WP_Query( $args ); ?>  
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	                    <div class="col-md-6 col-xl-4">
	                        <article class="post">
	                            <div class="article-v2"> 

	                                <?php if ( has_post_thumbnail() ) { ?>
	                                <figure class="article-thumb">
	                                    <a href="<?php the_permalink(); ?>">
	                                        <?php sh_lm_post_featured_image(325, 320, true); ?> 
	                                    </a>
	                                </figure> <!-- /.article-thumb -->
	                                <?php } ?>

	                                <div class="article-content-main"> 
	                                    <div class="article-header">
	                                        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	                                        <div class="entry-meta">
	                                            <div class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></div><!-- /.entry-date -->
	                                            <div class="entry-cat">
	                                                <?php 
	                                                	$categories = get_the_category();
	                                                	if ( ! empty( $categories ) ) {
	                                                	    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
	                                                	}
	                                                ?>
	                                            </div><!--  /.entry-cat -->
	                                        </div><!-- /.entry-meta -->
	                                    </div><!-- /.article-header -->
	                                    <div class="article-content">
	                                        <p><?php echo sh_lm_string_to_excerpt(get_the_content(), 16); ?></p>
	                                    </div><!--  /.article-content -->
	                                    <div class="article-footer">
	                                        <div class="row">
	                                            <div class="col-6 text-left footer-link">
	                                                <a href="<?php the_permalink(); ?>" class="more-link"><?php esc_html_e('Read More', 'litmus'); ?></a>
	                                            </div><!--  /.col-6 -->
	                                            <div class="col-6 text-right footer-meta">
	                                                <a href="<?php comments_link(); ?>"><?php comments_number( '0', '1', '%' ); ?> <i class="pe-7s-comment"></i></a>
	                                                <a href="<?php the_permalink(); ?>"><?php echo sh_lm_get_post_views( get_the_ID() ); ?> <i class="pe-7s-look"></i></a>
	                                            </div><!--  /.col-6 -->
	                                        </div><!--  /.row -->
	                                    </div><!--  /.article-footer -->
	                                </div><!--  /.article-content-main -->
	                            </div><!--  /.article-v2 -->
	                        </article><!--  /.post -->
	                    </div><!--  /.col-md-6 -->
						<?php endwhile; ?> 
	                </div><!--  /.blog-carousel -->
	            </div><!--  /.col-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.blog-block -->  
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_blog_two', 106 );
function sh_lm_kc_our_blog_two() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_blog_two' => array(
	                'name' => esc_html__('Our Blog Two', 'litmus'),
	                'description' => esc_html__('Our Blog Style Two', 'litmus'),
	                'icon' => 'et-book-open',
	                'category' => 'Litmus',
	                'params' => array( 
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => '<span>Latest</span> Form Blog',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'desc',
	                		'type' => 'text',
	                		'label' => esc_html__('Short Description', 'litmus'),  
	                		'value' => 'Etiam congue rhoncus gravida. Sed vel sodales tortor. Donec eget dictum enim. Donec tempus euismod metus ac maximus, aenean mattis.',  
	                	), 
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}