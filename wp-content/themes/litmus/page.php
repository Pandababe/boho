<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package Litmus
 */
get_header(); 

if( function_exists( 'kc_is_using' ) && kc_is_using() ) : 
    if ( have_posts() ) : 
        while ( have_posts() ) : the_post(); ?> 
        <div class="sh-kc-content clearfix">
            <?php the_content(); ?>
        </div>
        <?php 
        endwhile; 
    endif; 
else : ?>
<!-- Page Header
================================================== -->
<?php sh_lm_page_header( get_the_title() ); ?>

<!-- Blog Page Content
================================================== -->
<section class="blog-section-content pd-tb-90">
    <div class="container">
        <div class="row">
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-lg-10 full-content">
                <div class="list-of-blog blog-single-page">
                    <div class="row">
                        <div class="col-md-12">
                            <article class="post"> 

                                <?php if ( has_post_thumbnail() ) { ?>
                                <figure class="article-thumb"> 
                                    <?php sh_lm_post_featured_image(850, 500); ?> 
                                </figure> <!-- /.article-thumb -->
                                <?php } ?> 
                                <div class="article-content">
                                    <?php 
                                        the_content(); 
                                        sh_lm_wp_link_pages(); 
                                    ?>
                                </div><!--  /.article-content --> 
                            </article><!--  /.post -->

                            <?php
                                // If comments are open or we have at least one comment, load up the comment template
                                if ( comments_open() || get_comments_number() ) :
                                  comments_template();
                                endif;
                            ?> 
                        </div><!--  /.col-md-12 -->
                    </div><!--  /.row -->
                </div><!--  /.list-of-blog -->
            </div><!--  /.col-lg-10 -->
            <?php endwhile; ?>
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.blog-section-content -->
<?php endif; ?>
<?php get_footer(); ?>