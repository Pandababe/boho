<?php
add_shortcode( 'sh_lm_new_arrivals', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'title' => '',  
		'desc' => '',  
		'products' => '',   
	), $atts));  
	ob_start(); ?>  
	<section class="new-arrivals">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content"> 
	            	<?php 
	            		$array_from_to = array(
	            			'&lt;' => '<', 
	            			'&gt;' => '>', 
	            		);
	            		$title = strtr($title, $array_from_to);
	            		$desc = strtr($desc, $array_from_to);
	            	?>
	                <h2 class="section-title no-border v-3"><?php 
                            echo wp_kses( $title, LITMUS_Static::html_allow() );
                        ?></h2>
	                <p class="sub-section-title v-2"><?php 
                            echo wp_kses( $desc, LITMUS_Static::html_allow() );
                        ?></p>
	                <div class="mrb-90"></div><!--  /.mrb -->
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	        <div class="row">
	            <div class="col-md-12"> 
	            	<?php  
	            	if(!empty($products)) {
	            		$product_ids = array();
		            	$products = explode(",", $products);  
		            	
		            	foreach ( $products as $product ) {  
			            	$product = explode(":", $product); 
			            	$product_ids[] = (int) $product[0];
	            	 	}  
	            	?>
	                <div class="new-arrival-slider owl-carousel">
						<?php 
						$new_arrivals = new WP_Query(
						    array(
						        'post_type' => 'product', 
						        'posts_per_page' => count($product_ids),
						        'post__in' => $product_ids, 
						    )
						); 
						$parent_id = 1;
						while ($new_arrivals->have_posts()) : $new_arrivals->the_post(); ?> 
	                    <div class="item">
	                        <div class="new-arrival-item">
	                            <div class="row align-items-center">
	                                <div class="col-lg-5">
	                                    <div class="new-arrivals-content">
	                                        <?php
	                                        	/**
	                                        	 * woocommerce_single_product_summary hook.
	                                        	 *
	                                        	 * @hooked woocommerce_template_single_title - 5
	                                        	 * @hooked woocommerce_template_single_rating - 10
	                                        	 * @hooked woocommerce_template_single_price - 10
	                                        	 * @hooked woocommerce_template_single_excerpt - 20
	                                        	 * @hooked woocommerce_template_single_add_to_cart - 30
	                                        	 * @hooked woocommerce_template_single_meta - 40
	                                        	 * @hooked woocommerce_template_single_sharing - 50
	                                        	 * @hooked WC_Structured_Data::generate_product_data() - 60
	                                        	 */
	                                        	//remove_action( 'woocommerce_template_single_excerpt' );
	                                        	do_action( 'woocommerce_single_product_summary' );
	                                        ?>
	                                        
	                                    </div><!--  /.new-arrivals-content -->
	                                </div><!--  /.col-lg-5 -->

	                                <div class="col-lg-5">
	                                    <figure class="menu-thumbnail">
	                                    	<?php 
	                                    		global $product;

	                                    		$attachment_ids = $product->get_gallery_image_ids(); 

	                                    		if ( $attachment_ids && has_post_thumbnail() ) {
	                                    			$i = 1;
	                                    			foreach ( $attachment_ids as $attachment_id ) { ?>
	                                    			<img id="new-arrvial-gal-<?php echo esc_attr($parent_id); ?>-<?php echo esc_attr($i); ?>" src="<?php echo esc_url(sh_lm_get_image_crop_size($attachment_id, 424, 544)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />
	                                    			<?php $i++; }
	                                    		} elseif( has_post_thumbnail() ) { ?>
	                                    			<img id="new-arrvial-gal-1" src="<?php echo esc_url(sh_lm_get_image_crop_size(get_post_thumbnail_id( get_the_ID() ), 424, 544)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />
	                                    			<?php  
	                                    		} 
	                                    	?> 
	                                    </figure>
	                                </div><!--  /.col-lg-5 -->
	                                <div class="col-lg-2">
	                                    <div class="menu-single-tab">
	                                        <ul class="bsm-tabs" id="menu-single-tab-menu-1">
	                                        	<?php  
	                                        		if ( $attachment_ids && has_post_thumbnail() ) {
	                                        			$i = 1;
	                                        			foreach ( $attachment_ids as $attachment_id ) { ?>
	                                        			<li class="bsm-tab">
	                                        			    <a href="#new-arrvial-gal-<?php echo esc_attr($parent_id); ?>-<?php echo esc_attr($i); ?>"><img src="<?php echo esc_url(sh_lm_get_image_crop_size($attachment_id, 100, 128)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" /></a>
	                                        			</li> 
	                                        			<?php $i++; }
	                                        		} elseif( has_post_thumbnail() ) { ?>
	                                        			<li class="bsm-tab">
	                                        			    <a href="#new-arrvial-gal-1"><?php sh_lm_post_featured_image(100, 128, true); ?></a>
	                                        			</li> 
	                                        			<?php  
	                                        		} 
	                                        	?>  
	                                        </ul><!--  /.blog-layout-tab-menu -->
	                                    </div><!--  /.menu-single-tab -->  
	                                </div><!--  /.col-lg-2 -->
	                            </div><!--  /.row -->
	                        </div><!--  /.new-arrival-item -->
	                    </div><!--  /.item -->
						<?php $parent_id++; endwhile; ?> <?php wp_reset_postdata(); ?> 
	                </div><!--  /.new-arrival-slider -->

	                <?php } // (!empty($products) ?>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.new-arrivals -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_new_arrivals', 118 );
function sh_lm_kc_new_arrivals() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_new_arrivals' => array(
	                'name' => esc_html__('New Arrivals', 'litmus'),
	                'description' => esc_html__('Product New Arrivals', 'litmus'),
	                'icon' => 'sl-plane',
	                'category' => 'Litmus',
	                'params' => array( 
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => '<span>New</span> Arrivals',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'desc',
	                		'type' => 'text',
	                		'label' => esc_html__('Short Description', 'litmus'),  
	                		'value' => 'Etiam congue rhoncus gravida. Sed vel sodales tortor. Donec eget dictum enim. Donec tempus euismod metus ac maximus, aenean mattis.',  
	                	), 
	                	array(
	                	    'type'          => 'autocomplete',
	                	    'label'         => esc_html__('Select Product', 'litmus'),
	                	    'name'          => 'products',
	                	    'options'       => array( 
	                	        'multiple'      => true, 
	                	        'post_type'     => 'product',  
	                	    ), 
	                	), 
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}