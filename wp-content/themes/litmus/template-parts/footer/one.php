<?php
/**
 * This template for displaying footer part
 *
 * @package Litmus
 */
?>
<!-- Footer
================================================== -->
<?php if(sh_lm_get_customizer_field('footer_style_option','footer_style') !== 'layout_footer') : ?>
<?php 
    if(sh_lm_get_customizer_field('footer_style_option','footer_style') == 'creative2') {
        $bg_color = sh_lm_get_customizer_field('footer_style_option', 'creative2','background_color');
        $bg_class = 'creative-v2';
    } else {
        $bg_color = '';
        $bg_class = '';
    }
?>

<?php if(sh_lm_get_customizer_field('footer_style_option', 'ecommerce','service_offer_option','service_offer') || sh_lm_get_customizer_field('footer_style_option', 'creative2','service_offer_option','service_offer')): ?>
<!-- Service Offer Block
================================================== -->
<section class="service-offer <?php echo esc_attr($bg_class); ?>" <?php if($bg_color) {?> style="background-color: <?php echo esc_attr($bg_color); ?>" <?php } ?>>
    <div class="container">
        <div class="row">
            <?php 
                $offer_list = sh_lm_get_customizer_field('footer_style_option', 'ecommerce','service_offer_option','1','service_offers');
                if($offer_list) {
                foreach ($offer_list as $offer) { 
            ?>
            <div class="col-md-4">
                <div class="service-offer-item">
                    <div class="service-icon">
                        <?php if ($offer['icon']['type'] == "icon-font") { ?>
                            <i class="<?php echo esc_attr($offer['icon']['icon-class']);?>"></i>
                        <?php } else { ?>
                            <img src="<?php  echo esc_url($offer['icon']['url']); ?>" alt="<?php esc_attr_e('Icon','litmus' ); ?>" />
                        <?php } ?>
                    </div><!--  /.service-icon -->
                    <div class="service-content">
                        <h3 class="offer-title"><?php echo esc_html($offer['title']); ?></h3>
                        <p><?php echo esc_html($offer['short_desc']); ?></p>
                    </div><!--  /.service-content -->
                </div><!--  /.service-offer-item -->
            </div><!--  /.col-md-4 --> 
            <?php } } ?>

        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.service-offer -->
<?php endif; ?>

<?php 
    $footer_style = sh_lm_get_customizer_field('footer_style_option','footer_style');
    if( $footer_style == 'restaurant' ) {
        $footer_theme = 'citrus-theme';
        $footer_bg = 'bg-nero';
    } elseif( $footer_style == 'ecommerce' ) {
        $footer_theme = 'orange-theme';
        $footer_bg = '';
    } elseif($footer_style == 'creative2') {
        $footer_theme = '';
        $footer_bg = '';
    } else {
        $footer_theme = '';
        $footer_bg = 'bg-bl-russian';
    }
?>
<footer class="site-footer <?php echo esc_attr($footer_bg . $bg_class); ?>"> 
    <?php if(sh_lm_get_customizer_field('footer_newsletter')): ?>
    <div class="footer-top pd-tb-45" <?php if($bg_color) {?> style="background-color: <?php echo esc_attr($bg_color); ?>"<?php } ?>>
        <div class="container">
            <form class="newsletter-form" action="#">
                <div class="row align-items-center">
                    <div class="col-md-3">
                        <h2 class="form-title"><?php esc_html_e('Newsletter', 'litmus'); ?></h2>
                    </div><!--  /.col-md-3 -->
                    <div class="col-md-6">
                        <input type="email" class="form-controller" placeholder="<?php esc_attr_e('Enter your email', 'litmus'); ?>" />
                    </div><!--  /.col-md-6 -->
                    <div class="col-md-3">
                        <button type="submit" class="newsletter-btn"><?php esc_html_e('Subscribe', 'litmus'); ?></button>
                    </div><!--  /.col-md-3 -->
                </div><!--  /.row -->
            </form>
        </div><!--  /.container -->
    </div><!--  /.footer-top -->
    <?php endif; ?>


    <div class="footer-middle <?php if( $footer_style == 'ecommerce' ) echo 'bg-snow'; ?> <?php echo esc_attr($footer_theme); ?>" <?php if($bg_color) {?> style="background-color: <?php echo esc_attr($bg_color); ?>"<?php } ?>>
        <div class="container">
            <div class="row">
                <?php 
                $middle_col_small = sh_lm_get_customizer_field('footer_option', '4', 'middle_col_small'); 
                $columns = intval( sh_lm_get_customizer_field('footer_option', 'widgets_columns') );
                $col_class = 12 / max( 1, $columns );
                $col_class_sm = 12 / max( 1, $columns );
                if ( $columns == 4 ) {
                    $col_class_sm = 6;
                } 
                $col_class = "col-sm-$col_class_sm col-md-$col_class";
                for ( $i = 1; $i <= $columns ; $i++ ) {
                    if ( $columns == 3 ) :
                        if ( $i == 3 ) {
                            $col_class = "col-sm-12 col-md-$col_class";
                        } else {
                            $col_class = "col-sm-6 col-md-$col_class";
                        } 
                    endif;

                    if($middle_col_small == 1 && $columns == 4) {
                        if ( $i == 2 || $i == 3 ) {
                            $col_class = "col-sm-6 col-md-2";
                        } else {
                            $col_class = "col-sm-6 col-md-4";
                        }
                    } 
                    ?>
                    <div class="widget-area sidebar-footer-<?php echo esc_attr($i) ?> <?php echo esc_attr( $col_class ) ?>">
                        <?php if ( is_active_sidebar( 'sidebar-footer-'.$i ) ) { ?>
                        <?php dynamic_sidebar( 'sidebar-footer-'.$i ); ?>
                        <?php } ?>
                    </div>
                <?php } ?> 
            </div><!--  /.row -->
        </div><!--  /.container -->
    </div><!--  /.footer-middle -->

    <div class="footer-bottom <?php echo esc_attr($footer_theme); ?>" <?php if($bg_color) {?> style="background-color: <?php echo esc_attr($bg_color); ?>"<?php } ?>>
        <div class="container">
            <div class="row">
                <div class="col-md-6"> 
                    <p  class="copyright-text"><?php 
                            echo wp_kses( sh_lm_get_customizer_field(array( 'copyright_text', __('Copyright 2018 <a href="#">Litmus</a> All rights Reserved.', 'litmus') )), LITMUS_Static::html_allow() );
                        ?></p>
                </div><!--  /.col-md-6 -->
                <div class="col-md-6"> 
                    <?php 
                        wp_nav_menu ( array(
                            'menu_class' => 'footer-menu float-right',
                            'container'=> 'ul',
                            'depth' => 1,
                            'theme_location' => 'footer-menu',  
                            'fallback_cb'    => 'LITMUS_Walker_Nav_Menu::menu_callback' 
                        )); 
                    ?>
                </div><!--  /.col-md-6 -->
            </div><!--  /.row -->
        </div><!--  /.container -->
    </div><!--  /.footer-bottom -->
</footer><!--  /.site-footer -->
<?php else : ?>
<div class="clearfix"></div>
<?php 
    $getFooterPosts = sh_lm_get_customizer_field('footer_style_option','layout_footer', 'footer_layout_page','0');
    $page_obj = get_post($getFooterPosts);  
    echo apply_filters('the_content', $page_obj->post_content);
    ?>
    <div class="clearfix"></div>
<?php endif; ?>
