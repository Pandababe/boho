<?php
/**
 * This template for displaying header part
 *
 * @package Litmus
 */
sh_lm_preloader(); ?>

<!-- Header
================================================== --> 
<?php 
$header_style = sh_lm_get_customizer_field('header_style_option','header_style');
if( sh_lm_get_customizer_field('cart_widget') ) {
    $cartHleft = 'lm-left';
    $cartHmiddle = 'lm-middle';
    $cartHright = 'lm-right';
} else {
    $cartHleft = '';
    $cartHmiddle = '';
    $cartHright = '';
}
if( $header_style == 'restaurant' ) {
    $header_class = 'no-border bg-bl-transparent citrus-theme';
    $hamburger_class = 'citrus-theme';
    $header_col_left = 'col-5 col-sm-6 col-md-6 col-lg-6 col-xl-2 '.$cartHleft;
    $header_col_middle = 'col-md-8 '.$cartHmiddle;
    $header_col_right = 'col-7 col-sm-6 col-md-6 col-lg-6 col-xl-2 '.$cartHright;
} elseif( $header_style == 'ecommerce' ) {
    $header_class = 'no-border header-three';
    $hamburger_class = 'orange-theme';
    $header_col_left = 'col-5 col-sm-6 col-md-6 col-lg-6 col-xl-2';
    $header_col_middle = 'col-md-7';
    $header_col_right = 'col-7 col-sm-6 col-md-6 col-lg-6 col-xl-3';
} elseif( $header_style == 'creative2' ) {
    $header_class = '';
    $hamburger_class = '';
    $header_col_left = 'col-5 col-sm-6 col-md-6 col-lg-6 col-xl-2 '.$cartHleft;
    $header_col_middle = 'col-md-8 '.$cartHmiddle;
    $header_col_right = 'col-7 col-sm-6 col-md-6 col-lg-6 col-xl-2 '.$cartHright;
} else {
    $header_class = 'bg-bl-russian';
    $hamburger_class = '';
    $header_col_left = 'col-5 col-sm-6 col-md-6 col-lg-6 col-xl-2 '.$cartHleft;
    $header_col_middle = 'col-md-8 '.$cartHmiddle;
    $header_col_right = 'col-7 col-sm-6 col-md-6 col-lg-6 col-xl-2 '.$cartHright;
}
?>
<div class="clearfix"></div><!--  /.clearfix -->
<?php if( $header_style == 'creative2' ) { 
    if(sh_lm_get_customizer_field('header_style_option','creative2','background_color')) {
        $headerBG = sh_lm_get_customizer_field('header_style_option','creative2','background_color');
        $bg_class = 'creative-v2';
    } else {
        $headerBG = '';
        $bg_class = '';
    }
} else {
    $headerBG = '';
    $bg_class = '';
}
?>
<?php if(sh_lm_get_customizer_field('header_style_option','header_style') !== 'header_builder') : ?>
<header class="site-header <?php echo esc_attr($header_class .$bg_class); ?> <?php if (sh_lm_get_customizer_field('sticky_header')) echo 'sticky-header' ?>" id="site-header" <?php if($headerBG) {?> style="background-color: <?php echo esc_attr($headerBG); ?>"<?php } ?>>
    <?php if( $header_style == 'ecommerce' ) { ?>
    <div class="top-header">
        <div class="container">            
            <div class="row align-items-center">
                <div class="col-md-6 text-left mb-text-center">
                    <h2 class="site-welcome"><?php echo esc_html(sh_lm_get_customizer_field('header_style_option','ecommerce','header_top_title')); ?></h2><!--  /.site-welcome -->
                </div><!--  /.col-md-6 -->
                <div class="col-md-6 text-right mb-text-center">
                    <p class="support-phone"><?php echo esc_html(sh_lm_get_customizer_field('header_style_option','ecommerce','header_top_support_text')); ?> <a href="tel:<?php echo esc_html(sh_lm_get_customizer_field('header_style_option','ecommerce','header_top_phone')); ?>"><?php echo esc_html(sh_lm_get_customizer_field('header_style_option','ecommerce','header_top_phone')); ?></a></p>
                </div><!--  /.col-md-6 -->
            </div><!--  /.row -->
        </div><!--  /.container -->
    </div><!--  /.top-header -->
    <?php } ?>

    <?php if( $header_style == 'creative2' ) {
        $classHeader = 'creative-2';
    } else {
        $classHeader = '';
    }?>

    <div class="container clearfix <?php echo esc_attr($classHeader); ?>">
        <div class="row">
            <div class="<?php echo esc_attr($header_col_left); ?>">
                <div class="header-left">                    
                    <div class="site-logo">
                        <?php if(sh_lm_get_customizer_field('logo_settings','logo','selected_value') !== "text") { ?>

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo sh_lm_get_customizer_field('logo_settings','logo','image','image_logo', array('url', get_template_directory_uri()."/assets/images/logo.png" )); ?>" alt="<?php echo bloginfo('name'); ?>" class="litmus-retina" data-retina="<?php echo sh_lm_get_customizer_field('logo_settings','logo','image','retina_logo','1','retina_image_main','url'); ?>">

                        </a>
                        <?php } else { ?>
                            <?php if(sh_lm_get_customizer_field('logo_settings','logo','text','title') !== "") { ?>
                                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo esc_html(sh_lm_get_customizer_field('logo_settings','logo','text','title')); ?></a></h1>
                            <?php } ?>
                            
                            <?php if(sh_lm_get_customizer_field('logo_settings','logo','text','subtitle') !== "") { ?>
                                <p class="site-description"><?php echo esc_html(sh_lm_get_customizer_field('logo_settings','logo','text','subtitle')); ?></p>
                            <?php } ?>
                        <?php } ?>
                    </div><!-- /.site-logo -->
                </div><!-- /.header-left -->
            </div><!-- /.col-md-6 -->

            <div class="d-none d-xl-block <?php echo esc_attr($header_col_middle); ?>">
                <div class="header-middle">                    
                    <nav class="navigation">
                        <!-- Main Menu -->
                        <div class="menu-wrapper">
                            <div class="menu-content">
                                <?php 
                                    wp_nav_menu ( array(
                                        'menu_class' => 'mainmenu',
                                        'container'=> 'ul',
                                        'theme_location' => 'main-menu', 
                                        'walker'    => new LITMUS_Walker_Nav_Menu(), 
                                        'fallback_cb'    => 'LITMUS_Walker_Nav_Menu::menu_callback' 
                                    ));  
                                ?>
                            </div> <!-- /.hours-content-->
                        </div><!-- /.menu-wrapper --> 
                    </nav><!-- /.site-navigation -->
                </div>
            </div><!-- /.col-md-6 -->

            <div class="<?php echo esc_attr($header_col_right); ?>">
                <div class="header-right">                    
                    <ul class="right-menu <?php echo esc_attr($classHeader); ?>">
                        <?php if(sh_lm_get_customizer_field('search_menu')) { ?>
                        <li><a href="#" class="search-extend"><i class="pe-7s-search"></i></a></li>
                        <?php } ?>
                        <?php 
                        if(sh_lm_get_customizer_field('user_reg_menu')) {
                        if ( function_exists( 'sh_lm_reg_from' ) ) { ?>
                        <li><a href="#login-register" class="user-registration" data-toggle="modal" data-target="#login-register"><i class="pe-7s-user"></i></a></li>
                        <?php } ?>
                        <?php if( sh_lm_get_customizer_field('cart_widget') ) { ?>
                        <li>
                        <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
                            <a class="user-cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart','litmus' ); ?>"><i class="pe-7s-shopbag"></i><span class="budge"><?php echo sprintf( _n( '%d', '%d', WC()->cart->get_cart_contents_count(), 'litmus' ), WC()->cart->get_cart_contents_count() ); ?></span></a>
                        <?php } }?>
                        </li>
                        <?php } ?>
                        <?php if(sh_lm_get_customizer_field('hamburger_block')) { ?> 
                        <li><a href="#" class="hamburger-menu"><i class="icon-arrows-hamburger-2"></i></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</header><!-- /.site-header -->

<?php if( $header_style == 'creative2' ) { ?>
    <div id="call-to-get-quote-header" class="bsm-modal">
        <div class="bsm-modal-content">
            <?php 
                if ( function_exists( 'sh_lm_get_quote_from_clients' ) ) {
                    echo sh_lm_get_quote_from_clients(); 
                }
            ?>
        </div>
    </div>
<?php } ?>

<!-- Hamburger Menu
================================================== -->
<div class="hamburger-block <?php echo esc_attr($hamburger_class); ?> <?php echo esc_attr($classHeader); ?>"> 
    <div class="overlaybg"></div>
    <div class="hamburger-block-container expand-block" <?php if($headerBG) {?> style="background-color: <?php echo esc_attr($headerBG); ?>"<?php } ?>>
        <div class="hamburger-search-form">
            <?php get_search_form(); ?>
        </div><!--  /.hamburger-search-form -->
        <!-- Mobile Nav Close -->
        <div class="close-menu">
            <i class="pe-7s-close"></i>
        </div>
        
        <!-- Main Menu Mobile -->
        <div class="main-mobile-menu" id="mobile-main-menu"></div>

        <!-- Hamburger Menu Wrapper -->
        <div class="hamburger-wrapper">
            <!-- Hamburger Menu Content -->
            <div class="hamburger-content">
                <?php 
                    wp_nav_menu ( array(
                        'menu_class' => 'hamburger-mainmenu',
                        'container'=> 'ul',
                        'theme_location' => 'hamburger-menu', 
                        'walker'    => new LITMUS_Walker_Nav_Menu(), 
                        'fallback_cb'    => 'LITMUS_Walker_Nav_Menu::menu_callback' 
                    ));  
                ?>
            </div> <!-- /.hours-content-->

            <!-- Hamburger Widget Area -->
            <div class="hamburger-widget-area">
                <?php get_sidebar('hamburger'); ?>
            </div><!--  /.hamburger-widget-area -->
        </div> <!-- /.menu-wrapper --> 
    </div><!-- /.site-navigation -->
</div><!--  /.hamburger-block -->

<!-- User Registrar Form
================================================== -->
<?php 
if ( function_exists( 'sh_lm_reg_from' ) ) :
    echo sh_lm_reg_from(); ?>

    <?php if( function_exists( 'sh_lm_registar_form' ) ) {
        echo sh_lm_registar_form(); 
    } ?>
<?php endif; ?>

<!-- Header Modal
================================================== -->
<div class="overlay-search <?php echo esc_attr($hamburger_class); ?>">
    <button type="button" class="overlay-close"><i class="pe-7s-close"></i></button>
    <div class="header-search-content">
        <div class="container">            
            <div class="row justify-content-md-center">            
                <div class="col-sm-12 col-md-8">
                    <h4 class="search-title"><?php esc_html_e('What are you looking for?', 'litmus'); ?></h4>
                    <?php get_search_form(); ?>
                    <h5 class="search-footer-title">
                        <?php echo wp_kses( __('Press <span>enter</span> to confirm search term', 'litmus'), LITMUS_Static::html_allow() ); ?>
                    </h5><!-- /.search-footer-title -->
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- /.header-search-content -->
</div><!-- /.header-search-content --> 
<?php else : ?>
    <?php 
        $getPost = sh_lm_get_customizer_field('header_style_option','header_builder','header_layout_page','0');
        $page_obj = get_post($getPost);  
        echo apply_filters('the_content', $page_obj->post_content);
    ?>
    <!-- User Registrar Form
    ================================================== -->
    <div class="header-extra-block">
    <?php 
    if ( function_exists( 'sh_lm_reg_from' ) ) :
        echo sh_lm_reg_from(); ?>

        <?php if( function_exists( 'sh_lm_registar_form' ) ) {
            echo sh_lm_registar_form(); 
        } ?>
    <?php endif; ?>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>
