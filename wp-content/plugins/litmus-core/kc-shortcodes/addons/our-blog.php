<?php
add_shortcode( 'sh_lm_our_blog', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'title' => '',  
		'icon' => '',  
		'number' => '',  
	), $atts));  
	ob_start(); ?> 
	<section class="blog-block">
	    <div class="litmus-container">
	        <div class="row">
	        	<?php if($icon == '') { ?>
	            <div class="col-md-12">
	                <h2 class="section-title"><?php echo esc_html($title); ?></h2>
	            </div><!-- /.col-md-12 -->
		        <?php } else { ?>
		        <div class="col-md-6 text-center full-content">
		            <h2 class="section-title no-border"><?php echo esc_html($title); ?></h2>
		            <div class="resturent-title-border">
		                <i class="<?php echo esc_attr($icon); ?>"></i>
		            </div>
		        </div><!--  /.col-md-12 -->
		        <?php } ?>
	        </div><!-- /.row -->

	        

	        <div class="row">
	            <div class="col-12">
	                <div class="blog-slider owl-carousel">
	                	<?php  
	                	$args = array(
	                	  'posts_per_page' => $number,  
	                	  'post_type' => 'post'
	                	);
	                	$the_query = new WP_Query( $args ); ?>  
	                	<?php while ( $the_query->have_posts() ) : $the_query->the_post();  
	                	    $only_item_class = true; 
	                	    require get_template_directory().'/template-parts/post/content.php'; ?> 
	                	<?php endwhile; ?>  
	                </div><!--  /.blog-carousel -->
	            </div><!--  /.col-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.blog-block -->    
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_blog', 104 );
function sh_lm_kc_our_blog() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_blog' => array(
	                'name' => esc_html__('Our Blog', 'litmus'),
	                'description' => esc_html__('Our Blog', 'litmus'),
	                'icon' => 'et-book-open',
	                'category' => 'Litmus',
	                'params' => array( 
	                	array(
	                		'name' => 'title',
	                		'type' => 'text',
	                		'label' => esc_html__('Title', 'litmus'),  
	                		'value' => 'From The Blog',  
	                		'admin_label' => true,
	                	),
	                	array(
	                		'name' => 'icon',
	                		'type' => 'icon_picker',
	                		'label' => esc_html__('Icon', 'litmus'),     
	                		'description' => esc_html__('If you set icon title will show in center', 'litmus'),     
	                	),
	                    array(
	                    	'name' => 'number',
	                    	'label' => esc_html__('Total Post', 'litmus'), 
	                    	'type' => 'number_slider',  
	                    	'options' => array(  
	                    		'min' => 1,
	                    		'max' => 20, 
	                    		'show_input' => true
	                    	), 
	                    	'value' => 5
	                    ),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}