<?php 

/**
 * All WooCommerce Custom Functions Goes Here
 **/

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	/* Declare WooCommerce support */
	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
	    add_theme_support( 'woocommerce' );
	}

    
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 11 );

	
	// define the woocommerce_after_add_to_cart_button callback 
	function sh_lm_action_woocommerce_after_add_to_cart_button() { ?>
		<?php if ( function_exists( 'sh_lm_product_favorite_btn' ) ) { ?>
	    <a href="#" class="wishlist-btn"><?php sh_lm_product_favorite_btn( get_the_ID() ); ?></a>
	    <?php } ?>
	<?php };  
	add_action( 'woocommerce_after_add_to_cart_button', 'sh_lm_action_woocommerce_after_add_to_cart_button', 10, 0 ); 


	/* Header Add To Cart Ajax Function */
	add_filter( 'woocommerce_add_to_cart_fragments', 'sh_lm_header_add_to_cart_fragment' );
	function sh_lm_header_add_to_cart_fragment( $fragments ) {
	    ob_start();
	    ?>
	    <a class="user-cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart','litmus' ); ?>"><i class="pe-7s-shopbag"></i><span class="budge"><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() , 'litmus'), WC()->cart->get_cart_contents_count() ); ?></span></a>
	    <?php

	    $fragments['a.user-cart'] = ob_get_clean();

	    return $fragments;
	}

    /* Trim zeros in price decimals */
    //add_filter( 'woocommerce_price_trim_zeros', '__return_true' );
    function sh_lm_product_price() { 
    	global $product; echo($product->get_price_html()); 
    }
}

