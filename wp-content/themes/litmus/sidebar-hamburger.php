<?php
/**
 * The sidebar containing the hamburger widget area.
 *
 * @package Litmus
 */
?>
<!-- Hamburger Widget Area -->
<div class="hamburger-widget-area">
    <?php 
    $sidebar_id = "sidebar-hamburger"; 
    if ( is_active_sidebar( $sidebar_id ) ) : 
        dynamic_sidebar( $sidebar_id ); 
    endif; ?>
</div><!--  /.hamburger-widget-area -->