<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Litmus
 */
get_header();
sh_lm_get_template_part('404'); 
get_footer();