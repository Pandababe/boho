<?php 

class Litmus_Newsletter extends WP_Widget {

	function __construct() {
        $title_n_description = esc_html__('Litmus : Mailchimp Newsletter', 'litmus');
        parent::__construct(
            __CLASS__, //class name
            $title_n_description, // widget title
            array (
                'description' => $title_n_description // widget description
            )
        );
    }

	function widget( $args, $instance ) {
		extract( $args );
		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$action_url = $instance['action_url'];
		$first_last_name = $instance['first_last_name'];
		$button_text = $instance['button_text'];

		/* Before widget (defined by themes). */
		echo sh_lm_return($before_widget);

		/* Title of widget (before and after defined by themes). */
		if ( $title ) {
			echo sh_lm_return( $before_title . $title . $after_title );
		}

		// if this widget active load this script
		if ( is_active_widget( false, false, $this->id_base, true ) ) {
			wp_enqueue_script('ajaxchimp', LITMUS_TEMPLATE_DIR_URL .'/assets/js/ajaxchimp.js', array("jquery"));
			wp_enqueue_script('sh-lm-ajaxchimp-activation', LITMUS_TEMPLATE_DIR_URL .'/assets/js/ajaxchimp-activation.js', array("jquery"));
		} ?>

		<div class="litmus-theme-newsletter-box">
			<div class="newsletter-area">
			    <form method="post" name="mc-embedded-subscribe-form" class="validate mc-embedded-subscribe-form">
			        <div class="form-newsletter">
			            <div class="row">
			                <div class="mc-field-group col-md-12">
				                <input type="email" value="" placeholder="<?php esc_html_e( 'Type Your Email', 'litmus' );?>" name="EMAIL" class="form-controller email"  required="required">
				                <input type="hidden" value="<?php echo esc_url_raw($action_url) ?>" class="newsletter-action-url" />
							</div><!-- / col-md-12 -->

							<div class="col-md-12">
								<div class="text-center">
									<button class="mc-embedded-subscribe" type="submit" name="subscribe"><?php echo esc_html($button_text); ?></button>
								</div><!-- /.text-center -->
								<div class="subscription-success"></div>
								<div class="subscription-error"></div>
							</div>  <!-- / col-md-12 -->
						</div>  <!-- / row -->
					</div>  <!-- / form-newsletter -->
			    </form>  <!-- / signup form -->
			</div> <!-- / newsletter aria -->
		</div><!-- newsletter_box_footer-->
	<?php 
		/* After widget (defined by themes). */
		echo sh_lm_return($after_widget);
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$action_url = $instance['action_url'];
		$first_last_name = $instance['first_last_name'];
		$button_text = $instance['button_text'];


		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['action_url'] = $new_instance['action_url'];
		$instance['first_last_name'] = $new_instance['first_last_name'];
		$instance['button_text'] = $new_instance['button_text'];
		return $instance;
	}
	
	 /** @see WP_Widget::form */
	function form( $instance ) {

			/* Set up some default widget settings. */
			$defaults = array(
				'title' => esc_html__('Newsletter','litmus'),
				'action_url' => 'http://softhopper.us11.list-manage.com/subscribe/post?u=559ff170eee6949a359c40740&amp;id=fbbd18e68b',
				'button_text' => 'Subscribe',
				
	 			);
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		
			<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Title:', 'litmus') ?></label>
				<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php if( isset($instance['title']) ) echo esc_attr( $instance['title'] ); ?>" />
			</p>

	    	<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'action_url' )); ?>"><?php esc_html_e('Form Action URL:', 'litmus') ?></label>
				<input type="text" id="<?php echo esc_attr($this->get_field_id( 'action_url' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'action_url' )); ?>" value="<?php if( isset($instance['action_url']) ) echo esc_url( $instance['action_url'] ); ?>"  class="widefat" />
			</p>

	    	<p>
				<label for="<?php echo esc_attr($this->get_field_id( 'button_text' )); ?>"><?php esc_html_e('Button Text:', 'litmus') ?></label>
				<input type="text" id="<?php echo esc_attr($this->get_field_id( 'button_text' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'button_text' )); ?>" value="<?php if( isset($instance['button_text']) ) echo esc_attr($instance['button_text']); ?>"  class="widefat" />
			</p>
	   <?php 
	} // end form function
} //end class