<?php
/**
 * Hooks for template header
 *
 * @package Litmus
 */

/**
 * Get favicon and home screen icons
 *
 * @since  1.0
 */
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
	function sh_lm_header_icons() {
		$favicon = sh_lm_get_customizer_field('favicon', array('url', get_template_directory_uri().'/assets/images/favicon.ico') );
		$header_icons =  ( $favicon ) ? '<link rel="shortcut icon" type="image/x-ico" href="' . esc_url( $favicon ) . '" />' : '';

		$icon_iphone = sh_lm_get_customizer_field('icon_iphone','url');
		$header_icons .= ( $icon_iphone ) ? '<link rel="apple-touch-icon" sizes="57x57"  href="' . esc_url( $icon_iphone ) . '" />' : '';

		$icon_iphone_retina = sh_lm_get_customizer_field('icon_iphone_retina','url');
		$header_icons .= ( $icon_iphone_retina ) ? '<link rel="apple-touch-icon" sizes="114x114" href="' . esc_url( $icon_iphone_retina ). '" />' : '';

		$icon_ipad = sh_lm_get_customizer_field('icon_ipad','url');
		$header_icons .= ( $icon_ipad ) ? '<link rel="apple-touch-icon" sizes="72x72" href="' . esc_url( $icon_ipad ) . '" />' : '';

		$icon_ipad_retina = sh_lm_get_customizer_field('icon_ipad_retina','url');
		$header_icons .= ( $icon_ipad_retina ) ? '<link rel="apple-touch-icon" sizes="144x144" href="' . esc_url( $icon_ipad_retina ) . '" />' : '';

		$allowed_html_array = array(
	        'link' => array(
	            'rel' => array(),
	            'sizes' => array(),
	            'href' => array(),
	        ),
	    );
	    
	    if ( isset ( $header_icons ) ) echo wp_kses( $header_icons, $allowed_html_array );
	}
	add_action( 'wp_head', 'sh_lm_header_icons' );
}

function sh_lm_font_enqueue($selector, $font_property) { ?>
	<?php echo (isset($selector)) ? sh_lm_return($selector) : ''; ?> { 
		<?php 
		$family = (isset($font_property['family'])) ? $font_property['family'] : '';
		if ($family != '') echo "font-family: $family !important;";

		$style = (isset($font_property['style'])) ? $font_property['style'] : '';
		if ($style != '') echo "font-style: $style;";

		$weight = (isset($font_property['weight'])) ? $font_property['weight'] : '';
		if ($weight != '') echo "font-weight: $weight;";

		$size = (isset($font_property['size'])) ? $font_property['size'] : '';
		if ($size != '') echo "font-size: $size;";

		$line_height = (isset($font_property['line-height'])) ? $font_property['line-height'] : '';
		if ($line_height != '') echo "line-height: $line_height;";

		$latter_spacing = (isset($font_property['letter-spacing'])) ? $font_property['letter-spacing'] : '';
		if ($latter_spacing != '') echo "letter-spacing: $latter_spacing;";

		$color = (isset($font_property['color'])) ? $font_property['color'] : '';
		if ($color != '') echo "color: $color;";
		?>
	}
	<?php
}

/**
 * Custom scripts and styles on header
 *
 * @since  1.0
 */
function sh_lm_header_scripts_css() {	
	// Custom CSS
	ob_start();
	sh_lm_font_enqueue('body', sh_lm_get_customizer_field('global_body_fonts'));
	sh_lm_font_enqueue('h1', sh_lm_get_customizer_field('hading_one_font'));
	sh_lm_font_enqueue('h2', sh_lm_get_customizer_field('hading_two_font'));
	sh_lm_font_enqueue('h3', sh_lm_get_customizer_field('hading_three_font'));
	sh_lm_font_enqueue('h4', sh_lm_get_customizer_field('hading_four_font'));
	sh_lm_font_enqueue('h5', sh_lm_get_customizer_field('hading_five_font'));
	sh_lm_font_enqueue('h6', sh_lm_get_customizer_field('hading_six_font'));
	sh_lm_font_enqueue('.site-title', sh_lm_get_customizer_field('logo_settings','logo','text','logo_title_font'));
	sh_lm_font_enqueue('.site-description', sh_lm_get_customizer_field('logo_settings','logo','text','logo_subtitle_font'));

	$google_font = ob_get_clean();	
	$google_font .= sh_lm_get_customizer_field('custom_css', '');
	wp_add_inline_style( 'sh-lm-custom-google-font', $google_font );

	ob_start();
	
	require get_template_directory() . '/inc/frontend/color-schemer.php';
	$custom_css_code = ob_get_clean();
	$custom_css_code .= sh_lm_fw_post_meta('custom_css');
	$custom_css_code .= sh_lm_get_customizer_field('custom_css_code');
	wp_add_inline_style( 'sh-lm-style', $custom_css_code );
}
add_action( 'wp_enqueue_scripts', 'sh_lm_header_scripts_css', 300 );

