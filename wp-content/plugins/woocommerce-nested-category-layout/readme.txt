=== WooCommerce Nested Category Layout ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.4
Tested up to: 4.9.5

== Installation ==

 * Upload the plugin files to the '/wp-content/plugins/' directory
 * Activate the plugin through the 'Plugins' menu in WordPress
 * Go to your WooCommerce payment gateway settings page and enable/configure the gateway
