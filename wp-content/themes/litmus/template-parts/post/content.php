<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Litmus
 */
$col_class = (isset($post_style) && $post_style == 'list') ? 'col-md-12' : 'col-md-6'; 
if(isset($only_item_class) && $only_item_class == true){
    $col_class = '';
}
$custom_post_class = ( is_sticky() ) ? ' sticky post ' : 'post';
?> 
<div class="item <?php echo esc_attr($col_class); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class($custom_post_class); ?>>
        <?php if ( has_post_thumbnail() ) { ?>
        <figure class="article-thumb">
            <a href="<?php the_permalink(); ?>">
                <?php 
                if(isset($post_style) && $post_style == 'list') {
                    sh_lm_post_featured_image(850, 500, false);
                } else {
                    sh_lm_post_featured_image(410, 272, false);
                }
                ?>            
            </a>
        </figure> <!-- /.article-thumb -->
        <?php } ?> 
        <div class="article-header">
            <div class="entry-meta">
                <?php if( get_the_post_thumbnail() == "" && get_the_title() == ""): ?>
                <div class="entry-date no-title"><a href="<?php the_permalink(); ?>"><?php the_time( get_option( 'date_format' ) ); ?></a></div>
                <?php else: ?>
                <div class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></div>
                <?php endif; ?> 

                <div class="entry-cat">
                    <?php 
                        $post_type = get_post_type( get_the_ID() );
                        if($post_type == 'post') { 
                            $categories = get_the_category();
                            if ( ! empty( $categories ) ) {
                                echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                            }
                        } else {
                            if($post_type != 'page') {
                                $categories = get_the_terms( get_the_ID(), 'portfolio-category' );
                                if ( ! empty( $categories ) ) {
                                    echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                }
                            }
                        }
                    ?>
                </div><!-- /.entry-date -->
            </div><!-- /.entry-meta -->
            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
        </div><!-- /.article-header -->
        <div class="article-content">
            <?php the_excerpt(); ?>
        </div><!--  /.article-content -->
    </article><!--  /.post -->
</div><!--  /.col-md-6 -->
 