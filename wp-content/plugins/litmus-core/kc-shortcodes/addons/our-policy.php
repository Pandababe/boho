<?php
add_shortcode( 'sh_lm_our_policy', function($atts, $content = null) {
	extract(shortcode_atts(array( 
		'our_policy' => '', 
	), $atts));  
	ob_start(); ?>   
	<section class="policy-block">
	    <div class="litmus-container">
	        <div class="policy-container policy-xs-slider item-4 row"> 
	        	<?php 
	        	if(!empty($our_policy)) {
	        	foreach ($our_policy as $policy) { 
	        	?>
	            <div class="item col-lg-4">
	                <div class="thumb-icon">
	                    <i class="<?php echo esc_attr($policy->icon); ?>"></i>
	                </div><!-- /.thumb-icon -->
	                <div class="policy-desc">
	                    <h3 class="policy-title"><?php echo esc_html($policy->title); ?></h3>
	                    <p class="policy-content"><?php echo esc_html($policy->desc); ?></p>
	                </div><!-- /.work-desc -->
	            </div><!-- /.col-lg-4 -->
				<?php } } ?>
	            
	        </div><!-- /.row -->
	    </div><!-- /.container -->
	</section><!-- /.policy-block -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_policy', 98 );
function sh_lm_kc_our_policy() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_policy' => array(
	                'name' => esc_html__('Our Policy', 'litmus'),
	                'description' => esc_html__('Our Policy', 'litmus'),
	                'icon' => 'et-tools',
	                'category' => 'Litmus',
	                'params' => array(
	                    array(
	                    	'name'			=> 'our_policy',
	                    	'type'			=> 'group',
	                    	'label'			=> esc_html__('Our Policy', 'litmus'), 
	                    	'options'		=> array('add_text' => esc_html__('Add new policy', 'litmus')),
	                    	// default values when create new group
	                    	'value' => base64_encode( json_encode( array(
	                    		"1" => array(
	                    			"icon" => "pe-7s-tools", 
	                    			"title" => "AWESOME SUPPORT",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"2" => array(
	                    			"icon" => "pe-7s-cup",
	                    			"title" => "FAST DELIVERY",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		),
	                    		"3" => array(
	                    			"icon" => "pe-7s-light",
	                    			"title" => "CREATIVE DESIGN",
	                    			"desc" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
	                    		)
	                    	))), 
	                    	'params' => array( 
	                    		array(
	                    			'name' => 'icon',
	                    			'type' => 'icon_picker',
	                    			'label' => esc_html__('Icon', 'litmus'),   
	                    		),
	                    		array(
	                    			'name' => 'title',
	                    			'type' => 'text',
	                    			'label' => esc_html__('Title', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    		array(
	                    			'name' => 'desc',
	                    			'type' => 'textarea',
	                    			'label' => esc_html__('Short Description', 'litmus'),  
	                    			'admin_label' => true,
	                    		),
	                    	)
	                    )
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}