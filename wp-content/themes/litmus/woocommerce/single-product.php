<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php if(sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') { ?>
<!-- Restaurant Banner
================================================== -->  
<section class="restaurant-banner bg-snow">
    <div class="container">
        <div class="row">
            <div class="col-12 orange-theme"> 
                <?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
                    <?php echo fw_ext_get_breadcrumbs('<i class="fa fa-angle-right"></i>'); ?>
                <?php endif; ?>
            </div><!--  /.col-12 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.restaurant-banner -->  
<?php } else { ?>
<?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
<section class="restaurant-banner overlay-bg <?php echo (sh_lm_get_customizer_field('sticky_header')) ? 'pd-t-175-b-90' : 'pd-tb-90' ?>">
    <div class="container">
        <div class="row">
            <div class="col-12 citrus-theme text-center color-white"> 
                <?php echo fw_ext_get_breadcrumbs('<i class="fa fa-angle-right"></i>'); ?> 
            </div><!--  /.col-12 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.restaurant-banner --> 
<?php endif; ?>
<?php } ?>

		<?php while ( have_posts() ) : the_post(); ?> 

			<!-- Our Menu Block
			================================================== -->
			<section class="our-menu-block pd-tb-90">
			    <div class="container woo-single-page">
			        <div class="row">
			        	<div class="col-md-12 clear">
			        		<?php
			        			/**
			        			 * woocommerce_before_main_content hook.
			        			 *
			        			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			        			 * @hooked woocommerce_breadcrumb - 20
			        			 */
			        			do_action('woocommerce_before_single_product');
			        		?>
			        	</div><!--  /.col-md-12 -->

			        	<?php if(sh_lm_get_customizer_field('wc_single_style') == 'two') { ?>
							<div class="col-md-7">                    
							    <div class="menu-single-tab">
							        <ul class="bsm-tabs" id="menu-single-tab-menu"> 
							            <?php  
							            	$attachment_ids = $product->get_gallery_image_ids(); 
							            	if ( $attachment_ids && has_post_thumbnail() ) {
							            		$i = 1;
							            		foreach ( $attachment_ids as $attachment_id ) { ?>
							            		<li class="bsm-tab">
							            		    <a href="#product-gal-<?php echo esc_attr($i); ?>"><img src="<?php echo esc_url(sh_lm_get_image_crop_size($attachment_id, 140, 140)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" /></a>
							            		</li> 
							            		<?php $i++; }
							            	} elseif( has_post_thumbnail() ) { ?>
							            		<li class="bsm-tab">
							            		    <a href="#product-gal-1"><?php sh_lm_post_featured_image(140, 140, true); ?></a>
							            		</li> 
							            		<?php  
							            	} 
							            ?>
							        </ul><!--  /.blog-layout-tab-menu -->
							    </div><!--  /.menu-single-tab -->  

							    <figure class="menu-thumbnail"> 
							        <?php  
							        	if ( $attachment_ids && has_post_thumbnail() ) {
							        		$i = 1;
							        		foreach ( $attachment_ids as $attachment_id ) { ?>
							        		<img id="product-gal-<?php echo esc_attr($i); ?>" src="<?php echo esc_url(sh_lm_get_image_crop_size($attachment_id, 470, 470)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />
							        		<?php $i++; }
							        	} elseif( has_post_thumbnail() ) { ?>
							        		<img id="product-gal-1" src="<?php echo esc_url(sh_lm_get_image_crop_size(get_post_thumbnail_id( get_the_ID() ), 470, 470)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />
							        		<?php  
							        	} 
							        ?> 
							    </figure>
							</div><!--  /.col-md-7 -->
			        	<?php } ?>

			            <div class="col-lg-5">    
			                <div class="woo-menu-item woo-list woo-single-products"> 
			                    <div class="menu-desc pd-lr-0">
			                    	<?php
			                    		/**
			                    		 * woocommerce_single_product_summary hook.
			                    		 *
			                    		 * @hooked woocommerce_template_single_title - 5
			                    		 * @hooked woocommerce_template_single_rating - 10
			                    		 * @hooked woocommerce_template_single_price - 10
			                    		 * @hooked woocommerce_template_single_excerpt - 20
			                    		 * @hooked woocommerce_template_single_add_to_cart - 30
			                    		 * @hooked woocommerce_template_single_meta - 40
			                    		 * @hooked woocommerce_template_single_sharing - 50
			                    		 * @hooked WC_Structured_Data::generate_product_data() - 60
			                    		 */

			                    		do_action( 'woocommerce_single_product_summary' );
			                    	?> 
			                        <div class="menu-price"> 
			                            <div class="menu-share fl-none">
			                                <p class="share-item">
			                                    <?php if ( function_exists( 'sh_lm_social_share_link' ) ) {
			                                        sh_lm_social_share_link( esc_html__('Share:', 'litmus') );
			                                    } ?>
			                                </p>
			                            </div><!--  /.menu-share -->
			                        </div><!--  /.menu-price -->
			                    </div><!--  /.menu-desc -->
			                </div><!--  /.woo-menu-item -->                
			            </div><!--  /.col-lg-5 -->

			            <?php if(sh_lm_get_customizer_field(array('wc_single_style', 'one')) == 'one') { ?>
			            <div class="col-lg-7">
			                <div class="new-arrival-item woo-single-products">
			                    <div class="row">
			                        <div class="col-lg-9">
			                            <figure class="menu-thumbnail">
			                                <?php
			                                    global $product; 
			                                    if( $product->is_on_sale() ) {
			                                        $badge_class = 'badge-discount';
			                                    } else {
			                                        $badge_class = 'badge-new';
			                                    }
			                                ?>
			                                <span class="<?php echo esc_attr($badge_class); ?>">
			                                <?php 
			                                    if( $product->is_on_sale() ) {
			                                    	if(is_numeric($product->get_regular_price()) && is_numeric($product->get_sale_price())) {
			                                        	echo '-'.round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 ).'%';
			                                    	}
			                                    } else {
			                                        esc_html_e( 'New', 'litmus' );
			                                    }
			                                ?>
			                                </span> 
			                                <?php 
			                                	$attachment_ids = $product->get_gallery_image_ids(); 

			                                	if ( $attachment_ids && has_post_thumbnail() ) {
			                                		$i = 1;
			                                		foreach ( $attachment_ids as $attachment_id ) { ?>
			                                		<img id="product-gal-<?php echo esc_attr($i); ?>" src="<?php echo esc_url(sh_lm_get_image_crop_size($attachment_id, 507, 655)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />
			                                		<?php $i++; }
			                                	} elseif( has_post_thumbnail() ) { ?>
			                                		<img id="product-gal-1" src="<?php echo esc_url(sh_lm_get_image_crop_size(get_post_thumbnail_id( get_the_ID() ), 507, 655)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" />
			                                		<?php  
			                                	} 
			                                ?> 
			                            </figure>
			                        </div><!--  /.col-lg-5 -->
			                        <div class="col-lg-3">
			                            <div class="menu-single-tab">
			                                <ul class="bsm-tabs" id="menu-single-tab-menu">
			                                    <?php  
			                                    	if ( $attachment_ids && has_post_thumbnail() ) {
			                                    		$i = 1;
			                                    		foreach ( $attachment_ids as $attachment_id ) { ?>
			                                    		<li class="bsm-tab">
			                                    		    <a href="#product-gal-<?php echo esc_attr($i); ?>"><img src="<?php echo esc_url(sh_lm_get_image_crop_size($attachment_id, 103, 130)); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" /></a>
			                                    		</li> 
			                                    		<?php $i++; }
			                                    	} elseif( has_post_thumbnail() ) { ?>
			                                    		<li class="bsm-tab">
			                                    		    <a href="#product-gal-1"><?php sh_lm_post_featured_image(103, 130, true); ?></a>
			                                    		</li> 
			                                    		<?php  
			                                    	} 
			                                    ?>
			                                </ul><!--  /.blog-layout-tab-menu -->
			                            </div><!--  /.menu-single-tab -->  
			                        </div><!--  /.col-lg-2 -->
			                    </div><!--  /.row -->
			                </div><!--  /.new-arrival-item -->
			            </div><!--  /.col-lg-7 -->
			            <?php } ?>

			        </div><!--  /.row -->

			        <div class="border-bottom space-element"></div><!--  /.border -->

			        <div class="row">
			            <div class="col-12">
			            	<?php
			            		/**
			            		 * woocommerce_after_single_product_summary hook.
			            		 *
			            		 * @hooked woocommerce_output_product_data_tabs - 10
			            		 * @hooked woocommerce_upsell_display - 15
			            		 * @hooked woocommerce_output_related_products - 20
			            		 */
			            		do_action( 'woocommerce_after_single_product_summary' );
			            	?> 
			            </div><!--  /.col-12 -->
			        </div><!--  /.row -->
			    </div><!--  /.container -->
			</section><!--  /.our-menu-block -->


		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		//do_action( 'woocommerce_after_main_content' );
	?>

	<!-- Woo Suggest Products
	================================================== -->
	<?php if(sh_lm_get_customizer_field('wc_related_post_option', 'wc_related_post')): ?>
	<section class="woo-shop-best-seller pd-b-90 border-bottom">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-6 text-center full-content"> 
	                <h2 class="section-title no-border v-3"><?php 
                            echo wp_kses( sh_lm_get_customizer_field('wc_related_post_option', '1', 'wc_related_title'), LITMUS_Static::html_allow() );
                        ?></h2>
	                <p class="sub-section-title v-2"><?php 
                            echo wp_kses( sh_lm_get_customizer_field('wc_related_post_option', '1', 'wc_related_desc'), LITMUS_Static::html_allow() );
                        ?></p>
	                <div class="mrb-45"></div><!--  /.mrb -->
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	        <div class="row">
	            <div class="col-md-12">
	                <div class="woo-best-seller-slider owl-carousel">
                    	<?php   
                    	$query_type = sh_lm_get_customizer_field('wc_related_post_option', '1', 'wc_related_query');
                    	$related_no = sh_lm_get_customizer_field('wc_related_post_option', '1', 'wc_related_query_number'); 
                    	if ( $query_type == 'author' ) {
                    	    $args = array(
                    	    	'post_type' => 'product',
                    	    	'post__not_in' => array(get_the_ID()),
                    	    	'posts_per_page'=> $related_no,  
                    	    	'author'=> get_the_author_meta( 'ID' )
                    	    );
                    	} elseif ( $query_type == 'tag' ) {
                    	    $tags = get_the_terms( get_the_ID(), 'product_tag' );
                    	    $tags_ids = array();
                    	    foreach($tags as $tag) $tags_ids[] = $tag->term_id;
                    	    $args = array(
                    	    	'post_type' => 'product',
                    	    	'post__not_in' => array(get_the_ID()),
                    	    	'posts_per_page'=> $related_no,  
                    	    	'tax_query' => array(
                    	    	    array (
                    	    	        'taxonomy' => 'product_tag',
                    	    	        'field' => 'id',
                    	    	        'terms' => $tags_ids,
                    	    	    )
                    	    	), 
                    	    );
                    	} elseif ( $query_type == 'best_saller' ) { 
                    	    $args = array(
                    	        'post_type' => 'product', 
                    	        'meta_key' => 'total_sales',
                    	        'orderby' => 'meta_value_num',
                    	        'post__not_in' => array(get_the_ID()), 
                    	        'posts_per_page' => $related_no,   
                    	    );
                    	} else {
                    	    $cats = get_the_terms( get_the_ID(), 'product_cat' );
                    	    $cats_ids = array();
                    	    foreach($cats as $cat) $cats_ids[] = $cat->term_id;
                    	    $args = array(
                    	    	'post_type' => 'product',
                    	    	'post__not_in' => array(get_the_ID()),
                    	    	'posts_per_page'=> $related_no,  
                    	    	'tax_query' => array(
                    	    	    array (
                    	    	        'taxonomy' => 'product_cat',
                    	    	        'field' => 'id',
                    	    	        'terms' => $cats_ids,
                    	    	    )
                    	    	), 
                    	    );
                    	}         

                    	$product_query = new wp_query( $args ); 

                    	while ($product_query->have_posts()) : $product_query->the_post(); 
                    		$only_item_class = true; 
                    	    require get_template_directory().'/woocommerce/template-parts/content-product.php'; 
    					endwhile; wp_reset_postdata(); ?> 
	                </div><!--  /.woo-best-seller-slider -->
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.woo-shop-best-seller -->
	<?php endif; ?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer();

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
