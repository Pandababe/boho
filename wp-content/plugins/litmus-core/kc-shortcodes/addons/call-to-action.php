<?php
add_shortcode( 'sh_lm_call_to_action', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'template' => '',  
		'bg_img' => '',  
		'title' => '',  
		'desc' => '',  
		'btn_title' => '',  
		'btn_url' => '',  
		'elementimg' => '',  
	), $atts));  
	extract($atts);
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>
	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		<?php if($template == 'one') : ?>
		<div class="call-to-action ver-1">
		    <div class="litmus-container">
		        <div class="row justify-content-md-center">
		            <div class="col-md-9">
		                <div class="call-to-content text-center">
		                	<?php 
		                		$array_from_to = array(
		                			'&lt;' => '<', 
		                			'&gt;' => '>', 
		                		);
		                		$title = strtr($title, $array_from_to);
		                		$desc = strtr($desc, $array_from_to);
		                	?>
		                    <h2><?php echo wp_kses( $title, LITMUS_Static::html_allow() ); ?></h2>
		                    <p><?php echo wp_kses( $desc, LITMUS_Static::html_allow() ); ?></p>
		                    <?php if(!empty($btn_title)) { ?>
		                    <a href="<?php echo esc_url($btn_url); ?>" class="call-to-link"><?php echo esc_html($btn_title); ?></a>
		                    <?php } ?>
		                </div><!--  /.call-to-content -->
		            </div><!--  /.col-12 -->
		        </div><!--  /.row -->
		    </div><!--  /.container -->
		</div><!--  /.call-to-action -->
		<?php elseif($template == 'two') : ?>
			<div class="call-to-action">
				<div class="litmus-container">
					<div class="row">
						<div class="col-md-6">
							<div class="call-to-content">							
								<h2 class="call-to-title"><?php echo esc_html($title); ?></h2>
								<p class="call-to-desc"><?php echo esc_html($desc); ?></p>
								<a href="#call-to-get-quote" class="bsm-btn btn-creative waves-effect waves-light modal-trigger">Get A Quote</a>
								<div id="call-to-get-quote" class="bsm-modal">
								  	<div class="bsm-modal-content">
								  		<?php 
									  		if ( function_exists( 'sh_lm_get_quote_from_clients' ) ) {
									  		    echo sh_lm_get_quote_from_clients(); 
									  		}
								  		?>
								  	</div>
								</div>
							</div><!--  /.call-to-content -->
						</div><!--  /.col-md-6 -->
						<div class="col-md-6">
							<?php if($elementimg) : ?>
							<img class="brand-images" src="<?php echo esc_url(sh_lm_get_image_crop_size($elementimg, 555, 416)); ?>" alt="<?php echo esc_html__('Call to action element','litmus'); ?>" />
							<?php endif ?>
						</div><!--  /.col-md-6 -->
					</div><!--  /.row -->
				</div><!--  /.litmus-container -->
			</div><!--  /.call-to-action -->
		<?php endif ?>
	</div>
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_call_to_action', 103 );
function sh_lm_kc_call_to_action() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_call_to_action' => array(
	                'name' => esc_html__('Call To Action', 'litmus'),
	                'description' => esc_html__('Call To Action', 'litmus'),
	                'icon' => 'sl-paper-plane',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array(
	                		array(
	                			'name' => 'template',
	                			'label' => esc_html__('Select Template', 'pclick'),  
	                			'type' => 'radio_image',  
	                			'options' => array(  
	                				'one' => ''.plugin_dir_url( __FILE__ ).'assets/images/gallery-2.png', 
	                				'two' => ''.plugin_dir_url( __FILE__ ).'assets/images/gallery-4.png',  
	                			),
	                			'value' => 'one',
	                		),
		                	array(
	                			'name' => 'bg_img',
	                			'label' => esc_html__('Background Image', 'litmus'), 
	                			'type' => 'attach_image',
		                		'relation' => array(
	                		        'parent'    => 'template',
	                		        'show_when' => 'one' 
	                		    ),  
	                		),
		                    array(
		                    	'name' => 'title',
		                    	'type' => 'text',
		                    	'value' => '<span>Litmus</span> is a unique theme many features',
		                    	'label' => esc_html__('Title', 'litmus'),  
		                    	'admin_label' => true,
		                    ),
		                    array(
		                    	'name' => 'desc',
		                    	'type' => 'textarea',
		                    	'value' => 'Precision design, responsive ready,full shortcodes design, perfect portfolio, unique blog layout and many many options for you…',
		                    	'label' => esc_html__('Short Description', 'litmus'),  
		                    ),
		                    array(
		                    	'name' => 'btn_title',
		                    	'type' => 'text', 
		                    	'value' => 'Buy this theme',
		                    	'label' => esc_html__('Button Title', 'litmus'),
		                		'relation' => array(
	                		        'parent'    => 'template',
	                		        'show_when' => 'one' 
	                		    ),   
		                    ),
		                    array(
		                    	'name' => 'btn_url',
		                    	'type' => 'text',
		                    	'value' => '#',
		                    	'label' => esc_html__('Button URL', 'litmus'),
		                		'relation' => array(
	                		        'parent'    => 'template',
	                		        'show_when' => 'one' 
	                		    ),     
		                    ),
    	                	array(
                    			'name' => 'elementimg',
                    			'label' => esc_html__('Element Images', 'litmus'), 
                    			'type' => 'attach_image',
    	                		'relation' => array(
                    		        'parent'    => 'template',
                    		        'show_when' => 'two' 
                    		    ),  
                    		),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
	                					"screens" => "any,1024,999,767,479",
	                					'Background' => array(
	                						array(
	                							'property' => 'background',
	                							'selector' => '.call-to-action,.call-to-action.ver-1'
	                						),
	                					),
	                					'Heading'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Heading Color','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'font-size', 
	                							'label' => esc_html__('Heading Font Size','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'font-weight', 
	                							'label' => esc_html__('Heading Font Weight','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'font-style', 
	                							'label' => esc_html__('Heading Font Style','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'font-family', 
	                							'label' => esc_html__('Heading Font Family','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'text-align', 
	                							'label' => esc_html__('Heading Text Align','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'text-shadow', 
	                							'label' => esc_html__('Heading Text Shadow','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'text-transform', 
	                							'label' => esc_html__('Heading Text Transform','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'text-decoration', 
	                							'label' => esc_html__('Heading Text Decoration','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'line-height', 
	                							'label' => esc_html__('Heading Line Height','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                						array(
	                							'property' => 'letter-spacing', 
	                							'label' => esc_html__('Heading Letter Spacing','litmus'),
	                							'selector' => '.call-to-title'
	                						),
	                					),
	                					'Description'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Description Color','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'font-size', 
	                							'label' => esc_html__('Description Font Size','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'font-weight', 
	                							'label' => esc_html__('Description Font Weight','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'font-style', 
	                							'label' => esc_html__('Description Font Style','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'font-family', 
	                							'label' => esc_html__('Description Font Family','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'text-align', 
	                							'label' => esc_html__('Description Text Align','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'text-shadow', 
	                							'label' => esc_html__('Description Text Shadow','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'text-transform', 
	                							'label' => esc_html__('Description Text Transform','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'text-decoration', 
	                							'label' => esc_html__('Description Text Decoration','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'line-height', 
	                							'label' => esc_html__('Description Line Height','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                						array(
	                							'property' => 'letter-spacing', 
	                							'label' => esc_html__('Description Letter Spacing','litmus'),
	                							'selector' => '.call-to-desc'
	                						),
	                					),
	                					'Button'	=> array(
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Color','litmus'),
	                							'selector' => '.btn-creative'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Hover Color','litmus'),
	                							'selector' => '.btn-creative:hover'
	                						),	                						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Button Background Color','litmus'),
	                							'selector' => '.btn-creative'
	                						),
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Button Hover Background','litmus'),
	                							'selector' => '.btn-creative:hover'
	                						),
	                					),
	                					'Spacing'	=> array(
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Banner Padding','litmus'),
	                							'selector' => '.call-to-action'
	                						),            						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Heading Margin','litmus'),
	                							'selector' => '.call-to-title'
	                						),                						
	                					),
	                				),
	                			),
	                		),
	                	),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}