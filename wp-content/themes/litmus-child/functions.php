<?php
/**
 * Enqueue style of child theme
 */
function litmus_child_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('parent-style') ); 
}
add_action( 'wp_enqueue_scripts', 'litmus_child_theme_enqueue_styles', 100000 );

function csv_menu() {
    add_menu_page(
     "CSV Upload",
     "CSV Upload",
     0,
     "csv-menu-slug",
     "csv_options"
     );
}

function csv_options() {
    
    if(isset($_POST['submit'])) {
        if ( ! function_exists( 'wp_handle_upload' ) ) {
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
        }
        $uploadedfile = $_FILES['fileToUpload'];
        $upload_overrides = array( 'test_form' => false );
        $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
        $csv = Array();
        $rowcount = 0;
        $movefile['url'];
        if (($handle = fopen(get_stylesheet_directory_uri() . '/esf-catalog-4.csv', "r")) !== FALSE) {
            $max_line_length = defined('MAX_LINE_LENGTH') ? MAX_LINE_LENGTH : 10000;
            $header = fgetcsv($handle, $max_line_length);
            $header_colcount = count($header);
            while (($row = fgetcsv($handle, $max_line_length)) !== FALSE) {
                $row_colcount = count($row);
                if ($row_colcount == $header_colcount) {
                    $entry = array_combine($header, $row);
                    $csv[] = $entry;
                }
                else {
                    error_log("csvreader: Invalid number of columns at line " . ($rowcount + 2) . " (row " . ($rowcount + 1) . "). Expected=$header_colcount Got=$row_colcount");
                    return null;
                }
                $rowcount++;
            }
            fclose($handle);
        }
        else {
            error_log("csvreader: Could not read CSV");
            return null;
        }
        
        foreach( $csv as $item ) {
            wp_insert_term(
                $item['Manufacturer'],
                    'manufacturers_cat'
                );
       
            $code = $item['Code'];
            $name = $item['Name'];
            $description = $item['Description'];
            $price = $item['Price'];

            $default_set = $item['Default set qts'];
            $width = $item['Width'];
            $depth = $item['Depth'];
            $height = $item['Height'];
            $weight = $item['Weight'];
            $volume = $item['Volume'];
            $manufacturer  = $item['Manufacturer'];
            $arrival = '';

            $arrival_temp = $item['New Arrivals'];
            if($arrival_temp == 'TRUE') {
                $arrival = 'New Arrival';
            }

            $category = array();
            $category_type = $item['Category Type'];
            $category_sibling = $item['Category'];
            array_push($category, $category_type, $category_sibling);

            $stock_status = '';

            $stock_temp = $item['Stock Status'];
            if($stock_temp == 'stock') {
                $stock_status = 'instock';
            }

            $sku = $item['sku'];
            $qty = $item['Default set qts'];


            $post_id = wp_insert_post(array (
                'post_type' => 'product',
                'post_title' => $name,
                'post_content' => $description,
                'post_status' => 'publish'
            ));
            
            wp_set_object_terms( $post_id, $manufacturer, 'manufacturers_cat' );
            wp_set_object_terms( $post_id, $category, 'product_cat' );
            wp_set_object_terms( $post_id, $arrival, 'product_tag' );
            
            add_post_meta( $post_id, '_width', $width );
            add_post_meta( $post_id, '_length', $depth );
            add_post_meta( $post_id, '_height', $height );
            add_post_meta( $post_id, '_weight', $weight );
            
            add_post_meta( $post_id, '_regular_price', $price );
            add_post_meta( $post_id, '_sale_price', $price );
            add_post_meta( $post_id, '_stock_status', $stock_status );
            add_post_meta( $post_id, '_sku', $sku );

            add_post_meta( $post_id, '_stock', $qty );
        }
    }
    ?>
    <form action="" method="post" enctype="multipart/form-data">
        Select image to upload:
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input type="submit" value="Upload Image" name="submit">
    </form>

<?php }

add_action( 'admin_menu', 'csv_menu' );

add_action( 'init', 'create_manufacturers_hierarchical_taxonomy', 0 );
add_action( 'init', 'create_arrival_hierarchical_taxonomy', 0 );
 
function create_manufacturers_hierarchical_taxonomy() {
    // Settings
    $labels = array(
        'name' => _x( 'Manufacturers', 'taxonomy general name' ),
        'singular_name' => _x( 'Manufacturer', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Manufacturers' ),
        'all_items' => __( 'All Manufacturers' ),
        'parent_item' => __( 'Parent Manufacturer' ),
        'parent_item_colon' => __( 'Parent Manufacturer:' ),
        'edit_item' => __( 'Edit Manufacturer' ), 
        'update_item' => __( 'Update Manufacturer' ),
        'add_new_item' => __( 'Add New Manufacturer' ),
        'new_item_name' => __( 'New Manufacturer Name' ),
        'menu_name' => __( 'Manufacturers' ),
    );    
 
    // Now register the taxonomy
    register_taxonomy('manufacturers_cat',array('product'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'manufacturers_cat' ),
    ));
}