<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'icon'  => array(
        'label' => esc_html__('Icon', 'litmus'),
        'type'  => 'icon-v2',
    ),
); 