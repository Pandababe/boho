<?php
/**
 * Litmus functions and definitions
 *
 * @package Litmus
 */

/**
 * Define theme's constant
 */
if ( ! defined( 'LITMUS_VERSION' ) ) {
	define( 'LITMUS_VERSION', '1.0' );
}
if ( ! defined( 'LITMUS_TEMPLATE_DIR' ) ) {
	define( 'LITMUS_TEMPLATE_DIR', get_template_directory() );
}
if ( ! defined( 'LITMUS_TEMPLATE_DIR_URL' ) ) {
    define( 'LITMUS_TEMPLATE_DIR_URL', get_template_directory_uri() );
}

/**
 * Include all conditional tags
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/conditional-tags.php';

/**
 * Include Frontend_Master Class
 */
require LITMUS_TEMPLATE_DIR . '/inc/classes/frontend/master.php';

/**
 * Include Backend_Master Class
 */
require LITMUS_TEMPLATE_DIR . '/inc/classes/backend/backend-master.php';

/**
 * Include Backend_Master Class
 */
require LITMUS_TEMPLATE_DIR . '/inc/classes/static.php';

/**
 * Include the Redux theme options framework
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/unyson-functions.php';

/**
 * Query function to get post
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/theme-functions.php';

/**
 * Custom template tags for this theme.
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/template-tags.php';

/**
 * Customizer additions.
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/customizer.php';

/**
 * Customize Menu.
 */
require LITMUS_TEMPLATE_DIR . '/inc/mega-menu/menu.php';

/**
 * Load Jetpack compatibility file.
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/jetpack.php';

/**
 * Include the TGM_Plugin_Activation class.
 */
require LITMUS_TEMPLATE_DIR . '/inc/libs/tgm-plugin-activation/tgm-admin-config.php';

/**
 * WordPress comment seciton override 
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/wp-comment-section-override.php';

/**
 * Query function to get post
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/function-for-post.php';

/**
 * Popular Post functions
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/popular-post.php';

/**
 * Include header, Hooks for template header
 */
require LITMUS_TEMPLATE_DIR . '/inc/frontend/header.php';

/**
 * Include header, Hooks for template header
 */
require LITMUS_TEMPLATE_DIR . '/inc/frontend/footer.php';

/**
 * Include override functions
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/wordpress-override.php';

/**
 * Include ajax functions
 */
require LITMUS_TEMPLATE_DIR . '/inc/functions/ajax-functions.php';

/**
 * Include the Aqua-Resizer-master
 */
require LITMUS_TEMPLATE_DIR . '/inc/libs/aqua-resizer-master/aq_resizer.php';

/**
 * Include WooCommerce Custom Functions
 */
require LITMUS_TEMPLATE_DIR . '/woocommerce/woo-functions.php';

/**
 * Load custom widget
 */
require LITMUS_TEMPLATE_DIR . '/inc/widgets/widgets.php';

/*For Theme Location Shortcode*/
function themelocation_title_shortcode( $atts, $content = null ) {
	return '<h2 class="piece-of-content-title">' . do_shortcode($content) . '</h2>';
}
add_shortcode( 'title', 'themelocation_title_shortcode' );

function themelocation_post_meta_shortcoe( $atts,  $content = null ) {
    ob_start();
    $attributes = shortcode_atts( array(
       'term_order' => '',
       'icon' => '',
    ), $atts );
    if ( $attributes['term_order'] || $attributes['icon']) {
        $terms_array = explode( ',', $attributes['term_order'] );
        $icon_array = explode( ',', $attributes['icon'] );

        $combined_array = array_combine( $icon_array, $terms_array  );    
    }
    $output = "";
    $output .= '<div class="cat-titles-box">';
   	foreach ($combined_array as $key => $value) {
   		$output .= '<span class="piece-of-content-cat"><i class="fa fa-'.$key.'"></i>'. $value .'</span>';
   	}
   	$output .= '</div>';
   	return $output;
}
add_shortcode( 'postmeta', 'themelocation_post_meta_shortcoe' );

function themelocation_post_url( $atts,  $content = null ) {
    ob_start();
    $attributes = shortcode_atts( array(
       'url' => '',
       'title' => '',
    ), $atts );

  	return '<p class="sources-sec"><span class="sources_label">'.$attributes['title'].'</span><br><a target="_blank" href="'.$attributes['url'].'">'.$attributes['url'].'</a><br></p>';	
}
add_shortcode( 'posturl', 'themelocation_post_url' );