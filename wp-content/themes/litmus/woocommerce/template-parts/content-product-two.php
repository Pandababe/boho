<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Litmus
 */
if(isset($only_item_class) && $only_item_class == true){
    $col_class = '';
} else {
    $col_class = 'col-md-4';
}
?> 
<div class="item <?php echo esc_attr($col_class); ?>">
    <div class="restu-menu-item">  
        <?php if ( has_post_thumbnail() ) { ?> 
        <figure class="menu-thumbnail">
            <a href="<?php the_permalink(); ?>">
                <?php sh_lm_post_featured_image(260, 255); ?>            
            </a> 
        </figure>
        <?php } ?> 
        <div class="menu-desc">
            <h2 class="menu-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="menu-meta">
                <div class="menu-cat"> 
                    <?php 
                    $product_cats = get_the_terms( get_the_ID(), 'product_cat' );
                    if(!empty($product_cats)) { ?>
                    <a href="<?php echo esc_url(get_term_link($product_cats[0]->slug, 'product_cat')); ?>" rel="category tag"><?php echo esc_html($product_cats[0]->name); ?></a>  
                    <?php } ?> 
                </div><!--  /.menu-cat -->
            </div><!--  /.menu-meta --> 
            <div class="menu-price">
                <div class="price"><?php sh_lm_product_price(); ?></div><!--  /.price -->
                <div class="order-block">
                    <a rel="nofollow" href="#" data-quantity="1" data-product_id="<?php echo esc_attr( get_the_ID() ); ?>" data-product_sku="" class="add-cart order-btn button product_type_simple add_to_cart_button ajax_add_to_cart"><?php echo esc_html('Order','litmus'); ?></a>
                    <?php if ( function_exists( 'sh_lm_product_favorite_btn' ) ) {?>
                        <a href="#" class="wishlist-btn"><?php sh_lm_product_favorite_btn( get_the_ID() ); ?></a>
                    <?php } ?>
                </div><!--  /.order-block -->
            </div><!--  /.menu-price -->
        </div><!--  /.menu-desc -->
    </div><!--  /.restu-menu-item -->
</div><!--  /.item -->
 