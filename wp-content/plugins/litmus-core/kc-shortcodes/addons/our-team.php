<?php
add_shortcode( 'sh_lm_our_team', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'title' => '',  
		'our_team' => '',  
	), $atts));  
	ob_start(); ?>    
	<section class="our-team">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-12">
	                <h2 class="section-title"><?php echo esc_html($title); ?></h2>
	            </div><!-- /.col-md-12 -->
	        </div><!-- /.row -->

	        <div class="row">
	            <div class="col-12">
	                <div class="team-slider owl-carousel">
						<?php 
			        	if(!empty($our_team)) {
			        	foreach ($our_team as $team) { 
			        	?>
			        	<?php if(isset($team->name)) { ?>
	                    <div class="item">
	                        <div class="team-item">                            
	                            <div class="hexagonal">
	                                <div class="hexagonal-img-container">
	                                    <div class="hexagonal-img-hex">
	                                        <div class="hexagonal-img-hex1 hexagonal-img-hex2">
	                                            <div class="hexagonal-img-hex-in1">
	                                                <div class="hexagonal-img-hex-in2">
	                                                    <img src="<?php echo esc_url(sh_lm_get_image_crop_size($team->img, 250, 230)); ?>" class="hexagonal-image" alt="<?php esc_attr_e('team iamge', 'litmus'); ?>">
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div><!-- /.hexagonal -->

	                            <div class="team-content text-center">
	                                <h2 class="member-name"><?php if(isset($team->name)) echo esc_html($team->name); ?></h2>
	                                <p class="member-deseg"><?php if(isset($team->pos)) echo esc_html($team->pos); ?></p>
	                                <p class="member-description"><?php if(isset($team->desc)) echo esc_html($team->desc); ?></p>
	                                <div class="content-socials">
	                                	<?php if($team->fblink) {?>
	                                	<a href="<?php echo esc_url($team->fblink); ?>" target="_blank"><i class="fa-facebook"></i></a>
	                                	<?php } ?>
	                                	<?php if($team->twlink) {?>
	                                	<a href="<?php echo esc_url($team->twlink); ?>" target="_blank"><i class="fa-twitter"></i></a>
	                                	<?php } ?>
	                                	<?php if($team->gpluslink) {?>
	                                	<a href="<?php echo esc_url($team->gpluslink); ?>" target="_blank"><i class="fa-google-plus"></i></a>
	                                	<?php } ?>
	                                </div>
	                            </div><!--  /.team-content -->
	                        </div><!--  /.team-item -->
	                    </div><!--  /.item --> 
	                    <?php } ?>
						<?php } } ?>
	                </div><!--  /.team-slider -->
	            </div><!--  /.col-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.our-team -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_our_team', 99 );
function sh_lm_kc_our_team() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_our_team' => array(
	                'name' => esc_html__('Our Team', 'litmus'),
	                'description' => esc_html__('Our Team', 'litmus'),
	                'icon' => 'et-profile-male',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array(
		                	array(
		                		'name' => 'title',
		                		'type' => 'text',
		                		'label' => esc_html__('Title', 'litmus'),  
		                		'value' => 'Our Team',  
		                		'admin_label' => true,
		                	),
		                    array(
		                    	'name'			=> 'our_team',
		                    	'type'			=> 'group',
		                    	'label'			=> esc_html__('Our Team', 'litmus'), 
		                    	'options'		=> array('add_text' => esc_html__('Add new member', 'litmus')),  
		                    	'params' => array(  
		                    		array(
		                    			'name' => 'img',
		                    			'label' => esc_html__('Image', 'litmus'), 
		                    			'type' => 'attach_image',  
		                    		),
		                    		array(
		                    			'name' => 'name',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Name', 'litmus'),  
		                    			'admin_label' => true,
		                    		),
		                    		array(
		                    			'name' => 'pos',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Position', 'litmus'),  
		                    		),
		                    		array(
		                    			'name' => 'desc',
		                    			'type' => 'textarea',
		                    			'label' => esc_html__('Description', 'litmus'),  
		                    		),		                    		
		                    		array(
		                    			'name' => 'fblink',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Facebook Link', 'litmus'),  
		                    		),
		                    		array(
		                    			'name' => 'twlink',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Twitter Link', 'litmus'),  
		                    		),
		                    		array(
		                    			'name' => 'gpluslink',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Google Plus Link', 'litmus'),  
		                    		),
		                    	)
		                    ), 
	                	),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}