<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after
*
* @package Litmus
*/
?> 
    <?php sh_lm_get_template_part('footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>
