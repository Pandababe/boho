<?php
function sh_lm_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
add_filter( 'comment_form_fields', 'sh_lm_move_comment_field_to_bottom' );

function sh_lm_comment_form($args) {
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );

	$args['fields'] = array(
      'author' =>
        '<div class="col-md-6"><div class="bsm-input-field"><input id="name" placeholder="'. esc_html__( 'Your Name', 'litmus' ) . ( $req ? '*' : '' ) .'" class="form-control" name="author" required="required" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
        '" size="30"' . ( $req ? " aria-required='true'" : '' ) . '/></div></div>',

      'email' =>
        '<div class="col-md-6"><div class="bsm-input-field"><input id="email" placeholder="'. esc_html__( 'Your Email', 'litmus' ) . ( $req ? '*' : '' ) .'" class="form-control" name="email" required="required" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
        '" size="30"' . ( $req ? " aria-required='true'" : '' ) . ' /></div></div>',

      'url' =>
        '<div class="col-md-12"><div class="bsm-input-field"><input id="url" placeholder="'. esc_html__( 'Got a Website?', 'litmus' ) .'" class="form-control" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
        '" size="30" /></div></div>'
      );
	$args['id_form'] = "comment_form";
	$args['class_form'] = ( is_singular('product') ) ? "comment-form-main" : "row comment-form-main";
	$args['id_submit'] = "submit";
	$args['class_submit'] = "submit bg-golden";
	$args['name_submit'] = "submit";
	$args['title_reply'] = wp_kses( __( '<span>Leave a Reply</span>', 'litmus' ), LITMUS_Static::html_allow() );

	$args['title_reply_to'] = wp_kses( __( 'Leave a Reply to %s', 'litmus' ), LITMUS_Static::html_allow() );
	$args['cancel_reply_link'] = esc_html__( 'Cancel Reply', 'litmus' );
	$args['comment_notes_before'] = "";
	$args['comment_notes_after'] = "";
	$args['label_submit'] = esc_html__( 'Post Comment', 'litmus' );
	$args['comment_field'] = '<div class="col-md-12"><div class="bsm-input-field"><textarea id="message" class="bsm-textarea" placeholder="'. esc_html__( 'Your Comment here&hellip;', 'litmus' ) .'" name="comment" aria-required="true" rows="8" cols="45"></textarea></div></div>';
	return $args;
}

add_filter('comment_form_defaults', 'sh_lm_comment_form');

function sh_lm_comment_list($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);
	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>

<<?php echo($tag); ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
<?php if ( 'div' != $args['style'] ) : ?>
<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
<?php endif; ?>
	<div class="comment-meta">
		<div class="comment-author vcard">	
			<div class="author-img">
				<?php echo get_avatar($comment,$size='60'); ?>				
			</div><!-- /.author-img -->
		</div><!-- /.comment-author .vcard -->

		<div class="comment-metadata">
			<?php printf( __( '<b class="fn">%s</b>', 'litmus' ), get_comment_author_link() ); ?>
			<span class="date">
				<?php
					printf( __('%1$s at %2$s','litmus'), get_comment_date(),  get_comment_time() ); ?><?php edit_comment_link( esc_html__( '(Edit)','litmus' ), '  ', '' );
				?>
			</span>
		</div><!-- /.comment-metadata -->
	</div><!-- /.comment-meta -->

	<div class="comment-details">		
		<div class="comment-content">
			<?php comment_text(); ?>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<p><em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.','litmus' ); ?></em>
				</p>
			<?php endif; ?>
		</div><!-- /.comment-content -->
		
		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- /.reply -->
	</div><!-- /.comment-details -->
	<?php if ( 'div' != $args['style'] ) : ?>
	</div><!-- /.comment-body -->
	<?php endif; ?>
<?php }