<?php
add_shortcode( 'sh_lm_logo_block', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'logotype' => '',  
		'sitelogo' => '',  
		'sitetitle' => '',  
		'sitedesc' => '', 
		'siteretina' => '', 
		'siteretinaimg' => '', 
	), $atts));  
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>
	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?> site-logo-builder-block">     
		<div class="site-logo">
		    <?php if($logotype == "image_type") { ?>
		    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
		        <img src="<?php echo wp_get_attachment_url( $sitelogo ,'full' ); ?>" alt="<?php echo bloginfo('name'); ?>" class="litmus-retina image-logo" <?php if($siteretina == 'yes') { ?> data-retina="<?php echo wp_get_attachment_url( $siteretinaimg ,'full' ); ?>" <?php } ?>>
		    </a>
		    <?php } else { ?>
		        <?php if( $sitetitle !== "") { ?>
		            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo esc_html($sitetitle); ?></a></h1>
		        <?php } ?>
		        
		        <?php if($sitedesc !== "") { ?>
		            <p class="site-description"><?php echo esc_html($sitedesc); ?></p>
		        <?php } ?>
		    <?php } ?>
		</div>
	</div>
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_logo_block', 130 );
function sh_lm_kc_logo_block() {
	if (function_exists('kc_add_map') ) { 
	    kc_add_map(
	        array( 
	            'sh_lm_logo_block' => array(
	                'name' => esc_html__('Logo', 'litmus'),
	                'description' => esc_html__('This Component For Logo Layout Builder', 'litmus'),
	                'icon' => 'et-flag',
	                'category' => 'Litmus Layout',
	                'params' => array(
	                	'general' => array(
        		    		array(
        		    			'name' => 'logotype',
        		    			'type' => 'select',
        		    			'label' => esc_html__('Choose Logo Image or Text only', 'litmus'), 
        		    			'options' => array(
        							'text_type' => esc_html__('Text', 'litmus'),
        							'image_type' => esc_html__('Image Logo', 'litmus'),
        						),
        		    		),
        		    		array(
    			    			'name' => 'sitetitle',
    			    			'type' => 'text',
    			    			'label' => esc_html__('Your Site Title', 'litmus'),
    			    			'relation' => array( 
	    			    			'parent'    => 'logotype',
	    			    			'show_when' => 'text_type',
    			    			),
        		    		),        		    		
        		    		array(
    			    			'name' => 'sitedesc',
    			    			'type' => 'text',
    			    			'label' => esc_html__('Your Site Description', 'litmus'), 
    			    			'relation' => array( 
	    			    			'parent'    => 'logotype',
	    			    			'show_when' => 'text_type',
    			    			),
        		    		),        		    		
        		    		array(
    			    			'name' => 'sitelogo',
    			    			'type' => 'attach_image',
    			    			'label' => esc_html__('Your Site Logo', 'litmus'), 
    			    			'relation' => array( 
	    			    			'parent'    => 'logotype',
	    			    			'show_when' => 'image_type',
    			    			),
        		    		),        		    		
        		    		array(
    			    			'name' => 'siteretina',
    			    			'type' => 'toggle',
    			    			'label' => esc_html__('Retina Logo', 'litmus'), 
    			    			'relation' => array( 
	    			    			'parent'    => 'logotype',
	    			    			'show_when' => 'image_type',
    			    			),
        		    		),        		    		
        		    		array(
    			    			'name' => 'siteretinaimg',
    			    			'type' => 'attach_image',
    			    			'label' => esc_html__('Your Retina Logo', 'litmus'), 
    			    			'relation' => array( 
	    			    			'parent'    => 'siteretina',
	    			    			'show_when' => 'yes',
    			    			),
        		    		),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
	                					"screens" => "any,1024,999,767,479",
	                					'Title'	=> array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),	 	                						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.site-logo .site-title'
	                						),
	                					),
	                					'Description'	=> array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),	                 						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.site-logo .site-description'
	                						),
	                					),	                					
	                					'Logo'	=> array(
	                						array(
	                							'property' => 'width',
	                							'label' => esc_html__('Width','litmus'),
	                							'selector' => '.site-logo .image-logo'
											),
											array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.site-logo .image-logo'
	                						),	                						
	                						array(
	                							'property' => 'height',
	                							'label' => esc_html__('Height','litmus'),
	                							'selector' => '.site-logo .image-logo'
	                						),						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.site-logo .image-logo'
	                						),				
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.site-logo .image-logo'
	                						),
	                					),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}