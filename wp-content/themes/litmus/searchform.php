<?php
/**
 * The template for displaying search form.
 *
 * @package Litmus
 */
?>

<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="searchform">
    <div class="input-group">
        <input type="search" name="s" placeholder="<?php esc_html_e( 'Search here &hellip;', 'litmus' ); ?>" class="form-controller" required>
        <div class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <i class="fa fa-search"></i>
            </button>
        </div> 
    </div>
</form> 