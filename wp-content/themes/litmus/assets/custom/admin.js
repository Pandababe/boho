(function ($) {
    "use strict"; // use strict to start
    var ajaxurl = litmus.ajaxurl;

    // Item Verification
    function item_verification(form) {
        var form = $(form);
        form.submit(function (event) {
            event.preventDefault();
            var $self = $(this);
            var data = form.serialize(); 
            $.ajax({
                type: "POST",
                dataType: "html",
                url: litmus.ajaxurl,
                data: data,
                beforeSend: function() {
                    $self.children('.litmus-loader').css({'display': 'inline-block'});
                    $self.children('.litmus-loader').children().addClass('spin');
                },
                success: function (data) {          
                    $self.children('.litmus-loader').css({'display': 'none'});
                    $self.children('.litmus-loader').children().removeClass('spin'); 
                    if(data == 'true') {
                        $('.litmus-important-notice.registration-form-container .about-description-success').slideDown('slow');
                        $('.litmus-important-notice.registration-form-container .litmus-registration-form').slideUp();
                        $('.litmus-important-notice.registration-form-container .about-description-success-before').slideUp();
                    } else {
                        $('.litmus-important-notice.registration-form-container .about-description-faild-msg').slideDown('slow');
                        setTimeout(function() {
                            $('.litmus-important-notice.registration-form-container .about-description-faild-msg').slideUp('slow');
                        }, 5500);
                    }
                },
                error: function () { 
                    console.log("Something miss. Please recheck");
                }
            });
        });  
    }
     
    if ($('#sh_lm_product_registration').length) {
        item_verification('#sh_lm_product_registration');
    }

    $(function(){
        var windowLoc = window.location;
        var $parentMenuId = $('#toplevel_page_sh_lm_welcome');
        var $submenuHref = $parentMenuId.find('.wp-submenu li a');
        $submenuHref.each(function() {
            if($(this).parent().hasClass("current")) {
                $(this).parent().removeClass("current");
            }
            if(windowLoc.search === $(this)[0].search) {
                $(this).parent().addClass("current");
            }
            $(this).on('click', function() {
                if($(this).parent().hasClass("current")) {
                    $(this).parent().removeClass("current");
                } else {
                    $(this).parent().addClass("current");
                }
            });
        });
    });

})(jQuery);