<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
if ( !defined( 'FW' ) ) {
    sh_lm_page_header( get_the_title() );
} else { ?>
<?php if(sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') { ?>
<!-- Restaurant Banner
================================================== -->  
<section class="restaurant-banner bg-snow">
    <div class="container">
        <div class="row">
            <div class="col-12 orange-theme"> 
                <?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
                    <?php echo fw_ext_get_breadcrumbs('<i class="fa fa-angle-right"></i>'); ?>
                <?php endif; ?>
            </div><!--  /.col-12 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.restaurant-banner -->  
<?php } else { ?>
<section class="restaurant-banner overlay-bg <?php echo (sh_lm_get_customizer_field('sticky_header')) ? 'pd-t-150-b-90' : 'pd-tb-90' ?>">
    <div class="container">
        <div class="row">
            <div class="col-12 citrus-theme text-center color-white">
                <?php if( function_exists( 'fw_ext_get_breadcrumbs' )): ?>
                    <?php echo fw_ext_get_breadcrumbs('<i class="fa fa-angle-right"></i>'); ?>
                <?php endif; ?>
            </div><!--  /.col-12 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.restaurant-banner --> 
<?php } } ?>

<!-- Our Menu Block
================================================== -->
<section class="our-menu-block pd-tb-90">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mrb-60">
                <div class="restau-tab-menu">
                    <div class="row align-items-center">                        
                        <div class="col-6 col-md-2">
                            <ul class="blog-layout-tab-menu v-2 bsm-tabs align-items-center orange-theme" id="blog-tab-menu">
                                <li class="bsm-tab">
                                    <a href="#menu-style-grid"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                                </li>
                                <li class="bsm-tab">
                                    <a href="#menu-style-list"><i class="fa fa-list" aria-hidden="true"></i></a>
                                </li>
                            </ul><!--  /.blog-layout-tab-menu -->
                        </div><!--  /.col-md-3 -->
                        <div class="col-md-10">
                            <?php do_action( 'woocommerce_before_shop_loop' ); ?>
                        </div><!--  /.col-md-7 -->
                    </div><!--  /.row -->
                </div><!--  /.restau-tab-menu -->
            </div><!--  /.col-md-12 -->
        </div><!--  /.row -->
        <div class="row"> 
            <?php
            $archive_sidebar = (isset($_GET["archive_sidebar"])) ? $_GET["archive_sidebar"] : sh_lm_get_customizer_field(array('wc_archive_sidebar', '1'));
            if($archive_sidebar) {
                get_sidebar('shop'); 
            }
            ?>
            <div class="col-lg-<?php echo ($archive_sidebar) ? 9 : 12; ?>">
                <?php if ( have_posts() ) { ?>
                    <div class="grid-menu-tab" id="menu-style-grid">

                        <?php 
                            woocommerce_product_loop_start();

                            woocommerce_product_loop_end();
                            /**
                             * Hook: woocommerce_after_shop_loop.
                             *
                             * @hooked woocommerce_pagination - 10
                             */
                            do_action( 'woocommerce_after_shop_loop' );
                        ?>
                        <?php 
                            $postShowInRow = get_option( 'woocommerce_catalog_rows');
                            if($postShowInRow !== "") { ?>
                                <div class="item-<?php echo esc_attr($postShowInRow); ?> row">
                            <?php } else { ?>
                                <div class="item-3 row">
                            <?php }
                        ?>  
                        <?php 
    						if ( have_posts() ) :                          
    						    while ( have_posts() ) : the_post(); 
                                    if(sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') {
    						            require get_template_directory().'/woocommerce/template-parts/content-product.php'; 
                                    } else {
                                        require get_template_directory().'/woocommerce/template-parts/content-product-two.php'; 
                                    }
    						    endwhile; 
                            endif; 
                            ?> 
                        </div><!--  /.row -->

                        <?php if ( have_posts() ) { ?>
                        <div class="menu-pagination-block">
                            <div class="row">
                                <div class="col-lg-6">                   
                                    <p class="pagination-position orange-theme">
                                        <?php sh_lm_pagination_text(); ?>
                                    </p>
                                </div><!--  /.col-lg-6 -->
                                <div class="col-lg-6"> 
                                    <?php sh_lm_posts_pagination_nav(); ?>
                                </div><!--  /.col-lg-6 -->
                            </div><!--  /.row -->
                        </div><!--  /.menu-pagination-block -->
                        <?php } ?>
                    </div><!--  /.grid-menu-style -->
                    
                    <div class="list-menu-tab" id="menu-style-list">
                    	<?php
                    		if ( have_posts() ) :
                    		    while ( have_posts() ) : the_post(); 
                    		        global $product; 
                    		        if( $product->is_on_sale() ) {
                    		            $badge_class = 'badge-discount';
                    		            $con_class = 'discount';
                                        if( sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') {
                                            $con_class = '';
                                        }
                    		        } else {
                    		        	$con_class = '';
                    		            $badge_class = 'badge-new';
                    		        }
                    		    ?>
                    		    <div class="woo-menu-item woo-list <?php echo esc_attr($con_class); ?>"> <!-- for class is discount -->  
                    		    	<?php if ( has_post_thumbnail() ) { ?>    
                                    <div class="row">
                                        <div class="col-md-5">                                
                            		        <figure class="menu-thumbnail"> 
                                                <?php if( sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') { ?>
            	                		        <span class="<?php echo esc_attr($badge_class); ?>">
            	                		        <?php 
            	                		            if( $product->is_on_sale() && $product->get_regular_price() ) {
            	                		                echo '-'.round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 ).'%';
            	                		            } else {
            	                		                esc_html_e( 'New', 'litmus' );
            	                		            }
            	                		        ?>
            	                		        </span> 
                                                <?php } ?>
                            		            <?php sh_lm_post_featured_image(365, 477); ?>  
                            		        </figure>
                                        </div><!--  /.col-md-5 -->
                                        <div class="col-md-7">                                
                            		        <div class="menu-desc">
                            		            <?php
                                                    /**
                                                     * woocommerce_single_product_summary hook.
                                                     *
                                                     * @hooked woocommerce_template_single_title - 5
                                                     * @hooked woocommerce_template_single_rating - 10
                                                     * @hooked woocommerce_template_single_price - 10
                                                     * @hooked woocommerce_template_single_excerpt - 20
                                                     * @hooked woocommerce_template_single_add_to_cart - 30
                                                     * @hooked woocommerce_template_single_meta - 40
                                                     * @hooked woocommerce_template_single_sharing - 50
                                                     * @hooked WC_Structured_Data::generate_product_data() - 60
                                                     */
                                                    do_action( 'woocommerce_single_product_summary' );
                                                ?> 
                            		        </div><!--  /.menu-desc -->
                                        </div><!--  /.col-md-7 -->
                                    </div><!--  /.row -->                         
                                    <?php } ?> 

                    		    </div><!--  /.woo-menu-item -->
                    		    <?php endwhile;
                    		endif; 
                    	?> 

                        <?php if ( have_posts() ) { ?>
                        <div class="menu-pagination-block">
                            <div class="row">
                                <div class="col-lg-6">                   
                                    <p class="pagination-position orange-theme">
                                        <?php sh_lm_pagination_text(); ?>
                                    </p>
                                </div><!--  /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <?php sh_lm_posts_pagination_nav(); ?>
                                </div><!--  /.col-lg-6 -->
                            </div><!--  /.row -->
                        </div><!--  /.menu-pagination-block -->

                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="col-12">
                            <?php get_template_part( 'template-parts/post/content', 'none' );  ?>
                        </div><!--  /.col-12 -->
                    </div><!--  /.row -->
                <?php } ?>
            </div><!--  /.col-lg-9 -->
        </div><!--  /.row -->
    </div><!--  /.container -->
</section><!--  /.our-menu-block -->
<?php get_footer(); ?>