<?php
add_shortcode( 'sh_lm_testimonials', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'template' => '',  
		'testimonials' => '',  
		'testimonialsv2' => '',  
	), $atts));  
	extract($atts);
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>
	<?php if($template == 'one') : ?>
	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		<div class="client-review">
		    <div class="litmus-container">
		        <div class="row">
		            <div class="col-12">
		                <div class="review-slider owl-carousel">
		                	<?php 
				        	if(!empty($testimonials)) {
				        	foreach ($testimonials as $team) { 
				        	?>
		                    <?php if(isset($team->testimonial)) { ?>
		                    <div class="item">
		                    	<div class="quote-icon text-center">
		                    	    <i class="fa fa-quote-left"></i>
		                    	</div><!--  /.quote-icon -->
		                    	<div class="client-rating text-center">
		                    	    <?php echo LITMUS_Static::num_to_star($team->rating); ?>
		                    	</div><!--  /.client-rating -->
		                    	<div class="client-desc text-center">
		                    	    <p><?php echo esc_html($team->testimonial); ?></p>
		                    	</div><!--  /.client-desc -->
		                        <div class="client-thumb">
		                            <img src="<?php echo esc_url(sh_lm_get_image_crop_size($team->img, 90, 90, true)); ?>" class="hexagonal-image" alt="<?php echo esc_attr($team->name); ?>">
		                        </div><!--  /.client-thumb -->
		                        <div class="client-header text-center">
		                            <h2 class="client-name"><?php echo esc_html($team->name); ?></h2><!--  /.client-name -->
		                            <h5 class="client-deseg"><?php echo esc_html($team->pos); ?></h5><!--  /.client-desc -->
		                        </div><!--  /.client-header -->
		                    </div><!--  /.item --> 
		                    <?php } ?>
		                    <?php } } ?>
		                </div><!--  /.review-slider -->
		            </div><!--  /.col-12 -->
		        </div><!--  /.row -->
		    </div><!--  /.container -->        
		</div><!--  /.client-review -->
	</div>
	<?php elseif($template == 'two') : ?>
		<div class="client-review ver-2">
			<div class="row">
				<div class="col-md-12">
					<div class="review-slider owl-carousel">
	                	<?php 
			        	if(!empty($testimonialsv2)) {
			        	foreach ($testimonialsv2 as $team) { ?>
			        	<?php if(isset($team->testimonial)) : ?>
							<div class="item">
								<?php if(isset($team->logoimg)) : ?>					
								<div class="client-logo">
									<img src="<?php echo esc_url(sh_lm_get_image_crop_size($team->logoimg, 282, 256)); ?>" class="testimonial-image" alt="<?php echo esc_attr($team->name); ?>">
								</div>
								<?php endif; ?>
								<div class="client-content" style="background-image: url(<?php echo ''.plugin_dir_url( __FILE__ ).'assets/images/background-quote.svg' ?>);">
									<?php if(isset($team->testimonial)) { ?>
									<p class="description-text"><?php echo esc_html($team->testimonial); ?></p>
									<p class="client-info"><span><?php echo esc_html($team->name) ?></span><span><?php echo esc_html($team->pos) ?></span></p>
									<?php } ?>
								</div>
								<?php if(isset($team->img)) { ?>
								<div class="client-thumb" style="background-image: url(<?php echo ''.plugin_dir_url( __FILE__ ).'assets/images/comment-image.svg' ?>);">
									<img src="<?php echo esc_url(sh_lm_get_image_crop_size($team->img, 66, 73)); ?>" class="testimonial-image" alt="<?php echo esc_attr($team->name); ?>">
								</div>
								<?php } ?>
							</div><!--  /.item -->
						<?php endif ?>
						<?php } } ?>
					</div>
				</div><!--  /.col-md-12 -->
			</div><!--  /.row -->
		</div><!--  /.client-review -->
	<?php endif ?>
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_testimonials', 101 );
function sh_lm_kc_testimonials() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_testimonials' => array(
	                'name' => esc_html__('Testimonials', 'litmus'),
	                'description' => esc_html__('Testimonials', 'litmus'),
	                'icon' => 'et-chat',
	                'category' => 'Litmus',
	                'params' => array( 
	                	'general' => array(
	                		array(
	                			'name' => 'template',
	                			'label' => esc_html__('Select Template', 'pclick'),  
	                			'type' => 'radio_image',  
	                			'options' => array(  
	                				'one' => ''.plugin_dir_url( __FILE__ ).'assets/images/gallery-2.png', 
	                				'two' => ''.plugin_dir_url( __FILE__ ).'assets/images/gallery-4.png',  
	                			),
	                			'value' => 'one',
	                		),                		
		                    array(
		                    	'name'			=> 'testimonials',
		                    	'type'			=> 'group',
		                    	'label'			=> esc_html__('Testimonials', 'litmus'), 
		                    	'options'		=> array('add_text' => esc_html__('Add new testimonial', 'litmus')),
		                		'relation' => array(
	                		        'parent'    => 'template',
	                		        'show_when' => 'one' 
	                		    ),  
		                    	'params' => array(  
		                    		array(
		                    			'name' => 'img',
		                    			'label' => esc_html__('Client Image', 'litmus'), 
		                    			'type' => 'attach_image',  
		                    		),
		                    		array(
		                    			'name' => 'name',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Name', 'litmus'),  
		                    			'admin_label' => true,
		                    		),
		                    		array(
		                    			'name' => 'pos',
		                    			'type' => 'text',
		                    			'label' => esc_html__('Position', 'litmus'),  
		                    		), 
		                    		array(
		                    			'name' => 'rating',
		                    			'label' => esc_html__('Rating Star', 'litmus'), 
		                    			'type' => 'select',   
		                    			'options' => LITMUS_Static::star_option(),
		                    			'value' => '5',   
		                    		),
		                    		array(
		                    			'name' => 'testimonial',
		                    			'type' => 'textarea',
		                    			'label' => esc_html__('Testimonial', 'litmus'),   
		                    		),
		                    	)
		                    ), 
    	                    array(
    	                    	'name'			=> 'testimonialsv2',
    	                    	'type'			=> 'group',
    	                    	'label'			=> esc_html__('Testimonials', 'litmus'), 
    	                    	'options'		=> array('add_text' => esc_html__('Add new testimonial', 'litmus')),
    	                		'relation' => array(
                    		        'parent'    => 'template',
                    		        'show_when' => 'two' 
                    		    ),  
    	                    	'params' => array(  
    	                    		array(
    	                    			'name' => 'img',
    	                    			'label' => esc_html__('Client Image', 'litmus'), 
    	                    			'type' => 'attach_image',  
    	                    		),
    	                    		array(
    	                    			'name' => 'logoimg',
    	                    			'label' => esc_html__('Company Logo', 'litmus'), 
    	                    			'type' => 'attach_image',  
    	                    		),
    	                    		array(
    	                    			'name' => 'name',
    	                    			'type' => 'text',
    	                    			'label' => esc_html__('Name', 'litmus'),  
    	                    			'admin_label' => true,
    	                    		),
    	                    		array(
    	                    			'name' => 'pos',
    	                    			'type' => 'text',
    	                    			'label' => esc_html__('Position', 'litmus'),  
    	                    		), 
    	                    		array(
    	                    			'name' => 'testimonial',
    	                    			'type' => 'textarea',
    	                    			'label' => esc_html__('Testimonial', 'litmus'),   
    	                    		),
    	                    	)
    	                    ), 
	                	),
						'styling' => array(
							array(
								'name'		=> 'css_custom',
								'type'		=> 'css',
								'options'	=> array(
									array(
										"screens" => "any,1024,999,767,479",
										'Testimonial1'	=> array(
											array(
												'property' => 'background', 
												'label' => esc_html__('Testimonial One Background','litmus'), 
												'selector' => '.client-review'
											),
											array(
												'property' => 'color', 
												'label' => esc_html__('Rating and arrow color','litmus'), 
												'selector' => '.client-rating .fa,.review-slider .owl-nav > div'
											),
											array(
												'property' => 'padding', 
												'label' => 'Select Highlighted Font Family', 
												'selector' => '.client-review'
											),
										),
										'Testimonial2'	=> array(
											array(
												'property' => 'background', 
												'label' => 'Select Highlighted Text Color', 
												'selector' => '.section-title span'
											),
											array(
												'property' => 'font-family', 
												'label' => 'Select Highlighted Font Family', 
												'selector' => '.section-title span'
											),
										),
									),
								),
							),
						),
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}