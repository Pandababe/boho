<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Litmus
 */

$post_type = get_post_type( get_the_ID() );
if($post_type != 'product') { ?> 
<div class="item item-searh">
    <article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
        <div class="row mr-0">
            <?php if ( has_post_thumbnail() ) { ?>
            <div class="col-md-4">
            <figure class="article-thumb">
                <a href="<?php the_permalink(); ?>">
                    <?php 
                        sh_lm_post_featured_image(260, 210, true);
                    ?>            
                </a>
            </figure> <!-- /.article-thumb -->
            </div><!--  /.col-md-4 -->
            <?php } ?> 
            <?php if ( has_post_thumbnail() ) { ?>
            <div class="col-md-8">
            <?php } else { ?>
            <div class="col-md-12">
            <?php } ?>
                <div class="article-header">
                    <div class="entry-meta">
                        <?php if( get_the_post_thumbnail() == "" && get_the_title() == ""): ?>
                        <div class="entry-date no-title"><a href="<?php the_permalink(); ?>"><?php the_time( get_option( 'date_format' ) ); ?></a></div>
                        <?php else: ?>
                        <div class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></div>
                        <?php endif; ?> 

                        <div class="entry-cat">
                            <?php  
                                if($post_type == 'post') { 
                                    $categories = get_the_category();
                                    if ( ! empty( $categories ) ) {
                                        echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                    }
                                } else {
                                    if($post_type != 'page' && $post_type != 'product') {
                                        $categories = get_the_terms( get_the_ID(), 'portfolio-category' );
                                        if ( ! empty( $categories ) ) {
                                            echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                        }
                                    }
                                }
                            ?>
                        </div><!-- /.entry-date -->
                    </div><!-- /.entry-meta -->
                    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                </div><!-- /.article-header -->
                <div class="article-content">
                    <?php the_excerpt(); ?>
                </div><!--  /.article-content -->
            </div><!--  /.col-md-4 -->
        </div><!--  /.row -->
    </article><!--  /.post -->
</div><!--  /.col-md-6 -->
<?php } else { 
    global $product; 
    if( $product->is_on_sale() ) {
        $badge_class = 'badge-discount';
        $con_class = 'discount';
        if( sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') {
            $con_class = '';
        }
    } else {
        $con_class = '';
        $badge_class = 'badge-new';
    }
?>
<div class="woocommerce woo-menu-item woo-list <?php echo esc_attr($con_class); ?>">
    <div class="row mr-0">    
        <div class="col-md-5">    
            <?php if ( has_post_thumbnail() ) { ?>                             
            <figure class="menu-thumbnail"> 
                <?php if( sh_lm_get_customizer_field('wc_archive_style_option','wc_archive_style') == 'one') { ?>
                <span class="<?php echo esc_attr($badge_class); ?>">
                <?php 
                    if( $product->is_on_sale() && $product->get_regular_price() ) {
                        echo '-'.round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 ).'%';
                    } else {
                        esc_html_e( 'New', 'litmus' );
                    }
                ?>
                </span> 
                <?php } ?>
                <?php sh_lm_post_featured_image(365, 477); ?>  
            </figure>
            <?php } ?>
        </div>
        <div class="col-md-7">
            <div class="menu-desc">
                <?php
                    /**
                     * woocommerce_single_product_summary hook.
                     *
                     * @hooked woocommerce_template_single_title - 5
                     * @hooked woocommerce_template_single_rating - 10
                     * @hooked woocommerce_template_single_price - 10
                     * @hooked woocommerce_template_single_excerpt - 20
                     * @hooked woocommerce_template_single_add_to_cart - 30
                     * @hooked woocommerce_template_single_meta - 40
                     * @hooked woocommerce_template_single_sharing - 50
                     * @hooked WC_Structured_Data::generate_product_data() - 60
                     */
                    do_action( 'woocommerce_single_product_summary' );
                ?> 
            </div><!--  /.menu-desc -->
        </div><!--  /.col-12 -->
    </div><!--  /.row -->
</div><!--  /.woo-menu-item -->
<?php } ?>
 