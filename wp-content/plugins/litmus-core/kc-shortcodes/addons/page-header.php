<?php
add_shortcode( 'sh_lm_page_header', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'bg_img' => '',  
		'short_title' => '',  
		'title' => '', 
	), $atts));  
	ob_start(); ?>     
	<section class="page-header overlay-bg-snow" style="background-image: url(<?php echo esc_url(sh_lm_get_image_crop_size($bg_img, 1200, 1200)); ?>);">
	    <div class="litmus-container">
	        <div class="row">
	            <div class="col-md-12"> 
	                <div class="breadcrumbs">
	                    <span class="first-item">
	                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'domain'); ?></a>
	                    </span>
	                    <span class="separator"><i class="fa fa-angle-right"></i></span>
	                    <span class="last-item"><?php echo esc_html($short_title); ?></span>
	                </div>
	            </div><!--  /.col-md-12 -->
	            <div class="col-md-12">
	                <h2 class="section-title center-title no-border"><?php echo esc_html($title); ?></h2>
	            </div><!--  /.col-md-12 -->
	        </div><!--  /.row -->
	    </div><!--  /.container -->
	</section><!--  /.page-header -->
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_page_header', 97 );
function sh_lm_kc_page_header() {
	if (function_exists('kc_add_map')) { 
	    kc_add_map(
	        array( 
	            'sh_lm_page_header' => array(
	                'name' => esc_html__('Page Header', 'litmus'),
	                'description' => esc_html__('Page Header', 'litmus'),
	                'icon' => 'fa-header',
	                'category' => 'Litmus',
	                'params' => array( 
	                	array(
                			'name' => 'bg_img',
                			'label' => esc_html__('Background Image', 'litmus'), 
                			'type' => 'attach_image',  
                		), 
	                    array(
	                    	'name' => 'short_title',
	                    	'type' => 'text',
	                    	'value' => 'Graphics Design',
	                    	'label' => esc_html__('Short Title', 'litmus'),  
	                    	'admin_label' => true,
	                    ), 
	                    array(
	                    	'name' => 'title',
	                    	'type' => 'text',
	                    	'value' => 'Graphics Design Service',
	                    	'label' => esc_html__('Title', 'litmus'),  
	                    	'admin_label' => true,
	                    ), 
	                )
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}