<?php
/**
 * Register post type "property"
 *
 */
if ( ! function_exists( 'sh_lm_custom_posts' ) ) :
function sh_lm_custom_posts() {

    /* Portfolio Custom Post*/  
    $portfolio_label = array(
        'name' => esc_html_x('Portfolio', 'Post Type General Name', 'litmus'),
        'singular_name' => esc_html_x('Portfolio', 'Post Type Singular Name', 'litmus'),
        'menu_name' => esc_html__('Portfolio', 'litmus'),
        'parent_item_colon' => esc_html__('Parent Portfolio:', 'litmus'),
        'all_items' => esc_html__('All Portfolio', 'litmus'),
        'view_item' => esc_html__('View Portfolio', 'litmus'),
        'add_new_item' => esc_html__('Add New Portfolio', 'litmus'),
        'add_new' => esc_html__('New Portfolio', 'litmus'),
        'edit_item' => esc_html__('Edit Portfolio', 'litmus'),
        'update_item' => esc_html__('Update Portfolio', 'litmus'),
        'search_items' => esc_html__('Search Portfolio', 'litmus'),
        'not_found' => esc_html__('No portfolio found', 'litmus'),
        'not_found_in_trash' => esc_html__('No portfolio found in Trash', 'litmus'),
    );
    $portfolio_args = array(
        'label' => esc_html__('Portfolio', 'litmus'),
        'description' => esc_html__('Portfolio', 'litmus'),
        'labels' => $portfolio_label,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array('portfolio-category', 'portfolio-tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-screenoptions',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('portfolio', $portfolio_args);   

    // Add new taxonomy, make it hierarchical (like categories) 
    $portfolio_cat_taxonomy_labels = array(
        'name'              => esc_html__( 'Portfolio Categories','litmus' ),
        'singular_name'     => esc_html__( 'Portfolio Categories','litmus' ),
        'search_items'      => esc_html__( 'Search Portfolio Category','litmus' ),
        'all_items'         => esc_html__( 'All Portfolio Category','litmus' ),
        'parent_item'       => esc_html__( 'Parent Portfolio Category','litmus' ),
        'parent_item_colon' => esc_html__( 'Parent Portfolio Category:','litmus' ),
        'edit_item'         => esc_html__( 'Edit Portfolio Category','litmus' ),
        'update_item'       => esc_html__( 'Update Portfolio Category','litmus' ),
        'add_new_item'      => esc_html__( 'Add New Portfolio Category','litmus' ),
        'new_item_name'     => esc_html__( 'New Portfolio Category Name','litmus' ),
        'menu_name'         => esc_html__( 'Portfolio Category','litmus' ),
    );    

    // Now register the portfolio taxonomy
    register_taxonomy('portfolio-category', array('portfolio'), array(
        'hierarchical' => true,
        'labels' => $portfolio_cat_taxonomy_labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'portfolio-category' ),
    ));

    $portfolio_tag_taxonomy_labels = array(
        'name'              => esc_html__( 'Portfolio Tags','litmus' ),
        'singular_name'     => esc_html__( 'Portfolio Tags','litmus' ),
        'search_items'      => esc_html__( 'Search Portfolio Tag','litmus' ),
        'all_items'         => esc_html__( 'All Portfolio Tag','litmus' ),
        'parent_item'       => esc_html__( 'Parent Portfolio Tag','litmus' ),
        'parent_item_colon' => esc_html__( 'Parent Portfolio Tag:','litmus' ),
        'edit_item'         => esc_html__( 'Edit Portfolio Tag','litmus' ),
        'update_item'       => esc_html__( 'Update Portfolio Tag','litmus' ),
        'add_new_item'      => esc_html__( 'Add New Portfolio Tag','litmus' ),
        'new_item_name'     => esc_html__( 'New Portfolio Tag Name','litmus' ),
        'menu_name'         => esc_html__( 'Portfolio Tag','litmus' ),
    );    

    // Now register the portfolio taxonomy
    register_taxonomy('portfolio-tag', array('portfolio'), array(
        'hierarchical' => false,
        'labels' => $portfolio_tag_taxonomy_labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'portfolio-tag' ),
    ));


    /* Layout Custom Post*/  
    $layout_lable = array(
        'name' => esc_html_x('Layout', 'Post Type General Name', 'litmus'),
        'singular_name' => esc_html_x('Layout', 'Post Type Singular Name', 'litmus'),
        'menu_name' => esc_html__('Layout ', 'litmus'),
        'parent_item_colon' => esc_html__('Parent Layout:', 'litmus'),
        'all_items' => esc_html__('All Layout', 'litmus'),
        'view_item' => esc_html__('View Layout', 'litmus'),
        'add_new_item' => esc_html__('Add New Layout', 'litmus'),
        'add_new' => esc_html__('New Layout', 'litmus'),
        'edit_item' => esc_html__('Edit Layout', 'litmus'),
        'update_item' => esc_html__('Update Layout', 'litmus'),
        'search_items' => esc_html__('Search Layout', 'litmus'),
        'not_found' => esc_html__('No layout found', 'litmus'),
        'not_found_in_trash' => esc_html__('No Layout found in Trash', 'litmus'),
    );
    $layout_args = array(
        'label' => esc_html__('Layout', 'litmus'),
        'description' => esc_html__('Make your layout', 'litmus'),
        'labels' => $layout_lable,
        'supports' => array('title', 'editor'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => false,
        'capability_type' => 'page',
        'menu_position' => 15,
        'menu_icon' => 'dashicons-hammer',
        'capability_type' => 'page',
    );
    register_post_type('layout', $layout_args);  


}
endif;
add_action('init', 'sh_lm_custom_posts', 0);