<?php
add_shortcode( 'sh_lm_resume_selected_portfolio', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'portfoliolist' => '',  
		'number' => '',   
	), $atts));  
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>
	<?php 
	if(!empty($portfoliolist)) {
		$portfolioId = array();
    	$portfoliolist = explode(",", $portfoliolist);  
    	foreach ( $portfoliolist as $portfolio ) {  
        	$portfolio = explode(":", $portfolio); 
        	$portfolioId[] = (int) $portfolio[0];
	 	} 
		$argsPortfolio = array(
		    'posts_per_page'    => $number,
		    'post_type'         => 'portfolio',
		    'post__in' => $portfolioId,
		);
		$portfolio_query = new WP_Query( $argsPortfolio );
	} ?>
	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		<div class="selected-portfolio">
			<div class="row item-2">
				<?php while ($portfolio_query->have_posts()) : $portfolio_query->the_post(); ?> 
				<div class="col-md-6">
					<figure class="portfolio-thumb">
						<?php sh_lm_post_featured_image(365, 280, true); ?> 
						<div class="hover-content">
							<div class="hover-details">      
								<a href="<?php the_permalink(); ?>" class="ajax-single-link">
									<i class="fa fa-eye" aria-hidden="true"></i>		
								</a>
							</div><!--  /.hover-details -->
						</div><!-- /.hover-content -->
					</figure><!-- /.portfolio-thumb -->
				</div>
				<?php endwhile; ?> <?php wp_reset_postdata(); ?> 
			</div>
		</div>
	</div>
	<?php return ob_get_clean();
});

// King Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_resume_selected_portfolio', 136 );
function sh_lm_kc_resume_selected_portfolio() {
	if (function_exists('kc_add_map') ) { 
	    kc_add_map(
	        array( 
	            'sh_lm_resume_selected_portfolio' => array(
	                'name' => esc_html__('Selected Portfolio', 'litmus'),
	                'description' => esc_html__('This Component For Litmus Selected Portfolio', 'litmus'),
	                'icon' => 'et-target',
	                'category' => 'Litmus',
	                'params' => array(
	                	'general' => array(
        		    		array(
		                        'name' => 'portfoliolist',
		                        'label' => esc_html__('Select Portfolio','litmus'),
								'type' => 'autocomplete',
								'description' => esc_html__('Select portfolio from here.','litmus'),
								'options'       => array( 
		                	        'multiple'      => true, 
		                	        'post_type'     => 'portfolio',  
		                	    ),
		                    ),        		    		
		                    array(
		                        'name' => 'number',
		                        'label' => esc_html__('Your Name','litmus'),
								'type' => 'number_slider',
								'description' => esc_html__('How many post you want to display?','litmus'),
								'options' => array(  
		                    		'min' => 1,
		                    		'max' => 6, 
		                    		'show_input' => true
		                    	), 
		                    	'value' => 2
							),       		    		
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
										"screens" => "any,1024,999,767,479",
										'Item'	=> array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	 	                						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.litmus-resume-banner-info .say-hello'
	                						),              						
										),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}