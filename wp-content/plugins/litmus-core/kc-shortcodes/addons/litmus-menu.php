<?php
add_shortcode( 'sh_lm_menu_block', function($atts, $content = null) {
	extract(shortcode_atts(array(  
		'themenu' => '',  
		'mobilemenu' => '',  
		'menuicon' => '',  
	), $atts));  
	$master_class = apply_filters( 'kc-el-class', $atts );
	ob_start(); ?>
	<div class="<?php echo esc_attr( implode( ' ', $master_class ) ); ?>">
		<div class="header-middle menu-builder-block">                    
		    <nav class="navigation builder-navigation">
		        <div class="menu-wrapper">
		            <div class="menu-content">
		                <?php
		                    wp_nav_menu ( array(
		                        'menu_class' => 'mainmenu',
		                        'container'=> 'ul',
		                        'menu' => $themenu, 
		                        'walker'    => new LITMUS_Walker_Nav_Menu(), 
		                        'fallback_cb'    => 'LITMUS_Walker_Nav_Menu::menu_callback' 
		                    ));  
		                ?>
		            </div>
		            <?php if($mobilemenu == "yes") { ?>
		        	<a href="#" class="hamburger-menu mobile-hamburger"><i class="<?php echo esc_attr($menuicon); ?>"></i></a> 
		        	<?php } ?>
		        </div>
		    </nav>
		</div>
		
		<?php if($mobilemenu == "yes") { ?>
		<div class="hamburger-block mobilemenu-block menu-builder-block"> 
		    <div class="overlaybg"></div>
		    <div class="hamburger-block-container expand-block">
		        <div class="close-menu">
		            <i class="pe-7s-close"></i>
		        </div>
		        <div class="main-mobile-menu" id="mobile-main-menu"></div>
		    </div>
		</div>  
		<?php } ?>  
	</div>
	<?php return ob_get_clean();
});

// Visual Composer Custom Shortcode
add_action( 'init', 'sh_lm_kc_main_menu_block', 131 );
function sh_lm_kc_main_menu_block() {
	$menus = wp_get_nav_menus();
	$menuName = array();
	foreach ( $menus as $menu  ) : 
		$menuName[$menu->term_id] = $menu->name;
    endforeach;

	if (function_exists('kc_add_map') ) { 
	    kc_add_map(
	        array( 
	            'sh_lm_menu_block' => array(
	                'name' => esc_html__('Litmus Menu', 'litmus'),
	                'description' => esc_html__('This Component For Menu Layout Builder', 'litmus'),
	                'icon' => 'sl-menu',
	                'category' => 'Litmus Layout',
	                'params' => array(
	                	'general' => array(
        		    		array(
        		    			'name' => 'themenu',
        		    			'type' => 'select',
        		    			'label' => esc_html__('Choose a menu', 'litmus'), 
        		    			'options' => $menuName,
        		    		),
        		    		array(
		                        'name' => 'mobilemenu',
		                        'label' => esc_html__('Mobile menu','litmus'),
		                        'type' => 'toggle',
		                        'description' => esc_html__('Enable or disable Mobile menu','litmus'),
		                    ),        		    		
		                    array(
		                        'name' => 'menuicon',
		                        'label' => esc_html__('Mobile menu icon','litmus'),
		                        'type' => 'icon_picker',
		                        'description' => esc_html__('Mobile Menu Button Icon','litmus'),
    			    			'relation' => array( 
	    			    			'parent'    => 'mobilemenu',
	    			    			'show_when' => 'yes',
    			    			),
		                    ),
	                	),
	                	'styling' => array(
	                		array(
	                			'name'		=> 'css_custom',
	                			'type'		=> 'css',
	                			'options'	=> array(
	                				array(
	                					"screens" => "any,1024,999,767,479",
	                					'Basic'	=> array(
	                						array(
	                							'property' => 'font-family',
	                							'label' => esc_html__('Font Family','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),                						
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Font Size','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	                						
	                						array(
	                							'property' => 'font-style',
	                							'label' => esc_html__('Font Style','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	 	                						
	                						array(
	                							'property' => 'font-weight',
	                							'label' => esc_html__('Font Wight','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	                						
	                						array(
	                							'property' => 'text-transform',
	                							'label' => esc_html__('Text Transform','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	                						
	                						array(
	                							'property' => 'text-align',
	                							'label' => esc_html__('Text Align','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	                						
	                						array(
	                							'property' => 'letter-spacing',
	                							'label' => esc_html__('Letter Spacing','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	                						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Color','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),        						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Background','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),            						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),				
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Display','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),	                						
	                						array(
	                							'property' => 'position',
	                							'label' => esc_html__('Position','litmus'),
	                							'selector' => '.navigation .mainmenu > li > a'
	                						),
	                					),
	                					'Position'	=> array(
	                						array(
	                							'property' => 'float',
	                							'label' => esc_html__('Floating Position','litmus'),
	                							'selector' => '.mainmenu'
	                						),                						
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Menu Display','litmus'),
	                							'selector' => '.mainmenu'
	                						),	                						
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Margin','litmus'),
	                							'selector' => '.mainmenu'
	                						),	                 						
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Padding','litmus'),
	                							'selector' => '.menu-content'
	                						),
	                					),	                					
	                					'Submenu'	=> array(
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Sub Menu Background','litmus'),
	                							'selector' => '.mainmenu .sub-menu'
	                						),	 
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Sub Menu Color','litmus'),
	                							'selector' => '.mainmenu .sub-menu li a'
	                						),
	                						array(
	                							'property' => 'width',
	                							'label' => esc_html__('Sub Menu Width','litmus'),
	                							'selector' => '.mainmenu .sub-menu'
	                						),
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Sub Menu Padding','litmus'),
	                							'selector' => '.mainmenu .sub-menu'
	                						),	               						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Mega Menu Background','litmus'),
	                							'selector' => '.mainmenu .sub-menu.sh-mega-menu'
	                						),	               						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Mega Menu Color','litmus'),
	                							'selector' => '.mainmenu .sub-menu.sh-mega-menu'
	                						),		                						
	                						array(
	                							'property' => 'width',
	                							'label' => esc_html__('Mega Menu Width','litmus'),
	                							'selector' => '.mainmenu .sub-menu.sh-mega-menu'
	                						),			
	                						array(
	                							'property' => 'padding',
	                							'label' => esc_html__('Mega Menu Padding','litmus'),
	                							'selector' => '.mainmenu .sub-menu.sh-mega-menu'
	                						),				
	                					),
	                					'Mobilemenu'	=> array(
	                						array(
	                							'property' => 'display',
	                							'label' => esc_html__('Menu Display','litmus'),
	                							'selector' => '.hamburger-menu.mobile-hamburger'
	                						),	                						
	                						array(
	                							'property' => 'float',
	                							'label' => esc_html__('Button Float','litmus'),
	                							'selector' => '.hamburger-menu.mobile-hamburger'
	                						),	 
	                						array(
	                							'property' => 'margin',
	                							'label' => esc_html__('Button Margin','litmus'),
	                							'selector' => '.hamburger-menu.mobile-hamburger'
	                						),	               									
	                						array(
	                							'property' => 'font-size',
	                							'label' => esc_html__('Button Font Size','litmus'),
	                							'selector' => '.hamburger-menu.mobile-hamburger'
	                						),
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Button Color','litmus'),
	                							'selector' => '.hamburger-menu.mobile-hamburger'
	                						),	      						
	                						array(
	                							'property' => 'background',
	                							'label' => esc_html__('Menu Background','litmus'),
	                							'selector' => '.mobilemenu-block .expand-block'
	                						),              						
	                						array(
	                							'property' => 'color',
	                							'label' => esc_html__('Menu Background','litmus'),
	                							'selector' => '.mobilemenu-block .expand-block .close-menu, .mobilemenu-block .expand-block .main-mobile-menu ul li a, .mobilemenu-block .expand-block .main-mobile-menu .menu-click i'
	                						),	               						
	                					),
	                				),
	                			),
	                		),
	                	),
	                ),
	            ),  // End of elemnt kc_icon 

	        )
	    ); // End add map
	
	} // End if
}